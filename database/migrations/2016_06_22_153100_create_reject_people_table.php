<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRejectPeopleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reject_people', function (Blueprint $table) {
            $table->increments('id');
            $table->string('company_code');
            $table->string('cv_no');
            $table->text('reason');
            $table->integer('job_position');
            $table->string('resign_date');
            $table->integer('create_user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reject_people');
    }
}
