@extends('admin.layout.app')

@section('content')

    <div id="page_content">
        <div id="page_content_inner">

            <div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            @if(\Session::has('status'))
                                <div class="uk-alert uk-alert-success" data-uk-alert="">
                                    <a href="#" class="uk-alert-close uk-close"></a>
                                    {{ \Session::get('status') }}
                                </div>
                            @endif
                            <div class="uk-overflow-container">
                                <div class="md-card-list-header">
                                    <h3>Job Category Management</h3>
                                </div>

                                <?php $i = 1 ?>
                                <table id="dt_default" class="uk-table" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>No</th>
                                        <th>Category Code</th>
                                        <th>Description</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $user = Sentinel::findById(\Session::get('admin_id')); ?>
                                    @foreach( $job_category as $row )
                                        <tr>
                                            <td>{{ $i++ }}</td>
                                            <td>{{ $row->job_code }}</td>
                                            <td class="uk-text-large uk-text-nowrap">{{ $row->description }}</td>

                                            <td class="uk-text-nowrap">
                                               @if ($user->hasAccess(['user.delete', 'user.update']))
                                                    <a href="#" class="md-btn md-btn-small" data-uk-tooltip="{pos:'bottom'}" title="Edit" data-uk-modal="{target:'#modal_header_footer{{ $row->id }}'}"><i class="ion-ios-compose-outline" style="font-size: 20px"></i></a>
                                                    {{--<a href="{{ URL::to("admin/delete/{$row->user_id}") }}" onclick="deleteCountry()"><i class="material-icons md-24">&#xE872;</i></a>--}}
                                                    <a href="#" class="md-btn md-btn-small md-btn-danger" data-uk-tooltip="{pos:'top'}" title="Delete" onclick="deleteJob({{$row->id}})"><i class="ion-ios-trash-outline" style="font-size: 20px"></i></a>
                                                @endif
                                            </td>
                                        </tr>
                                        <div class="uk-modal" id="modal_header_footer{{ $row->id }}">
                                            <div class="uk-modal-dialog">
                                                <div class="uk-modal-header">
                                                    <h3 class="uk-modal-title">Edit</h3>
                                                </div>
                                                <form action="{{ URL::to("job_category/{$row->id}") }}" id="form_validation" class="uk-form-stacked" method="post">
                                                    <div class="uk-grid" data-uk-grid-margin="">
                                                        <input type="hidden" name="_method" value="PUT">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <div class="uk-width-medium-1-2">
                                                            <div class="uk-form-row">
                                                                <div class="md-input-wrapper md-input-filled"><label>Category Code</label><input class="md-input" name="job_code" id="job_code" required type="text" value="{{ $row->job_code }}"><span class="md-input-bar"></span></div>
                                                            </div>
                                                        </div>
                                                        <div class="uk-width-medium-1-2">
                                                            <div class="uk-form-row">
                                                                <div class="md-input-wrapper md-input-filled"><label>Job Category Name</label><input class="md-input" name="description" required type="text" value="{{ $row->description }}"><span class="md-input-bar"></span></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="uk-modal-footer uk-text-right">
                                                        <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                                                        <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary">Submit</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="md-fab-wrapper">
                <a class="md-fab md-fab-primary" data-uk-modal="{target:'#modal_header_footer'}" href="#">
                    <i class="material-icons">&#xE145;</i>
                </a>
            </div>

            <div class="uk-modal" id="modal_header_footer">
                <div class="uk-modal-dialog">
                    <div class="uk-modal-header">
                        <h3 class="uk-modal-title">Add Job Category</h3>
                    </div>
                    <form action="{{ url('job_category') }}" method="post">
                        <div class="uk-grid" data-uk-grid-margin="">
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <div class="uk-width-medium-1-2">
                                <div class="uk-form-row">
                                    <div class="md-input-wrapper md-input-filled"><label>Category Code</label><input class="md-input" name="job_code" id="job_code" required type="text" value=""><span class="md-input-bar"></span></div>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <div class="uk-form-row">
                                    <div class="md-input-wrapper md-input-filled"><label>Job Category Name</label><input class="md-input" name="description" id="description" required type="text" value=""><span class="md-input-bar"></span></div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-modal-footer uk-text-right">
                            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                            <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary">Submit</button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

    <script>
        function deleteJob($id){

            var id = $id;

            UIkit.modal.confirm(
                    'Are you sure?',
                    function(){

                        (function(modal){
                            modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'>Wait <br/><img class=\'uk-margin-top\' src=\'{{ url('backend/img/spinners/spinner.gif') }}\' alt=\'\'>');
                            setTimeout(function(){
                                $.ajax({
                                    url: '{{ url('/job_category') }}' + '/' + id,
                                    type: 'DELETE',
                                    data: '',
                                    success: function( msg ) {
                                        modal.hide();
                                        UIkit.notify({
                                            message: 'Job Successfully removed.',
                                            status: 'danger',
                                            timeout: 3000,
                                            pos: 'top-right',
                                            onClose: function() {
                                                location.reload();
                                            }
                                        });
                                        location.reload();
                                    },
                                    error: function( data ) {
                                        modal.hide();
                                        UIkit.notify({
                                            message: 'Job Successfully removed.',
                                            status: 'danger',
                                            timeout: 3000,
                                            pos: 'top-right',
                                            onClose: function() {
                                                location.reload();
                                            }
                                        });
                                        location.reload();
                                    }
                                });


                            }, 5000) })();
                    });
        }
    </script>
@stop