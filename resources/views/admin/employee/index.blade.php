@extends('admin.layout.app')
@section('content')
    <style>
        .pagination {
            margin: 0;
            padding: 0;
            list-style: none;
            text-align: right;
            font-size: 0;
        }

        .pagination > li {
            display: inline-block;
            font-size: 1rem;
            vertical-align: top;
            outline: 0 !important;
        }

        .pagination > li:nth-child(n+2) {
            margin-left: 5px;
        }

        .pagination > li.active > a, .pagination > li.active > a:hover, .pagination > li.active > span, .pagination > li.active > span:hover {
            color: #fff;
        }

        .pagination > li.active > a, .pagination > li.active > span {
            background: #7cb342;
        }

        .pagination > .active > span {
            background: #00a8e6;
            color: #fff;
            border-color: transparent;
            box-shadow: inset 0 0 5px rgba(0, 0, 0, .05);
            text-shadow: 0 -1px 0 rgba(0, 0, 0, .1);
        }

        .pagination > li > a, .pagination > li > span {
            border: none;
            padding: 4px 8px;
            min-width: 32px;
            line-height: 24px;
            box-sizing: border-box;
            text-shadow: none;
            color: #212121;
            border-radius: 4px;
        }

        .pagination > li > a, .pagination > li > span {
            display: inline-block;
            min-width: 16px;
            padding: 3px 5px;
            line-height: 20px;
            text-decoration: none;
            box-sizing: content-box;
            text-align: center;
            border: 1px solid rgba(0, 0, 0, .06);
            border-radius: 4px;
        }

        .pagination > li {
            font-size: 1rem;
        }
    </style>
    <div id="page_content">
        <div id="page_content_inner">
            <h4 class="heading_a uk-margin-bottom">Employee List ( Group By Job Receivied Date )</h4>

            <br>
            <form action="{{ url('employee/search') }}" method="post" class="uk-form-stacked">
                {!! csrf_field() !!}
                <div class="uk-grid">
                    <div class="uk-width-large-1-4">
                        <div class="uk-form-row">
                            <input id="kUI_datetimepicker_range_start" name="from_date" placeholder="From Date"/>
                        </div>
                    </div>

                    <div class="uk-width-large-1-4">
                        <div class="uk-form-row">
                            <input id="kUI_datetimepicker_range_end" name="to_date" placeholder="To Date"/>
                        </div>
                    </div>


                    <div class="uk-width-large-1-4">
                        <div class="uk-form-row">
                            <button type="submit" class="md-btn md-btn-danger"><i style="color: #FFF" class="material-icons">search</i>&nbsp;Search</button>
                            <a href="{{ url('employee') }}" class="md-btn">Reset</a>
                        </div>
                    </div>
                </div>
            </form>
            <br>

            @if(count(@$employee_list) > 0 )

                <div class="uk-grid">

                    <div class="uk-width-large-2-10">
                        <div class="md-card">
                            <div class="md-list-outside-wrapper">
                                <ul class="md-list md-list-addon md-list-outside" id="chat_user_list"
                                    data-uk-tab="{connect:'#tabs_7'}">
                                    @foreach(@$employee_list as $code)
                                        <li>
                                            <div class="md-list-content" style="padding: 15px;">
                                                <div class="md-list-action-placeholder"></div>
                                                <span class="md-list-heading">{{ Date('F j, Y',strtotime(@$code->job_receive_date)) }}</span>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="uk-width-large-8-10">
                        <div class="md-card md-card-single">
                            <div class="md-card-toolbar">
                                <h3 class="md-card-toolbar-heading-text large">
                                    <span class="uk-text-muted"><i class="material-icons">accessibility</i> Employed Person </span><a
                                            href="#"></a>
                                </h3>
                            </div>

                            <div class="" id="data_content">
                                <div class="md-card-content">
                                    <ul id="tabs_7" class="uk-switcher uk-margin">
                                        @foreach(@$employee_list as $code)
                                            <?php @$emp_person = \App\Models\EmployeeRecord::where('job_receive_date', @$code->job_receive_date)->get(); ?>
                                            <li>
                                                <div class="uk-grid-width-small-1-2 uk-grid-width-medium-1-3 uk-grid-width-large-2-4  hierarchical_show" id="contact_list">
                                                    @foreach($emp_person as $emp)
                                                        <?php @$applicant = \App\Models\ApplicantInformation::where('cv_no',@$emp->cv_id)->first() ?>
                                                        <div data-uk-filter>
                                                            <div class="md-card md-card-hover">
                                                                <div class="md-card-head">
                                                                    <div class="md-card-head-menu"
                                                                         data-uk-dropdown="{pos:'bottom-right'}">
                                                                        <i class="md-icon material-icons">&#xE5D4;</i>
                                                                        <div class="uk-dropdown uk-dropdown-small">
                                                                            <ul class="uk-nav">
                                                                                <li><a href="#" data-uk-modal="{target:'#more_detail{{ @$emp->id }}'}">More Detail</a></li>
                                                                                <li><a href="#" data-uk-modal="{target:'#edit_detail{{ @$emp->id }}'}">Edit</a></li>
                                                                                <li><a href="#" data-uk-modal="{target:'#resign{{ @$emp->id }}'}">Resign</a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                    <div class="uk-text-center">                                                                    
                                                                        @if(isset($applicant->cv_image))
                                                                            <img class="md-card-head-avatar"
                                                                                 src="{{ @$applicant->cv_image }}" alt=""/>
                                                                        @else
                                                                            <img class="md-card-head-avatar"
                                                                                 src="http://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&amp;f=y" alt=""/>
                                                                        @endif
                                                                    </div>
                                                                    <h3 class="md-card-head-text uk-text-center">
                                                                        {{ @$applicant->applicant_name }}
                                                                        <span class="uk-text-truncate">
                                                                        <a href="{{ url('cv').'/'. \App\Models\ApplicantInformation::where('cv_no',$emp->cv_id)->pluck('id') }}"
                                                                           data-uk-tooltip="{pos:'bottom'}" target="_blank">{{ @$applicant->cv_no }}</a>
                                                                    </span>
                                                                        <span class="uk-text-truncate">{{ \App\Models\Client::where('company_code',@$emp->company_code)->pluck('company_name') }}</span>
                                                                    </h3>
                                                                </div>

                                                                <div class="uk-modal" style="z-index: 998"
                                                                     id="resign{{ $emp->id }}">
                                                                    <div class="uk-modal-dialog">
                                                                        <div class="uk-modal-header">
                                                                            <h3 class="uk-modal-title">Resign</h3>
                                                                        </div>
                                                                        <form id="resign_data{{ $emp->id  }}">

                                                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                            <input type="hidden" name="cv_no" value="{{ @$emp->cv_id }}">
                                                                            <input type="hidden" name="company_code" value="{{ @$emp->company_code }}">
                                                                            <input type="hidden" name="job_position" value="{{ @$emp->job_position }}">

                                                                            <div class="uk-grid">
                                                                                <div class="uk-width-medium-1-1">
                                                                                    <label for="invoicing{{ $emp->id  }}">
                                                                                        Resign Date
                                                                                <span class="req">
                                                                                    *
                                                                                </span>
                                                                                    </label>
                                                                                    <input type="text" name="resign_date"
                                                                                           id="invoicing{{ $emp->id  }}"
                                                                                           class="md-input" value=""
                                                                                           data-parsley-date-message="This value should be a valid date"
                                                                                           data-uk-datepicker="{format:'YYYY-MM-DD'}"/>
                                                                                </div>
                                                                            </div>

                                                                            <div class="uk-grid">
                                                                                <div class="uk-width-medium-1-1">
                                                                                    <label for="Reason">
                                                                                        Reason
                                                                                    </label>
                                                                        <textarea name="resign_reason" class="md-input" id="Reason" cols="10"
                                                                                  rows="5"></textarea>
                                                                                </div>
                                                                            </div>


                                                                            <div class="uk-modal-footer uk-text-right">
                                                                                <button type="button"
                                                                                        class="md-btn md-btn-flat uk-modal-close">
                                                                                    Close
                                                                                </button>
                                                                                <button type="button"
                                                                                        onclick="resign_person({{ $emp->id }})"
                                                                                        class="md-btn md-btn-flat md-btn-flat-primary uk-modal-close">
                                                                                    Submit
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>

                                                                <div class="uk-modal" id="more_detail{{ $emp->id }}">
                                                                    <div class="uk-modal-dialog" style="width: 500px">
                                                                        <button type="button" class="uk-modal-close uk-close"></button>
                                                                        <div class="uk-modal-header">
                                                                            <h3 class="uk-modal-title"><i class="material-icons">description</i>&nbsp;&nbsp;Record Detail</h3>
                                                                        </div>
                                                                        <p><b>Name</b>     -  @if(isset($applicant)) {{ $applicant->applicant_name }} @endif</p>
                                                                        <p><b>Job Position</b> - {{ \App\Models\ApplicantJobPosition::where('id',$emp->job_position)->where('cv_id',$emp->cv_id)->pluck('apply_position') }}</p>
                                                                        <p><b>Salary</b> - {{ $emp->salary }}</p>
                                                                        <p><b>Actual Amount</b> - {{ $emp->actual_amount }}</p>
                                                                        <br>
                                                                        <p><b>Company Name</b> - {{ \App\Models\Client::where('company_code',$emp->company_code)->pluck('company_name') }}</p>
                                                                        <p><b>Company Contact Person</b> - {{ $emp->company_contact_person }}</p>
                                                                        <p><b>Company Email</b> - {{ $emp->company_email }}</p>
                                                                        <p><b>Company Address</b> - {{ \App\Models\Client::where('company_code',$emp->company_code)->pluck('address') }}</p>
                                                                        <br>
                                                                        <p><b>Oway Contact Person</b> - {{ $emp->owaycompany_contact_person }}</p>
                                                                        <br>
                                                                        <p><b>Job Received Date</b> - {{ $emp->job_receive_date }}</p>
                                                                        <p><b>Joining Date</b> - {{ $emp->joining_date }}</p>
                                                                        <p><b>Invoice Date</b> - {{ $emp->invoicing_date }}</p>
                                                                        <p><b>Payment Date</b> - {{ $emp->payment_date }}</p>
                                                                    </div>
                                                                </div>

                                                                <div class="uk-modal" style="z-index: 998"
                                                                     id="edit_detail{{ $emp->id }}">
                                                                    <div class="uk-modal-dialog">
                                                                        <div class="uk-modal-header">
                                                                            <h3 class="uk-modal-title">Edit Record</h3>
                                                                        </div>
                                                                        <form id="emp_record_data{{ $emp->id  }}">

                                                                            <div class="uk-grid">
                                                                                <div class="uk-width-medium-1-2">
                                                                                    <label for="received{{ $emp->id  }}">
                                                                                        Job Received Date
                                                                                <span class="req">
                                                                                    *
                                                                                </span>
                                                                                    </label>
                                                                                    <input type="text" name="job_received_date"
                                                                                           id="received{{ $emp->id  }}"
                                                                                           class="md-input"
                                                                                           value="{{ $emp->job_receive_date }}"
                                                                                           data-parsley-date-message="This value should be a valid date"
                                                                                           data-uk-datepicker="{format:'YYYY-MM-DD'}"/>
                                                                                </div>
                                                                                <div class="uk-width-medium-1-2">
                                                                                    <label for="joining{{ $emp->id  }}">
                                                                                        Joining Date
                                                                                <span class="req">
                                                                                    *
                                                                                </span>
                                                                                    </label>
                                                                                    <input type="text" name="joining_date"
                                                                                           id="joining{{ $emp->id  }}"
                                                                                           class="md-input" value="{{ $emp->joining_date }}"
                                                                                           data-parsley-date-message="This value should be a valid date"
                                                                                           data-uk-datepicker="{format:'YYYY-MM-DD'}"/>
                                                                                </div>
                                                                            </div>

                                                                            <div class="uk-grid">
                                                                                <div class="uk-width-medium-1-2">
                                                                                    <label for="invoicing{{ $emp->id  }}">
                                                                                        Invoicing Date
                                                                                <span class="req">
                                                                                    *
                                                                                </span>
                                                                                    </label>
                                                                                    <input type="text" name="invoicing_date"
                                                                                           id="invoicing{{ $emp->id  }}"
                                                                                           class="md-input" value="{{ $emp->invoicing_date }}"
                                                                                           data-parsley-date-message="This value should be a valid date"
                                                                                           data-uk-datepicker="{format:'YYYY-MM-DD'}"/>
                                                                                </div>
                                                                                <div class="uk-width-medium-1-2">
                                                                                    <label for="payment{{ $emp->id  }}">
                                                                                        Payment Date
                                                                                <span class="req">
                                                                                    *
                                                                                </span>
                                                                                    </label>
                                                                                    <input type="text" name="payment_date"
                                                                                           id="payment{{ $emp->id  }}"
                                                                                           class="md-input" value="{{ $emp->payment_date }}"
                                                                                           data-parsley-date-message="This value should be a valid date"
                                                                                           data-uk-datepicker="{format:'YYYY-MM-DD'}"/>
                                                                                </div>
                                                                            </div>

                                                                            <div class="uk-grid">
                                                                                <div class="uk-width-medium-1-2">
                                                                                    <label for="company">
                                                                                        Company Contact Person
                                                                                    </label>
                                                                                    <input type="text"
                                                                                           name="company_contact_person"
                                                                                           id="company{{ $emp->id  }}" value="{{ $emp->company_contact_person }}"
                                                                                           class="md-input"/>
                                                                                </div>
                                                                                <div class="uk-width-medium-1-2">
                                                                                    <label for="oway">
                                                                                        Oway Contact person
                                                                                    </label>
                                                                                    <input type="text"
                                                                                           name="oway_contact_person" value="{{ $emp->owaycompany_contact_person }}"
                                                                                           id="oway{{ $emp->id  }}"
                                                                                           class="md-input"/>
                                                                                </div>
                                                                            </div>

                                                                            <div class="uk-grid">
                                                                                <div class="uk-width-medium-1-2">
                                                                                    <label for="email">
                                                                                        Company Email
                                                                                    </label>
                                                                                    <input type="email" name="company_email" value="{{ $emp->company_email }}"
                                                                                           id="email{{ $emp->id  }}"
                                                                                           class="md-input"/>
                                                                                </div>
                                                                                <div class="uk-width-medium-1-2">
                                                                                    <label for="amount{{ $emp->id  }}">
                                                                                        Actual Amount
                                                                                    </label>
                                                                                    <input type="text" name="actual_amount"
                                                                                           id="amount{{ $emp->id  }}" value="{{ $emp->actual_amount }}"
                                                                                           class="md-input"/>
                                                                                </div>
                                                                            </div>

                                                                            <div class="uk-grid">
                                                                                <div class="uk-width-medium-1-2">
                                                                                    <label for="salary{{ $emp->id  }}">
                                                                                        Salary
                                                                                    </label>
                                                                                    <input type="text" name="salary"
                                                                                           id="salary{{ $emp->id  }}" value="{{ $emp->salary }}"
                                                                                           class="md-input"/>
                                                                                </div>
                                                                            </div>

                                                                            <div class="uk-modal-footer uk-text-right">
                                                                                <button type="button"
                                                                                        class="md-btn md-btn-flat uk-modal-close">
                                                                                    Close
                                                                                </button>
                                                                                <button type="button"
                                                                                        onclick="edit_employee_record({{ $emp->id }})"
                                                                                        class="md-btn md-btn-flat md-btn-flat-primary uk-modal-close">
                                                                                    Submit
                                                                                </button>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    @endforeach
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>

                <div class="uk-grid">
                    <div class="uk-width-medium-1-1">
                        {!! $employee_list->render() !!}
                    </div>
                </div>

            @else
                <div class="md-card">
                    <div class="md-card-content">
                        No result found
                    </div>
                </div>
            @endif

            <div class="md-fab-wrapper">
                <a class="md-fab md-fab-primary"
                   data-uk-modal="{target : '#new_interview_person'}"
                   data-uk-tooltip="{pos:'left'}"
                   title="Add Interview Person" href="#">
                    <i class="material-icons">&#xE145;</i>
                </a>
            </div>
        </div>
    </div>

    <script>
        function resign_person(id){
            altair_helpers.content_preloader_show();
            $contact_data = $('#resign_data' + id );
            var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);
//            UIkit.modal.alert('<p>Wizard data:</p><pre>' + form_serialized + '</pre>');

            $.ajax({
                url: '{{ url('/employee/resign') }}',
                type: 'post',
                data: {
                    'emp_resign_data': $contact_data.serializeObject(),
                },
                success: function (data, textStatus, jQxhr) {
                    UIkit.notify({
                        message: 'Resign Complete.',
                        status: 'success',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                            location.reload();
                        }
                    });
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    UIkit.notify({
                        message: 'Something went wrong.',
                        status: 'danger',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                        }
                    });
                }
            });
        }

        function edit_employee_record(id){
            altair_helpers.content_preloader_show();
            var salarys = $('#salary' + id).val();
            var receives = $('#received' + id).val();
            var joinings = $('#joining' + id).val();
            var invoicings = $('#invoicing' + id).val();
            var payments = $('#payment' + id).val();
            var com_contacts = $('#company' + id).val();
            var oway_contacts = $('#oway' + id).val();
            var com_emails = $('#email' + id).val();
            var actual_amounts = $('#amount' + id).val();


            $.ajax({
                url: '{{ url('/employee') }}' + '/' + id,
                type: 'PUT',
                data: {
                    'salary': salarys,
                    'receive': receives,
                    'joining': joinings,
                    'invoicing': invoicings,
                    'payment': payments,
                    'com_contact': com_contacts,
                    'oway_contact': oway_contacts,
                    'com_email': com_emails,
                    'actual_amount': actual_amounts,
                },
                success: function (data, textStatus, jQxhr) {
                    UIkit.notify({
                        message: 'Sucessfully Added Employee Record.',
                        status: 'success',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                            location.reload()
                        }
                    });
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    UIkit.notify({
                        message: 'Sucessfully Added Employee Record.',
                        status: 'danger',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                            location.reload()
                        }
                    });
                }
            });
        }
    </script>
@stop