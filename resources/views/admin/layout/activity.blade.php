<h4 class="heading_c uk-margin-bottom">Recent Activity</h4>
<?php $activities = \App\Models\Activity::orderBy('created_at', 'desc')->take(100)->get() ?>
<div class="timeline timeline_small">
    @foreach($activities as $activity)
        <div class="timeline_item">
            <div class="timeline_icon
                    @if($activity->type == 'create')
                    timeline_icon_success
                @elseif($activity->type == 'update')
                    timeline_icon_primary
                @elseif($activity->type == 'remark')
                    timeline_icon_danger
                @elseif($activity->type == 'delete')
                    timeline_icon_danger
                @elseif($activity->type == 'email')

            @elseif($activity->type == 'available')
                    timeline_icon_success
                @elseif($activity->type == 'hold')
                    timeline_icon_primary
                @elseif($activity->type == 'resign')
                    timeline_icon_danger
                @elseif($activity->type == 'login')
                    timeline_icon_primary
                @elseif($activity->type == 'logout')
                    timeline_icon_warning
                @endif ">

                @if($activity->type == 'create')
                    <i class="material-icons">&#xE85D;</i>
                @elseif($activity->type == 'update')
                    <i class="material-icons">&#xE0B9;</i>
                @elseif($activity->type == 'delete')
                    <i class="material-icons">&#xE5CD;</i>
                @elseif($activity->type == 'email')
                    <i class="material-icons">mail_outline</i>
                @elseif($activity->type == 'available')
                    <i class="material-icons">face</i>
                @elseif($activity->type == 'hold')
                    <i class="material-icons">traffic</i>
                @elseif($activity->type == 'remark')
                    <i class="material-icons">report</i>
                @elseif($activity->type == 'resign')
                    <i class="material-icons">&#xE5CD;</i>
                @elseif($activity->type == 'login')
                    <i class="material-icons">done</i>
                @elseif($activity->type == 'logout')
                    <i class="material-icons">remove_circle_outline</i>
                @endif
            </div>
            <div class="timeline_date">
                {{ $activity->created_at->diffForHumans() }}
            </div>
            <?php $user = \App\User::find($activity->user_id) ?>
            <div class="timeline_content">
                @if($activity->user_id === \Sentinel::check()->id)
                    You
                @else
                    {{ @$user->first_name }} {{ @$user->last_name }}
                @endif
                {!! $activity->description !!}
            </div>
        </div>
    @endforeach

</div>