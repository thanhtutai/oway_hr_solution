<aside id="sidebar_main">

    <div class="sidebar_main_header" style="height: 70px">
        <div class="sidebar_logo">
            <h3 style="padding: 20px;color:#000;font-weight: 400;text-align: center;text-shadow: 4px 4px 4px #aaa;
">O W A Y - HR Solutions</h3>
        </div>
    </div>

    <div class="menu_section">
        <ul>
            <li>
                <a href="{{ url('dashboard') }}">
                    <span class="menu_icon"><i class="material-icons">&#xE871;</i></span>
                    <span class="menu_title">Dashboard</span>
                </a>
            </li>
            @if(Sentinel::hasAccess('admin'))
            <li>
                <a href="{{ url('country') }}">
                    <span class="menu_icon"><i class="material-icons">public</i></span>
                    <span class="menu_title">Country Management</span>
                </a>
            </li>

            <li  title="Modules Management">
                <a href="{{ url('module') }}">
                    <span class="menu_icon"><i class="material-icons">view_module</i></span>
                    <span class="menu_title">Modules Management</span>
                </a>
            </li>

            @endif

            <li  title="Invoices">
                <a href="{{ url('invoice') }}">
                    <span class="menu_icon"><i class="material-icons">&#xE53E;</i></span>
                    <span class="menu_title">Invoices</span>
                </a>
            </li>
            <li  title="Send CV">
                <a href="{{ url('send_cv') }}">
                    <span class="menu_icon"><i class="material-icons">email</i></span>
                    <span class="menu_title">Send CV</span>
                </a>
            </li>


            <li  title="Create CV">
                <a href="{{ url('cv/create') }}">
                    <span class="menu_icon"><i class="material-icons">add_circle_outline</i></span>
                    <span class="menu_title">Create CV Form</span>
                </a>
            </li>

            @if(Sentinel::hasAccess('admin'))

            <li  title="Email Settings">
                <a href="{{ url('general_setting/email_setting') }}">
                    <span class="menu_icon"><i class="material-icons">mail_outline</i></span>
                    <span class="menu_title">Email Settings</span>
                </a>
            </li>

            @endif

            <li title="Interview">
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">assignment_turned_in</i></span>
                    <span class="menu_title">Interview</span>
                </a>
                <ul>
                    <li><a href="{{ url('interview') }}">Interview List</a></li>
                    <li><a href="{{ url('interview/second_interview') }}">Second Interview List</a></li>
                </ul>
            </li>

            @if(Sentinel::hasAccess('admin'))
                <li>
                    <a href="#">
                        <span class="menu_icon"><i class="material-icons">&#xE87C;</i></span>
                        <span class="menu_title">User</span>
                    </a>
                    <ul>
                        <li><a href="{{ url('admin') }}">User List</a></li>
                        <li><a href="{{ url('role') }}">Role List</a></li>
                    </ul>
                </li>
            @endif

            <li>
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">assignment_ind</i></span>
                    <span class="menu_title">Applicant</span>
                </a>
                <ul>
                    <li><a href="{{ url('cv') }}">Applicant List</a></li>
                    <li><a href="{{ url('employee') }}">Employee Record</a></li>
                </ul>
            </li>


            <li>
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">&#xE87B;</i></span>
                    <span class="menu_title">Job</span>
                </a>
                <ul>
                    <li><a href="{{ url('job_category') }}">Job Category Management</a></li>
                    <li><a href="{{ url('job_industry') }}">Job Industry Management</a></li>
                </ul>
            </li>


            <li>
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">business</i></span>
                    <span class="menu_title">Client</span>
                </a>
                <ul>
                    <li><a href="{{ url('client') }}">Client List</a></li>
                    <li><a href="{{ url('client/create') }}">Create New Client</a></li>
                </ul>
            </li>

		<li>
                <a href="#">
                    <span class="menu_icon"><i class="material-icons">add_circle_outline</i></span>
                    <span class="menu_title">Import Excel</span>
                </a>
                <ul>
                    <li><a href="{{ url('import_excel') }}">Import Excel</a></li>
                    <li><a href="{{ url('export_excel') }}">Export Excel</a></li>
                </ul>
            </li>
        </ul>
    </div>

    <p style="padding: 60px 0px 0px 25px;float: right;">Power by&nbsp;<a href="http://www.invisiblestudio-mm.com/" data-uk-tooltip="{pos:'top'}" title="Invisible Studio"><img src="{{ url('img/logo.jpg') }}" style="width: 50%;" alt="Invisible Studio"></a></p>
</aside>

