<!doctype html>
<!--[if lte IE 9]>
<html class="lte-ie9" lang="en"> <![endif]-->
<!--[if gt IE 9]><!-->
<html lang="en"> <!--<![endif]-->
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="initial-scale=1.0,maximum-scale=1.0,user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="">
    <meta name="author" content="Nyi Nyi Lwin">

    <title>Oway`s Online Database Management System</title>

    <link rel="shortcut icon" href="{{ asset('backend/img/favicons/favicon.png') }}">


    <link rel="icon" type="image/png" href="{{ asset('backend/img/favicons/favicon-16x16.png') }}" sizes="16x16">
    <link rel="icon" type="image/png" href="{{ asset('backend/img/favicons/favicon-32x32.png') }}" sizes="32x32">
    <link rel="icon" type="image/png" href="{{ asset('backend/img/favicons/favicon-96x96.png') }}" sizes="96x96">
    <link rel="icon" type="image/png" href="{{ asset('backend/img/favicons/favicon-160x160.png') }}" sizes="160x160">
    <link rel="icon" type="image/png" href="{{ asset('backend/img/favicons/favicon-192x192.png') }}" sizes="192x192">

    <link rel="apple-touch-icon" sizes="57x57" href="{{ asset('backend/img/favicons/apple-touch-icon-57x57.png') }}">
    <link rel="apple-touch-icon" sizes="60x60" href="{{ asset('backend/img/favicons/apple-touch-icon-60x60.png') }}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{ asset('backend/img/favicons/apple-touch-icon-72x72.png') }}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{ asset('backend/img/favicons/apple-touch-icon-76x76.png') }}">
    <link rel="apple-touch-icon" sizes="114x114"
          href="{{ asset('backend/img/favicons/apple-touch-icon-114x114.png') }}">
    <link rel="apple-touch-icon" sizes="120x120"
          href="{{ asset('backend/img/favicons/apple-touch-icon-120x120.png') }}">
    <link rel="apple-touch-icon" sizes="144x144"
          href="{{ asset('backend/img/favicons/apple-touch-icon-144x144.png') }}">
    <link rel="apple-touch-icon" sizes="152x152"
          href="{{ asset('backend/img/favicons/apple-touch-icon-152x152.png') }}">
    <link rel="apple-touch-icon" sizes="180x180"
          href="{{ asset('backend/img/favicons/apple-touch-icon-180x180.png') }}">
    <!-- END Icons -->
    <!-- END Icons -->


    <!-- weather icons -->
    <link rel="stylesheet" href="{{ asset('/backend/bower_components/weather-icons/css/weather-icons.min.css') }}"
          media="all">
    <!-- metrics graphics (charts) -->
    <link rel="stylesheet" href="{{ asset('/backend/bower_components/metrics-graphics/dist/metricsgraphics.css') }}">
    <!-- c3.js (charts) -->
    <link rel="stylesheet" href="{{ asset('/backend/bower_components/c3js-chart/c3.min.css') }}">

    <link rel="stylesheet" href="{{ asset('/backend/bower_components/codemirror/lib/codemirror.css') }}">

    <link rel="stylesheet" href="{{ asset('/backend/bower_components/fullcalendar/dist/fullcalendar.min.css') }}">

    <!-- uikit -->
    <link rel="stylesheet" href="{{ asset('/backend/bower_components/uikit/css/uikit.almost-flat.min.css') }}"
          media="all">
    <link rel="stylesheet" href="{{ asset('front/css/ionicons.css') }}">
    <!-- ionicons -->
    <link rel="stylesheet" href="{{ asset('backend/skins/dropify/css/dropify.css') }}">
    <!-- flag icons -->
    <link rel="stylesheet" href="{{ asset('backend/icons/flags/flags.min.css') }}" media="all">

    <!-- altair admin -->
    <link rel="stylesheet" href="{{ asset('backend/css/main.min.css') }}" media="all">

    <link href="http://fonts.googleapis.com/css?family=Lato:300,400,400italic,600,700|Raleway:300,400,500,600,700|Crete+Round:400italic"
          rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Tangerine">
    <link rel="stylesheet" href="{{ asset('backend/skins/jtable/jtable.min.css') }}">

    <link rel="stylesheet" href="{{ asset('backend/skins/jquery-ui/material/jquery-ui.min.css') }}">

    <!-- additional styles for plugins -->
    <!-- kendo UI -->
    <link rel="stylesheet"
          href="{{ asset('/backend/bower_components/kendo-ui/styles/kendo.common-material.min.css') }}"/>

    <link rel="stylesheet" href="{{ asset('/backend/bower_components/kendo-ui/styles/kendo.material.min.css') }}"/>
    <script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
        <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">

    <!-- Optional theme -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">

    <!-- Latest compiled and minified JavaScript -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous">		</script>
    <style>
        body, a {
            font-family: 'Raleway', sans-serif, Zawgyi-One, "Myanmar Text";
        }

        .page-footer {
            padding: 10px 20px 15px;
            font-size: 13px;
            height: 33px;
        }

        .page-footer .page-footer-inner {
            color: rgb(51, 64, 76);
        }
        .page-footer .page-footer-inner {
            float: right;
            display: inline-block;
        }
    </style>
</head>
<body class="sidebar_main_open sidebar_main_swipe " onload="auto_load_activity()">
<input type="hidden" name="csrf-token" value="{{ csrf_token() }}">
<!-- main header -->
@include('admin.layout.header')
        <!-- main header end -->

<!-- main sidebar -->
@include('admin.layout.sidebar')
        <!-- main sidebar end -->

@yield('content')

@if(Sentinel::hasAccess('admin'))
    @include('admin.layout.secondarysidebar')
@endif

<div class="page-footer">
    <div class="page-footer-inner"> 2016 © Oway HR Solutions</div>
</div>

<script>
    WebFontConfig = {
        google: {
            families: [
                'Source+Code+Pro:400,700:latin',
                'Roboto:400,300,500,700,400italic:latin'
            ]
        }
    };
    (function () {
        var wf = document.createElement('script');
        wf.src = ('https:' == document.location.protocol ? 'https' : 'http') +
                '://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js';
        wf.type = 'text/javascript';
        wf.async = 'true';
        var s = document.getElementsByTagName('script')[0];
        s.parentNode.insertBefore(wf, s);
    })();
</script>

<script src="{{ asset('backend/js/common.min.js') }}"></script>
<!-- uikit functions -->
<script src="{{ asset('backend/js/uikit_custom.min.js') }}"></script>
<!-- altair common functions/helpers -->
<script src="{{ asset('backend/js/altair_admin_common.min.js') }}"></script>

<!-- datatables -->
<script src="{{ asset('/backend/bower_components/datatables/media/js/jquery.dataTables.min.js') }}"></script>
<!-- datatables colVis-->
<script src="{{ asset('/backend/bower_components/datatables-colvis/js/dataTables.colVis.js') }}"></script>
<!-- datatables tableTools-->
<script src="{{ asset('/backend/bower_components/datatables-tabletools/js/dataTables.tableTools.js') }}"></script>
<!-- datatables custom integration -->
<script src="{{ asset('backend/js/custom/datatables_uikit.js') }}"></script>

<!--  datatables functions -->
<script src="{{ asset('backend/js/pages/plugins_datatables.js') }}"></script>

<script src="{{ asset('backend/js/pages/components_notifications.min.js') }}"></script>
<!--  contact list functions -->
<script src="{{ asset('backend/js/pages/page_contact_list.min.js') }}"></script>


<!-- fullcalendar -->
<script src="{{ asset('/backend/bower_components/fullcalendar/dist/fullcalendar.min.js') }}"></script>

<!--  calendar functions -->
<script src="{{ asset('backend/js/pages/plugins_fullcalendar.min.js') }}"></script>

<script>
    // load parsley config (altair_admin_common.js)
    altair_forms.parsley_validation_config();
    // load extra validators
    altair_forms.parsley_extra_validators();

</script>

<script src="{{ asset('backend/bower_components/parsleyjs/dist/parsley.js') }}">
</script>
<!-- jquery steps -->
<script src="{{ asset('backend/js/custom/wizard_steps.js') }}">
</script>
<!-- forms wizard functions -->
<script src="{{ asset('backend/js/pages/forms_wizard.js') }}">
</script>


<script src="{{ asset('backend/js/pages/page_settings.min.js') }}"></script>

<!-- c3.js (charts) -->
<script src="{{ asset('/backend/bower_components/c3js-chart/c3.min.js') }}"></script>

<!--  charts functions -->
<script src="{{ asset('/backend/js/pages/plugins_charts.js') }}"></script>

<script src="{{ asset('/backend/bower_components/d3/d3.min.js') }}"></script>
<!-- metrics graphics (charts) -->
<script src="{{ asset('/backend/bower_components/metrics-graphics/dist/metricsgraphics.min.js') }}"></script>
<!-- chartist (charts) -->
<script src="{{ asset('/backend/bower_components/chartist/dist/chartist.min.js') }}"></script>

<script src="{{ asset('/backend/bower_components/maplace.js/src/maplace-0.1.3.js') }}"></script>
<!-- peity (small charts) -->
<script src="{{ asset('/backend/bower_components/peity/jquery.peity.min.js') }}"></script>
<!-- easy-pie-chart (circular statistics) -->
<script src="{{ asset('/backend/bower_components/jquery.easy-pie-chart/dist/jquery.easypiechart.min.js') }}"></script>
<!-- countUp -->
<script src="{{ asset('/backend/bower_components/countUp.js/countUp.min.js') }}"></script>
<!-- handlebars.js -->
<script src="{{ asset('/backend/bower_components/handlebars/handlebars.min.js') }}"></script>
<script src="{{ asset('backend/js/custom/handlebars_helpers.min.js') }}"></script>
<!-- CLNDR -->
<script src="{{ asset('/backend/bower_components/clndr/src/clndr.js') }}"></script>
<!-- fitvids -->
<script src="{{ asset('/backend/bower_components/fitvids/jquery.fitvids.js') }}"></script>

<!--  dashbord functions -->
<script src="{{ asset('backend/js/pages/dashboard.js') }}"></script>

<script src="{{ asset('backend/js/pages/page_mailbox.min.js') }}"></script>


<script src="{{ asset('/backend/bower_components/jquery-ui/jquery-ui.min.js') }}"></script>
<!-- jTable -->

<script src="{{ asset('/backend/bower_components/jtable/lib/jquery.jtable.min.js') }}"></script>


<!--  forms advanced functions -->
<script src="{{ asset('backend/js/pages/forms_advanced.js') }}"></script>

<!-- ionrangeslider -->
<script src="{{ asset('/backend/bower_components/ion.rangeslider/js/ion.rangeSlider.min.js') }}"></script>
<!-- page specific plugins -->
<!-- kendo UI -->
<script src="{{ asset('backend/js/kendoui_custom.min.js') }}"></script>

<!--  kendoui functions -->
<script src="{{ asset('backend/js/pages/kendoui.js') }}"></script>


<script src="{{ asset('backend/js/custom/uikit_fileinput.js') }}"></script>

<!--  user edit functions -->
<script src="{{ asset('backend/js/pages/page_user_edit.js') }}"></script>

<script>
    $(function () {
        // enable hires images
        altair_helpers.retina_images();
        // fastClick (touch devices)
        if (Modernizr.touch) {
            FastClick.attach(document.body);
        }

        auto_load_activity();

        @yield('script')

    });

    function auto_load_activity() {
        $.ajax({
            url: '{{ url('activity') }}',
            type: 'get',
            data: '',
            success: function (msg) {
                $('#activity').html(msg);
            }
        });

    }

    setInterval(auto_load_activity, 2000);


</script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('input[name="csrf-token"]').attr('value')
        }
    });
</script>
<script src="//js.pusher.com/2.2/pusher.min.js"></script>
<script>
    var pusher = new Pusher("72ee7a91943014b120ad");
</script>

<script>
    var loadDeferredStyles = function () {
        var addStylesNode = document.getElementById("deferred-styles");
        var replacement = document.createElement("div");
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement)
        addStylesNode.parentElement.removeChild(addStylesNode);
    };
    var raf = requestAnimationFrame || mozRequestAnimationFrame ||
            webkitRequestAnimationFrame || msRequestAnimationFrame;
    if (raf) raf(function () {
        window.setTimeout(loadDeferredStyles, 0);
    });
    else window.addEventListener('load', loadDeferredStyles);
</script>

</body>
</html>