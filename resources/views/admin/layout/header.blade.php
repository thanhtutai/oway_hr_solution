<header id="header_main">
    <div class="header_main_content">
        <nav class="uk-navbar">

            <!-- main sidebar switch -->
            <a href="#" id="sidebar_main_toggle" class="sSwitch sSwitch_left">
                <span class="sSwitchIcon"></span>
            </a>

            <!-- secondary sidebar switch -->
            <a href="#" id="sidebar_secondary_toggle" class="sSwitch sSwitch_right sidebar_secondary_check">
                <span class="sSwitchIcon"></span>
            </a>

            <div class="uk-navbar-flip">
                <ul class="uk-navbar-nav user_actions">
                    <li><a href="#" id="full_screen_toggle" class="user_action_icon uk-visible-large"><i class="material-icons md-24 md-light">&#xE5D0;</i></a></li>
                    <li data-uk-dropdown="{mode:'click'}">
                        <a href="#" class="user_action_image"><img class="md-user-image" src="http://www.gravatar.com/avatar/{{ md5(strtolower(trim( \Session::get('admin_email') ))) }}/?d=wavatar&s=100&r=g" alt=""/></a>
                        <div class="uk-dropdown uk-dropdown-small uk-dropdown-flip">
                            <ul class="uk-nav js-uk-prevent">
                                <li><a href="#" data-uk-modal="{target : '#change_password'}">Change Password</a></li>
                                <li><a href="{{ url('byebye') }}">Logout</a></li>
                            </ul>
                        </div>
                    </li>
                </ul>
            </div>
        </nav>
    </div>

    <div class="uk-modal" id="change_password">
        <div class="uk-modal-dialog">
            <div class="uk-modal-header">
                <h3 class="uk-modal-title">Change Password</h3>
            </div>
            <form id="password_data" data-parsley-validate>
                <div class="uk-grid" data-uk-grid-margin="">
                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                    <div class="uk-width-medium-1-2">
                        <div class="uk-form-row" parsley-row>
                            <div class="md-input-wrapper"><label>Passsword</label><input id="password" class="md-input" name="password" required type="password"><span class="md-input-bar"></span></div>
                        </div>
                    </div>
                    <div class="uk-width-medium-1-2">
                        <div class="uk-form-row" parsley-row>
                            <div class="md-input-wrapper"><label>Comfirm Passsword</label><input id="con_password" data-parsley-equalto="#password"	class="md-input" name="password_comfirmation" required type="password"><span class="md-input-bar"></span></div>
                        </div>
                    </div>
                </div>
                <div class="uk-modal-footer uk-text-right">
                    <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                    <button type="submit" class="md-btn md-btn-flat md-btn-flat-primary uk-modal-close" onclick="changepassword()">Submit</button>
                </div>
            </form>
        </div>
    </div>
</header>

<script>
    function changepassword() {
        altair_helpers.content_preloader_show();
        $contact_data = $('#password_data');
        var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);

        $.ajax({
            url: '{{ url('changePassword') }}',
            type: 'post',
            data: {
                'password_data': $contact_data.serializeObject(),
            },
            success: function (data, textStatus, jQxhr) {
                UIkit.notify({
                    message: 'Sucessfully Change Password',
                    status: 'success',
                    timeout: 1000,
                    pos: 'top-right',
                    onClose: function () {
                        altair_helpers.content_preloader_hide();
                    }
                });
            },
            error: function (jqXhr, textStatus, errorThrown) {
                UIkit.notify({
                    message: 'Something went wrong.',
                    status: 'danger',
                    timeout: 1000,
                    pos: 'top-right',
                    onClose: function () {
                        altair_helpers.content_preloader_hide();
                    }
                });
            }
        });

    }
</script>
