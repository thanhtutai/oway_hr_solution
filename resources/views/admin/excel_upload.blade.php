@extends('admin.layout.app')
@section('content')
    <div id="page_content">
        <div id="page_content_inner">

            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-large-7-10">
                    <div class="md-card">
                        <div class="user_heading" data-uk-sticky="{ top: 48, media: 960 }">

                            <div class="user_heading_content">
                                <h2 class="heading_b">
                                    <span class="uk-text-truncate" style="text-align: center;font-size: 1.5em" id="">Import Excel File</span>
                                </h2>
                            </div>
                            <button title="Save" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'left'}" type="submit"
                                    class="md-fab md-fab-small md-fab-accent">
                                <i class="material-icons">&#xE161;</i>
                            </button>
                            {{--<div class="md-fab-wrapper">--}}
                            {{--<div class="md-fab md-fab-toolbar md-fab-small md-fab-accent ">--}}
                            {{--<i class="material-icons">&#xE8BE;</i>--}}
                            {{--<div class="md-fab-toolbar-actions">--}}
                            {{--<button type="submit" id="user_edit_save"--}}
                            {{--data-uk-tooltip="{cls:'uk-tooltip-small',pos:'bottom'}"--}}
                            {{--title="Save"><i class="material-icons md-color-white">&#xE161;</i>--}}
                            {{--</button>--}}

                            {{--<button type="reset" id="user_edit_delete"--}}
                            {{--data-uk-tooltip="{cls:'uk-tooltip-small',pos:'bottom'}"--}}
                            {{--title="Clear"><i class="material-icons md-color-white">&#xE872;</i>--}}
                            {{--</button>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                        </div>


                        <div class="user_content">
                            <ul id="user_edit_tabs" class="uk-tab"
                                data-uk-tab="{connect:'#user_edit_tabs_content'}">
                                <li class="uk-active"><a href="#">Import Excel File</a></li>
                            </ul>
                            <ul id="user_edit_tabs_content" class="uk-switcher uk-margin">
                                <li>
                                    <div class="uk-margin-top">



                                        {!! Form::open(['class'=>'form','files'=>'true']) !!}
                                        {!! Form::file('excel_file',null) !!}
                                        {!! Form::submit('submit') !!}
                                        {!! Form::close() !!}

                                                </div>
                                             </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
               
                          

        </div>
    </div>

    <script>
        function manual(val) {
            var modal = UIkit.modal('#emp_record');
            if (val == 3) {
                if (modal.isActive()) {
                    modal.hide();
                } else {
                    modal.show();
                }
            }else{
                $('#other_plan_date').val(null);
                $('#other_plan_description').val(null);
                var text = null;
                $('#date').html(text);
            }
        }

        function add_date(date,word){
             $('#other_plan_date').val(date);
            $('#other_plan_description').val(word);
            var text = "Date : " + date + "\n" + word;
            $('#date').html(text);
        }
    </script>
@stop
