@extends('admin.layout.app')
@section('content')
    <div id="page_content">
        <div id="page_content_inner">

            <h3 class="heading_a uk-margin-bottom">Sort products by name:</h3>
            @if(\Session::has('status'))
                <div class="uk-alert uk-alert-success" data-uk-alert="">
                    <a href="#" class="uk-alert-close uk-close"></a>
                    {{ \Session::get('status') }}
                </div>
            @endif

            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <div class="uk-grid" data-uk-grid-margin>
                        <div class="uk-width-medium-1-2">
                            <div class="uk-vertical-align">
                                <div class="uk-vertical-align-middle">
                                    <ul id="products_sort" class="uk-subnav uk-subnav-pill uk-margin-remove">
                                        <li data-uk-sort="uk-filter:asc"><a href="#">Ascending</a></li>
                                        <li data-uk-sort="uk-filter:desc"><a href="#">Descending</a></li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-2">
                            <label for="contact_list_search">Find client</label>
                            <input class="md-input" type="text" id="contact_list_search"/>
                        </div>
                    </div>
                </div>
            </div>
            <div id="contact_list" class="uk-grid-width-small-1-2 uk-grid-width-medium-1-3 uk-grid-width-large-1-4 hierarchical_show" data-uk-grid="{gutter: 20, controls: '#products_sort'}">
                @foreach($client as $row)
                 <?php $industry = \App\Models\Job_Industry::where('id',$row->industry)->first() ?>
                <div  data-uk-filter="{{ $row->company_name }} , {{ $row->company_name }} {{ $row->company_name }}">
                    <div class="md-card md-card-hover-img">

                        {{--<div class="md-card-head uk-text-center uk-position-relative">--}}
                            {{--<div class="uk-badge uk-badge-danger uk-position-absolute uk-position-top-left uk-margin-left uk-margin-top">$ 524.00</div>--}}
                            {{--<img class="md-card-head-img" src="{{ $row->client_logo }}" alt=""/>--}}
                        {{--</div>--}}
                        <div class="md-card-head">
                            <div class="md-card-head-menu" data-uk-dropdown="{pos:'bottom-right'}">
                                <i class="md-icon material-icons">&#xE5D4;</i>
                                <div class="uk-dropdown uk-dropdown-small">
                                    <ul class="uk-nav">
                                        <li><a href="{{ url('client').'/'.$row->id.'/edit' }}" > Edit</a></li>
                                        <li><a href="#" id="delete_client" onclick="delete_client({{ $row->id }})" > Delete</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="uk-text-center">
                                <img class="md-card-head-avatar" src="{{ $row->client_logo }}"  alt=""/>
                            </div>
                            <h3 class="md-card-head-text uk-text-center">
                                {{ $row->company_name }}
                            </h3>
                        </div>
                        <div class="md-card-content">
                            <ul class="md-list">
                                <li>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">Job Industry</span>
                                        <span class="uk-text-small uk-text-muted">
                                            {{ @$industry->industry_description }}
                                        </span>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">Company Code</span>
                                        <span class="uk-text-small uk-text-muted">
                                            {{ $row->company_code }}
                                        </span>
                                    </div>
                                </li>
                                <li>
                                    <div class="md-list-content">
                                        <span class="md-list-heading">Address</span>
                                        <span class="uk-text-small uk-text-muted">
                                            {{ $row->address }}
                                        </span>
                                    </div>
                                </li>
                            </ul>
                            <a style="width: 100%" class="md-btn md-btn-small" href="{{ url('client').'/'.$row->id }}">More</a>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>

            <div class="md-fab-wrapper">
                <a class="md-fab md-fab-primary" data-uk-tooltip="{pos:'left'}" title="Add New Client"  href="{{ url('client/create') }}">
                    <i class="material-icons">&#xE145;</i>
                </a>
            </div>

        </div>
    </div>

    <script>
        function delete_client($id){

            var id = $id;

            UIkit.modal.confirm(
                    'Are you sure?',
                    function(){

                        (function(modal){
                            modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'>Wait <br/><img class=\'uk-margin-top\' src=\'{{ url('backend/img/spinners/spinner.gif') }}\' alt=\'\'>');
                            setTimeout(function(){
                                $.ajax({
                                    url: '{{ url('/client') }}' + '/' + id,
                                    type: 'DELETE',
                                    data: '',
                                    success: function( msg ) {
                                        modal.hide();
                                        UIkit.notify({
                                            message: 'Client Successfully removed.',
                                            status: 'danger',
                                            timeout: 3000,
                                            pos: 'top-right',
                                            onClose: function() {
                                                window.location='{{ url('client') }}';
                                            }
                                        });
                                        window.location='{{ url('client') }}';
                                    },
                                    error: function( data ) {
                                        modal.hide();
                                        UIkit.notify({
                                            message: 'Client Successfully removed.',
                                            status: 'danger',
                                            timeout: 3000,
                                            pos: 'top-right',
                                            onClose: function() {
                                                window.location='{{ url('client') }}';
                                            }
                                        });
                                        window.location='{{ url('client') }}';
                                    }
                                });


                            }, 5000) })();
                    });
        }
    </script>
@stop