@extends('admin.layout.app')
@section('content')
    <div id="page_content">
        <div id="page_content_inner">
            {!!  Form::open(array('url' => 'client', 'class' => 'uk-form-stacked' , 'id'=>'user_edit_form', 'method'=>'post', 'files'=>true)) !!}
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-large-7-10">
                    <div class="md-card">
                        <div class="user_heading" data-uk-sticky="{ top: 48, media: 960 }">
                            <div class="user_heading_avatar fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail">
                                    <img src="{{ url('backend/img/blank.png') }}" alt="user avatar"/>
                                </div>
                                <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                <div class="user_avatar_controls">
                                        <span class="btn-file">
                                            <span class="fileinput-new"><i class="material-icons">&#xE2C6;</i></span>
                                            <span class="fileinput-exists"><i class="material-icons">&#xE86A;</i></span>
                                            <input type="file" name="company_logo"
                                                   id="user_edit_avatar_control">
                                        </span>
                                    <a href="#" class="btn-file fileinput-exists" data-dismiss="fileinput"><i
                                                class="material-icons">&#xE5CD;</i></a>
                                </div>
                            </div>
                            <div class="user_heading_content">
                                <h2 class="heading_b">
                                    <span class="uk-text-truncate" style="text-align: center;font-size: 1.5em" id="">CLIENT'S INFORMATION</span>
                                </h2>
                            </div>
                            <button title="Save" data-uk-tooltip="{cls:'uk-tooltip-small',pos:'left'}" type="submit"
                                    class="md-fab md-fab-small md-fab-accent">
                                <i class="material-icons">&#xE161;</i>
                            </button>
                            {{--<div class="md-fab-wrapper">--}}
                            {{--<div class="md-fab md-fab-toolbar md-fab-small md-fab-accent ">--}}
                            {{--<i class="material-icons">&#xE8BE;</i>--}}
                            {{--<div class="md-fab-toolbar-actions">--}}
                            {{--<button type="submit" id="user_edit_save"--}}
                            {{--data-uk-tooltip="{cls:'uk-tooltip-small',pos:'bottom'}"--}}
                            {{--title="Save"><i class="material-icons md-color-white">&#xE161;</i>--}}
                            {{--</button>--}}

                            {{--<button type="reset" id="user_edit_delete"--}}
                            {{--data-uk-tooltip="{cls:'uk-tooltip-small',pos:'bottom'}"--}}
                            {{--title="Clear"><i class="material-icons md-color-white">&#xE872;</i>--}}
                            {{--</button>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                            {{--</div>--}}
                        </div>
                        <div class="user_content">
                            <ul id="user_edit_tabs" class="uk-tab"
                                data-uk-tab="{connect:'#user_edit_tabs_content'}">
                                <li class="uk-active"><a href="#">Client's Information</a></li>
                            </ul>
                            <ul id="user_edit_tabs_content" class="uk-switcher uk-margin">
                                <li>
                                    <div class="uk-margin-top">
                                        <h3 class="full_width_in_card heading_c">
                                            General info
                                        </h3>
                                        <div class="uk-grid" data-uk-grid-margin>
                                            <div class="uk-width-medium-1-2">
                                                <label for="user_edit_uname_control">Company Name</label>
                                                <input class="md-input" required type="text"
                                                       id="user_edit_uname_control"
                                                       name="company_name"/>
                                            </div>
                                            <div class="uk-width-medium-1-2">
                                                <div class="uk-form-row">
                                                    <select required id="user_edit_position_control" data-md-selectize
                                                            name="industry">
                                                        <option value="">Type of Business...</option>
                                                        @foreach($industry as $row)
                                                            <option value="{{ $row->id }}">{{ $row->industry_description }}</option>
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="uk-grid">
                                            <div class="uk-width-1-1">
                                                <label for="user_edit_personal_info_control">Address</label>
                                                    <textarea required class="md-input" name="address" name="address"
                                                              id="user_edit_personal_info_control" cols="30"
                                                              rows="4"></textarea>
                                            </div>
                                        </div>

                                        <h3 class="full_width_in_card heading_c">
                                            Contact info
                                        </h3>
                                        <div class="uk-grid">
                                            <div class="uk-width-1-1">
                                                <div class="uk-grid uk-grid-width-1-1 uk-grid-width-large-1-2"
                                                     data-uk-grid-margin>
                                                    <div>
                                                        <div class="uk-input-group">
                                                                <span class="uk-input-group-addon">
                                                                    <i class="md-list-addon-icon material-icons">
                                                                        face</i>
                                                                </span>
                                                            <label>Contact Person</label>
                                                            <input type="text" class="md-input" required
                                                                   name="contact_person"
                                                                   value=""/>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="uk-input-group">
                                                                <span class="uk-input-group-addon">
                                                                    <i class="md-list-addon-icon material-icons">
                                                                        &#xE0CD;</i>
                                                                </span>
                                                            <label>Phone Number</label>
                                                            <input type="text" class="md-input" required
                                                                   name="contact_number"
                                                                   value=""/>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="uk-input-group">
                                                                <span class="uk-input-group-addon">
                                                                    <i class="md-list-addon-icon material-icons">email</i>
                                                                </span>
                                                            <label>Email</label>
                                                            <input type="email" class="md-input" required
                                                                   name="email"
                                                                   value=""/>
                                                        </div>
                                                    </div>
                                                    <div>
                                                        <div class="uk-input-group">
                                                                <span class="uk-input-group-addon">
                                                                    <i class="md-list-addon-icon material-icons">web</i>
                                                                </span>
                                                            <label>Company Website's Link ( optional )</label>
                                                            <input type="text" class="md-input"
                                                                   name="link"
                                                                   value=""/>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="uk-width-large-3-10">
                    <div class="md-card">
                        <div class="md-card-content">
                            {{--<h3 class="full_width_in_card heading_c">--}}
                            {{--Other Setting--}}
                            {{--</h3>--}}

                            {{--<div class="uk-form-row">--}}
                            {{--<input type="checkbox"  data-switchery name="is_paid" id="user_edit_active"/>--}}
                            {{--<label for="user_edit_active" class="inline-label">Is Paid</label>--}}
                            {{--</div>--}}

                            {{--<div class="uk-form-row">--}}
                            {{--<input type="checkbox"  data-switchery name="status" id="user_edit_active"/>--}}
                            {{--<label for="user_edit_active" class="inline-label">Approve</label>--}}
                            {{--</div>--}}

                            {{--<hr class="md-hr">--}}
                            <div class="uk-form-row">
                                <h3 class="full_width_in_card heading_c">
                                    Signed Contract
                                </h3>
                                <br>
                                {{--<label class="uk-form-label" for="user_edit_role">Signed Contract</label>--}}
                                <div class="user_heading_avatar fileinput fileinput-new" data-provides="fileinput">
                                    <div class="fileinput-new thumbnail">
                                        <img src="{{ url('backend/img/blank.png') }}" alt="user avatar"/>
                                    </div>
                                    <div class="fileinput-preview fileinput-exists thumbnail"></div>
                                    <div class="user_avatar_controls">
                                        <span class="btn-file">
                                            <span class="fileinput-new"><i class="material-icons">&#xE2C6;</i></span>
                                            <span class="fileinput-exists"><i class="material-icons">&#xE86A;</i></span>
                                            <input type="file" name="signed_contract_image"
                                                   id="user_edit_avatar_control">
                                        </span>
                                        <a href="#" class="btn-file fileinput-exists" data-dismiss="fileinput"><i
                                                    class="material-icons">&#xE5CD;</i></a>
                                    </div>
                                </div>
                            </div>
                            {{--<hr class="md-hr">--}}
                            <div class="uk-form-row">
                                <h3 class="full_width_in_card heading_c">
                                    Preferred Plan
                                </h3>
                                <br>
                                {{--<label class="uk-form-label" for="user_edit_role">Preferred Plan</label>--}}
                                <select data-md-selectize required id="plans" name="plan"
                                        onchange="manual($('#plans').val())">
                                    <option value="">Select...</option>
                                    <option value="1">Gold</option>
                                    <option value="2">Diamond</option>
                                    <option value="3">Other</option>
                                </select>
                                <p id="date" data-uk-tooltip="{pos:'bottom'}"
                                   title="Change Date"
                                   data-uk-modal="{target:'#emp_record'}"></p>
                                <input type="hidden" name="manual_date" id="other_plan_date">
                                <input type="hidden" name="manual_date_description" id="other_plan_description">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="uk-modal" style="z-index: 998"
                 id="emp_record">
                <div class="uk-modal-dialog">
                    <div class="uk-modal-header">
                        <h3 class="uk-modal-title">Other Plan Date</h3>
                    </div>
                    <form id="other_plan">

                        <div class="uk-grid">
                            <div class="uk-width-medium-1-1">
                                <label for="choose_date">
                                    Choose Date<span class="req">*</span>
                                </label>
                                <input type="text"
                                       id="choose_date"
                                       class="md-input"
                                       data-parsley-date-message="This value should be a valid date"
                                       data-uk-datepicker="{format:'YYYY-MM-DD'}"/>
                            </div>
                            <div class="uk-width-medium-1-1">
                                <label for="choose_date">
                                    Description<span class="req">*</span>
                                </label>
                                <textarea name="description" id="other_description" class="md-input" cols="15" rows="5"></textarea>
                            </div>
                        </div>

                        <div class="uk-modal-footer uk-text-right">
                            <button type="button"
                                    class="md-btn md-btn-flat uk-modal-close">
                                Close
                            </button>
                            <button type="button"
                                    onclick="add_date($('#choose_date').val(),$('#other_description').val())"
                                    class="md-btn md-btn-flat md-btn-flat-primary uk-modal-close">
                                Submit
                            </button>
                        </div>
                    </form>
                </div>
            </div>

            {!! Form::close() !!}
        </div>
    </div>

    <script>
        function manual(val) {
            var modal = UIkit.modal('#emp_record');
            if (val == 3) {
                if (modal.isActive()) {
                    modal.hide();
                } else {
                    modal.show();
                }
            }else{
                $('#other_plan_date').val(null);
                $('#other_plan_description').val(null);
                var text = null;
                $('#date').html(text);
            }
        }

        function add_date(date,word){
             $('#other_plan_date').val(date);
            $('#other_plan_description').val(word);
            var text = "Date : " + date + "\n" + word;
            $('#date').html(text);
        }
    </script>
@stop
