@extends('admin.layout.app')
@section('content')
    <div id="page_content">
        <div id="page_content_inner">
            <div class="uk-grid" data-uk-grid-margin data-uk-grid-match id="user_profile">
                <div class="uk-width-large-7-10">
                    <div class="md-card">
                        <div class="user_heading">
                            <div class="user_heading_menu" data-uk-dropdown="{pos:'left-top'}">
                                <i class="md-icon material-icons md-icon-light">&#xE5D4;</i>
                                <div class="uk-dropdown uk-dropdown-small">
                                    <ul class="uk-nav">
                                        <li><a href="{{ url('client').'/'.$client->id.'/edit' }}"> Edit</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="user_heading_avatar" style="margin-top: 20px;">
                                <img src="{{ $client->client_logo }}" alt="user avatar"/>
                            </div>
                            <div class="user_heading_content">
                                <?php $industry = \App\Models\Job_Industry::where('id', $client->industry)->first() ?>
                                <h2 class="heading_b uk-margin-bottom"><span
                                            class="uk-text-truncate">{{ $client->company_name }}</span><span
                                            class="sub-heading">{{ @$industry->industry_description }}</span></h2>
                                <ul class="user_stats">
                                    <li>
                                        <h4 class="heading_a">
                                            Company Code : {{ $client->company_code }}
                                            @if($client->plan == '1')
                                                ( Plan - Gold )
                                            @elseif($client->plan == '2')
                                                ( Plan - Diamond )
                                            @else
                                                ( Plan - Other / {{ $client->expire_date }} )
                                            @endif
                                        </h4>
                                    </li>
                                    @if($client->plan_description)
                                    <li>
                                        {{ $client->plan_description }}
                                    </li>
                                    @endif
                                    <li>
                                        <?php $user = SiteHelper::get_user_permission($client->create_user_id) ?>
                                        Created by : {{ $user->first_name }} {{ $user->last_name }}
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="user_content">
                            <ul id="user_profile_tabs" class="uk-tab"
                                data-uk-tab="{connect:'#user_profile_tabs_content', animation:'slide-horizontal'}"
                                data-uk-sticky="{ top: 48, media: 960 }">
                                <li class="uk-active"><a href="#">Detail</a></li>
                                <li><a href="#">Signed Contract </a></li>
                                <li style="z-index:997;"><a href="#">Employed Person </a></li>
                                <li><a href="#">Resign Person </a></li>
                            </ul>

                            <ul id="user_profile_tabs_content" class="uk-switcher uk-margin">
                                <li>
                                    <div class="uk-grid uk-margin-medium-top uk-margin-large-bottom"
                                         data-uk-grid-margin>
                                        <div class="uk-width-large-1-2">
                                            <h4 class="heading_c uk-margin-small-bottom">Contact Info</h4>
                                            <ul class="md-list md-list-addon">
                                                <li style="border: none">
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">face</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">{{ $client->contact_person }}</span>
                                                        <span class="uk-text-small uk-text-muted">Contact Person</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">&#xE0CD;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">{{ $client->contact_number }}</span>
                                                        <span class="uk-text-small uk-text-muted">Phone</span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="uk-width-large-1-2">
                                            <h4 class="heading_c uk-margin-small-bottom">&nbsp;</h4>
                                            <ul class="md-list">
                                                <li style="border: none">
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">&#xE158;</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">{{ $client->email }}</span>
                                                        <span class="uk-text-small uk-text-muted">Email</span>
                                                    </div>
                                                </li>
                                                <li>
                                                    <div class="md-list-addon-element">
                                                        <i class="md-list-addon-icon material-icons">web</i>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">
                                                            @if($client->client_link)
                                                                {{ $client->client_link }}
                                                            @else
                                                                -
                                                            @endif
                                                        </span>
                                                        <span class="uk-text-small uk-text-muted">Company's Website Link</span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>

                                    <div class="uk-margin-medium-top">
                                        <h4 class="heading_c uk-margin-small-bottom">Address</h4>
                                        {{ $client->address }}
                                    </div>
                                </li>
                                <li>
                                    <div id="user_profile_gallery" data-uk-check-display
                                         class="uk-grid-width-small-1-2 uk-grid-width-medium-1-3 uk-grid-width-large-1-4"
                                         data-uk-grid="{gutter: 4}">
                                        <?php $signed_image = \App\Models\ClientAttach::where('client_id', $client->id)->get() ?>

                                        @foreach($signed_image as $image)
                                            <div>
                                                <a href="#" data-uk-modal="{target:'#modal_lightbox{{ $image->id }}'}">
                                                    <img style="height: 141px;width: 164px"
                                                         src="{{ $image->image_name }}" alt=""/>
                                                </a>
                                                <div class="uk-modal" id="modal_lightbox{{$image->id}}">
                                                    <div class="uk-modal-dialog uk-modal-dialog-lightbox">
                                                        <button type="button"
                                                                class="uk-modal-close uk-close uk-close-alt"></button>
                                                        <img src="{{ $image->image_name }}" alt=""/>
                                                    </div>
                                                </div>
                                            </div>
                                        @endforeach
                                    </div>
                                </li>
                                <li>
                                    <div class="uk-grid" data-uk-grid-margin >
                                        <div class="uk-width-medium-1-1">
                                            <ul class="md-list md-list-addon">
                                                @if(count($employee_list)>0)
                                                    @foreach($employee_list as $person)
                                                        <?php $emp_info = \App\Models\ApplicantInformation::where('cv_no',$person->cv_id)->first(); ?>
                                                <li>
                                                    <div class="gallery_grid_image_menu" data-uk-dropdown="{pos:'left-center'}">
                                                        <i class="md-icon material-icons">&#xE5D4;</i>
                                                        <div class="uk-dropdown uk-dropdown-small" style="z-index:999;">
                                                            <ul class="uk-nav">
                                                                <li><a href="#" data-uk-modal="{target:'#resign{{ $person->id }}'}"><i class="material-icons uk-margin-small-right">&#xE872;</i> Resign </a></li>
                                                            </ul>
                                                        </div>
                                                    </div>

                                                    <div class="uk-modal" style="z-index: 998"
                                                         id="resign{{ $person->id }}">
                                                        <div class="uk-modal-dialog">
                                                            <div class="uk-modal-header">
                                                                <h3 class="uk-modal-title">Resign</h3>
                                                            </div>
                                                            <form id="resign_data{{ $person->id  }}">

                                                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                                <input type="hidden" name="cv_no" value="{{ $person->cv_id }}">
                                                                <input type="hidden" name="company_code" value="{{ $client->company_code }}">
                                                                <input type="hidden" name="job_position" value="{{ $person->job_position }}">

                                                                <div class="uk-grid">
                                                                    <div class="uk-width-medium-1-1">
                                                                        <label for="invoicing{{ $person->id  }}">
                                                                            Resign Date
                                                                                <span class="req">
                                                                                    *
                                                                                </span>
                                                                        </label>
                                                                        <input type="text" name="resign_date"
                                                                               id="invoicing{{ $person->id  }}"
                                                                               class="md-input" value=""
                                                                               data-parsley-date-message="This value should be a valid date"
                                                                               data-uk-datepicker="{format:'YYYY-MM-DD'}"/>
                                                                    </div>
                                                                </div>

                                                                <div class="uk-grid">
                                                                    <div class="uk-width-medium-1-1">
                                                                        <label for="Reason">
                                                                            Reason
                                                                        </label>
                                                                        <textarea name="resign_reason" class="md-input" id="Reason" cols="10"
                                                                                  rows="5"></textarea>
                                                                    </div>
                                                                </div>


                                                                <div class="uk-modal-footer uk-text-right">
                                                                    <button type="button"
                                                                            class="md-btn md-btn-flat uk-modal-close">
                                                                        Close
                                                                    </button>
                                                                    <button type="button"
                                                                            onclick="resign_person({{ $person->id }})"
                                                                            class="md-btn md-btn-flat md-btn-flat-primary uk-modal-close">
                                                                        Submit
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>

                                                    <div class="md-list-addon-element">
                                                        <img class="md-user-image md-list-addon-avatar" style="width: 100%;height: 100%;padding: 10px;" src="{{ $emp_info->cv_image }}" alt=""/>
                                                    </div>
                                                    <div class="md-list-content">
                                                        <span class="md-list-heading">{{ $emp_info->applicant_name }} | {{ $emp_info->cv_no }}</span>
                                                        <span class="uk-text-small uk-text-muted">
                                                            <b>Job Position</b> - {{ \App\Models\ApplicantJobPosition::where('id',$person->job_position)->where('cv_id',$person->cv_id)->pluck('apply_position') }}
                                                             | <b>Salary</b> - {{ $person->salary }}
                                                             | <b>Actual Amount</b> - {{ $person->actual_amount }}
                                                        </span>
                                                        <span class="uk-text-small uk-text-muted">
                                                            <b>Job Received Date</b> - {{ $person->job_receive_date }} |
                                                            <b>Joining Date</b> - {{ $person->joining_date }}
                                                        </span>
                                                        <span class="uk-text-small uk-text-muted">
                                                            <b>Last Interview Date</b> -  {{ \App\Models\Interview::where('company_code',$client->company_code )->where('cv_id',$person->cv_id)->where('interview_position',$person->job_position)->pluck('interview_date') }}
                                                        </span>
                                                    </div>
                                                </li>
                                                    @endforeach
                                                @else
                                                <li>
                                                    No Employed Person
                                                </li>
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="uk-grid" data-uk-grid-margin >
                                        <div class="uk-width-medium-1-1">
                                            <ul class="md-list md-list-addon">
                                                @if(count($resign_list)>0)
                                                    @foreach($resign_list as $person)
                                                        <?php $emp_info = \App\Models\ApplicantInformation::where('cv_no',$person->cv_no)->first(); ?>
                                                        <li>
                                                            <div class="md-list-addon-element">
                                                                <img class="md-user-image md-list-addon-avatar" style="width: 100%;height: 100%;padding: 10px;" src="{{ $emp_info->cv_image }}" alt=""/>
                                                            </div>
                                                            <div class="md-list-content">
                                                                <span class="md-list-heading">
                                                                    {{ $emp_info->applicant_name }} | {{ $emp_info->cv_no }} | Resign Date : {{ $person->resign_date }} | Job Position : {{ \App\Models\ApplicantJobPosition::where('id',$person->job_position)->where('cv_id',$person->cv_no)->pluck('apply_position') }}
                                                                </span>
                                                        <span class="uk-text-small uk-text-muted">
                                                            {{ $person->reason }}
                                                        </span>
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                @else
                                                    <li>
                                                        No Resign Person
                                                    </li>
                                                @endif
                                            </ul>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="uk-width-large-3-10">
                    <div class="md-card">
                        <div class="md-card-content">
                            <h3 class="heading_c uk-margin-bottom">Interview List</h3>

                            @if(count($interview_person) > 0)
                            <ul class="md-list md-list-addon uk-margin-bottom">
                                @foreach($interview_person as $interview)
                                    <li>
                                        <div class="md-list-addon-element">
                                            <img class="md-user-image md-list-addon-avatar"
                                                 src="{{ \App\Models\ApplicantInformation::where('cv_no',$interview->cv_id)->pluck('cv_image') }}"
                                                 alt=""/>
                                        </div>
                                        <div class="md-list-content">
                                        <span class="md-list-heading">{{ \App\Models\ApplicantInformation::where('cv_no',$interview->cv_id)->pluck('applicant_name') }}
                                            @if($interview->interview_status == 1)
                                                <span class="uk-badge uk-badge-warning">Pending</span>
                                            @elseif($interview->interview_status == 2)
                                                <span class="uk-badge uk-badge-primary">Avaliable</span>
                                            @elseif($interview->interview_status == 3)
                                                <span class="uk-badge">Hold</span>
                                            @elseif($interview->interview_status == 4)
                                                <span class="uk-badge uk-badge-success">Employed</span>
                                            @elseif($interview->interview_status == 5)
                                                <span class="uk-badge uk-badge-danger">Rejected</span>
                                            @else
                                                <span class="uk-badge">Second Interview</span>
                                            @endif
                                        </span>
                                            <span class="uk-text-small uk-text-muted">{{ \App\Models\ApplicantInformation::where('cv_no',$interview->cv_id)->pluck('cv_no') }}</span>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                            <a class="md-btn md-btn-flat md-btn-flat-primary"
                               href="{{ url('interview/showall/').'/'.$client->company_code }}">Show all</a>
                            @else
                                <p class="text-cnter">No Interview</p>
                            @endif
                        </div>
                    </div>

                    <?php $invoice = \App\Models\Invoice::where('company_code',$client->company_code)->first() ?>

                    @if($invoice)
                    <div class="md-card">
                        <div class="md-card-content">
                            <h3 class="heading_c uk-margin-bottom">Invoice Detail</h3>
                            <ul class="md-list">
                                <li>
                                    Invoice Number - {{ $invoice->invoice_code }}
                                </li>
                                <li>Due Date - {{ Date('F j, Y',strtotime($invoice->due_date)) }}</li>
                                <li>
                                    @if($invoice->status == 1)
                                        <span class="uk-badge uk-badge-success">Paid</span>
                                    @else
                                        <span class="uk-badge uk-badge-warning">Not Paid</span>
                                    @endif
                                </li>
                            </ul>
                        </div>
                    </div>
                    @endif
                </div>
            </div>

        </div>
    </div>

    <script>
        function resign_person(id){
            altair_helpers.content_preloader_show();
            $contact_data = $('#resign_data' + id );
            var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);
//            UIkit.modal.alert('<p>Wizard data:</p><pre>' + form_serialized + '</pre>');

            $.ajax({
                url: '{{ url('/employee/resign') }}',
                type: 'post',
                data: {
                    'emp_resign_data': $contact_data.serializeObject(),
                },
                success: function (data, textStatus, jQxhr) {
                    UIkit.notify({
                        message: 'Resign Complete.',
                        status: 'success',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                            location.reload();
                        }
                    });
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    UIkit.notify({
                        message: 'Something went wrong.',
                        status: 'danger',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                        }
                    });
                }
            });
        }
    </script>

@stop