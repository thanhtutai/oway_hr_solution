@extends('admin.layout.app')
@section('content')

    <style>

        @media print {
            body * {
                visibility: hidden;
            }
            .section-to-print, .section-to-print * {
                visibility: visible;
            }
            .section-to-print {
                position: absolute;
                left: 0;
                top: 0;
            }

            .section-to-hide{
                display: none;
            }
        }

        .pagination {
            margin: 0;
            padding: 0;
            list-style: none;
            text-align: right;
            font-size: 0;
        }

        .pagination > li {
            display: inline-block;
            font-size: 1rem;
            vertical-align: top;
            outline: 0 !important;
        }

        .pagination > li:nth-child(n+2) {
            margin-left: 5px;
        }

        .pagination > li.active > a, .pagination > li.active > a:hover, .pagination > li.active > span, .pagination > li.active > span:hover {
            color: #fff;
        }

        .pagination > li.active > a, .pagination > li.active > span {
            background: #7cb342;
        }

        .pagination > .active > span {
            background: #00a8e6;
            color: #fff;
            border-color: transparent;
            box-shadow: inset 0 0 5px rgba(0, 0, 0, .05);
            text-shadow: 0 -1px 0 rgba(0, 0, 0, .1);
        }

        .pagination > li > a, .pagination > li > span {
            border: none;
            padding: 4px 8px;
            min-width: 32px;
            line-height: 24px;
            box-sizing: border-box;
            text-shadow: none;
            color: #212121;
            border-radius: 4px;
        }

        .pagination > li > a, .pagination > li > span {
            display: inline-block;
            min-width: 16px;
            padding: 3px 5px;
            line-height: 20px;
            text-decoration: none;
            box-sizing: content-box;
            text-align: center;
            border: 1px solid rgba(0, 0, 0, .06);
            border-radius: 4px;
        }

        .pagination > li {
            font-size: 1rem;
        }
    </style>
    <div id="page_content">
        <div id="page_content_inner">
            <h3 class="heading_a">Interview List ( Group By Interview Date )</h3>
            <br>
            <form action="{{ url('interview/search') }}" method="post" class="uk-form-stacked">
                {!! csrf_field() !!}
                <div class="uk-grid">
                    <div class="uk-width-large-1-4">
                        <div class="uk-form-row">
                            <input id="kUI_datetimepicker_range_start" name="from_date" placeholder="From Date"/>
                        </div>
                    </div>

                    <div class="uk-width-large-1-4">
                        <div class="uk-form-row">
                            <input id="kUI_datetimepicker_range_end" name="to_date" placeholder="To Date"/>
                        </div>
                    </div>

                    <div class="uk-width-large-1-4">
                        <div class="uk-form-row">
                            <?php $language_lvl = \App\Models\Language::language_lvl() ?>
                            <select id="search_status" data-md-selectize
                                    name="interview_status">
                                <option value="">Interview Status
                                </option>
                                <option value="1">Pending</option>
                                <option value="2">Avaliable</option>
                                <option value="3">Hold</option>
                                <option value="4">Employed</option>
                                <option value="5">Rejected</option>
                                <option value="6">Second Interview
                                </option>
                            </select>
                        </div>
                    </div>

                    <div class="uk-width-large-1-4">
                        <div class="uk-form-row">
                            <button type="submit" class="md-btn md-btn-danger"><i style="color: #FFF"
                                                                                  class="material-icons">search</i>&nbsp;Search
                            </button>
                            <a href="{{ url('interview') }}" class="md-btn">Reset</a>
                        </div>
                    </div>
                </div>
            </form>
            <br>
            <div class="uk-grid">

                <div class="uk-width-large-2-10">
                    <div class="md-card">
                        <div class="md-list-outside-wrapper">
                            <ul class="md-list md-list-addon md-list-outside" id="chat_user_list"
                                data-uk-tab="{connect:'#tabs_7'}">
                                @foreach($interview_code as $code)
                                    <li>
                                        <div class="md-list-content" style="padding: 15px;">
                                            <div class="md-list-action-placeholder"></div>
                                            <span class="md-list-heading">{{ Date('F j, Y',strtotime($code->interview_date)) }}</span>
                                        </div>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>

                <div class="uk-width-large-8-10">
                    <ul id="tabs_7" class="uk-switcher uk-margin">
                        @foreach($interview_code as $code)
                            <?php
                            $interview_company = \App\Models\InterviewCompany::where('interview_code', $code->interview_code)->get();
                            ?>
                            <li>
                                <div class="uk-grid">
                                    <div class="uk-width-1-1">
                                        @foreach($interview_company as $company)
                                            <?php $user = SiteHelper::get_user_permission($company->create_user_id) ?>
                                            <div id="print{{ $company->id }}" class="section-to-print">
                                                <div class="md-card">
                                                    <div class="md-card-toolbar">
                                                        <div class="md-card-toolbar-actions section-to-hide">
                                                            <a href="#" data-uk-tooltip="{pos:'bottom'}"
                                                               style="cursor: pointer;"
                                                               title="Add Remark"
                                                               data-uk-modal="{target:'#add_remark{{ $company->id }}'}"><i class="md-icon material-icons">insert_comment</i></a>

                                                            <i class="md-icon material-icons" onclick="print_div({{ $company->id }})">
                                                                print</i>
                                                            <a href="#" data-uk-tooltip="{pos:'bottom'}"
                                                               style="cursor: pointer;"
                                                               title="Add Interview Person"
                                                               data-uk-modal="{target:'#add_interview_person{{ $company->id }}'}"><i class="md-icon material-icons">person_add</i></a>
                                                            <i class="md-icon material-icons"
                                                               data-uk-tooltip="{pos:'bottom'}"
                                                               title="Delete Interview Session" onclick="del_interview_session({{ $company->id }})">
                                                                cancel</i>
                                                            <i class="md-icon material-icons md-card-toggle">
                                                                &#xE316;</i>
                                                        </div>
                                                        <h3 class="md-card-toolbar-heading-text">
                                                            {{ $company->interview_code }}
                                                            - {{ \App\Models\Client::where('company_code',$company->company_code)->pluck('company_name') }}
                                                            | {{ $company->interview_date }} ,
                                                            <span>Created By {{ $user->first_name }} {{ $user->last_name }}</span>
                                                        </h3>
                                                    </div>

                                                    <div class="uk-modal" style="z-index: 998" id="add_remark{{ $company->id }}">
                                                        <div class="uk-modal-dialog">
                                                            <div class="uk-modal-header">
                                                                <h3 class="uk-modal-title">Remak : {{ \App\Models\Client::where('company_code',$company->company_code)->pluck('company_name') }}</h3>
                                                            </div>
                                                            <form id="interview_remark_data{{ $company->id }}">
                                                                <div class="uk-grid">
                                                                    <div class="uk-width-medium-1-1 parsley-row">
                                                                        <input type="hidden" name="company_code" value="{{ $company->company_code }}">
                                                                        <input type="hidden" name="interview_code" value="{{ $company->interview_code }}">
                                                                        <input type="hidden" name="interview_date" value="{{ $company->interview_date }}">
                                                                        <label>Remark</label>
                                                                        <textarea cols="30" rows="4" name="remark" class="md-input"></textarea>
                                                                    </div>
                                                                </div>

                                                                <div class="uk-modal-footer uk-text-right">
                                                                    <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                                                                    <button type="button" onclick="add_interview_remark({{ $company->id }})"
                                                                            class="md-btn md-btn-flat md-btn-flat-primary uk-modal-close">Submit
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>

                                                    <div class="uk-modal" style="z-index: 998" id="edit_remark{{ $company->id }}">
                                                        <div class="uk-modal-dialog">
                                                            <div class="uk-modal-header">
                                                                <h3 class="uk-modal-title">Remak : {{ \App\Models\Client::where('company_code',$company->company_code)->pluck('company_name') }}</h3>
                                                            </div>
                                                            <form id="interview_remark_edit_data{{ $company->id }}">
                                                                <div class="uk-grid">
                                                                    <div class="uk-width-medium-1-1 parsley-row">
                                                                        <input type="hidden" name="company_code" value="{{ $company->company_code }}">
                                                                        <input type="hidden" name="interview_code" value="{{ $company->interview_code }}">
                                                                        <input type="hidden" name="interview_date" value="{{ $company->interview_date }}">
                                                                        <label>Remark</label>
                                                                        <textarea cols="30" rows="4" name="remark" class="md-input">{{ @$company->remark }}</textarea>
                                                                    </div>
                                                                </div>

                                                                <div class="uk-modal-footer uk-text-right">
                                                                    <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                                                                    <button type="button" onclick="edit_interview_remark({{ $company->id }})"
                                                                            class="md-btn md-btn-flat md-btn-flat-primary uk-modal-close">Submit
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>

                                                    <div class="uk-modal" style="z-index: 998" id="add_interview_person{{ $company->id }}">
                                                        <div class="uk-modal-dialog">
                                                            <div class="uk-modal-header">
                                                                <h3 class="uk-modal-title">Interview Person - {{ \App\Models\Client::where('company_code',$company->company_code)->pluck('company_name') }}</h3>
                                                            </div>
                                                            <form id="interview_user_data{{ $company->id }}">
                                                                <div class="uk-grid">
                                                                    <div class="uk-width-medium-1-1 parsley-row">
                                                                        <input type="hidden" name="company_code" value="{{ $company->company_code }}">
                                                                        <input type="hidden" name="interview_code" value="{{ $company->interview_code }}">
                                                                        <input type="hidden" name="interview_date" value="{{ $company->interview_date }}">
                                                                        <?php $applicant = \App\Models\ApplicantInformation::all() ?>
                                                                        <select id="employee_name{{ $company->id }}" name="interview_person" data-placeholder="Select CV"
                                                                                multiple>
                                                                            @foreach($applicant as $row)
                                                                                <option value="{{ $row->cv_no }}">
                                                                                    {{ $row->applicant_name }} ( {{ $row->cv_no }} )
                                                                                </option>
                                                                            @endforeach
                                                                        </select>
                                                                    </div>
                                                                </div>

                                                                <div class="uk-modal-footer uk-text-right">
                                                                    <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                                                                    <button type="button" onclick="add_interview_person({{ $company->id }})"
                                                                            class="md-btn md-btn-flat md-btn-flat-primary uk-modal-close">Submit
                                                                    </button>
                                                                </div>
                                                            </form>
                                                        </div>
                                                    </div>

                                                    <div class="md-card-content">
                                                        <?php $interview_person = \App\Models\Interview::where('interview_status', '=', '6')->where('sec_employed', '>',0)->where('interview_date', $code->interview_date)->get(); ?>
                                                        @if(count($interview_person) > 0 )
                                                            <table cellspacing="0" width="100%">
                                                                <thead>
                                                                <tr>
                                                                    <th style="padding-bottom: 10px;">Name</th>
                                                                    <th style="padding-bottom: 10px;">Interview Job Position</th>
                                                                    <th style="padding-bottom: 10px;">Interview Time</th>
                                                                    <th style="padding-bottom: 10px;" class="section-to-hide">Contact Number</th>
                                                                    <th style="padding-bottom: 10px;" class="section-to-hide">Status</th>
                                                                    <th style="padding-bottom: 10px;" class="section-to-hide">Check By</th>
                                                                    <th style="padding-bottom: 10px;" class="section-to-hide">Action</th>
                                                                </tr>
                                                                </thead>

                                                                <tbody>
                                                                @foreach($interview_person as $interview)

                                                                    <tr id="tr-{{ $interview->id }}">
                                                                        <td style="padding-bottom: 15px;width: 15%">
                                                                            {{ \App\Models\ApplicantInformation::where('cv_no',$interview->cv_id)->pluck('applicant_name') }}
                                                                        </td>

                                                                        <td style="padding-bottom: 15px;">
                                                                <span data-uk-tooltip="{pos:'bottom'}"
                                                                      style="cursor: pointer;"
                                                                      title="Change Interview Position"
                                                                      data-uk-modal="{target:'#change_company_and_position{{ $interview->id }}'}">
                                                                    <?php $val = \App\Models\ApplicantJobPosition::where('id',$interview->interview_position)->where('cv_id',$interview->cv_id)->pluck('apply_position') ?>
                                                                    @if($val)
                                                                        {{ $val }}
                                                                    @else
                                                                        Click Here ! <br> To add Interview Job Position
                                                                    @endif
                                                                </span>
                                                                        </td>
                                                                        <td style="padding-bottom: 15px;"><span
                                                                                    data-uk-tooltip="{pos:'bottom'}"
                                                                                    style="cursor: pointer;"
                                                                                    title="Change Interview Time ( {{ \App\Models\ApplicantInformation::where('cv_no',$interview->cv_id)->pluck('applicant_name') }} )"
                                                                                    data-uk-modal="{target:'#change_time{{ $interview->id }}'}">
                                                                {{ date('F j, Y',strtotime($interview->interview_date)) }}
                                                                                &nbsp;{{ date('g:i a',strtotime($interview->interview_time)) }}</span>
                                                                        </td>
                                                                        <td style="padding-bottom: 15px;" class="section-to-hide">
                                                                            <?php  $contacts = \App\Models\ApplicantInformation::where('cv_no', $interview->cv_id)->pluck('contact_number') ?>
                                                                            {{ $contacts }}
                                                                        </td>
                                                                        <td style="padding-bottom: 15px;width: 10%" class="section-to-hide">
                                                                            @if($interview->interview_status == 1)
                                                                                <span class="uk-badge uk-badge-warning">Pending</span>
                                                                            @elseif($interview->interview_status == 2)
                                                                                <span class="uk-badge uk-badge-primary">Avaliable</span>
                                                                            @elseif($interview->interview_status == 3)
                                                                                <span class="uk-badge">Hold</span>
                                                                            @elseif($interview->interview_status == 4)
                                                                                <span class="uk-badge uk-badge-success">Employed</span>
                                                                            @elseif($interview->interview_status == 5)
                                                                                <span class="uk-badge uk-badge-danger">Rejected</span>
                                                                            @elseif($interview->interview_status == 6)
                                                                               
                                                                                @if($interview->sec_employed==2)
                                                                                    <span class="uk-badge uk-badge-success">Employed</span>
                                                                                    
                                                                                @else           
                                                                                    <span class="uk-badge">Second Interview</span>
                                                                                @endif
                                                                                
                                                                            @else
                                                                                -
                                                                            @endif
                                                                        </td>
                                                                        <td style="padding-bottom: 15px;" class="section-to-hide">
                                                                             <?php $user = SiteHelper::get_user_permission(@$interview->create_user_id) ?>
                                                                                {{ @$user->first_name }} {{ @$user->last_name }}             
                                                                        </td>
                                                                        <td style="padding-bottom: 15px;width: 21%" class="section-to-hide">
                                                                            <a href="#" class="md-btn md-btn-small"
                                                                               data-uk-tooltip="{pos:'bottom'}"
                                                                               title="More Detail"
                                                                               data-uk-modal="{target:'#more_detail{{ $interview->id }}'}"><i
                                                                                        class="ion-ios-help-outline"
                                                                                        style="font-size: 20px"></i></a>
                                                                            <a href="#" class="md-btn md-btn-small"
                                                                               data-uk-tooltip="{pos:'bottom'}"
                                                                               title="Change Status"
                                                                               data-uk-modal="{target:'#change_status{{ $interview->id }}'}"><i
                                                                                        class="ion-ios-compose-outline"
                                                                                        style="font-size: 20px"></i></a>
                                                                            <a href="#"
                                                                               onclick="deleteInterview({{ $interview->id }})"
                                                                               class="md-btn md-btn-small md-btn-danger"
                                                                               data-uk-tooltip="{pos:'bottom'}"
                                                                               title="Remove"><i
                                                                                        class="ion-ios-trash-outline"
                                                                                        style="font-size: 20px"></i></a>
                                                                        </td>
                                                                    </tr>
                                                                    <div class="uk-modal"
                                                                         id="more_detail{{ $interview->id }}">
                                                                        <div class="uk-modal-dialog" style="width: 780px">
                                                                            <button type="button"
                                                                                    class="uk-modal-close uk-close"></button>
                                                                            <?php $applicant_information = \App\Models\ApplicantInformation::where('cv_no', $interview->cv_id)->first() ?>
                                                                            <div class="uk-modal-header">
                                                                                <h3 class="uk-modal-title"><i
                                                                                            class="material-icons">description</i>&nbsp;&nbsp;Resume
                                                                                    Nos
                                                                                    : {{ $applicant_information->cv_no }}
                                                                                </h3>
                                                                            </div>
                                                                            <div class="uk-grid">
                                                                                <div class="uk-width-small-2-5">
                                                                                    @if($applicant_information->cv_image)
                                                                                        <img style="height: 200px;width: 100%;border-radius: 10px;padding: 14px 14px 0px 14px;"
                                                                                             src="{{ $applicant_information->cv_image }}"
                                                                                             alt="">
                                                                                    @else
                                                                                        <img style="height: 200px;width: 100%;border-radius: 10px;padding: 14px 14px 0px 14px;"
                                                                                             src="http://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&amp;f=y"
                                                                                             alt="">
                                                                                    @endif
                                                                                </div>
                                                                                <div class="uk-width-small-3-5">
                                                                                    <div class="md-card-toolbar">
                                                                                        <h3 class="md-card-toolbar-heading-text">
                                                                                            Contact Information
                                                                                        </h3>
                                                                                    </div>
                                                                                    <ul class="md-list"
                                                                                        style="text-align: left">
                                                                                        <li>
                                                                                            <div class="md-list-content">
                                                                                                <span class="md-list-heading">Contact Number </span>
                                                    <span class="uk-text-small uk-text-muted">{{ $applicant_information->contact_number }}
                                                        , {{ $applicant_information->sec_contact }}</span>
                                                                                            </div>
                                                                                        </li>
                                                                                        <li>
                                                                                            <div class="md-list-content">
                                                                                                <span class="md-list-heading">Email Address</span>
                                                                                                <span class="uk-text-small uk-text-muted">{{ $applicant_information->email }}</span>
                                                                                            </div>
                                                                                        </li>
                                                                                        <li>
                                                                                            <div class="md-list-content">
                                                                                                <span class="md-list-heading">Address</span>
                                                                                                <span class="uk-text-small uk-text-muted">{{ $applicant_information->address }}</span>
                                                                                            </div>
                                                                                        </li>
                                                                                    </ul>
                                                                                </div>
                                                                            </div>

                                                                            <div class="md-card-toolbar">
                                                                                <h3 class="md-card-toolbar-heading-text">
                                                                                    Personal Information
                                                                                </h3>
                                                                            </div>
                                                                            <div class="md-card-content">
                                                                                <div class="uk-grid">
                                                                                    <div class="uk-width-medium-1-3">
                                                                                        <ul class="md-list"
                                                                                            style="text-align: left">
                                                                                            <li>
                                                                                                <div class="md-list-content">
                                                                                                    <span class="md-list-heading">Name</span>
                                                                                                    <span class="uk-text-small uk-text-muted">{{ $applicant_information->applicant_name }}</span>
                                                                                                </div>
                                                                                            </li>
                                                                                            <li>
                                                                                                <div class="md-list-content">
                                                                                                    <span class="md-list-heading">NRC No</span>
                                                                                                    <span class="uk-text-small uk-text-muted">{{ $applicant_information->nrc }}</span>
                                                                                                </div>
                                                                                            </li>
                                                                                            <li>
                                                                                                <div class="md-list-content">
                                                                                                    <span class="md-list-heading">Date of Birth</span>
                                                                                                    <span class="uk-text-small uk-text-muted">{{ date('F d, Y',strtotime($applicant_information->dob))}}</span>
                                                                                                </div>
                                                                                            </li>
                                                                                            <li>
                                                                                                <div class="md-list-content">
                                                                                                    <span class="md-list-heading">Weight</span>
                                                    <span class="uk-text-small uk-text-muted">{{ $applicant_information->weight }}
                                                        lbs</span>
                                                                                                </div>
                                                                                            </li>
                                                                                            <li>
                                                                                                <div class="md-list-content">
                                                                                                    <span class="md-list-heading">Current Position</span>
                                                                                                    <span class="uk-text-small uk-text-muted">{{ $applicant_information->curr_position }}</span>
                                                                                                </div>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                    <div class="uk-width-medium-1-3">
                                                                                        <ul class="md-list"
                                                                                            style="text-align: left">
                                                                                            <li>
                                                                                                <div class="md-list-content">
                                                                                                    <span class="md-list-heading">Father Name</span>
                                                                                                    <span class="uk-text-small uk-text-muted">{{ $applicant_information->father_name }}</span>
                                                                                                </div>
                                                                                            </li>
                                                                                            <li>
                                                                                                <div class="md-list-content">
                                                                                                    <span class="md-list-heading">Religion</span>
                                                                                                    <span class="uk-text-small uk-text-muted">{{ $applicant_information->religion }}</span>
                                                                                                </div>
                                                                                            </li>
                                                                                            <li>
                                                                                                <div class="md-list-content">
                                                                                                    <span class="md-list-heading">Gender</span>
                                                                                                    @if($applicant_information->gender == 1)
                                                                                                        <span class="uk-text-small uk-text-muted">Male</span>
                                                                                                    @else
                                                                                                        <span class="uk-text-small uk-text-muted">Female</span>
                                                                                                    @endif
                                                                                                </div>
                                                                                            </li>
                                                                                            <li>
                                                                                                <div class="md-list-content">
                                                                                                    <span class="md-list-heading">Height</span>
                                                                                                    <span class="uk-text-small uk-text-muted">{{ $applicant_information->height }}</span>
                                                                                                </div>
                                                                                            </li>
                                                                                            <li>
                                                                                                <div class="md-list-content">
                                                                                                    <span class="md-list-heading">Current Company</span>
                                                                                                    <span class="uk-text-small uk-text-muted">{{ $applicant_information->curr_company }}</span>
                                                                                                </div>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                    <div class="uk-width-medium-1-3">
                                                                                        <ul class="md-list"
                                                                                            style="text-align: left">
                                                                                            <li>
                                                                                                <div class="md-list-content">
                                                                                                    <span class="md-list-heading">Nationality</span>
                                                                                                    <span class="uk-text-small uk-text-muted">{{ $applicant_information->nationality }}</span>
                                                                                                </div>
                                                                                            </li>
                                                                                            <li>
                                                                                                <div class="md-list-content">
                                                                                                    <span class="md-list-heading">Martial Status</span>
                                                                                                    @if($applicant_information->martial_status == 1)
                                                                                                        <span class="uk-text-small uk-text-muted">Married</span>
                                                                                                    @else
                                                                                                        <span class="uk-text-small uk-text-muted">Single</span>
                                                                                                    @endif
                                                                                                </div>
                                                                                            </li>
                                                                                            <li>
                                                                                                <div class="md-list-content">
                                                                                                    <span class="md-list-heading">Current Location</span>
                                                                                                    <span class="uk-text-small uk-text-muted">{{ \App\Models\Country::where('id',$applicant_information->curr_location)->pluck('country_name') }}</span>
                                                                                                </div>
                                                                                            </li>
                                                                                            <li>
                                                                                                <div class="md-list-content">
                                                                                                    <span class="md-list-heading">Salary Expectations</span>
                                                    <span class="uk-text-small uk-text-muted">{{ $applicant_information->salary_exp }}
                                                        MMK</span>
                                                                                                </div>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="uk-grid">
                                                                                    <div class="uk-width-medium-1-1">
                                                                                        <ul class="md-list"
                                                                                            style="text-align: left">
                                                                                            <li>
                                                                                                <div class="md-list-content">
                                                                                                    <span class="md-list-heading">Noticed Period</span>
                                                                                                    <span class="uk-text-small uk-text-muted">{{ $applicant_information->not_period }}</span>
                                                                                                </div>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="md-card-toolbar">
                                                                                <h3 class="md-card-toolbar-heading-text">
                                                                                    Career Objective
                                                                                </h3>
                                                                            </div>
                                                                            <div class="md-card-content">
                                                                                <span class="md-list-heading">{{ $applicant_information->c_objective }}</span>
                                                                            </div>

                                                                            <div class="md-card-toolbar">
                                                                                <h3 class="md-card-toolbar-heading-text">
                                                                                    Apply Job Position
                                                                                </h3>
                                                                            </div>
                                                                            <div class="md-card-content">
                                                                                <?php $applicant_apply_position = SiteHelper::get_applicant_apply_position($applicant_information->cv_no) ?>

                                                                                @foreach($applicant_apply_position as $job)


                                                                                    <p class="">
                                                                                        <b>Job Category</b>
                                                                                        - {{ \App\Models\Job_Category::where('id',$job->job_category)->pluck('description') }}
                                                                                    </p>
                                                                                    <p class="">
                                                                                        <b>Job Industry</b>
                                                                                        - {{ \App\Models\Job_Industry::where('id',$job->job_industry)->pluck('industry_description') }}
                                                                                    </p>
                                                                                    <p class="">
                                                                                        <b>Apply position</b>
                                                                                        - {{ $job->apply_position }}
                                                                                    </p>
                                                                                    <p class="">
                                                                                        <b>Job Type</b>
                                                                                        - {{ \App\Models\JobType::where('id',$job->job_type)->pluck('type') }}
                                                                                    </p>
                                                                                    <p class="">
                                                                                        <b>Job Location</b>
                                                                                        - @if($job->location == 1)
                                                                                            Local @else Oversea @endif
                                                                                    </p>

                                                                                    <br>
                                                                                @endforeach

                                                                            </div>

                                                                            <div class="md-card-toolbar">
                                                                                <h3 class="md-card-toolbar-heading-text">
                                                                                    Working Experience
                                                                                </h3>
                                                                            </div>
                                                                            <div class="md-card-content">
                                                                                <?php $applicant_work_exp = SiteHelper::get_applicant_work_exp($applicant_information->cv_no) ?>

                                                                                @foreach($applicant_work_exp as $work)

                                                                                    <p class="">
                                                                                        <b>Company Name</b>
                                                                                        - {{ $work->exp_company_name }}
                                                                                    </p>
                                                                                    <p class="">
                                                                                        <b>Job Title</b>
                                                                                        - {{ $work->exp_job_title }}
                                                                                    </p>
                                                                                    <p class="">
                                                                                        <b>Job Category</b>
                                                                                        - {{ \App\Models\Job_Category::where('id',$work->exp_job_category)->pluck('description') }}
                                                                                    </p>
                                                                                    <p class="">
                                                                                        <b>Job Industry</b>
                                                                                        - {{ \App\Models\Job_Industry::where('id',$work->exp_job_industry)->pluck('industry_description') }}
                                                                                    </p>
                                                                                    <p class="">
                                                                                        <b>Place</b>
                                                                                        - {{ $work->exp_job_city }}
                                                                                        , {{ \App\Models\Country::where('id',$work->exp_job_country )->pluck('country_name') }}
                                                                                    </p>
                                                                                    <p class="">
                                                                                        <b>Worked Period</b>
                                                                                        - {{ $work->exp_from_date }}
                                                                                        &nbsp;to&nbsp; {{ $work->exp_to_date }}
                                                                                    </p>
                                                                                    <p class="">
                                                                                        <b>Description</b>
                                                                                        - {{ $work->exp_description }}
                                                                                    </p>
                                                                                    <br>
                                                                                @endforeach

                                                                            </div>

                                                                            <div class="md-card-toolbar">
                                                                                <h3 class="md-card-toolbar-heading-text">
                                                                                    Educational Background
                                                                                </h3>
                                                                            </div>
                                                                            <div class="md-card-content">
                                                                                <?php $edu_back = SiteHelper::get_applicant_education($applicant_information->cv_no) ?>

                                                                                @foreach($edu_back as $edu)

                                                                                    <p class="">
                                                                                        <b> {{ $edu->uni_name }}
                                                                                            ,{{ \App\Models\University::where('id',$edu->degree_lvl)->pluck('degree_name') }}
                                                                                            ,{{ $edu->major_name }}</b>
                                                                                    </p>
                                                                                    <p>
                                                                                        {{ \App\Models\Country::where('id',$edu->uni_country)->pluck('country_name') }}
                                                                                    </p>
                                                                                    <p>
                                                                                        From - {{ $edu->uni_start_date }}
                                                                                        to {{ $edu->uni_graduation_date }}
                                                                                    </p>
                                                                                    <br>
                                                                                @endforeach

                                                                            </div>

                                                                            <div class="md-card-toolbar">
                                                                                <h3 class="md-card-toolbar-heading-text">
                                                                                    Other Qualifications
                                                                                </h3>
                                                                            </div>
                                                                            <div class="md-card-content">
                                                                                <?php $other_qualification = SiteHelper::get_applicant_other_qualification($applicant_information->cv_no) ?>
                                                                                @foreach($other_qualification as $other)
                                                                                    <p class="">
                                                                                        <b> {{ $other->certi_name }} </b>
                                                                                    </p>
                                                                                    <p>
                                                                                        {{ $other->training_center }}
                                                                                        ,{{ \App\Models\Country::where('id',$other->certi_country)->pluck('country_name') }}
                                                                                    </p>
                                                                                    <p>
                                                                                        {{ $other->certi_date }}
                                                                                    </p>
                                                                                    <br>
                                                                                @endforeach
                                                                            </div>

                                                                            <div class="md-card-toolbar">
                                                                                <h3 class="md-card-toolbar-heading-text">
                                                                                    IT Skill
                                                                                </h3>
                                                                            </div>
                                                                            <div class="md-card-content">
                                                                                <?php $it_skill = SiteHelper::get_applicant_it_skill($applicant_information->cv_no) ?>
                                                                                @foreach($it_skill as $it)
                                                                                    <p class="">
                                                                                        <b> {{ $it->topic_app_name }} </b>
                                                                                    </p>
                                                                                    <p>
                                                                                        {{ \App\Models\IT::where('id',$it->it_type)->pluck('type_name') }}
                                                                                    </p>
                                                                                    <p>
                                                                                        {{ \Illuminate\Support\Facades\DB::table('it_lvl')->where('id',$it->it_skill)->pluck('it_lvl_name')  }}
                                                                                    </p>
                                                                                    <br>
                                                                                @endforeach
                                                                            </div>

                                                                            <div class="md-card-toolbar">
                                                                                <h3 class="md-card-toolbar-heading-text">
                                                                                    Language Skill
                                                                                </h3>
                                                                            </div>
                                                                            <div class="md-card-content">
                                                                                <?php $lang_skill = SiteHelper::get_applicant_lang_skill($applicant_information->cv_no) ?>
                                                                                @foreach($lang_skill as $lang)
                                                                                    <p class="">
                                                                                        <b>  {{ \App\Models\Language::where('id', $lang->language_name )->pluck('lang_name')}} </b>
                                                                                    </p>
                                                                                    <p class="">
                                                                                        {{ \Illuminate\Support\Facades\DB::table('language_lavel')->where('id',$lang->language_skill)->pluck('lang_lvl')  }}
                                                                                    </p>
                                                                                    <br>
                                                                                @endforeach
                                                                            </div>

                                                                            <div class="md-card-toolbar">
                                                                                <h3 class="md-card-toolbar-heading-text">
                                                                                    Reference Person
                                                                                </h3>
                                                                            </div>
                                                                            <div class="md-card-content">
                                                                                <?php $ref_person = SiteHelper::get_applicant_ref_person($applicant_information->cv_no) ?>
                                                                                @foreach($ref_person as $ref)

                                                                                    <p class="">
                                                                                        <b>Name</b>
                                                                                        - {{ $ref->reference_person_name }}
                                                                                    </p>
                                                                                    <p class="">
                                                                                        <b>Company</b>
                                                                                        - {{ $ref->reference_person_company }}
                                                                                    </p>
                                                                                    <p class="">
                                                                                        <b>Position</b>
                                                                                        - {{ $ref->reference_person_position }}
                                                                                    </p>
                                                                                    <p class="">
                                                                                        <b>Mobile No</b>
                                                                                        - {{ $ref->reference_person_mobile }}
                                                                                    </p>
                                                                                    <p class="">
                                                                                        <b>Email</b>
                                                                                        - {{ $ref->reference_person_email }}

                                                                                        <br>
                                                                                @endforeach
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="uk-modal" style="z-index: 998"
                                                                         id="emp_record{{ $interview->id }}">
                                                                        <div class="uk-modal-dialog">
                                                                            <div class="uk-modal-header">
                                                                                <h3 class="uk-modal-title">Employee
                                                                                    Record</h3>
                                                                            </div>
                                                                            <form id="emp_record_data{{ $interview->id  }}">
                                                                                {!! csrf_field() !!}

                                                                                <div class="uk-grid">
                                                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                                                        <p><b> Name </b>
                                                                                            : {{ \App\Models\ApplicantInformation::where('cv_no',$interview->cv_id)->pluck('applicant_name') }}
                                                                                        </p>
                                                                                    </div>
                                                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                                                        <p><b> Resume Nos </b>
                                                                                            : {{ $interview->cv_id }}</p>
                                                                                    </div>
                                                                                </div>
                                                                                <hr>
                                                                                <div class="uk-grid">
                                                                                    <div class="uk-width-medium-1-1 parsley-row">

                                                                                        <input type="hidden"
                                                                                               name="interview_person"
                                                                                               value="{{ $interview->cv_id }}">
                                                                                        <input type="hidden"
                                                                                               name="interview_status"
                                                                                               value="4">
                                                                                        <input type="hidden"
                                                                                               name="company_code"
                                                                                               value="{{ $interview->company_code }}">
                                                                                        <input type="hidden"
                                                                                               name="job_position"
                                                                                               value="{{ $interview->interview_position }}">

                                                                                        <p><b>Job Position</b>
                                                                                            : {{ \App\Models\ApplicantJobPosition::where('cv_id', $interview->cv_id)->where('id',@$interview->interview_position)->pluck('apply_position') }}
                                                                                        </p>

                                                                                        <p><b>Exp - Salary </b>
                                                                                            : {{ \App\Models\ApplicantInformation::where('cv_no',@$interview->cv_id)->pluck('salary_exp') }}
                                                                                        </p>
                                                                                        <p><b> Name Of Company </b>
                                                                                            : {{ \App\Models\Client::where('company_code',@$interview->company_code)->pluck('company_name') }}
                                                                                        </p>
                                                                                        <?php $industry = \App\Models\Client::where('company_code', @$interview->company_code)->first() ?>
                                                                                        <p><b> Type Of Company </b>
                                                                                            : {{ \App\Models\Job_Industry::where('id',@$industry->industry)->pluck('industry_description') }}
                                                                                        </p>
                                                                                        <p><b> Company Address</b>
                                                                                            : {{ \App\Models\Client::where('company_code',@$interview->company_code)->pluck('address') }}
                                                                                        </p>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="uk-grid">
                                                                                    <div class="uk-width-medium-1-2">
                                                                                        <label for="salary{{ $interview->id  }}">
                                                                                            Salary
                                                                                        </label>
                                                                                        <input type="text" name="salary"
                                                                                               id="salary{{ $interview->id  }}"
                                                                                               class="md-input"/>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="uk-grid">
                                                                                    <div class="uk-width-medium-1-2">
                                                                                        <label for="received{{ $interview->id  }}">
                                                                                            Job Received Date
                                                                                <span class="req">
                                                                                    *
                                                                                </span>
                                                                                        </label>
                                                                                        <input type="text"
                                                                                               name="job_received_date"
                                                                                               id="received{{ $interview->id  }}"
                                                                                               class="md-input"
                                                                                               data-parsley-date-message="This value should be a valid date"
                                                                                               data-uk-datepicker="{format:'YYYY-MM-DD'}"/>
                                                                                    </div>
                                                                                    <div class="uk-width-medium-1-2">
                                                                                        <label for="joining{{ $interview->id  }}">
                                                                                            Joining Date
                                                                                <span class="req">
                                                                                    *
                                                                                </span>
                                                                                        </label>
                                                                                        <input type="text"
                                                                                               name="joining_date"
                                                                                               id="joining{{ $interview->id  }}"
                                                                                               class="md-input"
                                                                                               data-parsley-date-message="This value should be a valid date"
                                                                                               data-uk-datepicker="{format:'YYYY-MM-DD'}"/>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="uk-grid">
                                                                                    <div class="uk-width-medium-1-2">
                                                                                        <label for="invoicing{{ $interview->id  }}">
                                                                                            Invoicing Date
                                                                                <span class="req">
                                                                                    *
                                                                                </span>
                                                                                        </label>
                                                                                        <input type="text"
                                                                                               name="invoicing_date"
                                                                                               id="invoicing{{ $interview->id  }}"
                                                                                               class="md-input"
                                                                                               data-parsley-date-message="This value should be a valid date"
                                                                                               data-uk-datepicker="{format:'YYYY-MM-DD'}"/>
                                                                                    </div>
                                                                                    <div class="uk-width-medium-1-2">
                                                                                        <label for="payment{{ $interview->id  }}">
                                                                                            Payment Date
                                                                                <span class="req">
                                                                                    *
                                                                                </span>
                                                                                        </label>
                                                                                        <input type="text"
                                                                                               name="payment_date"
                                                                                               id="payment{{ $interview->id  }}"
                                                                                               class="md-input"
                                                                                               data-parsley-date-message="This value should be a valid date"
                                                                                               data-uk-datepicker="{format:'YYYY-MM-DD'}"/>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="uk-grid">
                                                                                    <div class="uk-width-medium-1-2">
                                                                                        <label for="company">
                                                                                            Company Contact Person
                                                                                        </label>
                                                                                        <input type="text"
                                                                                               name="company_contact_person"
                                                                                               id="company{{ $interview->id  }}"
                                                                                               class="md-input" value="{{ \App\Models\Client::where('company_code',$interview->company_code)->pluck('contact_person') }}"/>
                                                                                    </div>
                                                                                    <div class="uk-width-medium-1-2">
                                                                                        <label for="oway">
                                                                                            Oway Contact person
                                                                                        </label>
                                                                                        <input type="text"
                                                                                               name="oway_contact_person"
                                                                                               id="oway{{ $interview->id  }}"
                                                                                               class="md-input" value="{{ \Sentinel::check()->first_name }} {{ \Sentinel::check()->last_name }}"/>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="uk-grid">
                                                                                    <div class="uk-width-medium-1-2">
                                                                                        <label for="email">
                                                                                            Company Email
                                                                                        </label>
                                                                                        <input type="email"
                                                                                               name="company_email"
                                                                                               id="email{{ $interview->id  }}"
                                                                                               class="md-input" value="{{ \App\Models\Client::where('company_code',$interview->company_code)->pluck('email') }}" />
                                                                                    </div>
                                                                                    <div class="uk-width-medium-1-2">
                                                                                        <label for="amount{{ $interview->id  }}">
                                                                                            Actual Amount
                                                                                        </label>
                                                                                        <input type="text"
                                                                                               name="actual_amount"
                                                                                               id="amount{{ $interview->id  }}"
                                                                                               class="md-input"/>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="uk-modal-footer uk-text-right">
                                                                                    <button type="button"
                                                                                            class="md-btn md-btn-flat uk-modal-close">
                                                                                        Close
                                                                                    </button>
                                                                                    <button type="button"
                                                                                            onclick="add_employee_record({{ $interview->id }})"
                                                                                            class="md-btn md-btn-flat md-btn-flat-primary uk-modal-close">
                                                                                        Submit
                                                                                    </button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>

                                                                    <div class="uk-modal" style="z-index: 998"
                                                                         id="change_company_and_position{{ $interview->id }}">
                                                                        <div class="uk-modal-dialog">
                                                                            <div class="uk-modal-header">
                                                                                <h3 class="uk-modal-title">Change Interview Position</h3>
                                                                            </div>
                                                                            <form id="interview_company_data{{ $interview->id  }}">

                                                                                <div class="uk-grid">
                                                                                    <div class="uk-width-medium-1-1">
                                                                                        <?php $app_jobs = \App\Models\ApplicantJobPosition::where('cv_id', $interview->cv_id)->get() ?>
                                                                                        <select id="select_demo_1"
                                                                                                data-md-selectize
                                                                                                name="apply_job"
                                                                                                data-placeholder="Choose Job Position">
                                                                                            <option value="">Choose Job
                                                                                                Position
                                                                                            </option>
                                                                                            @foreach($app_jobs as $row)
                                                                                                <option @if($interview->interview_position == $row->id) selected
                                                                                                        @endif value="{{ $row->id }}">
                                                                                                    {{ $row->apply_position }}
                                                                                                </option>
                                                                                            @endforeach
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="uk-modal-footer uk-text-right">
                                                                                    <button type="button"
                                                                                            class="md-btn md-btn-flat uk-modal-close">
                                                                                        Close
                                                                                    </button>
                                                                                    <button type="button"
                                                                                            onclick="change_company_position({{$interview->id}})"
                                                                                            class="md-btn md-btn-flat md-btn-flat-primary uk-modal-close">
                                                                                        Submit
                                                                                    </button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>

                                                                    <div class="uk-modal" style="z-index: 998"
                                                                         id="change_time{{ $interview->id }}">
                                                                        <div class="uk-modal-dialog">
                                                                            <div class="uk-modal-header">
                                                                                <h3 class="uk-modal-title">Change Interview
                                                                                    Time</h3>
                                                                            </div>
                                                                            <form id="interview_time{{ $interview->id }}">
                                                                                <div class="uk-grid">
                                                                                    <div class="uk-width-medium-1-1 parsley-row">
                                                                                        <input id="cv_interview_date{{ $interview->id }}"
                                                                                               name="interview_time" type="text"
                                                                                               value=""
                                                                                               class="uk-form-width-medium"/>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="uk-modal-footer uk-text-right">
                                                                                    <button type="button"
                                                                                            class="md-btn md-btn-flat uk-modal-close">
                                                                                        Close
                                                                                    </button>
                                                                                    <button type="button"
                                                                                            onclick="change_time({{ $interview->id }})"
                                                                                            class="md-btn md-btn-flat md-btn-flat-primary uk-modal-close">
                                                                                        Submit
                                                                                    </button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>

                                                                    <div class="uk-modal" style="z-index: 998"
                                                                         id="change_status{{ $interview->id }}">
                                                                        <div class="uk-modal-dialog">
                                                                            <div class="uk-modal-header">
                                                                                <h3 class="uk-modal-title">Change Interview
                                                                                    Status</h3>
                                                                            </div>
                                                                            <form id="interview_status{{ $interview->id }}">
                                                                                <div class="uk-grid">
                                                                                    <div class="uk-width-medium-1-1 parsley-row">
                                                                                        <?php $language_lvl = \App\Models\Language::language_lvl() ?>
                                                                                        <select id="select_demo_1"
                                                                                                data-md-selectize
                                                                                                name="interview_status">
                                                                                            <option value="">Interview
                                                                                                Status
                                                                                            </option>
                                                                                            <option @if($interview->interview_status == 1) selected
                                                                                                    @endif value="1">Pending
                                                                                            </option>
                                                                                            <option @if($interview->interview_status == 2) selected
                                                                                                    @endif value="2">
                                                                                                Avaliable
                                                                                            </option>
                                                                                            <option @if($interview->interview_status == 3) selected
                                                                                                    @endif value="3">Hold
                                                                                            </option>
                                                                                            <option @if($interview->interview_status == 4) selected
                                                                                                    @endif value="4">
                                                                                                Employed
                                                                                            </option>
                                                                                            <option @if($interview->interview_status == 5) selected
                                                                                                    @endif value="5">
                                                                                                Rejected
                                                                                            </option>
                                                                                            <option @if($interview->interview_status == 6) selected
                                                                                                    @endif value="6">Second
                                                                                                Interview
                                                                                            </option>
                                                                                        </select>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="uk-modal-footer uk-text-right">
                                                                                    <button type="button"
                                                                                            class="md-btn md-btn-flat uk-modal-close">
                                                                                        Close
                                                                                    </button>
                                                                                    <button type="button"
                                                                                            onclick="change_status({{ $interview->id }})"
                                                                                            class="md-btn md-btn-flat md-btn-flat-primary uk-modal-close">
                                                                                        Submit
                                                                                    </button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>

                                                                    <div class="uk-modal" style="z-index: 998"
                                                                         id="second_interview_status{{ $interview->id }}">
                                                                        <div class="uk-modal-dialog">
                                                                            <div class="uk-modal-header">
                                                                                <h3 class="uk-modal-title">Second Interview
                                                                                    Date</h3>
                                                                            </div>
                                                                            <form id="sec_interview_status{{ $interview->id }}">
                                                                                <div class="uk-grid">
                                                                                    <div class="uk-width-medium-1-1 parsley-row">
                                                                                        <input type="hidden" name="interview_status" value="6">
                                                                                        <input id="second_interview_time{{ $interview->id }}"
                                                                                               name="interview_time"
                                                                                               type="text"
                                                                                               value=""
                                                                                               class="uk-form-width-medium"/>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="uk-modal-footer uk-text-right">
                                                                                    <button type="button"
                                                                                            class="md-btn md-btn-flat uk-modal-close">
                                                                                        Close
                                                                                    </button>
                                                                                    <button type="button"
                                                                                            onclick="second_interview({{ $interview->id }})"
                                                                                            class="md-btn md-btn-flat md-btn-flat-primary uk-modal-close">
                                                                                        Submit
                                                                                    </button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                @endforeach
                                                                </tbody>
                                                            </table>
                                                                @if($company->remark)
                                                                    <div class="uk-grid">
                                                                        <div class="uk-width-1-1">
                                                                            <div class="md-card-toolbar"
                                                                                 id="remark{{ $company->id }}"
                                                                                 style="border-bottom:none">
                                                                                <div class="md-card-toolbar-actions section-to-hide">
                                                                                    <a href="#"
                                                                                       data-uk-tooltip="{pos:'bottom'}"
                                                                                       style="cursor: pointer;"
                                                                                       title="Edit Remark"
                                                                                       data-uk-modal="{target:'#edit_remark{{ $company->id }}'}"><i
                                                                                                class="md-icon material-icons">edit</i></a>

                                                                                    <i data-uk-tooltip="{pos:'bottom'}"
                                                                                       style="cursor: pointer;"
                                                                                       title="Remove Remark"
                                                                                       class="md-icon material-icons"
                                                                                       onclick="del_remark({{ $company->id }})">
                                                                                        cancel</i>
                                                                                </div>
                                                                                <p class="uk-text-danger uk-margin-remove">
                                                                                    Remark : {{ @$company->remark }}</p>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                @endif
                                                        @else
                                                            No Interview Person
                                                        @endif
                                                    </div>
                                                </div>
                                            </div>

                                        @endforeach
                                    </div>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>

            </div>

            <div class="uk-grid">
                <div class="uk-width-medium-1-1">
                    {!! $interview_code->render() !!}
                </div>
            </div>

            <div class="md-fab-wrapper">
                <a class="md-fab md-fab-primary"
                   data-uk-modal="{target : '#new_interview_person'}"
                   data-uk-tooltip="{pos:'left'}"
                   title="Add Interview Person" href="#">
                    <i class="material-icons">&#xE145;</i>
                </a>
            </div>

            <div class="uk-modal" style="z-index: 998" id="new_interview_person">
                <div class="uk-modal-dialog">
                    <div class="uk-modal-header">
                        <h3 class="uk-modal-title">Interview Person</h3>
                    </div>
                    <form id="interview_data">
                        <div class="uk-grid">
                            <div class="uk-width-medium-1-1 parsley-row">
                                <?php $applicant = \App\Models\ApplicantInformation::all() ?>
                                <select id="interview_person" name="interview_person" data-placeholder="Select CV"
                                        multiple>
                                    @foreach($applicant as $row)
                                        <option value="{{ $row->cv_no }}">
                                            {{ $row->applicant_name }} ( {{ $row->cv_no }} )
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="uk-grid">
                            <div class="uk-width-medium-1-2 parsley-row">
                                <input id="interview_date" name="interview_date" placeholder="Choose Interview Date"/>
                            </div>
                            <div class="uk-width-medium-1-2 parsley-row">
                                <?php $client = \App\Models\Client::all() ?>

                                <select id="select_demo_1"
                                        data-md-selectize
                                        name="company_name" data-placeholder="Select Company">
                                    <option value="">Choose Company
                                    </option>
                                    @foreach($client as $row)
                                        <option value="{{ $row->company_code }}">
                                            {{ $row->company_name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="uk-modal-footer uk-text-right">
                            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                            <button type="button" onclick="add_interview()"
                                    class="md-btn md-btn-flat md-btn-flat-primary uk-modal-close">Submit
                            </button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>
    
    <script>
        function deleteInterview(id) {

            UIkit.modal.confirm(
                    'Are you sure?',
                    function () {

                        (function (modal) {
                            modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'>Wait <br/><img class=\'uk-margin-top\' src=\'{{ url('backend/img/spinners/spinner.gif') }}\' alt=\'\'>');
                            setTimeout(function () {
                                $.ajax({
                                    url: '{{ url('/interview') }}' + '/' + id,
                                    type: 'DELETE',
                                    data: '',
                                    success: function (msg) {
                                        modal.hide();
                                        UIkit.notify({
                                            message: 'Successfully removed.',
                                            status: 'success',
                                            timeout: 2000,
                                            pos: 'top-right',
                                            onClose: function () {
                                                $('#tr-' + id).hide('slow').remove();
                                            }
                                        });
                                    },
                                    error: function (data) {
                                        modal.hide();
                                        UIkit.notify({
                                            message: 'Something went wrong.',
                                            status: 'danger',
                                            timeout: 3000,
                                            pos: 'top-right',
                                            onClose: function () {
                                                location.reload();
                                            }
                                        });
                                    }
                                });
                            }, 5000)
                        })();
                    });
        }

        function add_interview() {

            altair_helpers.content_preloader_show();
            $contact_data = $('#interview_data');
            var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);

            $.ajax({
                url: '{{ url('interview') }}',
                type: 'post',
                data: {
                    'interview_data': $contact_data.serializeObject(),
                },
                success: function (data, textStatus, jQxhr) {
                    UIkit.notify({
                        message: 'Sucessfully Added Interview Person',
                        status: 'success',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                            location.reload();
                        }
                    });
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    UIkit.notify({
                        message: 'Something went wrong.',
                        status: 'danger',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                        }
                    });
                }
            });
        }

        function second_interview(id){

            altair_helpers.content_preloader_show();
            var time = $('#second_interview_time' + id).val();
            $contact_data = $('#sec_interview_status' + id);
            var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);

            $.ajax({
                url: '{{ url('/interview') }}' + '/' + id,
                type: 'PUT',
                data: {
                    'interview_edit_data': $contact_data.serializeObject(),
                    'interview_date' : time,
                },
                success: function (data, textStatus, jQxhr) {
                    UIkit.notify({
                        message: 'Sucessfully Updated Interview Status.',
                        status: 'success',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                            location.reload();
                        }
                    });
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    UIkit.notify({
                        message: 'Something went wrong.',
                        status: 'danger',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                        }
                    });
                }
            });
        }

        function change_status(id) {
            altair_helpers.content_preloader_show();
            var modal = UIkit.modal('#emp_record' + id);
            var sec_modal = UIkit.modal('#second_interview_status' + id );
            $contact_data = $('#interview_status' + id);
            var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);

            if ($contact_data.serializeObject().interview_status == '4') {

                altair_helpers.content_preloader_hide();
                if (modal.isActive()) {
                    modal.hide();
                } else {
                    modal.show();
                }

            } else if ($contact_data.serializeObject().interview_status == '6') {

                altair_helpers.content_preloader_hide();
                if (sec_modal.isActive()) {
                    sec_modal.hide();
                } else {
                    sec_modal.show();
                }

            } else {
                $.ajax({
                    url: '{{ url('/interview') }}' + '/' + id,
                    type: 'PUT',
                    data: {
                        'interview_edit_data': $contact_data.serializeObject(),
                    },
                    success: function (data, textStatus, jQxhr) {
                        UIkit.notify({
                            message: 'Sucessfully Updated Interview Status.',
                            status: 'success',
                            timeout: 1000,
                            pos: 'top-right',
                            onClose: function () {
                                altair_helpers.content_preloader_hide();
                                location.reload();
                            }
                        });
                    },
                    error: function (jqXhr, textStatus, errorThrown) {
                        UIkit.notify({
                            message: 'Something went wrong.',
                            status: 'danger',
                            timeout: 1000,
                            pos: 'top-right',
                            onClose: function () {
                                altair_helpers.content_preloader_hide();
                            }
                        });
                    }
                });
            }
        }

        function change_time(id) {
            altair_helpers.content_preloader_show();

            var form_serialized = $('#edit_interview_time' + id).val();

            $.ajax({
                url: '{{ url('/interview/change_time') }}' + '/' + id,
                type: 'POST',
                data: {
                    'interview_edit_data': form_serialized,
                },
                success: function (data, textStatus, jQxhr) {
                    UIkit.notify({
                        message: 'Sucessfully Updated Interview Time.',
                        status: 'success',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                            location.reload();
                        }
                    });
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    UIkit.notify({
                        message: 'Something went wrong.',
                        status: 'danger',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                        }
                    });
                }
            });
        }

        function change_company_position(id) {
            altair_helpers.content_preloader_show();

            $contact_data = $('#interview_company_data' + id);
            var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);

            $.ajax({
                url: '{{ url('/interview/change_company_and_position') }}' + '/' + id,
                type: 'POST',
                data: {
                    'interview_company_data': $contact_data.serializeObject(),
                },
                success: function (data, textStatus, jQxhr) {
                    UIkit.notify({
                        message: 'Sucessfully Updated Interview Company And Position.',
                        status: 'success',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                            location.reload();
                        }
                    });
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    UIkit.notify({
                        message: 'Something went wrong.',
                        status: 'danger',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                        }
                    });
                }
            });
        }

        function add_interview_person(id){
            altair_helpers.content_preloader_show();
            $contact_data = $('#interview_user_data' + id);
            var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);

            $.ajax({
                url: '{{ url('interview/add_interview_person') }}',
                type: 'post',
                data: {
                    'interview_data': $contact_data.serializeObject(),
                },
                success: function (data, textStatus, jQxhr) {
                    UIkit.notify({
                        message: 'Sucessfully Added Interview Person',
                        status: 'success',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                            location.reload();
                        }
                    });
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    UIkit.notify({
                        message: 'Something went wrong.',
                        status: 'danger',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                        }
                    });
                }
            });
        }

        function add_employee_record(id) {
            altair_helpers.content_preloader_show();
            $contact_data = $('#emp_record_data' + id);

            var salarys = $('#salary' + id).val();
            var receives = $('#received' + id).val();
            var joinings = $('#joining' + id).val();
            var invoicings = $('#invoicing' + id).val();
            var payments = $('#payment' + id).val();
            var com_contacts = $('#company' + id).val();
            var oway_contacts = $('#oway' + id).val();
            var com_emails = $('#email' + id).val();
            var actual_amounts = $('#amount' + id).val();

            var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);

            $.ajax({
                url: '{{ url('/interview/employee_record') }}' + '/' + id,
                type: 'POST',
                data: {
                    'salary': salarys,
                    'receive': receives,
                    'joining': joinings,
                    'invoicing': invoicings,
                    'payment': payments,
                    'com_contact': com_contacts,
                    'oway_contact': oway_contacts,
                    'com_email': com_emails,
                    'actual_amount': actual_amounts,
                    'employee_record_data': $contact_data.serializeObject()
                },
                success: function (data, textStatus, jQxhr) { $('#show').html(data);
                    UIkit.notify({
                        message: 'Sucessfully Added Employee Record.',
                        status: 'success',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                            location.reload()
                        }
                    });
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    UIkit.notify({
                        message: 'Sucessfully Added Employee Record.',
                        status: 'success',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                            location.reload()
                        }
                    });
                }
            });
        }

        function print_div(id){
            UIkit.modal.confirm('Do you want to print this Interview Section?', function () {
                setTimeout(function () {
                    $('.section-to-hide').hide();
                    var printContents = document.getElementById('print'+id).innerHTML;
                    $('.section-to-hide').show();
                    var originalContents = document.body.innerHTML;
                    document.body.innerHTML = printContents;
                    window.print();
                    document.body.innerHTML = originalContents;
                }, 300)
            }, {
                labels: {
                    'Ok': 'print'
                }
            });
        }

        function del_interview_session(id){
            UIkit.modal.confirm('Do you want to delete this Interview session?', function () {
                setTimeout(function () {
                    $.ajax({
                        url: '{{ url('/interview/delete_interview_section') }}' + '/' + id,
                        type: 'post',
                        data: '',
                        success: function (data, textStatus, jQxhr) {
                            UIkit.notify({
                                message: 'Successfully removed.',
                                status: 'success',
                                timeout: 1000,
                                pos: 'top-right',
                                onClose: function () {
                                    location.reload();
                                }
                            });
                        },
                        error: function (data) {
                            UIkit.notify({
                                message: 'Something went wrong.',
                                status: 'danger',
                                timeout: 3000,
                                pos: 'top-right',
                                onClose: function () {
                                    location.reload();
                                }
                            });
                        }
                    });
                }, 300)
            }, {
                labels: {
                    'Ok': 'Delete'
                }
            });
        }

        function add_interview_remark(id){
            altair_helpers.content_preloader_show();
            $contact_data  = $('#interview_remark_data' + id);
            var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);

            $.ajax({
                url: '{{ url('/interview/remark') }}' + '/' + id,
                type: 'POST',
                data: {
                    'interview_remark': $contact_data.serializeObject()
                },
                success: function (data, textStatus, jQxhr) {
                    UIkit.notify({
                        message: 'Sucessfully Added Interview Remark.',
                        status: 'success',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                            location.reload()
                        }
                    });
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    UIkit.notify({
                        message: 'Sucessfully Added Employee Record.',
                        status: 'success',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                            location.reload()
                        }
                    });
                }
            });
        }

        function edit_interview_remark(id){
            altair_helpers.content_preloader_show();
            $contact_data  = $('#interview_remark_edit_data' + id);
            var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);

            $.ajax({
                url: '{{ url('/interview/remark') }}' + '/' + id,
                type: 'POST',
                data: {
                    'interview_remark': $contact_data.serializeObject()
                },
                success: function (data, textStatus, jQxhr) {
                    UIkit.notify({
                        message: 'Sucessfully Updated Interview Remark.',
                        status: 'success',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                            location.reload()
                        }
                    });
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    UIkit.notify({
                        message: 'Sucessfully Added Employee Record.',
                        status: 'success',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                            location.reload()
                        }
                    });
                }
            });
        }

        function del_remark(id){
            UIkit.modal.confirm('Do you want to delete this Interview remark?', function () {
                setTimeout(function () {
                    $.ajax({
                        url: '{{ url('/interview/remark_delete') }}' + '/' + id,
                        type: 'post',
                        data: '',
                        success: function (data, textStatus, jQxhr) {
                            UIkit.notify({
                                message: 'Successfully removed.',
                                status: 'success',
                                timeout: 1000,
                                pos: 'top-right',
                                onClose: function () {
                                    $('#remark' + id).hide('slow').remove();
                                }
                            });
                        },
                        error: function (data) {
                            UIkit.notify({
                                message: 'Something went wrong.',
                                status: 'danger',
                                timeout: 3000,
                                pos: 'top-right',
                                onClose: function () {
                                    location.reload();
                                }
                            });
                        }
                    });
                }, 300)
            }, {
                labels: {
                    'Ok': 'Delete'
                }
            });
        }
    </script>


@stop
