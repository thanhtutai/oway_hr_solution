@extends('admin.layout.app')
@section('content')
    <div id="page_content">

        <script>
            tinymce.init({
                skin_url: '/../../backend/skins/tinymce/material_design',
                selector: '#wysiwyg_tinymce',
                height: 200,
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste "
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media "
            });

        </script>


        <div id="page_content_inner">

            <div class="md-card ">
                <div class="md-card-content">
                    <h3 class="heading_a">
                        Send CV Form
                    </h3>
                    <br><br>
                    <form class="uk-form-stacked" action="{{ url('cv/send_cv') }}" method="post" id="cv_mail_data">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <input type="hidden" name="cv_no" value="{{ $information->cv_no }}">
                        <div class="uk-grid">
                            <div class="uk-width-large-1-4 ">
                                <p>
                                    <a class="md-btn md-btn-flat"   onclick="select_all()" href="#">Select all / Deselect all</a>
                                </p>
                                <p>
                                    <input type="checkbox" name="contact_info" id="checkbox_demo_1" value="1"
                                           data-md-icheck/>
                                    <label for="checkbox_demo_1" class="inline-label">Contact
                                        Information</label>
                                </p>
                                <p>
                                    <input type="checkbox" name="personal_info" value="1" id="checkbox_demo_2"
                                           data-md-icheck/>
                                    <label for="checkbox_demo_2" class="inline-label">Personal
                                        Information</label>
                                </p>
                                <p>
                                    <input type="checkbox" name="c_obj" id="checkbox_demo_3" value="1" data-md-icheck/>
                                    <label for="checkbox_demo_3" class="inline-label">Career Objective</label>
                                </p>
                                <p>
                                    <input type="checkbox" name="work_exp" id="checkbox_demo_5" value="1"
                                           data-md-icheck/>
                                    <label for="checkbox_demo_5" class="inline-label">Working Experience
                                    </label>
                                </p>
                                <p>
                                    <input type="checkbox" name="edu_back" id="checkbox_demo_6" value="1"
                                           data-md-icheck/>
                                    <label for="checkbox_demo_6" class="inline-label">Educational Background
                                    </label>
                                </p>

                                <p>
                                    <input type="checkbox" name="other_qual" id="checkbox_demo_7" value="1"
                                           data-md-icheck/>
                                    <label for="checkbox_demo_7" class="inline-label"> Other
                                        Qualifications </label>
                                </p>
                                <p>
                                    <input type="checkbox" name="it_skill" id="checkbox_demo_8" value="1"
                                           data-md-icheck/>
                                    <label for="checkbox_demo_8" class="inline-label"> IT Skill </label>
                                </p>
                                <p>
                                    <input type="checkbox" name="lang_skill" id="checkbox_demo_10" value="1"
                                           data-md-icheck/>
                                    <label for="checkbox_demo_10" class="inline-label"> Language Skill </label>
                                </p>
                                <p>
                                    <input type="checkbox" name="ref_person" id="checkbox_demo_11" value="1"
                                           data-md-icheck/>
                                    <label for="checkbox_demo_11" class="inline-label"> Reference
                                        Person </label>
                                </p>
                                <p>
                                    <input type="checkbox" name="cv_img_n_cv_letter" id="checkbox_demo_12" value="1"
                                           data-md-icheck/>
                                    <label for="checkbox_demo_12" class="inline-label">Cover Image &
                                        Letter</label>
                                </p>

                                <p>
                                    <?php $app_jobs = \App\Models\ApplicantJobPosition::where('cv_id', $information->cv_no)->get() ?>
                                    <select id="cv_app_job1" name="apply_job"
                                            data-placeholder="Choose Job Position">
                                        <option value="">Choose Job Position</option>
                                        @foreach($app_jobs as $row)
                                            <option value="{{ $row->id }}">
                                                {{ $row->apply_position }}
                                            </option>
                                        @endforeach
                                    </select>
                                </p>

                                <p>
                                    <?php $attachment = \App\Models\ApplicantAttachment::where('cv_id', $information->cv_no)->get() ?>
                                    <select id="cv_app_job2" name="attachment[]"
                                            data-placeholder="Choose Attachment" multiple>
                                        <option value="">Choose Job Position</option>
                                        @foreach($attachment as $row)
                                            <option value="{{ $row->attachment }}">
                                                {{ $row->filename }}
                                            </option>
                                        @endforeach
                                    </select>
                                    {{--<input type="checkbox" name="attachment" id="checkbox_demo_9" value="1"--}}
                                    {{--data-md-icheck/>--}}
                                    {{--<label for="checkbox_demo_9" class="inline-label">Attachment</label>--}}
                                </p>

                            </div>
                            <div class="uk-width-large-3-4 ">
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-large-1-1 parsley-row">
                                        <select id="to" name="to" multiple>
                                            <option value="">To</option>
                                        </select>
                                    </div>
                                </div>

                                <input type="hidden" name="filename" value="{{ $information->applicant_name }}">

                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-large-1-1 parsley-row">
                                        <input type="text" placeholder="Subject" name="subject" class="md-input">
                                    </div>
                                </div>

                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-1 parsley-row">
                                        <textarea id="wysiwyg_tinymce" name="cover_letter" cols="30"
                                                  rows="20"></textarea>
                                    </div>
                                </div>

                                <div class="uk-grid">
                                    <div class="uk-width-1-1">
                                        <a href="#" class="md-btn md-btn-primary" onclick="send_cv()">Submit</a>
                                        {{--<button type="submit"  class="md-btn md-btn-primary">Submit</button>--}}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>

    <script>
        function send_cv() {
            altair_helpers.content_preloader_show();
            var as = $('#wysiwyg_tinymce').html(tinymce.get('wysiwyg_tinymce').getContent());
            $contact_data = $('#cv_mail_data');
            var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);
//            UIkit.modal.alert('<p>Wizard data:</p><pre>' + form_serialized + '</pre>');
            $.ajax({
                url: '{{ url('cv/send_cv') }}',
                type: 'post',
                data: {
                    'cv_data': $contact_data.serializeObject(),
                },
                success: function (data, textStatus, jQxhr) {
                    UIkit.notify({
                        message: 'Sucessfully Send CV',
                        status: 'success',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                        }
                    });
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    UIkit.notify({
                        message: 'Something went wrong.',
                        status: 'danger',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                        }
                    });
                }
            });
        }

        function select_all() {
            var $mailbox = $('#cv_mail_data');
            if ($mailbox.find('[data-md-icheck]').is(':checked')) {
                $mailbox.find('[data-md-icheck]').iCheck('uncheck')
            } else {
                $mailbox.find('[data-md-icheck]').iCheck('check')
            }
        }

    </script>
@stop