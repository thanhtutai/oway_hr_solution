@extends('admin.layout.app')
@section('content')
    <div id="page_content">
        <div id="page_content_inner">
            <h4 class="heading_a uk-margin-bottom"></h4>

            <div class="md-card">
                <div class="md-card-content large-padding">
                    <h3 class="heading_a"><i class="material-icons">search</i>&nbsp;&nbsp;Application List Search</h3>
                    <br><br>
                    
                    {!! Form::open(['url'=>'cv','method'=>'GET', 'class'=>'uk-form-stacked', 'id'=>'search_data']) !!}
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-3">
                                <div class="parsley-row">
                                    <label for="fullname">NRC Number<span class="req">*</span></label>
                                    <input type="text" name="nrc" value="0" class="md-input"/>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-3">
                                <div class="parsley-row">
                                    <?php $category = \App\Models\Job_Category::all(); ?>
                                    <select id="val_select" name="job_cat" data-md-selectize>
                                        <option value="">Select JoB Category</option>
                                        @foreach($category as $row)
                                            <option value="{{ $row->id }}">
                                                {{ $row->job_code }} - {{ $row->description }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-3">
                                <div class="parsley-row">
                                    <?php $salary = \App\Models\Salary::all() ?>
                                    <select id="val_select" name="exp_salary" data-md-selectize>
                                        <option value="">Select Expected Salary</option>
                                        @foreach($salary as $sal)
                                            <option value="{{ $sal->id }}">{{ $sal['salary'] }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>


                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-3">
                                <div class="parsley-row">
                                    <?php $job_type = \App\Models\JobType::all() ?>
                                    <select id="val_select" name="job_type" data-md-selectize>
                                        <option value="">Select JoB Type</option>
                                        @foreach($job_type as $row)
                                            <option value="{{ $row->id }}">
                                                {{ $row->type }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-3">
                                <div class="parsley-row">
                                <?php $date_meta = json_decode(DATE_META,true); ?>
                                    <select id="val_select" name="exp" data-md-selectize>
                                        <option value="">Experience</option>
                                        @foreach($date_meta as $key=>$value)
                                        <option value="{{$key}}">{{$value}}</option>
                                        @endforeach
                                        </select>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-3">
                                <div class="parsley-row">
                                    <?php $industry = \App\Models\Job_Industry::all() ?>
                                    <select id="val_select" data-md-selectize name="job_industry">
                                        <option value="">Experience Job Industry</option>
                                        @foreach($industry as $row)
                                            <option value="{{ $row->id }}">
                                                {{ $row->industry_description }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-3">
                                <?php $exp_job_title = \App\Models\ApplicantWorkExperience::all() ?>
                                <div class="parsley-row">
                                    <select id="val_select" name="exp_job_title" data-md-selectize>
                                        <option value="">Experience Job Title</option>
                                        @foreach($exp_job_title as $exp)
                                            <option value="{{ $exp->exp_job_title }}">{{ $exp->exp_job_title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-3">
                                <?php $apply_job_pos = \App\Models\ApplicantJobPosition::all() ?>
                                <div class="parsley-row">
                                    <select id="val_select" name="app_job_post" data-md-selectize>
                                        <option value="">Apply Position</option>
                                        @foreach($apply_job_pos as $pos)
                                            <option value="{{ $pos->apply_position }}">{{ $pos->apply_position }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="uk-width-medium-1-3">
                                <div class="parsley-row">
                                    <?php $industry = \App\Models\Job_Industry::all() ?>
                                    <select id="val_select" data-md-selectize name="apply_job_industry">
                                        <option value="">Apply Job Industry</option>
                                        @foreach($industry as $row)
                                            <option value="{{ $row->id }}">
                                                {{ $row->industry_description }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="uk-grid">
                            <div class="uk-width-1-1">
                                <button type="submit" class="md-btn md-btn-primary" style="float: right;">Search
                                </button>
                                <a href="{{ url('cv') }}" class="md-btn md-btn-danger"
                                   style="float: right;margin-right: 20px">Reset</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <div class="table-responsive">
                    <table id="" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Action</th>
                            <th>CV Status</th>
                            <th>Application-Id</th>
                            <th>Full Name</th>
                             <th>NRC NUMBER</th>
                            <th>Email</th>
                            <th>Phone Number</th>
                            <th>Job Category</th>
                            <th>Apply Position</th>
                            <th>Salary Expectations</th>
                            <th>Experience Job Title</th>
                            <th>Who Created</th>
                            <th>Created Date</th>
                            <th>CV Strength</th>
                        </tr>
                        </thead>
                        <tfoot>
                        <tr>
                            <th>Action</th>
                            <th>CV Status</th>
                            <th>Application-Id</th>
                            <th>Full Name</th>
                             <th>NRC NUMBER</th>
                            <th>Email</th>
                            <th>Phone Number</th>
                            <th>Job Category</th>
                            <th>Apply Position</th>
                            <th>Salary Expectations</th>
                            <th>Experience Job Title</th>
                            <th>Who Created</th>
                            <th>Created Date</th>
                            <th>CV Strength</th>
                        </tr>
                        </tfoot>
                        <tbody>

                        @foreach($applicant_information as $information)
    
                            <tr>
                                <td>
                                    <a data-uk-tooltip="{pos:'top'}" title="More Info"
                                       href="{{ url('cv').'/'.$information->id }}"><i class="material-icons md-24">assignment_ind</i></a>
                                    <a data-uk-tooltip="{pos:'top'}" title="Download CV"
                                       href="#" data-uk-modal="{target : '#send_cv{{ $information->id }}'}"><i
                                                class="material-icons md-24">print</i></a>
                                    @if($information->cv_status == 4)
                                    <a data-uk-tooltip="{pos:'top'}"
                                       data-uk-modal=""
                                       title="Interview" href="#"><i
                                                class="material-icons md-24">block</i></a>
                                    @else
                                    <a data-uk-tooltip="{pos:'top'}"
                                       data-uk-modal="{target : '#new_interview_person{{ $information->id }}'}"
                                       title="Interview" href="#"><i
                                                class="material-icons md-24">assignment_turned_in</i></a>
                                    @endif
                                    <a data-uk-tooltip="{pos:'top'}" onclick="delete_cv({{ $information->id }})"
                                       title="Delete CV" href="#"><i class="material-icons md-24">delete_sweep</i></a>

                                    @if($information->cv_status != 3)
                                        <a data-uk-tooltip="{pos:'top'}" onclick="hold({{ $information->id }})"
                                           title="Make CV Hold/Cancel" href="#"><i class="material-icons md-24">block</i></a>
                                    @else
                                        <a data-uk-tooltip="{pos:'top'}" onclick="avaliable({{ $information->id }})"
                                           title="Make Avaliable" href="#"><i class="material-icons md-24">play_circle_outline</i></a>
                                    @endif
                                </td>
                                <td>
                                    @if($information->cv_status == 1)
                                        <span class="uk-badge uk-badge-warning">Pending</span>
                                    @elseif($information->cv_status == 2)
                                        <span class="uk-badge uk-badge-primary">Avaliable</span>
                                    @elseif($information->cv_status == 3)
                                        <span class="uk-badge">Hold</span>
                                    @elseif($information->cv_status == 4)
                                        <span class="uk-badge uk-badge-success">Employed</span>
                                    @elseif($information->cv_status == 5)
                                        <span class="uk-badge uk-badge-danger">Rejected</span>
                                    @elseif($information->cv_status == 6)
                                        <span class="uk-badge">Second Interview</span>
                                    @else
                                    -
                                    @endif
                                </td>
                                <td>{{ $information->cv_no }}</td>
                                <td>{{ $information->applicant_name }}</td>
                                 <td>{{ $information->nrc }}</td>
                                <td>{{ $information->email }}</td>
                                <td>{{ $information->contact_number }}</td>
                                <td>
                                    <?php $job_cat = \App\Models\Job_Category::applicant_job_category($information->cv_no) ?>
                                    @if(isset($job_cat))
                                    {{ $job_cat->description }}
                                    @endif
                                </td>
                                <td>
                                    <?php $apply_job_pos = \App\Models\ApplicantJobPosition::where('cv_id', $information->cv_no)->get() ?>
                                    @if(isset($apply_job_pos))
                                    @foreach($apply_job_pos as $pos)
                                        {{ $pos->apply_position }} ,
                                    @endforeach
                                    @endif
                                </td>
                                <td>
                                    <?php $salary_bind = \App\Models\Salary::where('id',$information->salary_exp)->first(); ?>
                                    {{$salary_bind['salary']}}
                                    @if($information->negotiable == '1' )
                                        <span class="uk-badge uk-badge-success">Negotiable</span>
                                    @endif
                                </td>
                                <td>
                                    <?php $title = \App\Models\ApplicantWorkExperience::where('cv_id', $information->cv_no)->pluck('exp_job_title') ?>
                                    {{ $title }}</td>
                                <td>
                                    <?php $user = SiteHelper::get_user_permission($information->create_user_id) ?>
                                    {{ $user->first_name }} {{ $user->last_name }}
                                </td>
                                <td>{{ date('F d, Y',strtotime($information->created_at))}}</td>
                                <td>
                                    <div class="uk-progress">
                                        <div class="uk-progress-bar"
                                             style="width: {{ SiteHelper::cv_strength($information->cv_no) }}%;">{{ SiteHelper::cv_strength($information->cv_no) }}
                                            %
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            <div class="uk-modal" style="z-index: 998" id="new_interview_person{{ $information->id }}">
                                <div class="uk-modal-dialog">
                                    <div class="uk-modal-header">
                                        <h3 class="uk-modal-title">Interview Person</h3>
                                    </div>
                                    <form id="interview_data{{ $information->id  }}">
                                        <div class="uk-grid">
                                            <div class="uk-width-medium-1-1 parsley-row">
                                                @if($information->cv_no=='')
                                                <?php $job_cat = \App\Models\Job_Category::applicant_job_category($information->cv_no) ?>
                                                <input type="hidden" name="interview_person"
                                                       value="{{ $information->cv_no }}">
                                                <p>Name : {{ $information->applicant_name }} ( {{ $information->cv_no }}
                                                    )</p>
                                                <p>Job Category : {{ $job_cat->description }}</p>

                                                <p>Apply Job Position
                                                    : {{ \App\Models\ApplicantJobPosition::where('cv_id', $information->cv_no)->pluck('apply_position') }}</p>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="uk-grid">
                                            <div class="uk-width-medium-1-2">
                                                <input id="cv_interview_date{{ $information->id  }}" value=""
                                                       type="text" name="interview_date">
                                            </div>
                                            <div class="uk-width-medium-1-2">
                                                <?php $client = \App\Models\Client::all() ?>
                                                {{--<select id="cv_company_name{{ $information->id }}" name="company_name"--}}
                                                        {{--data-placeholder="Select Company"--}}
                                                        {{--multiple>--}}
                                                    {{--@foreach($client as $row)--}}
                                                        {{--<option value="{{ $row->company_code }}">--}}
                                                            {{--{{ $row->company_name }}--}}
                                                        {{--</option>--}}
                                                    {{--@endforeach--}}
                                                {{--</select>--}}

                                                    <select id="select_demo_1"
                                                            data-md-selectize
                                                            name="company_name" data-placeholder="Select Company">
                                                        <option value="">Choose Company
                                                        </option>
                                                        @foreach($client as $row)
                                                            <option value="{{ $row->company_code }}">
                                                                {{ $row->company_name }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                            </div>
                                        </div>
                                        <div class="uk-grid">
                                            <div class="uk-width-medium-1-2">
                                                <?php $app_jobs = \App\Models\ApplicantJobPosition::where('cv_id', $information->cv_no)->get() ?>
                                                <select id="cv_app_job{{ $information->id }}" name="apply_job"
                                                        data-placeholder="Choose Job Position">
                                                    <option value="">Choose Job Position</option>
                                                    @foreach($app_jobs as $row)
                                                        <option value="{{ $row->id }}">
                                                            {{ $row->apply_position }}
                                                        </option>
                                                    @endforeach
                                                </select>


                                            </div>
                                        </div>
                                        <div class="uk-modal-footer uk-text-right">
                                            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close
                                            </button>
                                            <button type="button"
                                                    onclick="add_interview_person({{ $information->id  }})"
                                                    class="md-btn md-btn-flat md-btn-flat-primary uk-modal-close">Submit
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div class="uk-modal" style="z-index: 998" id="send_cv{{ $information->id }}">
                                <div class="uk-modal-dialog">
                                    <div class="uk-grid">
                                        <div class="uk-width-1-3">
                                            <div class="uk-modal-header">
                                                <h3 class="uk-modal-title">Download CV</h3>
                                            </div>
                                        </div>
                                        <div class="uk-width-2-3">
                                            <a class="md-btn md-btn-flat"  id="check_all" style="float:right" onclick="select_all()" href="#">Select all / Deselect all</a>
                                        </div>
                                    </div>
                                    <form action="{{ url('pdf/print').'/'.$information->id }}" method="post" id="pdf_data">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <div class="uk-grid">
                                            <input type="hidden" name="cv_no" value="{{ $information->cv_no }}">
                                            <div class="uk-width-medium-1-2 ">
                                                <p>
                                                    <input type="checkbox" name="contact_info" id="checkbox_demo_1" value="1"
                                                           data-md-icheck/>
                                                    <label for="checkbox_demo_1" class="inline-label">Contact
                                                        Information</label>
                                                </p>
                                                <p>
                                                    <input type="checkbox" name="personal_info" value="1" id="checkbox_demo_2"
                                                           data-md-icheck/>
                                                    <label for="checkbox_demo_2" class="inline-label">Personal
                                                        Information</label>
                                                </p>
                                                <p>
                                                    <input type="checkbox" name="c_obj" id="checkbox_demo_3" value="1" data-md-icheck/>
                                                    <label for="checkbox_demo_3" class="inline-label">Career Objective</label>
                                                </p>
                                                <p>
                                                    <input type="checkbox" name="work_exp" id="checkbox_demo_5" value="1" data-md-icheck/>
                                                    <label for="checkbox_demo_5" class="inline-label">Working Experience
                                                    </label>
                                                </p>
                                                <p>
                                                    <input type="checkbox" name="edu_back" id="checkbox_demo_6" value="1" data-md-icheck/>
                                                    <label for="checkbox_demo_6" class="inline-label">Educational Background
                                                    </label>
                                                </p>
                                                <p>
                                                    <?php $app_jobs = \App\Models\ApplicantJobPosition::where('cv_id', $information->cv_no)->get() ?>
                                                    <select id="cv_company_name{{ $information->id }}" name="apply_job"
                                                            data-placeholder="Choose Job Position">
                                                        <option value="">Choose Job Position</option>
                                                        @foreach($app_jobs as $row)
                                                            <option value="{{ $row->id }}">
                                                                {{ $row->apply_position }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                </p>
                                            </div>
                                            <div class="uk-width-medium-1-2">
                                                <p>
                                                    <input type="checkbox" name="other_qual" id="checkbox_demo_7" value="1"
                                                           data-md-icheck/>
                                                    <label for="checkbox_demo_7" class="inline-label"> Other
                                                        Qualifications </label>
                                                </p>
                                                <p>
                                                    <input type="checkbox" name="it_skill" id="checkbox_demo_8" value="1" data-md-icheck/>
                                                    <label for="checkbox_demo_8" class="inline-label"> IT Skill </label>
                                                </p>
                                                <p>
                                                    <input type="checkbox" name="lang_skill" id="checkbox_demo_10" value="1"
                                                           data-md-icheck/>
                                                    <label for="checkbox_demo_10" class="inline-label"> Language Skill </label>
                                                </p>
                                                <p>
                                                    <input type="checkbox" name="ref_person" id="checkbox_demo_11" value="1"
                                                           data-md-icheck/>
                                                    <label for="checkbox_demo_11" class="inline-label"> Reference
                                                        Person </label>
                                                </p>
                                                <p>
                                                    <input type="checkbox" name="cv_img_n_cv_letter" id="checkbox_demo_12" value="1"
                                                           data-md-icheck/>
                                                    <label for="checkbox_demo_12" class="inline-label">Cover Image &
                                                        Letter</label>
                                                </p>
                                                <p>
                                                    <?php $attachment = \App\Models\ApplicantAttachment::where('cv_id', $information->cv_no)->get() ?>
                                                    <select id="select_demo_1" data-md-selectize name="attachment[]"
                                                            data-placeholder="Choose Attachment" multiple>
                                                        <option value="">Choose Job Position</option>
                                                        @foreach($attachment as $row)
                                                            <option value="{{ $row->attachment }}">
                                                                {{ $row->filename }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                    {{--<input type="checkbox" name="attachment" id="checkbox_demo_9" value="1"--}}
                                                    {{--data-md-icheck/>--}}
                                                    {{--<label for="checkbox_demo_9" class="inline-label">Attachment</label>--}}
                                                </p>
                                            </div>

                                        </div>
                                        <div class="uk-modal-footer uk-text-right">
                                            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                                            {{--<a href="#" onclick="print_pdf()" id="edit_contact_info"--}}
                                            {{--class="md-btn uk-modal-close md-btn-flat md-btn-flat-primary">Submit--}}
                                            {{--</a>--}}
                                            <button type="submit" class="md-btn  md-btn-flat md-btn-flat-primary">Submit</button>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        @endforeach
                        </tbody>
                    </table>
                    </div>
                    <nav>
                      <ul class="pagination">
                        <li><?php echo $applicant_information->render(); ?></li>
                      </ul>
                    </nav>
                </div>
            </div>

            <div class="md-fab-wrapper">
                <a class="md-fab md-fab-primary" data-uk-tooltip="{pos:'left'}" title="Create New CV"
                   href="{{ url('cv/create') }}">
                    <i class="material-icons">&#xE145;</i>
                </a>
            </div>
        </div>
    </div>

    <script>
        function search_cv_data() {
            $contact_data = $('#search_data');
            var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);
            UIkit.modal.alert('<p>Wizard data:</p><pre>' + form_serialized + '</pre>');
        }

        function delete_cv(id) {
            UIkit.modal.confirm(
                    'Are you sure?',
                    function () {

                        (function (modal) {
                            modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'>Wait...<br/><img class=\'uk-margin-top\' src=\'{{ url('backend/img/spinners/spinner.gif') }}\' alt=\'\'>');
                            setTimeout(function () {
                                $.ajax({
                                    url: '{{ url('/cv') }}' + '/' + id,
                                    type: 'DELETE',
                                    data: '',
                                    success: function (msg) {
                                        modal.hide()
                                        location.reload();
                                    },
                                    error: function (data) {
                                        modal.hide()
                                        location.reload();
                                    }
                                });


                            }, 5000)
                        })();
                    });
        }

        function add_interview_person(id) {
            $contact_data = $('#interview_data' + id);
            var interview_date = $('#cv_interview_date' + id);
            var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);
            $.ajax({
                url: '{{ url('cv/set_as_interview_person') }}',
                type: 'post',
                data: {
                    'interview_data': $contact_data.serializeObject(),
                    'interview_date': interview_date.serializeObject(),
                },
                success: function (data, textStatus, jQxhr) {
                    UIkit.notify({
                        message: 'Sucessfully Added Interview Person',
                        status: 'success',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                        }
                    });
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    UIkit.notify({
                        message: 'This CV is on Hold / Cancel.',
                        status: 'danger',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                        }
                    });
                }
            });
        }

        function hold(id){
            UIkit.modal.confirm(
                    'Make CV Hold/Cancel?',
                    function () {

                        (function (modal) {
                            modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'>Wait...<br/><img class=\'uk-margin-top\' src=\'{{ url('backend/img/spinners/spinner.gif') }}\' alt=\'\'>');
                            setTimeout(function () {
                                $.ajax({
                                    url: '{{ url('/cv/change_cv_status') }}' + '/' + id,
                                    type: 'post',
                                    data: {
                                        status : '3'
                                    },
                                    success: function (msg) {
                                        modal.hide();
                                        location.reload();
                                    },
                                    error: function (data) {
                                        modal.hide();
                                        location.reload();
                                    }
                                });


                            }, 2000)
                        })();
                    });
        }

        function avaliable(id){
            UIkit.modal.confirm(
                    'Make CV Avaliable?',
                    function () {

                        (function (modal) {
                            modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'>Wait...<br/><img class=\'uk-margin-top\' src=\'{{ url('backend/img/spinners/spinner.gif') }}\' alt=\'\'>');
                            setTimeout(function () {
                                $.ajax({
                                    url: '{{ url('/cv/change_cv_status') }}' + '/' + id,
                                    type: 'post',
                                    data: {
                                        status : '1'
                                    },
                                    success: function (msg) {
                                        modal.hide();
                                        location.reload();
                                    },
                                    error: function (data) {
                                        modal.hide();
                                        location.reload();
                                    }
                                });


                            }, 2000)
                        })();
                    });
        }
    </script>

@stop
