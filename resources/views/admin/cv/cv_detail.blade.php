@extends('admin.layout.app')
@section('content')

    <style>
        .aa {
            z-index: 999;
        }
    </style>

    <div id="page_content">

        <script>
            tinymce.init({
                skin_url: '/../../backend/skins/tinymce/material_design',
                selector: '#wysiwyg_tinymce',
                height: 200,
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste "
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media "
            });


            tinymce.init({
                skin_url: '/../../backend/skins/tinymce/material_design',
                selector: '#description',
                height: 200,
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste "
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media "
            });

            tinymce.init({
                skin_url: '/../../backend/skins/tinymce/material_design',
                selector: '.work',
                height: 200,
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste "
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media "
            });

        </script>

        <div id="page_heading" data-uk-sticky="{ top: 48, media: 960 }">
            <div class="heading_actions">
                <a href="{{ url('cv/send_cv').'/'.$applicant_information->id }}"
                   data-uk-tooltip="{pos:'bottom'}"
                   title="Archive"><i class="md-icon material-icons">
                        mail_outline</i></a>
                {{--<a href="{{ url('pdf/print').'/'.$applicant_information->id }}" target="_self"--}}
                {{--data-uk-tooltip="{pos:'bottom'}"--}}
                {{--title="Download PDF"><i class="md-icon material-icons">--}}
                {{--&#xE8AD;</i></a>--}}
                <a href="#" target="_self"
                   data-uk-tooltip="{pos:'bottom'}"
                   data-uk-modal="{target:'#send_cv'}"
                   title="Download PDF"><i class="md-icon material-icons">
                        &#xE8AD;</i></a>
                <a href="{{ url('cv/preview').'/'.$applicant_information->id }}" target="_blank"
                   data-uk-tooltip="{pos:'bottom'}" title="Preview"><i class="md-icon material-icons">
                        pageview</i></a>
            </div>
            <h1>APPLICANT INFORMATION</h1>
            <span class="uk-text-upper uk-text-small"><a href="#">{{ $applicant_information->cv_no }}</a> </span>
        </div>

        <div id="page_content_inner">
            <div class="md-card">
                <div class="md-card-content">
                    <div class="uk-grid uk-grid-width-small-1-2 uk-grid-width-
                    ge-1-3 uk-grid-width-xlarge-1-5 uk-text-center "
                         data-uk-grid-margin>
                        <div class="uk-width-medium-1-5">
                            @if($applicant_information->cv_image)
                                <img style="height: 200px;width: 100%;border-radius: 10px"
                                     src="{{ $applicant_information->cv_image }}"
                                     data-uk-modal="{target : '#edit_applicant_image' }" alt="">
                            @else
                                <img style="height: 200px;width: 100%;border-radius: 10px"
                                     src="http://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&amp;f=y"
                                     data-uk-modal="{target : '#edit_applicant_image' }" alt="">
                            @endif
                        </div>
                        <div class="uk-width-medium-3-5">

                            <div class="md-card-toolbar">
                                <div class="md-card-toolbar-actions">
                                    <i data-uk-tooltip="{pos:'left'}"
                                       data-uk-modal="{target:'#edit_contact_information'}"
                                       title="Edit Contact Information"
                                       class="md-icon material-icons md-color-light-blue-500">mode_edit</i>
                                </div>
                                <h3 class="md-card-toolbar-heading-text">
                                    Contact Information
                                </h3>
                            </div>
                            <div class="md-card-content">
                                <div class="uk-grid">
                                    <div class="uk-width-medium-3-6">
                                        <ul class="md-list" style="text-align: left">
                                            <li>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">Contact Number </span>
                                                    <span class="uk-text-small uk-text-muted">{{ $applicant_information->contact_number }}
                                                        , {{ $applicant_information->sec_contact }}</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="uk-width-medium-3-6">
                                        <ul class="md-list" style="text-align: left">
                                            <li>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">Email Address</span>
                                                    <span class="uk-text-small uk-text-muted">{{ $applicant_information->email }}</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="uk-grid">
                                    <div class="uk-width-1-1">
                                        <ul class="md-list" style="text-align: left">
                                            <li>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">Address</span>
                                                    <span class="uk-text-small uk-text-muted">{{ $applicant_information->address }}</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="uk-width-medium-1-5">
                            <div class="md-card-content">
                                <div class="epc_chart"
                                     data-percent="{{ SiteHelper::cv_strength($applicant_information->cv_no) }}"
                                     data-bar-color="#009688">
                                        <span class="epc_chart_text"><span
                                                    class="countUpMe">{{ SiteHelper::cv_strength($applicant_information->cv_no) }}</span>%</span>
                                </div>
                            </div>
                            <div class="md-card-overlay-content">
                                <div class="uk-clearfix md-card-overlay-header">
                                    <h3 style="text-align: center">
                                        CV STRENGTH
                                    </h3>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="uk-modal" id="edit_contact_information">
                        <div class="uk-modal-dialog">
                            <div class="uk-modal-header">
                                <h3 class="uk-modal-title">Edit Contact Information</h3>
                            </div>
                            <form action="#" id="contact_data">
                                <div class="uk-grid "
                                     data-uk-grid-margin>
                                    <input type="hidden" name="cv_no" value="{{ $applicant_information->cv_no }}">
                                    <div class="uk-width-medium-1-2 parsley-row">

                                        <label for="wizard_phone">
                                            Phone Number
                                        </label>
                                        <input type="text" class="md-input" name="contact_number"
                                               value="{{ $applicant_information->contact_number }}" id="wizard_phone"/>

                                    </div>
                                    <div class="uk-width-medium-1-2 parsley-row">
                                        <label for="wizard_phone">
                                            Secondary Phone Number
                                        </label>
                                        <input type="text" class="md-input" name="sec_contact_number"
                                               value="{{ $applicant_information->sec_contact }}"
                                               id="wizard_phone"/>
                                    </div>
                                </div>
                                <div class="uk-grid "
                                     data-uk-grid-margin>
                                    <div class="uk-width-medium-1-2 parsley-row">
                                        <label for="wizard_email">
                                            Email
                                        </label>
                                        <input type="text" class="md-input" value="{{ $applicant_information->email }}"
                                               name="email" id="wizard_email"/>
                                    </div>
                                    <div class="uk-width-medium-1-2 parsley-row">
                                        <label for="wizard_address">
                                            Address
                                                    <span class="req">
                                                        *
                                                    </span>
                                        </label>
                                        <input type="text" name="address" id="wizard_address"
                                               value="{{ $applicant_information->address }}"
                                               class="md-input"/>
                                    </div>
                                </div>
                                <div class="uk-modal-footer uk-text-right">
                                    <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                                    <button type="button" id="edit_contact_info" onclick="edit_contact()"
                                            class="md-btn uk-modal-close md-btn-flat md-btn-flat-primary">Submit
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="uk-grid uk-margin-large-bottom" data-uk-grid-margin>
                        <div class="uk-width-1-1">
                            <div class="md-card-toolbar">
                                <div class="md-card-toolbar-actions">
                                    <i data-uk-tooltip="{pos:'left'}" title="Edit Personal Information"
                                       data-uk-modal="{target:'#edit_personal_information'}"
                                       class="md-icon material-icons md-color-light-blue-500">mode_edit</i>
                                </div>
                                <h3 class="md-card-toolbar-heading-text">
                                    Personal Information
                                </h3>
                            </div>
                            <div class="md-card-content">
                                <div class="uk-grid">
                                    <div class="uk-width-medium-1-3">
                                        <ul class="md-list" style="text-align: left">
                                            <li>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">Name</span>
                                                    <span class="uk-text-small uk-text-muted">{{ $applicant_information->applicant_name }}</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">NRC No</span>
                                                    <span class="uk-text-small uk-text-muted">{{ $applicant_information->nrc }}</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">Date of Birth</span>
                                                    <span class="uk-text-small uk-text-muted">{{ date('F d, Y',strtotime($applicant_information->dob))}}</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">Weight</span>
                                                    <span class="uk-text-small uk-text-muted">{{ $applicant_information->weight }}
                                                        lbs</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-content">
                                                    <?php $applicant_apply_position = SiteHelper::get_applicant_apply_position($applicant_information->cv_no) ?>

                                                        @foreach($applicant_apply_position as $job)
                                                    <span class="md-list-heading">Current Position </span>
                                                    <span class="uk-text-small uk-text-muted"> {{ $applicant_information->curr_position }}</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="uk-width-medium-1-3">
                                        <ul class="md-list" style="text-align: left">
                                            <li>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">Father Name</span>
                                                    <span class="uk-text-small uk-text-muted">{{ $applicant_information->father_name }}</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">Religion</span>
                                                    <span class="uk-text-small uk-text-muted">{{ $applicant_information->religion }}</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">Gender</span>
                                                    @if($applicant_information->gender == 1)
                                                        <span class="uk-text-small uk-text-muted">Male</span>
                                                    @else
                                                        <span class="uk-text-small uk-text-muted">Female</span>
                                                    @endif
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">Height</span>
                                                    <span class="uk-text-small uk-text-muted">{{ $applicant_information->height }}</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">Current Company</span>
                                                    <span class="uk-text-small uk-text-muted">{{ $applicant_information->curr_company }}</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="uk-width-medium-1-3">
                                        <ul class="md-list" style="text-align: left">
                                            <li>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">Nationality</span>
                                                    <span class="uk-text-small uk-text-muted">{{ $applicant_information->nationality }}</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">Martial Status</span>
                                                    @if($applicant_information->martial_status == 1)
                                                        <span class="uk-text-small uk-text-muted">Married</span>
                                                    @else
                                                        <span class="uk-text-small uk-text-muted">Single</span>
                                                    @endif
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">Current Location</span>
                                                    <span class="uk-text-small uk-text-muted">{{ \App\Models\Country::where('id',$applicant_information->curr_location)->pluck('country_name') }}</span>
                                                </div>
                                            </li>
                                            <li>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">Salary Expectations</span>
                                                    <span class="uk-text-small uk-text-muted">
                                                    @if($applicant_information->salary_exp!='')
                                                        <?php $salary = \App\Models\Salary::find($applicant_information->salary_exp); ?>
                                                         @if($applicant_information->currency == '1')
                                                          $ {{ $salary->salary }}
                                                         @else
                                                            {{ $salary->salary }} MMK
                                                        @endif
                                                    @endif
                                                        </span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="uk-grid">
                                    <div class="uk-width-medium-1-1">
                                        <ul class="md-list" style="text-align: left">
                                            <li>
                                                <div class="md-list-content">
                                                    <span class="md-list-heading">Noticed Period</span>
                                                    <span class="uk-text-small uk-text-muted">{{ $applicant_information->not_period }}</span>
                                                </div>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="uk-modal" style="z-index: 998" id="edit_personal_information">
                        <div class="uk-modal-dialog">
                            <div class="uk-modal-header">
                                <h3 class="uk-modal-title">Edit Personal Information</h3>
                            </div>
                            <form action="#" id="personal_data" class="uk-form-stacked">

                                <div class="md-card-content" data-uk-observe>
                                    <input type="hidden" name="cv_no" value="{{ $applicant_information->cv_no }}">

                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-3 parsley-row">
                                            <label for="wizard_fullname">
                                                Full Name
                                                    <span class="req">
                                                        *
                                                    </span>
                                            </label>
                                            <input type="text" name="applicant" id="wizard_fullname"
                                                   class="md-input"
                                                   value="{{ $applicant_information->applicant_name }}"/>
                                        </div>
                                        <div class="uk-width-medium-1-3 parsley-row">
                                            <label for="wizard_fullname">
                                                Father Name
                                                    <span class="req">
                                                        *
                                                    </span>
                                            </label>
                                            <input type="text" name="father_name" id="wizard_fullname"
                                                   class="md-input" value="{{ $applicant_information->father_name }}"/>
                                        </div>
                                        <div class="uk-width-medium-1-3 parsley-row">
                                            <label for="wizard_fullname">
                                                NRC No
                                                    <span class="req">
                                                        *
                                                    </span>
                                            </label>
                                            <input type="text" name="nrc" id="wizard_fullname"
                                                   value="{{ $applicant_information->nrc }}"
                                                   class="md-input"/>
                                        </div>
                                    </div>
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-3 parsley-row">
                                            <label for="wizard_birth">
                                                Birth Date
                                                    <span class="req">
                                                        *
                                                    </span>
                                            </label>
                                            <input type="text" name="dob" id=""
                                                   value="{{ $applicant_information->dob }}"
                                                   class="md-input"
                                                   data-parsley-date-message="This value should be a valid date"
                                                   data-uk-datepicker="{format:'YYYY-MM-DD', addClass: 'dropdown-modal aa' }"/>
                                        </div>
                                        <div class="uk-width-medium-1-3 parsley-row">
                                            <label class="uk-form-label">
                                                Gender
                                                    <span class="req">
                                                        *
                                                    </span>
                                            </label>
                                                <span class="icheck-inline">
                                                    <input type="radio" name="gender"
                                                           @if($applicant_information->gender == 1) checked @endif
                                                           data-md-icheck value="1"/>
                                                    <label for="wizard_status_married" class="inline-label">
                                                        Male
                                                    </label>
                                                </span>

                                                <span class="icheck-inline">
                                                    <input type="radio" name="gender"
                                                           @if($applicant_information->gender == 2) checked @endif
                                                           data-md-icheck value="2"/>
                                                    <label for="wizard_status_single" class="inline-label">
                                                        Female
                                                    </label>
                                                </span>
                                        </div>
                                        <div class="uk-width-medium-1-3 parsley-row">
                                            <label class="uk-form-label">
                                                Martial Status
                                                    <span class="req">
                                                        *
                                                    </span>
                                            </label>
                                                <span class="icheck-inline">
                                                    <input type="radio" name="martial_status"
                                                           @if($applicant_information->martial_status == 1) checked
                                                           @endif
                                                           data-md-icheck value="1"/>
                                                    <label for="wizard_status_married" class="inline-label">
                                                        Married
                                                    </label>
                                                </span>
                                                <span class="icheck-inline">
                                                    <input type="radio" name="martial_status"
                                                           @if($applicant_information->martial_status == 2) checked
                                                           @endif
                                                           data-md-icheck value="2"/>
                                                    <label for="wizard_status_single" class="inline-label">
                                                        Single
                                                    </label>
                                                </span>
                                        </div>
                                    </div>

                                    <div class="uk-grid uk-grid-width-medium-1-3 "
                                         data-uk-grid-margin>
                                        <div class="parsley-row">
                                            <div class="uk-input-group">
                                                <label for="wizard_phone">
                                                    Weight
                                                </label>
                                                <input type="text" class="md-input" name="weight"
                                                       value="{{ $applicant_information->weight }}" id="wizard_phone"/>
                                            </div>
                                        </div>
                                        <div class=" parsley-row">
                                            <div class="uk-input-group">
                                                <label for="wizard_email">
                                                    Height
                                                </label>
                                                <input type="text" class="md-input" name="height"
                                                       value="{{ $applicant_information->height }}" id="wizard_email"/>
                                            </div>
                                        </div>
                                        <div class="parsley-row">
                                            <div class="uk-input-group">
                                                <label for="wizard_skype">
                                                    Nationality
                                                </label>
                                                <input type="text" class="md-input" name="nationality"
                                                       value="{{ $applicant_information->nationality }}"
                                                       id="wizard_skype"/>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-3 parsley-row">
                                            <label for="wizard_birth">
                                                Current Position
                                                    <span class="req">
                                                        *
                                                    </span>
                                            </label>
                                            <input type="text" name="current_position"
                                                   value="{{ $applicant_information->curr_position }}"
                                                   id="wizard_fullname"
                                                   class="md-input"/>
                                        </div>
                                        <div class="uk-width-medium-1-3 parsley-row">
                                            <label for="wizard_birth">
                                                Salary Expectations
                                                    <span class="req">
                                                        *
                                                    </span>
                                            </label>
                                            <input type="text" name="exp_salary" id="wizard_fullname"
                                                   value="{{ $applicant_information->salary_exp }}"
                                                   class="md-input"/>
                                        </div>
                                        <div class="uk-width-medium-1-3 parsley-row">
                                            <label class="uk-form-label">
                                                Currency
                                                    <span class="req">
                                                        *
                                                    </span>
                                            </label>
                                                <span class="icheck-inline">
                                                    <input type="radio" name="currency" id="currency_dollar" @if($applicant_information->currency == '1') checked @endif
                                                           class="wizard-icheck" data-md-icheck value="1"/>
                                                    <label for="currency_dollar" class="inline-label">
                                                        Dollar
                                                    </label>
                                                </span>
                                                <span class="icheck-inline">
                                                    <input type="radio" name="currency" id="currency_mmk" @if($applicant_information->currency == '2') checked @endif
                                                           class="wizard-icheck" data-md-icheck value="2"/>
                                                    <label for="currency_mmk" class="inline-label">
                                                        MMK
                                                    </label>
                                                </span>
                                        </div>
                                    </div>

                                    <div class="uk-grid">
                                        <div class="uk-width-medium-1-3 parsley-row">
                                            <div class="uk-input-group">
                                                <label for="wizard_skype">
                                                    Religion
                                                </label>
                                                <input type="text" class="md-input" name="religion"
                                                       value="{{ $applicant_information->religion }}"
                                                       id="wizard_skype"/>
                                            </div>
                                        </div>

                                        <div class="uk-width-medium-1-3 parsley-row">
                                            <label for="wizard_birth">
                                                Current Company Name
                                                    <span class="req">
                                                        *
                                                    </span>
                                            </label>
                                            <input type="text" name="current_company_name"
                                                   value="{{ $applicant_information->curr_company }}"
                                                   id="wizard_fullname"
                                                   class="md-input"/>
                                        </div>
                                        <div class="uk-width-medium-1-3 parsley-row">
                                            <?php $country = \App\Models\Country::all() ?>
                                            <select id="select_demo_1" data-md-selectize name="cur_location">
                                                <option value="">Location</option>
                                                @foreach($country as $row)
                                                    <option
                                                            @if($applicant_information->curr_location == $row->id) selected
                                                            @endif
                                                            value="{{ $row->id }}">
                                                        {{ $row->country_name }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="uk-grid">
                                        <div class="uk-width-medium-1-1 parsley-row">
                                            <label for="wizard_address">
                                                Noticed Period
                                                    <span class="req">
                                                        *
                                                    </span>
                                            </label>
                                            <input type="text" name="noticed_period"
                                                   value="{{ $applicant_information->not_period }}" id="wizard_address"
                                                   class="md-input"/>
                                        </div>
                                    </div>

                                    <div class="uk-modal-footer uk-text-right">
                                        <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                                        <button type="button" onclick="edit_personal_data()"
                                                class="md-btn uk-modal-close md-btn-flat md-btn-flat-primary">Submit
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="uk-modal" id="edit_applicant_image">
                        <div class="uk-modal-dialog">
                            <div class="uk-modal-header">
                                <h3 class="uk-modal-title">Edit Applicant Image</h3>
                            </div>
                            <form action="#" id="edit_applicant_image_data">
                                <div class="uk-grid">
                                    <input type="hidden" name="cv_no" value="{{ $applicant_information->cv_no }}">
                                    <input type="hidden" id="applicant_image" name="applicant_image" value="">
                                    <div class="uk-width-medium-1-1 parsley-row">
                                        <img style="height: 400px;width: 551px;border-radius: 10px"
                                             src="{{ @$applicant_information->cv_image }}" id="applicant_original_image"
                                             alt=""/>
                                    </div>
                                </div>
                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-1 parsley-row">
                                        <a class="uk-form-file md-btn">choose file<input
                                                    id="file_upload-select-cover_applicant" onchange="applicantImage()"
                                                    type="file"></a>
                                    </div>
                                </div>
                                <div class="uk-modal-footer uk-text-right">
                                    <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                                    <button type="button" id="edit_contact_info" onclick="edit_applicant_image()"
                                            class="md-btn uk-modal-close md-btn-flat md-btn-flat-primary">Submit
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>

                    <div class="uk-modal" id="send_cv">
                        <div class="uk-modal-dialog">
                            <div class="uk-grid">
                                <div class="uk-width-1-3">
                                    <div class="uk-modal-header">
                                        <h3 class="uk-modal-title">Download CV</h3>
                                    </div>
                                </div>
                                <div class="uk-width-2-3">
                                    <a class="md-btn md-btn-flat"  id="check_all" style="float:right" onclick="select_all()" href="#">Select all / Deselect all</a>
                                </div>
                            </div>
                            <form action="{{ url('pdf/print').'/'.$applicant_information->id }}" method="post" id="pdf_data">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                <div class="uk-grid">
                                    <input type="hidden" name="cv_no" value="{{ $applicant_information->cv_no }}">
                                    <div class="uk-width-medium-1-2 ">
                                        <p>
                                            <input type="checkbox" name="contact_info" id="checkbox_demo_1" value="1"
                                                   data-md-icheck/>
                                            <label for="checkbox_demo_1" class="inline-label">Contact
                                                Information</label>
                                        </p>
                                        <p>
                                            <input type="checkbox" name="personal_info" value="1" id="checkbox_demo_2"
                                                   data-md-icheck/>
                                            <label for="checkbox_demo_2" class="inline-label">Personal
                                                Information</label>
                                        </p>
                                        <p>
                                            <input type="checkbox" name="c_obj" id="checkbox_demo_3" value="1" data-md-icheck/>
                                            <label for="checkbox_demo_3" class="inline-label">Career Objective</label>
                                        </p>
                                        <p>
                                            <input type="checkbox" name="work_exp" id="checkbox_demo_5" value="1" data-md-icheck/>
                                            <label for="checkbox_demo_5" class="inline-label">Working Experience
                                            </label>
                                        </p>
                                        <p>
                                            <input type="checkbox" name="edu_back" id="checkbox_demo_6" value="1" data-md-icheck/>
                                            <label for="checkbox_demo_6" class="inline-label">Educational Background
                                            </label>
                                        </p>
                                        <p>
                                            <?php $app_jobs = \App\Models\ApplicantJobPosition::where('cv_id', $applicant_information->cv_no)->get() ?>
                                            <select id="cv_app_job{{ $applicant_information->id }}" name="apply_job"
                                                    data-placeholder="Choose Job Position">
                                                <option value="">Choose Job Position</option>
                                                @foreach($app_jobs as $row)
                                                    <option value="{{ $row->id }}">
                                                        {{ $row->apply_position }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </p>
                                    </div>
                                    <div class="uk-width-medium-1-2">
                                        <p>
                                            <input type="checkbox" name="other_qual" id="checkbox_demo_7" value="1"
                                                   data-md-icheck/>
                                            <label for="checkbox_demo_7" class="inline-label"> Other
                                                Qualifications </label>
                                        </p>
                                        <p>
                                            <input type="checkbox" name="it_skill" id="checkbox_demo_8" value="1" data-md-icheck/>
                                            <label for="checkbox_demo_8" class="inline-label"> IT Skill </label>
                                        </p>
                                        <p>
                                            <input type="checkbox" name="lang_skill" id="checkbox_demo_10" value="1"
                                                   data-md-icheck/>
                                            <label for="checkbox_demo_10" class="inline-label"> Language Skill </label>
                                        </p>
                                        <p>
                                            <input type="checkbox" name="ref_person" id="checkbox_demo_11" value="1"
                                                   data-md-icheck/>
                                            <label for="checkbox_demo_11" class="inline-label"> Reference
                                                Person </label>
                                        </p>
                                        <p>
                                            <input type="checkbox" name="cv_img_n_cv_letter" id="checkbox_demo_12" value="1"
                                                   data-md-icheck/>
                                            <label for="checkbox_demo_12" class="inline-label">Cover Image &
                                                Letter</label>
                                        </p>
                                        <p>
                                            <?php $attachment = \App\Models\ApplicantAttachment::where('cv_id', $applicant_information->cv_no)->get() ?>
                                                <select id="cv_app_job2" name="attachment[]"
                                                        data-placeholder="Choose Attachment" multiple>
                                                    <option value="">Choose Job Position</option>
                                                    @foreach($attachment as $row)
                                                        <option value="{{ $row->attachment }}">
                                                            {{ $row->filename }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                {{--<input type="checkbox" name="attachment" id="checkbox_demo_9" value="1"--}}
                                                   {{--data-md-icheck/>--}}
                                            {{--<label for="checkbox_demo_9" class="inline-label">Attachment</label>--}}
                                        </p>
                                    </div>

                                </div>
                                <div class="uk-modal-footer uk-text-right">
                                    <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                                    {{--<a href="#" onclick="print_pdf()" id="edit_contact_info"--}}
                                       {{--class="md-btn uk-modal-close md-btn-flat md-btn-flat-primary">Submit--}}
                                    {{--</a>--}}
                                    <button type="submit" class="md-btn  md-btn-flat md-btn-flat-primary">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <div class="uk-grid">
                <div class="uk-width-medium-1-1">
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <div class="md-card-toolbar-actions">
                                <i data-uk-tooltip="{pos:'left'}"
                                   data-uk-modal="{target:'#edit_career_objective'}" title="Edit Career Objective"
                                   class="md-icon material-icons md-color-light-blue-500">mode_edit</i>
                            </div>
                            <h3 class="md-card-toolbar-heading-text">
                                Career Objective
                            </h3>
                        </div>
                        <div class="md-card-content">
                            <span class="md-list-heading">{{ $applicant_information->c_objective }}</span>
                        </div>
                    </div>
                </div>
                <div class="uk-modal" id="edit_career_objective">
                    <div class="uk-modal-dialog">
                        <div class="uk-modal-header">
                            <h3 class="uk-modal-title">Edit Career Objective</h3>
                        </div>
                        <form action="#" id="c_obj_data">
                            <div class="uk-grid "
                                 data-uk-grid-margin>
                                <input type="hidden" name="cv_no" value="{{ $applicant_information->cv_no }}">

                                <div class="uk-width-medium-1-1 parsley-row">
                                    <label for="wizard_phone">
                                        Career Objective
                                    </label>
                                    <textarea name="c_objective"
                                              style="overflow-x: hidden; word-wrap: break-word; height: 121px;"
                                              cols="30" rows="4"
                                              class="md-input autosize_init">{{ $applicant_information->c_objective }}</textarea>
                                    {{--<input type="text" name="c_objective" id="wizard_address" value="{{ $applicant_information->c_objective }}"--}}
                                    {{--class="md-input"/>--}}
                                </div>
                            </div>
                            <div class="uk-modal-footer uk-text-right">
                                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                                <button type="button" id="edit_contact_info" onclick="edit_career_obj()"
                                        class="md-btn uk-modal-close md-btn-flat md-btn-flat-primary">Submit
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-1">
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <div class="md-card-toolbar-actions">
                                <i class="md-icon material-icons md-color-red-500"
                                   data-uk-modal="{target:'#add_new_job_position'}"
                                   data-uk-tooltip="{pos:'left'}"
                                   title="Add New Job Position">note_add</i>
                            </div>
                            <h3 class="md-card-toolbar-heading-text">
                                Apply Job Position
                            </h3>
                        </div>
                        <div class="md-card-content">
                            <?php $applicant_apply_position = SiteHelper::get_applicant_apply_position($applicant_information->cv_no) ?>
                            <div class="uk-grid " data-uk-grid-margin>
                                @foreach($applicant_apply_position as $job)
                                    <div class="uk-grid-width-medium-1-1">
                                        <div class="md-card-primary">
                                            <div class="uk-button-dropdown" data-uk-dropdown>
                                                <div class="md-card-content">
                                                    <p class="">
                                                        <b>Job Category</b>
                                                        - {{ \App\Models\Job_Category::where('id',$job->job_category)->pluck('description') }}
                                                    </p>
                                                    <p class="">
                                                        <b>Job Industry</b>
                                                        - {{ \App\Models\Job_Industry::where('id',$job->job_industry)->pluck('industry_description') }}
                                                    </p>
                                                    <p class="">
                                                        <b>Apply position</b> - {{ $job->apply_position }}
                                                    </p>
                                                    <p class="">
                                                        <b>Job Type</b>
                                                        - {{ \App\Models\JobType::where('id',$job->job_type)->pluck('type') }}
                                                    </p>
                                                    <p class="">
                                                        <b>Job Location</b>
                                                        - @if($job->location == 1) Local @else Oversea @endif
                                                    </p>
                                                </div>
                                                {{--<div class="uk-dropdown uk-dropdown-small">--}}
                                                {{--<ul class="uk-nav uk-nav-dropdown">--}}
                                                {{--<li><a href="#"--}}
                                                {{--data-uk-modal="{target:'#edit_job_position{{ $job->id }}'}">Edit</a>--}}
                                                {{--</li>--}}
                                                {{--<li><a href="#" onclick="delete_job({{ $job->id }})">Delete</a>--}}
                                                {{--</li>--}}
                                                {{--</ul>--}}
                                                {{--</div>--}}
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="uk-modal" style="z-index: 998" id="edit_job_position{{ $job->id }}">
                                        <div class="uk-modal-dialog">
                                            <div class="uk-modal-header">
                                                <h3 class="uk-modal-title">Edit Job Position</h3>
                                            </div>
                                            <form action="#" id="edit_job_data{{ $job->id }}">
                                                <div class="uk-grid">
                                                    <input type="hidden" name="cv_no"
                                                           value="{{ $applicant_information->cv_no }}">
                                                    <input type="hidden" name="job_id" value="{{ $job->id }}">
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <?php $category = \App\Models\Job_Category::all() ?>
                                                        <select id="select_demo_1" data-md-selectize
                                                                name="job_category">
                                                            <option value="">JoB Category</option>
                                                            @foreach($category as $row)
                                                                <option @if($job->job_category == $row->id ) selected
                                                                        @endif value="{{ $row->id }}">
                                                                    {{ $row->job_code }} - {{ $row->description }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <?php $industry = \App\Models\Job_Industry::all() ?>
                                                        <select id="select_demo_1" data-md-selectize
                                                                name="job_industry">
                                                            <option value="">JoB Industry</option>
                                                            @foreach($industry as $row)
                                                                <option @if($job->job_industry == $row->id ) selected
                                                                        @endif value="{{ $row->id }}">
                                                                    {{ $row->industry_description }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="uk-grid">
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <?php $job_type = \App\Models\JobType::all() ?>
                                                        <select id="select_demo_1" data-md-selectize name="job_type">
                                                            <option value="">JoB Type</option>
                                                            @foreach($job_type as $row)
                                                                <option @if($job->job_type == $row->id ) selected
                                                                        @endif value="{{ $row->id }}">
                                                                    {{ $row->type }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>

                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <select id="select_demo_1" data-md-selectize
                                                                name="target_location">
                                                            <option value="">Location</option>
                                                            <option @if($job->location == 1 ) selected @endif value="1">
                                                                Local
                                                            </option>
                                                            <option @if($job->location == 2 ) selected @endif value="2">
                                                                Oversea
                                                            </option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="uk-grid">
                                                    <div class="uk-width-medium-1-1 parsley-row">
                                                        <label for="wizard_birth">
                                                            Apply Position
                                                    <span class="req">
                                                        *
                                                    </span>
                                                        </label>
                                                        <input type="text" name="apply_position"
                                                               value="{{ $job->apply_position }}"
                                                               class="md-input"/>
                                                    </div>
                                                </div>
                                                <div class="uk-modal-footer uk-text-right">
                                                    <button type="button" class="md-btn md-btn-flat uk-modal-close">
                                                        Close
                                                    </button>
                                                    <button type="button" id="edit_contact_info"
                                                            onclick="edit_job({{ $job->id }})"
                                                            class="md-btn uk-modal-close md-btn-flat md-btn-flat-primary">
                                                        Submit
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                @endforeach
                                    @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-modal" style="z-index: 998" id="add_new_job_position">
                    <div class="uk-modal-dialog">
                        <div class="uk-modal-header">
                            <h3 class="uk-modal-title">New Job Position</h3>
                        </div>
                        <form action="#" id="new_job_data">
                            <div class="uk-grid">
                                <input type="hidden" name="cv_no" value="{{ $applicant_information->cv_no }}">
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <?php $category = \App\Models\Job_Category::all() ?>
                                    <select id="select_demo_1" data-md-selectize name="job_category">
                                        <option value="">JoB Category</option>
                                        @foreach($category as $row)
                                            <option value="{{ $row->id }}">
                                                {{ $row->job_code }} - {{ $row->description }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <?php $industry = \App\Models\Job_Industry::all() ?>
                                    <select id="select_demo_1" data-md-selectize name="job_industry">
                                        <option value="">JoB Industry</option>
                                        @foreach($industry as $row)
                                            <option value="{{ $row->id }}">
                                                {{ $row->industry_description }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="uk-grid">
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <?php $job_type = \App\Models\JobType::all() ?>
                                    <select id="select_demo_1" data-md-selectize name="job_type">
                                        <option value="">JoB Type</option>
                                        @foreach($job_type as $row)
                                            <option value="{{ $row->id }}">
                                                {{ $row->type }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="uk-width-medium-1-2 parsley-row">
                                    <select id="select_demo_1" data-md-selectize name="target_location">
                                        <option value="">Location</option>
                                        <option value="1">Local</option>
                                        <option value="2">Oversea</option>
                                    </select>
                                </div>
                            </div>
                            <div class="uk-grid">
                                <div class="uk-width-medium-1-1 parsley-row">
                                    <label for="wizard_birth">
                                        Apply Position
                                                    <span class="req">
                                                        *
                                                    </span>
                                    </label>
                                   <!--  <input type="text" name="apply_position"
                                           class="md-input"/> -->
                                    <input list="apply_position" name="apply_position" class="md-input">
                                                        <?php $apply_job_pos = \App\Models\ApplicantJobPosition::all() ?>
                                                        
                                                        <datalist id="apply_position">
                                                            @foreach($apply_job_pos as $row)
                                                            <option value="{{$row->apply_position}}"/>  
                                                            @endforeach
                                                        </datalist>  
                                </div>
                            </div>
                            <div class="uk-modal-footer uk-text-right">
                                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                                <button type="button" id="edit_contact_info" onclick="add_new_job()"
                                        class="md-btn uk-modal-close md-btn-flat md-btn-flat-primary">Submit
                                </button>
                            </div>
                        </form>
                    </div>
                </div>

            </div>


            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-1">
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <div class="md-card-toolbar-actions">
                                <i class="md-icon material-icons md-color-red-500"
                                   data-uk-modal="{target:'#add_new_edu_back'}"
                                   data-uk-tooltip="{pos:'left'}"
                                   title="Add New Educational Background">note_add</i>
                            </div>
                            <h3 class="md-card-toolbar-heading-text">
                                Educational Background
                            </h3>
                        </div>
                        <div class="md-card-content">
                            <?php $edu_back = SiteHelper::get_applicant_education($applicant_information->cv_no) ?>
                            <div class="uk-grid" data-uk-grid-margin>
                                @foreach($edu_back as $edu)
                                    <div>
                                        <div class="md-card-primary">
                                            <div class="uk-button-dropdown" data-uk-dropdown>
                                                <div class="md-card-content">
                                                    <p class="">
                                                        <b> {{ $edu->uni_name }}
                                                            ,{{ \App\Models\University::where('id',$edu->degree_lvl)->pluck('degree_name') }},{{ $edu->other }}
                                                            ,{{ $edu->major_name }}</b>
                                                    </p>
                                                    <p>
                                                        {{ \App\Models\Country::where('id',$edu->uni_country)->pluck('country_name') }}
                                                    </p>
                                                    <p>
                                                        From - {{ $edu->uni_start_date }}
                                                        to {{ $edu->uni_graduation_date }}
                                                    </p>
                                                </div>
                                                <div class="uk-dropdown uk-dropdown-small">
                                                    <ul class="uk-nav uk-nav-dropdown">
                                                        <li><a href="#"
                                                               data-uk-modal="{target:'#edit_edu_back{{ $edu->id }}'}">Edit</a>
                                                        </li>
                                                        <li><a href="#" onclick="delete_edu({{ $edu->id }})">Delete</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="uk-modal" style="z-index: 998" id="edit_edu_back{{ $edu->id }}">
                                        <div class="uk-modal-dialog">
                                            <div class="uk-modal-header">
                                                <h3 class="uk-modal-title">Edit Education Background</h3>
                                            </div>
                                            <form action="#" id="edit_edu_data{{ $edu->id }}">
                                                <div class="uk-grid">
                                                    <input type="hidden" name="cv_no"
                                                           value="{{ $applicant_information->cv_no }}">
                                                    <input type="hidden" name="edu_id" value="{{ $edu->id }}">
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <label for="wizard_vehicle_registration_address">
                                                            University
                                                        </label>
                                                        <input type="text" name="university"
                                                               value="{{ $edu->uni_name }}"
                                                               id="wizard_vehicle_registration_address"
                                                               class="md-input"/>
                                                    </div>
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <label for="wizard_vehicle_registration_address">
                                                            Subject/Major
                                                        </label>
                                                        <input type="text" name="major" value="{{ $edu->major_name }}"
                                                               id="wizard_vehicle_registration_address"
                                                               class="md-input"/>
                                                    </div>
                                                </div>

                                                <div class="uk-grid">
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <input placeholder="Start Date" name="uni_from_date"
                                                               value="{{ $edu->uni_start_date }}"
                                                               id="edit_uni_from_date{{ $edu->id }}"/>
                                                    </div>
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <input placeholder="Graduation Date " name="uni_to_date"
                                                               value="{{ $edu->uni_graduation_date }}"
                                                               id="edit_uni_to_date{{ $edu->id }}"/>
                                                    </div>
                                                </div>

                                                <div class="uk-grid">
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <?php $country = \App\Models\Country::all() ?>
                                                        <select id="select_demo_1" data-md-selectize name="edu_country">
                                                            <option value="">Country</option>
                                                            @foreach($country as $row)
                                                                <option @if($edu->uni_country ==  $row->id ) selected
                                                                        @endif value="{{ $row->id }}">
                                                                    {{ $row->country_name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <?php $uni_degree = \App\Models\University::all() ?>
                                                        <select id="select_demo_1" data-md-selectize
                                                                name="degree_level">
                                                            <option value="">Degree Level</option>
                                                            @foreach($uni_degree as $row)
                                                                <option @if($edu->degree_lvl ==  $row->id ) selected
                                                                        @endif value="{{ $row->id }}">
                                                                    {{ $row->degree_name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="uk-grid">
                                                    <div class="uk-width-medium-1-1 parsley-row">
                                                        <textarea name="other" class="md-input" id="" placeholder="Other Degree ( Optional )" cols="30" rows="3">{{ $edu->other }}</textarea>
                                                    </div>
                                                </div>
                                                <div class="uk-modal-footer uk-text-right">
                                                    <button type="button" class="md-btn md-btn-flat uk-modal-close">
                                                        Close
                                                    </button>
                                                    <button type="button" id="edit_contact_info"
                                                            onclick="edit_edu({{ $edu->id }})"
                                                            class="md-btn uk-modal-close md-btn-flat md-btn-flat-primary">
                                                        Submit
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                <div class="uk-modal" style="z-index: 998" id="add_new_edu_back">
                    <div class="uk-modal-dialog">
                        <div class="uk-modal-header">
                            <h3 class="uk-modal-title">Add New Education Background</h3>
                        </div>
                        <form action="#" id="new_edu_data">
                            <div class="uk-grid">
                                <input type="hidden" name="cv_no" value="{{ $applicant_information->cv_no }}">
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <label for="wizard_vehicle_registration_address">
                                        University
                                    </label>
                                    <input type="text" name="university"
                                           id="wizard_vehicle_registration_address" class="md-input"/>
                                </div>
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <label for="wizard_vehicle_registration_address">
                                        Subject/Major
                                    </label>
                                    <input type="text" name="major"
                                           id="wizard_vehicle_registration_address" class="md-input"/>
                                </div>
                            </div>

                            <div class="uk-grid">
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <input placeholder="Start Date" name="uni_from_date"
                                           id="uni_from_date"/>
                                </div>
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <input placeholder="Graduation Date " name="uni_to_date"
                                           id="uni_to_date"/>
                                </div>
                            </div>

                            <div class="uk-grid">
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <?php $country = \App\Models\Country::all() ?>
                                    <select id="select_demo_1" data-md-selectize name="edu_country">
                                        <option value="">Country</option>
                                        @foreach($country as $row)
                                            <option value="{{ $row->id }}">
                                                {{ $row->country_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <?php $uni_degree = \App\Models\University::all() ?>
                                    <select id="select_demo_1" data-md-selectize name="degree_level">
                                        <option value="">Degree Level</option>
                                        @foreach($uni_degree as $row)
                                            <option value="{{ $row->id }}">
                                                {{ $row->degree_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="uk-grid">
                                <div class="uk-width-medium-1-1 parsley-row">
                                    <textarea name="other" class="md-input" id="" placeholder="Other Degree ( Optional )" cols="30" rows="3"></textarea>
                                </div>
                            </div>

                            <div class="uk-modal-footer uk-text-right">
                                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                                <button type="button" id="edit_contact_info" onclick="add_new_edu()"
                                        class="md-btn uk-modal-close md-btn-flat md-btn-flat-primary">Submit
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-1">
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <div class="md-card-toolbar-actions">
                                <i class="md-icon material-icons md-color-red-500"
                                   data-uk-modal="{target:'#add_new_other_qual'}"
                                   data-uk-tooltip="{pos:'left'}"
                                   title="Add New Other Qualification">note_add</i>
                            </div>
                            <h3 class="md-card-toolbar-heading-text">
                                Other Qualifications
                            </h3>
                        </div>
                        <div class="md-card-content">
                            <?php $other_qualification = SiteHelper::get_applicant_other_qualification($applicant_information->cv_no) ?>
                            <div class="uk-grid" data-uk-grid-margin>
                                @foreach($other_qualification as $other)
                                    <div>
                                        <div class="md-card-primary">
                                            <div class="uk-button-dropdown" data-uk-dropdown>
                                                <div class="md-card-content">
                                                    <p class="">
                                                        <b> {{ $other->certi_name }} </b>
                                                    </p>
                                                    <p>
                                                        {{ $other->training_center }}
                                                        ,{{ \App\Models\Country::where('id',$other->certi_country)->pluck('country_name') }}
                                                    </p>
                                                    <p>
                                                        {{ $other->certi_date }}
                                                    </p>
                                                </div>
                                                <div class="uk-dropdown uk-dropdown-small">
                                                    <ul class="uk-nav uk-nav-dropdown">
                                                        <li><a href="#"
                                                               data-uk-modal="{target:'#edit_other_qual{{$other->id}}'}">Edit</a>
                                                        </li>
                                                        <li><a href="#"
                                                               onclick="delete_other({{$other->id}})">Delete</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="uk-modal" style="z-index: 998" id="edit_other_qual{{$other->id}}">
                                        <div class="uk-modal-dialog">
                                            <div class="uk-modal-header">
                                                <h3 class="uk-modal-title">Edit Other Qualifications</h3>
                                            </div>
                                            <form action="#" id="edit_other_qual_data{{$other->id}}">
                                                <div class="uk-grid">
                                                    <input type="hidden" name="cv_no"
                                                           value="{{ $applicant_information->cv_no }}">
                                                    <input type="hidden" name="other_id" value="{{ $other->id }}">
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <label for="wizard_vehicle_registration_address">
                                                            Certificate
                                                        </label>
                                                        <input type="text" name="certificate"
                                                               value="{{ $other->certi_name }}"
                                                               id="wizard_vehicle_registration_address"
                                                               class="md-input"/>
                                                    </div>
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <label for="wizard_vehicle_registration_address">
                                                            Training Center
                                                        </label>
                                                        <input type="text" name="training_center"
                                                               value="{{ $other->training_center }}"
                                                               id="wizard_vehicle_registration_address"
                                                               class="md-input"/>
                                                    </div>
                                                </div>

                                                <div class="uk-grid">
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <?php $country = \App\Models\Country::all() ?>
                                                        <select id="select_demo_1" data-md-selectize
                                                                name="certi_country">
                                                            <option value="">Country</option>
                                                            @foreach($country as $row)
                                                                <option @if($other->certi_country == $row->id ) selected
                                                                        @endif value="{{ $row->id }}">
                                                                    {{ $row->country_name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <input placeholder="Date Issued"
                                                               value="{{ $other->certi_date }}" name="certi_date"
                                                               id="edit_certi_date{{ $other->id }}"/>
                                                    </div>
                                                </div>

                                                <div class="uk-modal-footer uk-text-right">
                                                    <button type="button" class="md-btn md-btn-flat uk-modal-close">
                                                        Close
                                                    </button>
                                                    <button type="button" id="edit_contact_info"
                                                            onclick="edit_other_qual({{ $other->id }})"
                                                            class="md-btn uk-modal-close md-btn-flat md-btn-flat-primary">
                                                        Submit
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                <div class="uk-modal" style="z-index: 998" id="add_new_other_qual">
                    <div class="uk-modal-dialog">
                        <div class="uk-modal-header">
                            <h3 class="uk-modal-title">Add New Other Qualifications</h3>
                        </div>
                        <form action="#" id="new_other_qual_data">
                            <div class="uk-grid">
                                <input type="hidden" name="cv_no" value="{{ $applicant_information->cv_no }}">
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <label for="wizard_vehicle_registration_address">
                                        Certificate
                                    </label>
                                    <input type="text" name="certificate"
                                           id="wizard_vehicle_registration_address" class="md-input"/>
                                </div>
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <label for="wizard_vehicle_registration_address">
                                        Training Center
                                    </label>
                                    <input type="text" name="training_center"
                                           id="wizard_vehicle_registration_address" class="md-input"/>
                                </div>
                            </div>

                            <div class="uk-grid">
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <?php $country = \App\Models\Country::all() ?>
                                    <select id="select_demo_1" data-md-selectize name="certi_country">
                                        <option value="">Country</option>
                                        @foreach($country as $row)
                                            <option value="{{ $row->id }}">
                                                {{ $row->country_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <input placeholder="Date Issued" name="certi_date" id="certi_date"/>
                                </div>
                            </div>

                            <div class="uk-modal-footer uk-text-right">
                                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                                <button type="button" id="edit_contact_info" onclick="add_new_other_qual()"
                                        class="md-btn uk-modal-close md-btn-flat md-btn-flat-primary">Submit
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-1">
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <div class="md-card-toolbar-actions">
                                <i class="md-icon material-icons md-color-red-500"
                                   data-uk-modal="{target:'#add_new_it_skill'}"
                                   data-uk-tooltip="{pos:'left'}"
                                   title="Add New It Skill">note_add</i>
                            </div>
                            <h3 class="md-card-toolbar-heading-text">
                                IT Skill
                            </h3>
                        </div>
                        <div class="md-card-content">
                            <?php $it_skill = SiteHelper::get_applicant_it_skill($applicant_information->cv_no) ?>
                            <div class="uk-grid" data-uk-grid-margin>
                                @foreach($it_skill as $it)
                                    <div>
                                        <div class="md-card-primary">
                                            <div class="uk-button-dropdown" data-uk-dropdown>
                                                <div class="md-card-content">
                                                    <p class="">
                                                        <b> {{ $it->topic_app_name }} </b>
                                                    </p>
                                                    <p>
                                                        {{ \App\Models\IT::where('id',$it->it_type)->pluck('type_name') }}
                                                    </p>
                                                    <p>
                                                        {{ \Illuminate\Support\Facades\DB::table('it_lvl')->where('id',$it->it_skill)->pluck('it_lvl_name')  }}
                                                    </p>
                                                </div>
                                                <div class="uk-dropdown uk-dropdown-small">
                                                    <ul class="uk-nav uk-nav-dropdown">
                                                        <li><a href="#"
                                                               data-uk-modal="{target:'#edit_it_skill{{ $it->id }}'}">Edit</a>
                                                        </li>
                                                        <li><a href="#" onclick="delete_it({{ $it->id }})">Delete</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="uk-modal" style="z-index: 998" id="edit_it_skill{{ $it->id }}">
                                        <div class="uk-modal-dialog">
                                            <div class="uk-modal-header">
                                                <h3 class="uk-modal-title">Edit It Skill</h3>
                                            </div>
                                            <form action="#" id="edit_it_data{{ $it->id }}">
                                                <div class="uk-grid">
                                                    <input type="hidden" name="cv_no"
                                                           value="{{ $applicant_information->cv_no }}">
                                                    <input type="hidden" name="it_id" value="{{ $it->id }}">
                                                    <div class="uk-width-medium-1-1 parsley-row">
                                                        <label for="wizard_vehicle_registration_address">
                                                            Application, topic or language
                                                        </label>
                                                        <input type="text" name="topic_app"
                                                               value="{{ $it->topic_app_name }}"
                                                               id="wizard_vehicle_registration_address"
                                                               class="md-input"/>
                                                    </div>
                                                </div>

                                                <div class="uk-grid">
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <?php $its = \App\Models\IT::all() ?>
                                                        <select id="select_demo_1" data-md-selectize name="it_type">
                                                            <option value="">Type</option>
                                                            @foreach($its as $row)
                                                                <option @if($it->it_type == $row->id) selected
                                                                        @endif value="{{ $row->id }}">
                                                                    {{ $row->type_name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <?php $it_lvl = \App\Models\IT::it_lvl() ?>
                                                        <select id="select_demo_1" data-md-selectize
                                                                name="it_skill_level">
                                                            <option value="">Skill Level</option>
                                                            @foreach($it_lvl as $row)
                                                                <option @if($it->it_skill == $row->id ) selected
                                                                        @endif value="{{ $row->id }}">
                                                                    {{ $row->it_lvl_name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="uk-modal-footer uk-text-right">
                                                    <button type="button" class="md-btn md-btn-flat uk-modal-close">
                                                        Close
                                                    </button>
                                                    <button type="button" id="edit_contact_info"
                                                            onclick="edit_it_skill({{ $it->id }})"
                                                            class="md-btn uk-modal-close md-btn-flat md-btn-flat-primary">
                                                        Submit
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                <div class="uk-modal" style="z-index: 998" id="add_new_it_skill">
                    <div class="uk-modal-dialog">
                        <div class="uk-modal-header">
                            <h3 class="uk-modal-title">Add New It Skill</h3>
                        </div>
                        <form action="#" id="new_other_it_data">
                            <div class="uk-grid">
                                <input type="hidden" name="cv_no" value="{{ $applicant_information->cv_no }}">
                                <div class="uk-width-medium-1-1 parsley-row">
                                    <label for="wizard_vehicle_registration_address">
                                        Application, topic or language
                                    </label>
                                    <input type="text" name="topic_app"
                                           id="wizard_vehicle_registration_address" class="md-input"/>
                                </div>
                            </div>

                            <div class="uk-grid">
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <?php $it = \App\Models\IT::all() ?>
                                    <select id="select_demo_1" data-md-selectize name="it_type">
                                        <option value="">Type</option>
                                        @foreach($it as $row)
                                            <option value="{{ $row->id }}">
                                                {{ $row->type_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <?php $it_lvl = \App\Models\IT::it_lvl() ?>
                                    <select id="select_demo_1" data-md-selectize name="it_skill_level">
                                        <option value="">Skill Level</option>
                                        @foreach($it_lvl as $row)
                                            <option value="{{ $row->id }}">
                                                {{ $row->it_lvl_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="uk-modal-footer uk-text-right">
                                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                                <button type="button" id="edit_contact_info" onclick="add_new_it_skill()"
                                        class="md-btn uk-modal-close md-btn-flat md-btn-flat-primary">Submit
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-1">
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <div class="md-card-toolbar-actions">
                                <i class="md-icon material-icons md-color-red-500"
                                   data-uk-modal="{target:'#add_new_lang_skill'}"
                                   data-uk-tooltip="{pos:'left'}"
                                   title="Add New Language Skill">note_add</i>
                            </div>
                            <h3 class="md-card-toolbar-heading-text">
                                Language Skill
                            </h3>
                        </div>
                        <div class="md-card-content">
                            <p class="sub-heading">
                                <b>Basic</b> - I can have a simple conversation and understand the written language
                            </p>
                            <p class="sub-heading">
                                <b>Conversational</b> - I can use the language for Conversational, read documents
                                and be interviewed
                            </p>
                            <p class="sub-heading">
                                <b>Fluent</b> - I can read, write and speak fluently in this language without any
                                mistakes
                            </p>
                            <p class="sub-heading">
                                <b>Native</b> - Mother tongue
                            </p>
                            <?php $lang_skill = SiteHelper::get_applicant_lang_skill($applicant_information->cv_no) ?>
                            <div class="uk-grid" data-uk-grid-margin>
                                @foreach($lang_skill as $lang)
                                    <div>
                                        <div class="md-card-primary">
                                            <div class="uk-button-dropdown" data-uk-dropdown>
                                                <div class="md-card-content">
                                                    <p class="">
                                                        <b>  {{ \App\Models\Language::where('id', $lang->language_name )->pluck('lang_name')}} </b>
                                                    </p>
                                                    <p class="">
                                                        {{ \Illuminate\Support\Facades\DB::table('language_lavel')->where('id',$lang->language_skill)->pluck('lang_lvl')  }}
                                                    </p>
                                                    <p>
                                                        {{ $lang->language_other }}
                                                    </p>
                                                </div>
                                                <div class="uk-dropdown uk-dropdown-small">
                                                    <ul class="uk-nav uk-nav-dropdown">
                                                        <li><a href="#"
                                                               data-uk-modal="{target:'#edit_lang_skill{{ $lang->id }}'}">Edit</a>
                                                        </li>
                                                        <li><a href="#"
                                                               onclick="delete_lang({{ $lang->id }})">Delete</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="uk-modal" style="z-index: 998" id="edit_lang_skill{{ $lang->id }}">
                                        <div class="uk-modal-dialog">
                                            <div class="uk-modal-header">
                                                <h3 class="uk-modal-title">Add New Language Skill</h3>
                                            </div>
                                            <form action="#" id="edit_lang_data{{ $lang->id }}">
                                                <div class="uk-grid">
                                                    <input type="hidden" name="cv_no"
                                                           value="{{ $applicant_information->cv_no }}">
                                                    <input type="hidden" name="lang_id" value="{{ $lang->id }}">
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <?php $language = \App\Models\Language::all() ?>
                                                        <select id="select_demo_1" data-md-selectize name="language">
                                                            <option value="">Language</option>
                                                            @foreach($language as $row)
                                                                <option @if( $lang->language_name == $row->id) selected
                                                                        @endif value="{{ $row->id }}">
                                                                    {{ $row->lang_name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <?php $language_lvl = \App\Models\Language::language_lvl() ?>
                                                        <select id="select_demo_1" data-md-selectize
                                                                name="language_skill">
                                                            <option value="">Skill Level</option>
                                                            @foreach($language_lvl as $row)
                                                                <option @if( $lang->language_skill == $row->id) selected
                                                                        @endif value="{{ $row->id }}">
                                                                    {{ $row->lang_lvl }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="uk-grid">
                                                    <div class="uk-width-medium-1-1 parsley-row">
                                                        <textarea name="language_other" class="md-input" id="" placeholder="Other ( Optional )" cols="30" rows="3">{{ $lang->language_name  }}</textarea>
                                                    </div>
                                                </div>

                                                <div class="uk-modal-footer uk-text-right">
                                                    <button type="button" class="md-btn md-btn-flat uk-modal-close">
                                                        Close
                                                    </button>
                                                    <button type="button" id="edit_contact_info"
                                                            onclick="edit_lang_skill({{ $lang->id }})"
                                                            class="md-btn uk-modal-close md-btn-flat md-btn-flat-primary">
                                                        Submit
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>

                <div class="uk-modal" style="z-index: 998" id="add_new_lang_skill">
                    <div class="uk-modal-dialog">
                        <div class="uk-modal-header">
                            <h3 class="uk-modal-title">Add New Language Skill</h3>
                        </div>
                        <form action="#" id="new_lang_data">
                            <div class="uk-grid">
                                <input type="hidden" name="cv_no" value="{{ $applicant_information->cv_no }}">
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <?php $language = \App\Models\Language::all() ?>
                                    <select id="select_demo_1" data-md-selectize name="language">
                                        <option value="">Language</option>
                                        @foreach($language as $row)
                                            <option value="{{ $row->id }}">
                                                {{ $row->lang_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <?php $language_lvl = \App\Models\Language::language_lvl() ?>
                                    <select id="select_demo_1" data-md-selectize name="language_skill">
                                        <option value="">Skill Level</option>
                                        @foreach($language_lvl as $row)
                                            <option value="{{ $row->id }}">
                                                {{ $row->lang_lvl }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="uk-grid">
                                <div class="uk-width-medium-1-1 parsley-row">
                                    <textarea name="language_other" class="md-input" id="" placeholder="Other ( Optional )" cols="30" rows="3"></textarea>
                                </div>
                            </div>

                            <div class="uk-modal-footer uk-text-right">
                                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                                <button type="button" id="edit_contact_info" onclick="add_new_language_skill()"
                                        class="md-btn uk-modal-close md-btn-flat md-btn-flat-primary">Submit
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-1">
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <div class="md-card-toolbar-actions">
                                <i class="md-icon material-icons md-color-red-500"
                                   data-uk-modal="{target:'#add_new_work_exp'}"
                                   data-uk-tooltip="{pos:'left'}"
                                   title="Add Working Experience">note_add</i>
                            </div>
                            <h3 class="md-card-toolbar-heading-text">
                                Working Experience
                            </h3>
                        </div>
                        <div class="md-card-content">
                            <?php $applicant_work_exp = SiteHelper::get_applicant_work_exp($applicant_information->cv_no) ?>
                            <div class="uk-grid " data-uk-grid-margin>
                                @foreach($applicant_work_exp as $work)
                                    <div>
                                        <div class="md-card-primary">
                                            <div class="uk-button-dropdown" data-uk-dropdown>
                                                <div class="md-card-content">
                                                    <p class="">
                                                        <b>Company Name</b> - {{ $work->exp_company_name }}
                                                    </p>
                                                    <p class="">
                                                        <b>Job Title</b> - {{ $work->exp_job_title }}
                                                    </p>
                                                    <p class="">
                                                        <b>Job Category</b>
                                                        - {{ \App\Models\Job_Category::where('id',$work->exp_job_category)->pluck('description') }}
                                                    </p>
                                                    <p class="">
                                                        <b>Job Industry</b>
                                                        - {{ \App\Models\Job_Industry::where('id',$work->exp_job_industry)->pluck('industry_description') }}
                                                    </p>
                                                    <p class="">
                                                        <b>Place</b> - {{ $work->exp_job_city }}
                                                        , {{ \App\Models\Country::where('id',$work->exp_job_country )->pluck('country_name') }}
                                                    </p>
                                                    <p class="">
                                                        <b>Worked Period</b> - {{ $work->exp_from_date }}
                                                        &nbsp;to&nbsp; {{ $work->exp_to_date }}
                                                    </p>
                                                    <p class="">
                                                        <b>Description</b> - {!!   $work->exp_description !!}
                                                    </p>
                                                </div>
                                                <div class="uk-dropdown uk-dropdown-small">
                                                    <ul class="uk-nav uk-nav-dropdown">
                                                        <li><a href="#"
                                                               data-uk-modal="{target:'#edit_cur_work_exp{{ $work->id }}'}">Edit</a>
                                                        </li>
                                                        <li><a href="#" onclick="delete_exp({{ $work->id }})">Delete</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="uk-modal" style="z-index: 998" id="edit_cur_work_exp{{ $work->id }}">
                                        <div class="uk-modal-dialog">
                                            <div class="uk-modal-header">
                                                <h3 class="uk-modal-title">Edit Working Experience</h3>
                                            </div>
                                            <form action="#" id="edit_work_exp_data{{ $work->id }}">
                                                <div class="uk-grid">
                                                    <input type="hidden" name="cv_no"
                                                           value="{{ $applicant_information->cv_no }}">
                                                    <input type="hidden" name="exp_id" value="{{ $work->id }}">
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <?php $category = \App\Models\Job_Category::all() ?>
                                                        <select id="select_demo_1" data-md-selectize
                                                                name="exp_job_category">
                                                            <option value="">JoB Category</option>
                                                            @foreach($category as $row)
                                                                <option @if($work->exp_job_category == $row->id ) selected
                                                                        @endif value="{{ $row->id }}">
                                                                    {{ $row->job_code }} - {{ $row->description }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <?php $industry = \App\Models\Job_Industry::all() ?>
                                                        <select id="select_demo_1" data-md-selectize
                                                                name="exp_job_idustry">
                                                            <option value="">JoB Industry</option>
                                                            @foreach($industry as $row)
                                                                <option @if($work->exp_job_industry == $row->id ) selected
                                                                        @endif value="{{ $row->id }}">
                                                                    {{ $row->industry_description }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class="uk-grid">
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <?php $country = \App\Models\Country::all() ?>
                                                        <select id="select_demo_1" data-md-selectize name="exp_country">
                                                            <option value="">Country</option>
                                                            @foreach($country as $row)
                                                                <option @if($work->exp_job_country == $row->id ) selected
                                                                        @endif value="{{ $row->id }}">
                                                                    {{ $row->country_name }}
                                                                </option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <label for="wizard_birth">
                                                            City
                                                    <span class="req">
                                                        *
                                                    </span>
                                                        </label>
                                                        <input type="text" name="exp_city" id="wizard_fullname"
                                                               value="{{ $work->exp_job_city }}"
                                                               class="md-input"/>
                                                    </div>
                                                </div>

                                                <div class="uk-grid">
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <label for="wizard_birth">
                                                            Company Name
                                                    <span class="req">
                                                        *
                                                    </span>
                                                        </label>
                                                        <input type="text" name="exp_company_name" id="wizard_fullname"
                                                               value="{{ $work->exp_company_name }}"
                                                               class="md-input"/>
                                                    </div>
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <label for="wizard_birth">
                                                            JoB Title
                                                    <span class="req">
                                                        *
                                                    </span>
                                                        </label>
                                                        <input type="text" name="exp_job_title" id="wizard_fullname"
                                                               value="{{ $work->exp_job_title }}"
                                                               class="md-input"/>
                                                    </div>
                                                </div>

                                                <div class="uk-grid">
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <input placeholder="From Date" name="from_date"
                                                               id="edit_from_date{{ $work->id }}"
                                                               value="{{ $work->exp_from_date }}"/>
                                                    </div>
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <input placeholder="To Date" name="to_date"
                                                               id="edit_to_date{{ $work->id }}"
                                                               value="{{ $work->exp_to_date }}"/>
                                                    </div>
                                                </div>

                                                <div class="uk-grid">
                                                    <div class="uk-width-medium-1-1 parsley-row">
                                                        <label for="wizard_address">
                                                            Description
                                                    <span class="req">
                                                        *
                                                    </span>
                                                        </label>
                                        <textarea name="description" id="work{{ $work->id }}"
                                                  style="overflow-x: hidden; word-wrap: break-word; height: 121px;"
                                                  cols="30" rows="4"
                                                  class="md-input autosize_init work">{{ $work->exp_description }}</textarea>
                                                    </div>
                                                </div>
                                                <div class="uk-modal-footer uk-text-right">
                                                    <button type="button" class="md-btn md-btn-flat uk-modal-close">
                                                        Close
                                                    </button>
                                                    <button type="button" id="edit_contact_info"
                                                            onclick="edit_work_exp({{ $work->id }})"
                                                            class="md-btn uk-modal-close md-btn-flat md-btn-flat-primary">
                                                        Submit
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-modal" style="z-index: 998" id="add_new_work_exp">
                    <div class="uk-modal-dialog">
                        <div class="uk-modal-header">
                            <h3 class="uk-modal-title">Add Working Experience</h3>
                        </div>
                        <form action="#" id="new_work_exp_data">
                            <div class="uk-grid">
                                <input type="hidden" name="cv_no" value="{{ $applicant_information->cv_no }}">
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <?php $category = \App\Models\Job_Category::all() ?>
                                    <select id="select_demo_1" data-md-selectize name="exp_job_category">
                                        <option value="">JoB Category</option>
                                        @foreach($category as $row)
                                            <option value="{{ $row->id }}">
                                                {{ $row->job_code }} - {{ $row->description }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <?php $industry = \App\Models\Job_Industry::orderBy('industry_description','asc')->get() ?>
                                    <select id="select_demo_1" data-md-selectize name="exp_job_idustry">
                                        <option value="">JoB Industry</option>
                                        @foreach($industry as $row)
                                            <option value="{{ $row->id }}">
                                                {{ $row->industry_description }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>

                            <div class="uk-grid">
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <?php $country = \App\Models\Country::all() ?>
                                    <select id="select_demo_1" data-md-selectize name="exp_country">
                                        <option value="">Country</option>
                                        @foreach($country as $row)
                                            <option value="{{ $row->id }}">
                                                {{ $row->country_name }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <label for="wizard_birth">
                                        City
                                                    <span class="req">
                                                        *
                                                    </span>
                                    </label>
                                    <input type="text" name="exp_city" id="wizard_fullname"
                                           class="md-input"/>
                                </div>
                            </div>

                            <div class="uk-grid">
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <label for="wizard_birth">
                                        Company Name
                                                    <span class="req">
                                                        *
                                                    </span>
                                    </label>
                                    <input type="text" name="exp_company_name" id="wizard_fullname"
                                           class="md-input"/>
                                </div>
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <label for="wizard_birth">
                                        JoB Title
                                                    <span class="req">
                                                        *
                                                    </span>
                                    </label>
                                    
                                    <input list="exp_job_title" id="wizard_fullname" name="exp_job_title" class="md-input">
                                        <?php $apply_job_pos = \App\Models\ApplicantWorkExperience::all() ?>
                                                        
                                        <datalist id="exp_job_title">
                                            @foreach($apply_job_pos as $row)
                                                <option value="{{$row->exp_job_title}}"/>  
                                            @endforeach
                                        </datalist>  
                                </div>
                            </div>

                            <div class="uk-grid">
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <input placeholder="From Date" name="from_date" id="from_date"/>
                                </div>
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <input placeholder="To Date" name="to_date" id="to_date"/>
                                </div>
                            </div>

                            <div class="uk-grid">
                                <div class="uk-width-medium-1-1 parsley-row">
                                    <label for="wizard_address">
                                        Description
                                                    <span class="req">
                                                        *
                                                    </span>
                                    </label>
                                        <textarea name="description" id="description"
                                                  style="overflow-x: hidden; word-wrap: break-word; height: 121px;"
                                                  cols="30" rows="4" class="md-input autosize_init"></textarea>
                                </div>
                            </div>
                            <div class="uk-modal-footer uk-text-right">
                                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                                <button type="button" id="edit_contact_info" onclick="add_new_work_exp()"
                                        class="md-btn uk-modal-close md-btn-flat md-btn-flat-primary">Submit
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-1">
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <div class="md-card-toolbar-actions">
                                <i class="md-icon material-icons md-color-red-500"
                                   data-uk-modal="{target:'#add_new_ref_person'}"
                                   data-uk-tooltip="{pos:'left'}"
                                   title="Add Reference Person">note_add</i>
                            </div>
                            <h3 class="md-card-toolbar-heading-text">
                                Reference Person
                            </h3>
                        </div>
                        <div class="md-card-content">
                            <?php $ref_person = SiteHelper::get_applicant_ref_person($applicant_information->cv_no) ?>
                            <div class="uk-grid" data-uk-grid-margin>
                                @foreach($ref_person as $ref)
                                    <div>
                                        <div class="md-card-primary">
                                            <div class="uk-button-dropdown" data-uk-dropdown>
                                                <div class="md-card-content">
                                                    <p class="">
                                                        <b>Name</b> - {{ $ref->reference_person_name }}
                                                    </p>
                                                    <p class="">
                                                        <b>Company</b> - {{ $ref->reference_person_company }}
                                                    </p>
                                                    <p class="">
                                                        <b>Position</b> - {{ $ref->reference_person_position }}
                                                    </p>
                                                    <p class="">
                                                        <b>Mobile No</b> - {{ $ref->reference_person_mobile }}
                                                    </p>
                                                    <p class="">
                                                        <b>Email</b> - {{ $ref->reference_person_email }}
                                                </div>
                                                <div class="uk-dropdown uk-dropdown-small">
                                                    <ul class="uk-nav uk-nav-dropdown">
                                                        <li><a href="#"
                                                               data-uk-modal="{target:'#edit_ref_person{{ $ref->id }}'}">Edit</a>
                                                        </li>
                                                        <li><a href="#" onclick="delete_ref({{ $ref->id }})">Delete</a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <br>
                                    <div class="uk-modal" style="z-index: 998" id="edit_ref_person{{ $ref->id }}">
                                        <div class="uk-modal-dialog">
                                            <div class="uk-modal-header">
                                                <h3 class="uk-modal-title">Edit Reference Person</h3>
                                            </div>
                                            <form action="#" id="edit_ref_data{{ $ref->id }}">
                                                <input type="hidden" name="cv_no"
                                                       value="{{ $applicant_information->cv_no }}">
                                                <input type="hidden" name="ref_id" value="{{ $ref->id }}">
                                                <div class="uk-grid" data-uk-grid-margin="">
                                                    <div class="uk-width-medium-1-3 parsley-row">
                                                        <label for="wizard_vehicle_registration_address">
                                                            Name
                                                        </label>
                                                        <input type="text" name="reference_person_name"
                                                               value="{{ $ref->reference_person_name }}"
                                                               id="wizard_vehicle_registration_address"
                                                               class="md-input"/>
                                                    </div>
                                                    <div class="uk-width-medium-1-3 parsley-row">
                                                        <label for="wizard_vehicle_registration_address">
                                                            Position
                                                        </label>
                                                        <input type="text" name="reference_person_position"
                                                               value="{{ $ref->reference_person_position }}"
                                                               id="wizard_vehicle_registration_address"
                                                               class="md-input"/>
                                                    </div>
                                                    <div class="uk-width-medium-1-3 parsley-row">
                                                        <label for="wizard_vehicle_registration_address">
                                                            Company Name
                                                        </label>
                                                        <input type="text" name="reference_person_company"
                                                               value="{{ $ref->reference_person_company }}"
                                                               id="wizard_vehicle_registration_address"
                                                               class="md-input"/>
                                                    </div>
                                                </div>

                                                <div class="uk-grid">
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <label for="wizard_birth">
                                                            Mobile No
                                                    <span class="req">
                                                        *
                                                    </span>
                                                        </label>
                                                        <input type="text" name="reference_person_mobile"
                                                               id="wizard_fullname"
                                                               value="{{ $ref->reference_person_mobile }}"
                                                               class="md-input"/>
                                                    </div>
                                                    <div class="uk-width-medium-1-2 parsley-row">
                                                        <label for="wizard_birth">
                                                            Email
                                                    <span class="req">
                                                        *
                                                    </span>
                                                        </label>
                                                        <input type="text" name="reference_person_email"
                                                               id="wizard_fullname"
                                                               value="{{ $ref->reference_person_email }}"
                                                               class="md-input"/>
                                                    </div>
                                                </div>

                                                <div class="uk-modal-footer uk-text-right">
                                                    <button type="button" class="md-btn md-btn-flat uk-modal-close">
                                                        Close
                                                    </button>
                                                    <button type="button" id="edit_contact_info"
                                                            onclick="edit_ref_person({{ $ref->id }})"
                                                            class="md-btn uk-modal-close md-btn-flat md-btn-flat-primary">
                                                        Submit
                                                    </button>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-modal" style="z-index: 998" id="add_new_ref_person">
                    <div class="uk-modal-dialog">
                        <div class="uk-modal-header">
                            <h3 class="uk-modal-title">Add New Reference Person</h3>
                        </div>
                        <form action="#" id="new_ref_data">
                            <input type="hidden" name="cv_no" value="{{ $applicant_information->cv_no }}">

                            <div class="uk-grid" data-uk-grid-margin="">
                                <div class="uk-width-medium-1-3 parsley-row">
                                    <label for="wizard_vehicle_registration_address">
                                        Name
                                    </label>
                                    <input type="text" name="reference_person_name"
                                           id="wizard_vehicle_registration_address" class="md-input"/>
                                </div>
                                <div class="uk-width-medium-1-3 parsley-row">
                                    <label for="wizard_vehicle_registration_address">
                                        Position
                                    </label>
                                    <input type="text" name="reference_person_position"
                                           id="wizard_vehicle_registration_address" class="md-input"/>
                                </div>
                                <div class="uk-width-medium-1-3 parsley-row">
                                    <label for="wizard_vehicle_registration_address">
                                        Company Name
                                    </label>
                                    <input type="text" name="reference_person_company"
                                           id="wizard_vehicle_registration_address" class="md-input"/>
                                </div>
                            </div>

                            <div class="uk-grid">
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <label for="wizard_birth">
                                        Mobile No
                                                    <span class="req">
                                                        *
                                                    </span>
                                    </label>
                                    <input type="text" name="reference_person_mobile" id="wizard_fullname"
                                           class="md-input"/>
                                </div>
                                <div class="uk-width-medium-1-2 parsley-row">
                                    <label for="wizard_birth">
                                        Email
                                                    <span class="req">
                                                        *
                                                    </span>
                                    </label>
                                    <input type="text" name="reference_person_email" id="wizard_fullname"
                                           class="md-input"/>
                                </div>
                            </div>

                            <div class="uk-modal-footer uk-text-right">
                                <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                                <button type="button" id="edit_contact_info" onclick="add_ref_person()"
                                        class="md-btn uk-modal-close md-btn-flat md-btn-flat-primary">Submit
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-1">
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <div class="md-card-toolbar-actions">
                                <i class="md-icon material-icons md-color-red-500"
                                   data-uk-modal="{target:'#add_new_attachment'}"
                                   data-uk-tooltip="{pos:'left'}"
                                   title="Add New Attachment">note_add</i>
                            </div>
                            <h3 class="md-card-toolbar-heading-text">
                                Attachment
                            </h3>
                        </div>
                        <div class="md-card-content">
                            <?php $attachment_name = \App\Models\ApplicantAttachment::where('cv_id', $applicant_information->cv_no)->get() ?>
                            <div id="user_profile_gallery" data-uk-check-display
                                 class="uk-grid-width-small-1-2 uk-grid-width-medium-1-3 uk-grid-width-large-1-4"
                                 data-uk-grid="{gutter: 4}">
                                @foreach($attachment_name as $image)
                                    <div>
                                        @if( $image->ext == 'docx' || $image->ext == 'doc' || $image->ext == 'docm' )
                                            <img src="http://collegeareabid.com/wp-content/uploads/2014/03/Docs-Icon.png"
                                                 alt=""/>
                                        @elseif($image->ext == 'pdf')
                                            <img src="https://cdn1.iconfinder.com/data/icons/miscellaneous-circle-style/48/book-512.png"
                                                 alt=""/>
                                        @else
                                            <img src="{{ url('upload/attachment/').'/'.$image->attachment }}" alt=""/>
                                        @endif
                                        <div class="gallery_grid_image_caption">
                                            <p>{{ $image->filename }}</p>
                                            <div class="gallery_grid_image_menu"
                                                 style="margin-top: -50px;background: #fff;border-radius: 100%;"
                                                 data-uk-dropdown="{pos:'top-right'}">
                                                <i class="md-icon material-icons">&#xE5D4;</i>
                                                <div class="uk-dropdown uk-dropdown-small">
                                                    <ul class="uk-nav">
                                                        <li><a href="#" onclick="delete_attach({{ $image->id }})"><i
                                                                        class="material-icons uk-margin-small-right">
                                                                    &#xE872;</i> Remove</a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-modal" id="add_new_attachment">
                    <div class="uk-modal-dialog">
                        <div class="uk-modal-header">
                            <h3 class="uk-modal-title">Edit Cover Image</h3>
                        </div>

                        {!! Form::open(array('url' => 'cv/add_new_attach_image' , 'id' => 'add_new_attachment_data' , 'files'=>true)) !!}
                        <div class="uk-grid">
                            <input type="hidden" name="cv_no" value="{{ $applicant_information->cv_no }}">
                            <input type="hidden" id="attach_image" name="attach_image" value="">
                            <div class="uk-width-medium-1-1 parsley-row">
                                <img style="height: 400px;width: 551px;border-radius: 10px"
                                     src="" id="attach_original_image" alt=""/>
                            </div>
                        </div>
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-1 parsley-row">
                                <label for="filename">Enter FileName</label>
                                <input type="text" name="filename" required class="md-input" id="filename">
                            </div>
                        </div>
                        <div class="uk-grid" data-uk-grid-margin>
                            <div class="uk-width-medium-1-1 parsley-row">
                                <a class="uk-form-file md-btn">choose file<input
                                            id="file_upload-select-cover_attach" onchange="attchImage()"
                                            name="attachment"
                                            type="file"></a>
                            </div>
                        </div>
                        <div class="uk-modal-footer uk-text-right">
                            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                            <button type="submit" id="edit_contact_info"
                                    class="md-btn md-btn-flat md-btn-flat-primary">Submit
                            </button>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>

            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-2-5">
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <div class="md-card-toolbar-actions">
                                <i class="md-icon material-icons md-color-light-blue-500"
                                   data-uk-modal="{target:'#edit_cover_image'}"
                                   data-uk-tooltip="{pos:'left'}"
                                   title="Edit Cover Image"
                                >mode_edit</i>
                            </div>
                            <h3 class="md-card-toolbar-heading-text">
                                Cover Image
                            </h3>
                        </div>
                        <div class="md-card-content">
                            <?php $attachment_name = \App\Models\ApplicantCover::where('cv_id', $applicant_information->cv_no)->get() ?>
                            @foreach($attachment_name as $image)
                                <div>
                                    <a href="{{ @$image->cover_image }}" data-uk-lightbox="{group:'user-photos'}">
                                        <img style="height: 100%;width: 100%;border-radius: 10px"
                                             src="{{ @$image->cover_image }}" alt=""/>
                                    </a>
                                </div>
                            @endforeach

                        </div>
                        <div class="uk-modal" id="edit_cover_image">
                            <div class="uk-modal-dialog">
                                <div class="uk-modal-header">
                                    <h3 class="uk-modal-title">Edit Cover Image</h3>
                                </div>
                                <form action="#" id="edit_cover_image_data">
                                    <div class="uk-grid">
                                        <input type="hidden" name="cv_no" value="{{ $applicant_information->cv_no }}">
                                        <input type="hidden" id="cover_image" name="cover_image" value="">
                                        <div class="uk-width-medium-1-1 parsley-row">
                                            <img style="height: 400px;width: 551px;border-radius: 10px"
                                                 src="{{ @$image->cover_image }}" id="original_image" alt=""/>
                                        </div>
                                    </div>
                                    <div class="uk-grid" data-uk-grid-margin>
                                        <div class="uk-width-medium-1-1 parsley-row">
                                            <a class="uk-form-file md-btn">choose file<input
                                                        id="file_upload-select-cover" onchange="coverImage()"
                                                        type="file"></a>
                                        </div>
                                    </div>
                                    <div class="uk-modal-footer uk-text-right">
                                        <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                                        <button type="button" id="edit_contact_info" onclick="edit_cover_image()"
                                                class="md-btn uk-modal-close md-btn-flat md-btn-flat-primary">Submit
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="uk-width-medium-3-5">
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <div class="md-card-toolbar-actions">
                                <i class="md-icon material-icons md-color-light-blue-500"
                                   data-uk-modal="{target:'#edit_cover_letter'}"
                                   data-uk-tooltip="{pos:'left'}"
                                   title="Edit Cover Letter">mode_edit</i>
                            </div>
                            <h3 class="md-card-toolbar-heading-text">
                                Cover Letter
                            </h3>
                        </div>
                        <div class="md-card-content">
                            {{  strip_tags(\App\Models\ApplicantCover::where('cv_id',$applicant_information->cv_no)->pluck('cover_letter') )}}
                        </div>
                        <div class="uk-modal" style="z-index: 998" id="edit_cover_letter">
                            <div class="uk-modal-dialog">
                                <div class="uk-modal-header">
                                    <h3 class="uk-modal-title">Edit Cover Letter</h3>
                                </div>
                                <form action="#" id="edit_cover_letter_data">
                                    <div class="uk-grid">
                                        <input type="hidden" name="cv_no" value="{{ $applicant_information->cv_no }}">
                                        <div class="uk-grid" data-uk-grid-margin>
                                            <div class="uk-width-medium-1-1 parsley-row">
                                                <textarea id="wysiwyg_tinymce" name="cover_letter" cols="30"
                                                          rows="20">{{\App\Models\ApplicantCover::where('cv_id',$applicant_information->cv_no)->pluck('cover_letter')}}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="uk-modal-footer uk-text-right">
                                        <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                                        <button type="button" id="edit_contact_info" onclick="edit_cover_letter()"
                                                class="md-btn uk-modal-close md-btn-flat md-btn-flat-primary">Submit
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <script src="{{ asset('backend/js/cv.js') }}"></script>
    <script>

       function select_all() {
           var $mailbox = $('#pdf_data');
           if ($mailbox.find('[data-md-icheck]').is(':checked')) {
               $mailbox.find('[data-md-icheck]').iCheck('uncheck')
           } else {
               $mailbox.find('[data-md-icheck]').iCheck('check')
           }
       }

    </script>
@stop            