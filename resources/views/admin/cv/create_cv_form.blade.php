@extends('admin.layout.app')
@section('content')
    <div id="page_content">
        <div id="page_content_inner">
            <input type="hidden" id="url" value="{{ url('cv') }}">
            <script>
                tinymce.init({
                    skin_url: '/../../backend/skins/tinymce/material_design',
                    selector: '#wysiwyg_tinymce',
                    height: 200,
                    plugins: [
                        "advlist autolink lists link image charmap print preview anchor",
                        "searchreplace visualblocks code fullscreen",
                        "insertdatetime media table contextmenu paste "
                    ],
                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media "
                });

            </script>

            <div class="md-card uk-margin-large-bottom">
                <div class="md-card-content">
                    <form class="uk-form-stacked" id="wizard_advanced_form">
                        <div id="wizard_advanced" data-uk-observe>
                            <!-- first section -->
                            <h3>
                                Personal information
                            </h3>
                            <section>

                                <div class="uk-grid">
                                    <div class="uk-width-medium-2-10">
                                        <input type="hidden" id="applicant_image" name="applicant_image" value="">
                                        <div class="user_heading_avatar fileinput fileinput-new"
                                             data-provides="fileinput">
                                            <div class="fileinput-new thumbnail" style="padding-left: 50px ">
                                                <img src="{{ url('backend/img/blank.png') }}" alt="user avatar"/>
                                            </div>
                                            <div class="fileinput-preview fileinput-exists thumbnail "
                                                 style="padding-left: 50px "></div>
                                            <div class="user_avatar_controls">
                                        <span class="btn-file">
                                            <span class="fileinput-new"><i class="material-icons">&#xE2C6;</i></span>
                                            <span class="fileinput-exists"><i class="material-icons">&#xE86A;</i></span>
                                            <input type="file" name="applicant_image" onchange="encodeImageFileAsURL()"
                                                   id="user_edit_avatar_control">
                                        </span>
                                                <a href="#" style="margin-left: 45px" class="btn-file fileinput-exists"
                                                   data-dismiss="fileinput"><i
                                                            class="material-icons">&#xE5CD;</i></a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="uk-width-medium-8-10">
                                        <div class="uk-grid" data-uk-grid-margin>
                                            <div class="uk-width-medium-1-3 parsley-row">
                                                <label for="wizard_fullname">
                                                    Full Name
                                                    <span class="req">
                                                        *
                                                    </span>
                                                </label>
                                                <input type="text" name="applicant" id="wizard_fullname"
                                                       class="md-input"/>
                                            </div>
                                            <div class="uk-width-medium-1-3 parsley-row">
                                                <label for="wizard_fullname">
                                                    Father Name
                                                    <span class="req">
                                                        *
                                                    </span>
                                                </label>
                                                <input type="text" name="father_name" id="wizard_fullname"
                                                       class="md-input"/>
                                            </div>
                                            <div class="uk-width-medium-1-3 parsley-row">
                                                <label for="wizard_fullname">
                                                    NRC No
                                                    <span class="req">
                                                        *
                                                    </span>
                                                </label>
                                                <input type="text" name="nrc" id="wizard_fullname"
                                                       class="md-input"/>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="uk-grid">
                                    <div class="uk-width-medium-1-2 parsley-row">
                                        <label for="wizard_address">
                                            Address
                                                    <span class="req">
                                                        *
                                                    </span>
                                        </label>
                                        <input type="text" name="address" id="wizard_address"
                                               class="md-input"/>
                                    </div>
                                    <div class="uk-width-medium-1-2 parsley-row">
                                        <?php $country = \App\Models\Country::all() ?>
                                        <select id="selec_adv_2" name="cur_location">
                                            <option value="">Location</option>
                                            @foreach($country as $row)
                                                <option value="{{ $row->id }}">
                                                    {{ $row->country_name }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-3 parsley-row">
                                        <label for="wizard_birth">
                                            Birth Date
                                                    <span class="req">
                                                        *
                                                    </span>
                                        </label>
                                        <input type="text" name="dob" id=""
                                               class="md-input"
                                               data-parsley-date-message="This value should be a valid date"
                                               data-uk-datepicker="{format:'YYYY-MM-DD'}"/>
                                    </div>
                                    <div class="uk-width-medium-1-3 parsley-row">
                                        <label class="uk-form-label">
                                            Gender
                                                    <span class="req">
                                                        *
                                                    </span>
                                        </label>
                                                <span class="icheck-inline">
                                                    <input type="radio" name="gender" id="wizard_status_male"
                                                           class="wizard-icheck" value="1"/>
                                                    <label for="wizard_status_male" class="inline-label">
                                                        Male
                                                    </label>
                                                </span>
                                                <span class="icheck-inline">
                                                    <input type="radio" name="gender" id="wizard_status_female"
                                                           class="wizard-icheck" value="2"/>
                                                    <label for="wizard_status_female" class="inline-label">
                                                        Female
                                                    </label>
                                                </span>
                                    </div>
                                    <div class="uk-width-medium-1-3 parsley-row">
                                        <label class="uk-form-label">
                                            Martial Status
                                                    <span class="req">
                                                        *
                                                    </span>
                                        </label>
                                                <span class="icheck-inline">
                                                    <input type="radio" name="martial_status" id="wizard_status_married"
                                                           class="wizard-icheck" value="1"/>
                                                    <label for="wizard_status_married" class="inline-label">
                                                        Married
                                                    </label>
                                                </span>
                                                <span class="icheck-inline">
                                                    <input type="radio" name="martial_status" id="wizard_status_single"
                                                           class="wizard-icheck" value="2"/>
                                                    <label for="wizard_status_single" class="inline-label">
                                                        Single
                                                    </label>
                                                </span>
                                    </div>
                                </div>

                                <div class="uk-grid uk-grid-width-medium-1-2 uk-grid-width-large-1-4"
                                     data-uk-grid-margin>
                                    <div class="parsley-row">
                                        <div class="uk-input-group">
                                            <label for="wizard_phone">
                                                Weight
                                            </label>
                                            <input type="text" class="md-input" name="weight" id="wizard_phone"/>
                                        </div>
                                    </div>
                                    <div class=" parsley-row">
                                        <div class="uk-input-group">
                                            <label for="wizard_email">
                                                Height
                                            </label>
                                            <input type="text" class="md-input" name="height" id="wizard_email"/>
                                        </div>
                                    </div>
                                    <div class="parsley-row">
                                        <div class="uk-input-group">
                                            <label for="wizard_skype">
                                                Nationality
                                            </label>
                                            <input type="text" class="md-input" name="nationality" id="wizard_skype"/>
                                        </div>
                                    </div>
                                    <div class="parsley-row">
                                        <div class="uk-input-group">
                                            <label for="wizard_skype">
                                                Religion
                                            </label>
                                            <input type="text" class="md-input" name="religion" id="wizard_skype"/>
                                        </div>
                                    </div>
                                </div>

                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-3 parsley-row">
                                        <label for="wizard_birth">
                                            Last Position
                                                    <span class="req">
                                                        *
                                                    </span>
                                        </label>
                                      <!--   <input type="text" name="apply_position" id="wizard_fullname"
                                               class="md-input"/>   -->
                                        <input type="text" name="current_position" class="md-input">
                                               
                                    </div>
                                    <div class="uk-width-medium-1-3 parsley-row">
                                        <label for="wizard_birth">
                                            Last Company Name
                                                    <span class="req">
                                                        *
                                                    </span>
                                        </label>
                                        <input type="text" name="current_company_name" id="wizard_fullname"
                                               class="md-input"/>
                                    </div>
                                    <div class="uk-width-medium-1-3 parsley-row">
                                       <!--  <label for="wizard_birth">
                                            Salary Expectations
                                                    <span class="req">
                                                        *
                                                    </span>
                                        </label> -->
                                       
                                        <?php $salary = \App\Models\Salary::all() ?>
                                        <select id="wizard_fullname" name="exp_salary">
                                            <option value="">Select Expected Salary</option>
                                            @foreach($salary as $sal)
                                                <option value="{{ $sal->id }}">{{ $sal->salary }}</option>
                                            @endforeach
                                        </select>
                                       <!--  <input type="text" name="exp_salary" id="wizard_fullname"
                                               class="md-input"/> -->
                                    </div>
                                </div>

                                <div class="uk-grid">
                                    <div class="uk-width-medium-1-4 parsley-row">
                                        <label class="uk-form-label">
                                            Salary Negotiable
                                                    <span class="req">
                                                        *
                                                    </span>
                                        </label>
                                                <span class="icheck-inline">
                                                    <input type="radio" name="negotiable" id="negotiable_yes"
                                                           class="wizard-icheck" value="1"/>
                                                    <label for="negotiable_yes" class="inline-label">
                                                        Yes
                                                    </label>
                                                </span>
                                                <span class="icheck-inline">
                                                    <input type="radio" name="negotiable" id="negotiable_no"
                                                           class="wizard-icheck" value="2"/>
                                                    <label for="negotiable_no" class="inline-label">
                                                        No
                                                    </label>
                                                </span>
                                    </div>
                                    <div class="uk-width-medium-1-4 parsley-row">
                                        <label class="uk-form-label">
                                            Currency
                                                    <span class="req">
                                                        *
                                                    </span>
                                        </label>
                                                <span class="icheck-inline">
                                                    <input type="radio" name="currency" id="currency_dollar"
                                                           class="wizard-icheck" value="1"/>
                                                    <label for="currency_dollar" class="inline-label">
                                                        Dollar
                                                    </label>
                                                </span>
                                                <span class="icheck-inline">
                                                    <input type="radio" name="currency" id="currency_mmk"
                                                           class="wizard-icheck" value="2"/>
                                                    <label for="currency_mmk" class="inline-label">
                                                        MMK
                                                    </label>
                                                </span>
                                    </div>
                                </div>

                                <div class="uk-grid">
                                    <div class="uk-width-medium-1-1 parsley-row">
                                        <label for="wizard_address">
                                            Noticed Period
                                                    <span class="req">
                                                        *
                                                    </span>
                                        </label>
                                        <input type="text" name="noticed_period" id="wizard_address"
                                               class="md-input"/>
                                    </div>
                                </div>

                                <span class="uk-alert uk-alert-info">
                                    Contact information
                                </span>
                                <div class="uk-grid "
                                     data-uk-grid-margin>
                                    <div class="uk-width-medium-1-3 parsley-row">

                                        <label for="wizard_phone">
                                            Phone Number
                                        </label>
                                        <input type="text" class="md-input" name="contact_number" id="wizard_phone"/>

                                    </div>
                                    <div class="uk-width-medium-1-3 parsley-row">

                                        <label for="wizard_phone">
                                            Secondary Phone Number
                                        </label>
                                        <input type="text" class="md-input" name="sec_contact_number"
                                               id="wizard_phone"/>

                                    </div>
                                    <div class="uk-width-medium-1-3 parsley-row">

                                        <label for="wizard_email">
                                            Email
                                        </label>
                                        <input type="text" class="md-input" name="email" id="wizard_email"/>

                                    </div>
                                </div>

                                <span class="uk-alert uk-alert-info">
                                    Career Objective
                                </span>
                                <div class="uk-grid">
                                    <div class="uk-width-medium-1-1 parsley-row">
                                        <label for="wizard_address">
                                            Career Objective
                                                    <span class="req">
                                                        *
                                                    </span>
                                        </label>
                                        <input type="text" name="c_objective" id="wizard_address"
                                               class="md-input"/>
                                    </div>
                                </div>

                                <span class="uk-alert uk-alert-info">
                                    Apply Job Position
                                </span>
                                <div class="uk-grid">
                                    <div class="uk-width-medium-1-2 parsley-row">
                                        <?php $category = \App\Models\Job_Category::orderBy('description','asc')->get() ?>
                                        <select id="selec_adv_2" name="job_category">
                                            <option value="">JoB Category</option>
                                            @foreach($category as $row)
                                                <option value="{{ $row->id }}">
                                                    {{ $row->job_code }} - {{ $row->description }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="uk-width-medium-1-2 parsley-row">
                                        <?php $industry = \App\Models\Job_Industry::orderBy('industry_description','asc')->get() ?>
                                        <select id="selec_adv_2" name="job_industry">
                                            <option value="">JoB Industry</option>
                                            @foreach($industry as $row)
                                                <option value="{{ $row->id }}">
                                                    {{ $row->industry_description }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="uk-grid" data-uk-grid-margin>
                                    <div class="uk-width-medium-1-3 parsley-row">
                                        <select id="selec_adv_2" name="target_location">
                                            <option value="">Location</option>
                                            <option value="1">Local</option>
                                            <option value="2">Oversea</option>
                                        </select>
                                    </div>
                                    <div class="uk-width-medium-1-3 parsley-row">
                                        <?php $job_type = \App\Models\JobType::all() ?>
                                        <select id="selec_adv_2" name="job_type">
                                            <option value="">JoB Type</option>
                                            @foreach($job_type as $row)
                                                <option value="{{ $row->id }}">
                                                    {{ $row->type }}
                                                </option>
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="uk-width-medium-1-3 parsley-row">
                                        <label for="wizard_birth">
                                            Apply Position
                                                    <span class="req">
                                                        *
                                                    </span>
                                        </label>
                                        
                                        <input list="apply_position" name="apply_position" class="md-input">
                                        <?php $apply_job_pos = \App\Models\ApplicantJobPosition::all() ?>
                                        
                                        <datalist id="apply_position">
                                            @foreach($apply_job_pos as $row)
                                            <option value="{{$row->apply_position}}"/>  
                                            @endforeach
                                        </datalist>   
                                    </div>
                                </div>
                            </section>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
                                   
    <script>
        
        function coverImage() {
            var preview = '';
            var file = document.getElementById('file_upload-select-cover').files[0];
            var reader = new FileReader();

            reader.addEventListener("load", function () {
                $('#cover_image').val(reader.result);
                UIkit.notify({
                    message: "Upload Completed",
                    pos: 'top-right'
                });
            }, false);

            if (file) {
                reader.readAsDataURL(file);
            }
        }

        function encodeImageFileAsURL() {

            var preview = '';
            var file = document.getElementById('file_upload-select').files[0];
            var reader = new FileReader();

            reader.addEventListener("load", function () {
                $('#attachment').val(reader.result);
                UIkit.notify({
                    message: "Upload Completed",
                    pos: 'top-right'
                });
            }, false);

            if (file) {
                reader.readAsDataURL(file);
            }
        }
    </script>
@stop
