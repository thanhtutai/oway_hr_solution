<html lang="en">
<head>
    <title>Oway`s Online Database Management System</title>
    <link rel="stylesheet" href="{{ asset('front/css/cv.css') }}">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
    <style>
        #wrapper {
            width: 900px;
            margin: 0 auto;
            margin-top: 60px;
            margin-bottom: 100px;
        }

        .wrapper-top {
            width: 900px;
            height: 19px;
            margin: 0 auto;
            background-repeat: no-repeat;
        }

        .wrapper-mid {
            width: 900px;
            margin: 0 auto;
            background-repeat: repeat-y;
            padding-bottom: 40px;
        }

        .wrapper-bottom {
            width: 900px;
            height: 22px;
            padding: 0;
            margin: 0 auto;
            background-repeat: no-repeat;
        }

        /* PAPER BACKGROUND */ /* ----------------------------------------- */
        #paper {
            width: 800px;
            margin: 0 auto;
        }

        #wrapper, .wrapper-mid {
            background: none;
        }

        .wrapper-top, .wrapper-bottom {
            display: none;
        }

        .print-pp p {
            font-size: 15px;
        }

        h3 {
            font-size: 16px;
            color: #7dc24b;
        }

        h5 {
            font-weight: bold;
            font-size: 14px;
        }

        div.line {
            border-bottom: 2px solid #7dc24b;
        }

        .list-symbol-bullet li {
            line-height: 30px;
            list-style-type: none;
        }

        .address {
            margin-top: 20px;
        }

        td {
            font-size: 14px;
            padding: 5px;
        }

    </style>
</head>
<body style="background: #f0dfaf">

<div id="wrapper" class="DTTT" style="background: #fff">
    <div class="wrapper-top"></div>
    <div class="wrapper-mid">
        <!-- Begin Paper -->
        <div id="paper">

            <div class="modal-body text-body print-pp">

                <div class="row bigsidepadding">
                    <div class="col-md-4" style="margin-left: 15px;">
                        <img src="{{ url('img/oway.png') }}" width="180px" style="float: left;"/>
                    </div>
                    <div class="col-md-8 smallpadding"
                         style="float:right;padding: 10px;margin-right: -180px;">
                        <p>49/B, 1st Floor, Moe Sandar Street, Ward (1),<br> Kamayut, Yangon, Myanmar</p>
                        <span class="text-muted">Telephone:</span> 01 525011, 01 503196 <br>
                        <span class="text-muted">Email:</span> <a
                                href="#">cvs@owayhrsolutions
                            .com.mm<br></a>
                    </div>
                </div>

                <div style="margin-bottom:10px;padding: 1px;border-bottom: 2px solid #7dc24b;"></div>

                <?php
                if ($applicant_information->currency == '1') {
                    $salary = '$ ' . $applicant_information->salary_exp;
                } else {
                    $salary = $applicant_information->salary_exp . ' MMK';
                }
                ?>
                <div class="row bigsidepadding">
                    <div class="col-md-8" style="float: left;">
                        <h3>
                            {{ $applicant_information->applicant_name }}
                        </h3>
                        <table style="display: table">
                            <tr>
                                <td>Ref</td>
                                <td>&nbsp;&nbsp;&nbsp;:&nbsp;{{ $applicant_information->cv_no }}</td>
                            </tr>
                            <tr>
                                <td>Mobile</td>
                                <td>&nbsp;&nbsp;&nbsp;:&nbsp;{{ $applicant_information->contact_number }}
                                    ,{{ $applicant_information->sec_contact }}</td>
                            </tr>
                            @if(@$apply_job)
                                <tr>
                                    <td>Apply Position</td>
                                    <td>
                                        &nbsp;&nbsp;&nbsp;:&nbsp;{{ \App\Models\ApplicantJobPosition::where('id',$apply_job)->pluck('apply_position') }}</td>
                                </tr>
                            @endif

                            <tr>
                                <td>Salary Expectations</td>
                                <td>&nbsp;&nbsp;&nbsp;:&nbsp;{{ $salary }}</td>
                            </tr>
                            @if($applicant_information->not_period)
                                <tr>
                                    <td> Noticed Period</td>
                                    <td>&nbsp;&nbsp;&nbsp;:&nbsp;{{ $applicant_information->not_period }}</td>
                                </tr>
                            @endif

                        </table>
                    </div>
                    <div class="col-md-4" style="float: right;">
                        @if($applicant_information->cv_image)
                            <img src="{{ $applicant_information->cv_image }}" style="width: 200px;height:200px;"
                                 class="img-responsive">
                        @endif
                    </div>
                </div>

                <br>

                @if(!@$contact_info)
                    <h3>Summary</h3>
                    <div class="line"></div>
                    <div class="row bigsidepadding smallpadding">
                        <div class="col-md-12">
                            <ul class="list-symbol-bullet space-bottom-none">
                                <li>
                                    <div class="row">
                                        <div class="col-md-4" style="float: left;">Profile</div>
                                        <div class="col-md-8" style="float: right;">&nbsp;-&nbsp;
                                            @if($applicant_information->gender == 1)
                                                Male
                                            @else
                                                Female
                                            @endif
                                            ,
                                            @if($applicant_information->martial_status == 1)
                                                Married
                                            @else
                                                Single
                                            @endif
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="col-md-4" style="float: left;">Date of birth</div>
                                        <div class="col-md-8" style="float: right;">&nbsp;-&nbsp;
                                            {{ date('F d, Y',strtotime($applicant_information->dob))}}
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="col-md-4" style="float: left;">Father Name</div>
                                        <div class="col-md-8" style="float: right;">&nbsp;-&nbsp;
                                            {{ $applicant_information->father_name }}
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="col-md-4" style="float: left;">N.R.C No</div>
                                        <div class="col-md-8" style="float: right;">&nbsp;-&nbsp;
                                            {{ $applicant_information->nrc }}
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="col-md-4" style="float: left;">Nationality</div>
                                        <div class="col-md-8" style="float: right;">&nbsp;-&nbsp;
                                            {{ $applicant_information->nationality }}
                                        </div>
                                    </div>
                                </li>
                                @if($applicant_information->height )
                                    <li>
                                        <div class="row">
                                            <div class="col-md-4" style="float: left;">Height</div>
                                            <div class="col-md-8" style="float: right;">&nbsp;-&nbsp;
                                                {{ $applicant_information->height }}
                                            </div>
                                        </div>
                                    </li>
                                @endif

                                @if($applicant_information->weight )
                                    <li>
                                        <div class="row">
                                            <div class="col-md-4" style="float: left;">Weight</div>
                                            <div class="col-md-8" style="float: right;">&nbsp;-&nbsp;
                                                {{ $applicant_information->weight }}
                                            </div>
                                        </div>
                                    </li>
                                @endif
                                <li>
                                    <div class="row">
                                        <div class="col-md-4" style="float: left;">Current Location</div>
                                        <div class="col-md-8" style="float: right;">&nbsp;-&nbsp;
                                            {{ \App\Models\Country::where('id',$applicant_information->curr_location)->pluck('country_name') }}
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="row">
                                        <div class="col-md-4" style="float: left;">Address</div>
                                        <div class="col-md-8" style="float: right;">&nbsp;-&nbsp;
                                            {{ $applicant_information->address }}
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <p>

                        <div class="col-md-12">
                            <ul class="list-symbol-bullet space-bottom-none">
                                @if( $applicant_information->curr_position ||  $applicant_information->curr_position )
                                    <li>
                                        <div class="row">
                                            <div class="col-md-4">Last Position</div>
                                            <div class="col-md-8">&nbsp;-&nbsp;
                                                {{ $applicant_information->curr_position }}
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="row">
                                            <div class="col-md-4">Last Company</div>
                                            <div class="col-md-8">&nbsp;-&nbsp;
                                                {{ $applicant_information->curr_company }}
                                            </div>
                                        </div>
                                    </li>
                                @endif
                            </ul>
                        </div>
                    </div>
                @endif

                @if($applicant_information->c_objective)
                    <h3>Career Objective</h3>
                    <div class="line"></div>
                    <div class="row bigsidepadding smallpadding" style="padding: 16px;">
                        {{ $applicant_information->c_objective }}
                    </div>
                @endif
                <br>
                <?php $edu_back = SiteHelper::get_applicant_education($applicant_information->cv_no) ?>
                @if(count($edu_back) != 0 )
                    <h3>Educational Background</h3>
                    <div class="line"></div>
                    @foreach($edu_back as $edu)

                        <div class="row bigsidepadding smallpadding">
                            <div class="col-md-4" style="padding: 10px 0px 0px 14px;">
                                {{ $edu->uni_start_date }} - {{ $edu->uni_graduation_date }}
                            </div>
                            <div class="col-md-4">
                                <h5 class="nopadding">{{ $edu->uni_name }}</h5>
                            </div>
                            <div class="col-md-4 text-right" style="padding: 5px 16px 0px 0px;">
                                <h5 class="nopadding">  {{ \App\Models\University::where('id',$edu->degree_lvl)->pluck('degree_name') }}
                                    {{ $edu->other }} , {{ $edu->major_name }} </h5>
                            </div>
                        </div>

                        <div class="row bigsidepadding ">
                            <div class="col-md-8 col-md-offset-4">
                                {{ \App\Models\Country::where('id',$edu->uni_country)->pluck('country_name') }}
                            </div>
                        </div>
                        <br>
                    @endforeach
            </div>
            @endif

            <?php $other_qualification = SiteHelper::get_applicant_other_qualification($applicant_information->cv_no) ?>
            @if(count($other_qualification) != 0 )
                <h3>Other Qualifications</h3>
                <div class="line"></div>
                @foreach($other_qualification as $other)
                    <div class="row bigsidepadding smallpadding">
                        <div class="col-md-4" style="padding: 10px 0px 0px 14px;">
                            {{ $other->certi_date }}
                        </div>
                        <div class="col-md-4">
                            <h5 class="nopadding"><b>{{ $other->training_center }}</b></h5>
                            {{ \App\Models\Country::where('id',$other->certi_country)->pluck('country_name') }}
                        </div>
                        <div class="col-md-4 text-right" style="padding: 5px 16px 0px 0px;">
                            <h5 class="nopadding"><b>{{ $other->certi_name }}</b></h5>
                        </div>
                    </div>
                    <br>
                @endforeach
            @endif

            <?php $applicant_work_exp = SiteHelper::get_applicant_work_exp($applicant_information->cv_no) ?>
            @if(count($applicant_work_exp) != 0)

                <h3>Working Experience</h3>
                <div class="line"></div>

                @foreach($applicant_work_exp as $work)
                    <div class="row bigsidepadding smallpadding">
                        <div class="col-md-4" style="padding: 10px 0px 0px 14px;">
                            {{ $work->exp_from_date }} - {{ $work->exp_to_date }}<br>
                        </div>
                        <div class="col-md-4">
                            <h5 class="nopadding"><b>{{ $work->exp_job_title }}</b></h5>
                        </div>

                        <div class="col-md-4 text-right" style="padding: 5px 16px 0px 0px;">
                            <h5 class="nopadding"><b>{{ $work->exp_company_name }}</b></h5>
                        </div>
                    </div>
                    <div class="row bigsidepadding">
                        <div class="col-md-4">
                            <span><b>Industry</b></span>

                        </div>
                        <div class="col-md-4">
                            : {{ \App\Models\Job_Industry::where('id',$work->exp_job_industry)->pluck('industry_description') }}
                        </div>
                        <div class="col-md-4 text-right">
                            <p class="nopadding"><b>{{ $work->exp_job_city }}
                                    , {{ \App\Models\Country::where('id',$work->exp_job_country )->pluck('country_name') }}</b>
                            </p>
                        </div>
                    </div>

                    <div class="row bigsidepadding smallpadding">
                        <div class="col-md-4">
                            <?php
                            if(\App\Models\Job_Category::where('id', $work->exp_job_category)->pluck('description')):
                            ?>
                            <h5 class="nopadding">Job Description</h5>
                            <?php endif;?>
                        </div>
                        <div class="col-md-8 ">
                            {!! $work->exp_description !!}
                        </div>
                    </div>
                    <br>
                @endforeach

            @endif

            <?php $it_skill = SiteHelper::get_applicant_it_skill($applicant_information->cv_no) ?>
            @if(count($it_skill) != 0 )
                <h3>IT Skills</h3>
                <div class="line"></div>
                @foreach($it_skill as $it)
                    <div class="row bigsidepadding smallpadding">
                        <div class="col-md-12">

                            <div class="row">
                                <div class="col-md-4" style="padding: 0px 0px 0px 14px;">
                                    <h5>- {{ $it->topic_app_name }} </h5>
                                </div>
                                <div class="col-md-4"
                                     style="padding-top: 5px">{{ \App\Models\IT::where('id',$it->it_type)->pluck('type_name') }}</div>
                                <div class="col-md-4 text-right" style="padding: 5px 16px 0px 0px;">
                                    <b>{{ \Illuminate\Support\Facades\DB::table('it_lvl')->where('id',$it->it_skill)->pluck('it_lvl_name')  }}</b>
                                </div>
                            </div>

                        </div>
                    </div>
                    <br>
                @endforeach
            @endif


            <?php $lang_skill = SiteHelper::get_applicant_lang_skill($applicant_information->cv_no) ?>
            @if(count($lang_skill) != 0 )
                <h3>Languages</h3>
                <div class="line"></div>
                @foreach($lang_skill as $lang)
                    <div class="row bigsidepadding smallpadding">
                        <div class="col-md-8 col-md-offset-2">

                            <div class="row">
                                <div class="col-md-6" style="padding-top: 5px">- {{ \App\Models\Language::where('id', $lang->language_name )->pluck('lang_name')}} </div>
                                <div class="col-md-6" style="padding-top: 5px">
                                    <b>{{ \Illuminate\Support\Facades\DB::table('language_lavel')->where('id',$lang->language_skill)->pluck('lang_lvl')  }}</b>
                                </div>
                            </div>

                        </div>
                    </div>
                    <br>
                @endforeach
            @endif

            <?php $ref_person = SiteHelper::get_applicant_ref_person($applicant_information->cv_no) ?>

            @if(count($ref_person) != 0 )
                <h3>Reference Person</h3>
                <div class="line"></div>

                @foreach($ref_person as $ref)
                    <div class="row bigsidepadding smallpadding">
                        <div class="col-md-4" style="padding-top: 5px">
                            {{ $ref->reference_person_name }}
                        </div>
                        <div class="col-md-4">
                            <h5 class="nopadding">{{ $ref->reference_person_position }}</h5>
                        </div>

                        <div class="col-md-4 text-right" style="padding: 5px 16px 0px 0px;">
                            <h5 class="nopadding">" {{ $ref->reference_person_company }} "</h5>
                        </div>
                    </div>
                    <div class="row bigsidepadding">
                        <div class="col-md-8">
                            @if($ref->reference_person_mobile)
                                <span><b>Mobile No:</b></span>
                                {{ $ref->reference_person_mobile }}
                            @endif
                        </div>

                        <div class="col-md-4 text-right" style="padding: 5px 16px 0px 0px;">
                            {{ $ref->reference_person_email }}
                        </div>
                    </div>
                    <br>
                @endforeach
            @endif
        </div>
        <!-- End Paper -->
    </div>
    <div class="wrapper-bottom"></div>
</div>
</body>
</html>