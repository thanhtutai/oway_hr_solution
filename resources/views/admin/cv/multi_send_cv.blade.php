@extends('admin.layout.app')
@section('content')

    <div id="top_bar">
        <div class="md-top-bar">
            <div class="uk-width-large-8-10 uk-container-center">
                <div class="uk-clearfix">
                    <div class="md-top-bar-actions-left">
                        <div class="md-top-bar-checkbox">
                            <input type="checkbox" name="mailbox_select_all" id="mailbox_select_all" data-md-icheck/>
                        </div>
                    </div>
                    <div class="md-top-bar-actions-right">
                        <div class="md-top-bar-icons">
                            <i id="mailbox_list_split" class=" md-icon material-icons">&#xE8EE;</i>
                            <i id="mailbox_list_combined" class="md-icon material-icons">&#xE8F2;</i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="page_content">


        <script>
            tinymce.init({
                skin_url: '/../../backend/skins/tinymce/material_design',
                selector: '#wysiwyg_tinymce',
                height: 200,
                plugins: [
                    "advlist autolink lists link image charmap print preview anchor",
                    "searchreplace visualblocks code fullscreen",
                    "insertdatetime media table contextmenu paste "
                ],
                toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media "
            });

        </script>

        <div id="page_content_inner">

            <div class="md-card-list-wrapper" id="mailbox">
                <div class="uk-width-large-8-10 uk-container-center">

                    @if(\Session::has('status'))
                        <div class="uk-alert uk-alert-success" data-uk-alert="">
                            <a href="#" class="uk-alert-close uk-close"></a>
                            {{ \Session::get('status') }}
                        </div>
                    @endif

                    @if(\Session::has('error'))
                        <div class="uk-alert uk-alert-danger" data-uk-alert="">
                            <a href="#" class="uk-alert-close uk-close"></a>
                            {{ \Session::get('error') }}
                        </div>
                    @endif

                    <?php $today = \App\Models\Email::where('send_date', date('Y-m-d', strtotime(\Carbon\Carbon::today())))->get() ?>
                    @if(count($today)>0)
                        <div class="md-card-list">
                        <div class="md-card-list-header heading_list">Today</div>
                        <div class="md-card-list-header md-card-list-header-combined heading_list"
                             style="display: none">All Messages
                        </div>
                        <ul class="hierarchical_slide">
                            @foreach($today as $to_emails)
                                <li>
                                    <div class="md-card-list-item-menu"
                                         data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                                        <a href="#" class="md-icon material-icons">&#xE5D4;</a>
                                        <div class="uk-dropdown uk-dropdown-small">
                                            <ul class="uk-nav">
                                                <li><a href="#"><i class="material-icons">&#xE872;</i> Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <span class="md-card-list-item-date">{{ date('j F',strtotime($to_emails->send_date)) }}</span>
                                    <div class="md-card-list-item-select">
                                        <input type="checkbox" data-md-icheck/>
                                    </div>
                                    <div class="md-card-list-item-avatar-wrapper">
                                    <span class="md-card-list-item-avatar md-bg-grey">
                                        <img class="md-user-image"
                                             src="http://www.gravatar.com/avatar/{{ md5(strtolower(trim( $to_emails->to ))) }}/?d=wavatar&s=100&r=g"
                                             alt=""/>
                                    </span>
                                    </div>
                                    <div class="md-card-list-item-sender">
                                        <span>{{ $to_emails->to }}</span>
                                    </div>
                                    <div class="md-card-list-item-subject">
                                        <div class="md-card-list-item-sender-small">
                                            <span>{{ $to_emails->to }}</span>
                                        </div>
                                        <span>{{ $to_emails->subject }}</span>
                                    </div>
                                    <div class="md-card-list-item-content-wrapper">
                                        <div class="md-card-list-item-content">
                                            {!! $to_emails->message !!}
                                            <form class="md-card-list-item-reply">
                                                <?php $user = SiteHelper::get_user_permission($to_emails->create_user_id) ?>
                                                <label for="mailbox_reply_7254">Send Email by
                                                    <span>{{ $user->first_name }} {{ $user->last_name }}</span></label>
                                            </form>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                    <?php $today = \App\Models\Email::where('send_date', date('Y-m-d', strtotime(\Carbon\Carbon::yesterday())))->get() ?>
                        @if(count($today)>0)
                        <div class="md-card-list">
                        <div class="md-card-list-header heading_list">Yesterday</div>
                        <ul class="hierarchical_slide">
                            @foreach($today as $to_emails)
                                <li>
                                    <div class="md-card-list-item-menu"
                                         data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                                        <a href="#" class="md-icon material-icons">&#xE5D4;</a>
                                        <div class="uk-dropdown uk-dropdown-small">
                                            <ul class="uk-nav">
                                                <li><a href="#"><i class="material-icons">&#xE872;</i> Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <span class="md-card-list-item-date">{{ date('j F',strtotime($to_emails->send_date)) }}</span>
                                    <div class="md-card-list-item-select">
                                        <input type="checkbox" data-md-icheck/>
                                    </div>
                                    <div class="md-card-list-item-avatar-wrapper">
                                    <span class="md-card-list-item-avatar md-bg-grey">
                                        <img class="md-user-image"
                                             src="http://www.gravatar.com/avatar/{{ md5(strtolower(trim( $to_emails->to ))) }}/?d=wavatar&s=100&r=g"
                                             alt=""/>
                                    </span>
                                    </div>
                                    <div class="md-card-list-item-sender">
                                        <span>{{ $to_emails->to }}</span>
                                    </div>
                                    <div class="md-card-list-item-subject">
                                        <div class="md-card-list-item-sender-small">
                                            <span>{{ $to_emails->to }}</span>
                                        </div>
                                        <span>{{ $to_emails->subject }}</span>
                                    </div>
                                    <div class="md-card-list-item-content-wrapper">
                                        <div class="md-card-list-item-content">
                                            {!! $to_emails->message !!}
                                            <form class="md-card-list-item-reply">
                                                <?php $user = SiteHelper::get_user_permission($to_emails->create_user_id) ?>
                                                <label for="mailbox_reply_7254">Send Email by
                                                    <span>{{ $user->first_name }} {{ $user->last_name }}</span></label>
                                            </form>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    <?php $today = \App\Models\Email::whereBetween('send_date', [date('Y-m-d', strtotime(\Carbon\Carbon::now()->startOfMonth())), date('Y-m-d', strtotime(\Carbon\Carbon::yesterday()))])->where('send_date','!=' ,date('Y-m-d', strtotime(\Carbon\Carbon::today())))->where('send_date','!=' , date('Y-m-d', strtotime(\Carbon\Carbon::yesterday())))->get() ?>

                        @if(count($today)>0)
                    <div class="md-card-list">
                        <div class="md-card-list-header heading_list">This Month</div>
                        <ul class="hierarchical_slide">
                            @foreach($today as $to_emails)
                                <li>
                                    <div class="md-card-list-item-menu"
                                         data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                                        <a href="#" class="md-icon material-icons">&#xE5D4;</a>
                                        <div class="uk-dropdown uk-dropdown-small">
                                            <ul class="uk-nav">
                                                <li><a href="#"><i class="material-icons">&#xE872;</i> Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <span class="md-card-list-item-date">{{ date('j F',strtotime($to_emails->send_date)) }}</span>
                                    <div class="md-card-list-item-select">
                                        <input type="checkbox" data-md-icheck/>
                                    </div>
                                    <div class="md-card-list-item-avatar-wrapper">
                                    <span class="md-card-list-item-avatar md-bg-grey">
                                        <img class="md-user-image"
                                             src="http://www.gravatar.com/avatar/{{ md5(strtolower(trim( $to_emails->to ))) }}/?d=wavatar&s=100&r=g"
                                             alt=""/>
                                    </span>
                                    </div>
                                    <div class="md-card-list-item-sender">
                                        <span>{{ $to_emails->to }}</span>
                                    </div>
                                    <div class="md-card-list-item-subject">
                                        <div class="md-card-list-item-sender-small">
                                            <span>{{ $to_emails->to }}</span>
                                        </div>
                                        <span>{{ $to_emails->subject }}</span>
                                    </div>
                                    <div class="md-card-list-item-content-wrapper">
                                        <div class="md-card-list-item-content">
                                            {!! $to_emails->message !!}
                                            <form class="md-card-list-item-reply">
                                                <?php $user = SiteHelper::get_user_permission($to_emails->create_user_id) ?>
                                                <label for="mailbox_reply_7254">Send Email by
                                                    <span>{{ $user->first_name }} {{ $user->last_name }}</span></label>
                                            </form>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    @endif

                        <?php $today = \App\Models\Email::old_email() ?>
                        @if(count($today)>0)

                        <div class="md-card-list">
                        <div class="md-card-list-header heading_list">Older Messages</div>
                        <ul class="hierarchical_slide">
                            @foreach($today as $to_emails)
                                <li>
                                    <div class="md-card-list-item-menu"
                                         data-uk-dropdown="{mode:'click',pos:'bottom-right'}">
                                        <a href="#" class="md-icon material-icons">&#xE5D4;</a>
                                        <div class="uk-dropdown uk-dropdown-small">
                                            <ul class="uk-nav">
                                                <li><a href="#"><i class="material-icons">&#xE872;</i> Delete</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                    <span class="md-card-list-item-date">{{ date('j F',strtotime($to_emails->send_date)) }}</span>
                                    <div class="md-card-list-item-select">
                                        <input type="checkbox" data-md-icheck/>
                                    </div>
                                    <div class="md-card-list-item-avatar-wrapper">
                                    <span class="md-card-list-item-avatar md-bg-grey">
                                        <img class="md-user-image"
                                             src="http://www.gravatar.com/avatar/{{ md5(strtolower(trim( $to_emails->to ))) }}/?d=wavatar&s=100&r=g"
                                             alt=""/>
                                    </span>
                                    </div>
                                    <div class="md-card-list-item-sender">
                                        <span>{{ $to_emails->to }}</span>
                                    </div>
                                    <div class="md-card-list-item-subject">
                                        <div class="md-card-list-item-sender-small">
                                            <span>{{ $to_emails->to }}</span>
                                        </div>
                                        <span>{{ $to_emails->subject }}</span>
                                    </div>
                                    <div class="md-card-list-item-content-wrapper">
                                        <div class="md-card-list-item-content">
                                            {!! $to_emails->message !!}
                                            <form class="md-card-list-item-reply">
                                                <?php $user = SiteHelper::get_user_permission($to_emails->create_user_id) ?>
                                                <label for="mailbox_reply_7254">Send Email by
                                                    <span>{{ $user->first_name }} {{ $user->last_name }}</span></label>
                                            </form>
                                        </div>
                                    </div>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                        @endif
                </div>
            </div>

        </div>
    </div>

    <div class="md-fab-wrapper">
        <a class="md-fab md-fab-accent" href="#mailbox_new_message" data-uk-modal="{center:true}">
            <i class="material-icons">&#xE150;</i>
        </a>
    </div>

    <div class="uk-modal" id="mailbox_new_message">
        <div class="uk-modal-dialog">
            <button class="uk-modal-close uk-close" type="button"></button>
            {!! Form::open(array('url' => 'send_multi_cv', 'files' => true , 'class' => 'uk-form-stacked')) !!}
            <div class="uk-modal-header">
                <h3 class="uk-modal-title">Compose Message</h3>
            </div>
            <div class="uk-margin-medium-bottom">
                <select id="to" name="to" multiple>
                    <option value="">To</option>
                </select>
            </div>
            <div class="uk-margin-medium-bottom">
                <input type="text" placeholder="Subject" name="subject" class="md-input">
            </div>
            <div class="uk-margin-large-bottom">
                <textarea id="wysiwyg_tinymce" name="cover_letter" cols="30" rows="20"></textarea>
            </div>
            <div class="uk-grid" data-uk-grid-margin>
                <div class="uk-width-medium-1-1 parsley-row">
                    <input type="file" name="attachment[]" multiple>
                </div>
            </div>
            <div class="uk-modal-footer">
                <a href="#" class="md-icon-btn"><i class="md-icon material-icons">&#xE226;</i></a>
                <button type="submit" class="uk-float-right md-btn md-btn-flat md-btn-flat-primary">Send</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

    <script>
        function send_cv() {
            var as = $('#wysiwyg_tinymce').html(tinymce.get('wysiwyg_tinymce').getContent());
            $contact_data = $('#cv_mail_data');
            var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);
            UIkit.modal.alert('<p>Wizard data:</p><pre>' + form_serialized + '</pre>');
        }

    </script>
@stop