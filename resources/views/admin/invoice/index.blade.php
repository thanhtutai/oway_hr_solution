@extends('admin.layout.app')
@section('content')
    <div id="page_content">
        <div id="page_content_inner">
            <div class="md-card uk-margin-medium-bottom">
                <div class="md-card-content">
                    <table id="dt_tableTools" class="uk-table" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Invoice No</th>
                            <th>Ref-Invoice No</th>
                            <th>Company Name</th>

                            <th>Plan</th>
                            <th>Percentage( % )</th>
                            <th>Status</th>
                            <th>Due date</th>
                            <th>Invoice date</th>
                            <th>Payment date</th>
                            <th>Paid date</th>
                            <th>Who Created</th>
                            <th>Action</th>
                        </tr>
                        </thead>

                        <tbody>
                        @foreach($invoice_list as $invoice)
                            <?php $company = \App\Models\Client::where('company_code', $invoice->company_code)->first() ?>
                            <tr id="tr-{{ $invoice->id }}">
                                <td>{{ $invoice->invoice_code }}</td>
                                <td>{{ $invoice->ref_invoice }}</td>
                                <td>{{ $company->company_name }}</td>

                                <td>
                                    @if($company->plan == '1')
                                        Plan - Gold
                                    @elseif($company->plan == '2')
                                        Plan - Diamond
                                    @else
                                        Plan - Other ( {{ $company->expire_date }} )
                                    @endif
                                </td>
                                <td>{{ $invoice->percent }}&nbsp;%</td>
                                <td>
                                    @if($invoice->status == 1)
                                        <span class="uk-badge uk-badge-success">Paid</span>
                                    @else
                                        <span class="uk-badge uk-badge-warning">Not Paid</span>
                                    @endif
                                </td>
                                <td>{{ Date('F j, Y',strtotime($invoice->due_date)) }}</td>
                                <td>{{ Date('F j, Y',strtotime($invoice->invoice_date)) }}</td>
                                <td>{{ Date('F j, Y',strtotime($invoice->payment_date)) }}</td>
                                <td>{{ Date('F j, Y',strtotime($invoice->paid_date)) }}</td>
                                <td>
                                    <?php $user = SiteHelper::get_user_permission($invoice->create_user_id) ?>
                                    {{ $user->first_name }} {{ $user->last_name }}
                                </td>
                                <td>
                                    <a href="#" class="md-btn md-btn-small" data-uk-tooltip="{pos:'bottom'}"
                                       title="More Detail"
                                       data-uk-modal="{target:'#more_details{{ $invoice->id }}'}"><i
                                                class="ion-ios-help-outline" style="font-size: 20px"></i></a>
                                    <a href="#" class="md-btn md-btn-small" data-uk-tooltip="{pos:'bottom'}"
                                       title="Add Employee"
                                       data-uk-modal="{target:'#add_employee{{ $invoice->id }}'}"><i
                                                class="ion-ios-personadd-outline" style="font-size: 20px"></i></a>
                                    <a href="#" class="md-btn md-btn-small" data-uk-tooltip="{pos:'bottom'}"
                                       title="Change Status"
                                       data-uk-modal="{target:'#change_status{{ $invoice->id }}'}"><i
                                                class="ion-ios-compose-outline" style="font-size: 20px"></i></a>
                                    <a href="#" onclick="deleteInvoice({{ $invoice->id }})"
                                       class="md-btn md-btn-small md-btn-danger" data-uk-tooltip="{pos:'bottom'}"
                                       title="Remove"><i class="ion-ios-trash-outline"
                                                         style="font-size: 20px"></i></a>
                                </td>
                            </tr>
                            <div class="uk-modal" style="z-index: 998" id="more_details{{ $invoice->id }}">
                                <div class="uk-modal-dialog" style="padding: 0px">
                                    <div class="md-card-toolbar">
                                        <h3 class="md-card-toolbar-heading-text large" id="invoice_name">
                                            Invoice No - {{ $invoice->invoice_code }} / Ref No - {{ $invoice->ref_invoice }}
                                        </h3>
                                    </div>
                                    <div class="md-card-content">
                                        <div class="uk-margin-medium-bottom">
                                            <span class="uk-text-muted uk-text-small uk-text-italic">Invoice Date:</span>
                                            {{ date('Y.m.d',strtotime($invoice->invoice_date)) }}
                                            <br>
                                            <span class="uk-text-muted uk-text-small uk-text-italic">Payment Date:</span>
                                            {{ date('Y.m.d',strtotime($invoice->payment_date)) }}
                                            <br>
                                            <span class="uk-text-muted uk-text-small uk-text-italic">Due Date:</span>
                                            {{ date('Y.m.d',strtotime($invoice->due_date)) }}
                                        </div>
                                        <div class="uk-grid" data-uk-grid-margin="">
                                            <div class="uk-width-small-3-5">
                                                <div class="uk-margin-bottom">
                                                    <span class="uk-text-muted uk-text-small uk-text-italic">From:</span>
                                                    <address>
                                                        <p><strong>Oway HR Soultions</strong></p>
                                                        <p>49/B, 1st Floor, Moe Sandar Street, Ward (1)</p>
                                                        <p>Kamayut, Yangon, Myanmar</p>
                                                    </address>
                                                </div>
                                                <div class="uk-margin-medium-bottom">
                                                    <span class="uk-text-muted uk-text-small uk-text-italic">To:</span>
                                                    <address>
                                                        <p><strong>{{ $company->company_name }}</strong></p>
                                                        <p>{{ $company->address }}</p>
                                                    </address>
                                                </div>
                                            </div>
                                            <div class="uk-width-small-2-5">
                                                <span class="uk-text-muted uk-text-small uk-text-italic">Total:</span>
                                                <p class="heading_b uk-text-success">{{ \App\Models\EmployeeInvoice::get_emp_total_salary($invoice->invoice_code,$invoice->company_code) }}</p>
                                            </div>
                                        </div>
                                        <hr>
                                        <div class="uk-grid" data-uk-grid-margin="">
                                            <div class="uk-width-medium-1-1">
                                                <?php $emp_invoice = \App\Models\EmployeeInvoice::where('invoice_code', $invoice->invoice_code)->where('company_code', $invoice->company_code)->get() ?>
                                                <ul class="md-list md-list-addon md-list-right">
                                                    @foreach($emp_invoice as $row)
                                                        <?php $emp_data = \App\Models\ApplicantInformation::where('cv_no', $row->cv_id)->first() ?>
                                                        <li id="li-{{ $row->id }}">
                                                            <a href="#" data-uk-tooltip="{pos:'bottom'}"
                                                               onclick="deleteEmpInvoice({{ $row->id }})"
                                                               title="Remove Employee" class="md-list-addon-element"><i
                                                                        class="md-list-addon-icon material-icons uk-text-danger"></i></a>
                                                            <div class="md-list-content">
                                                                <span class="md-list-heading">{{ $emp_data->applicant_name }}</span>
                                                                <span class="uk-text-small uk-text-muted"><b>Job
                                                                        Postion</b> : {{ SiteHelper::get_employeed_job_position($row->cv_id ,$row->company_code) }}
                                                                    | <b>Salary</b> : {{ SiteHelper::get_employeed_salary($row->cv_id ,$row->company_code)->salary }}</span>
                                                            </div>
                                                        </li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="uk-modal" style="z-index: 998" id="add_employee{{ $invoice->id }}">
                                <div class="uk-modal-dialog">
                                    <div class="uk-modal-header">
                                        <h3 class="uk-modal-title">Choose Employee</h3>
                                    </div>
                                    <form id="emp_status{{ $invoice->id }}">
                                        <div class="uk-grid">
                                            <input type="hidden" name="company_code"
                                                   value="{{ $invoice->company_code }}">
                                            <input type="hidden" name="invoice_code"
                                                   value="{{ $invoice->invoice_code }}">
                                            <div class="uk-width-medium-1-1 parsley-row">
                                                <?php $employee = \App\Models\EmployeeRecord::where('company_code', $invoice->company_code)->get() ?>
                                                @if(count($employee)>0)
                                                    <select id="employee_name{{ $invoice->id }}" name="employee_name"
                                                            data-placeholder="Select Employee"
                                                            multiple>
                                                        @foreach($employee as $row)
                                                            <option value="{{ $row->cv_id }}">
                                                                {{ \App\Models\ApplicantInformation::where('cv_no',$row->cv_id)->pluck('applicant_name') }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                @else
                                                <p class="text-center">No Employee Available</p>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="uk-modal-footer uk-text-right">
                                            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close
                                            </button>
                                            <button type="button" onclick="check_employee({{ $invoice->id }})"
                                                    class="md-btn md-btn-flat md-btn-flat-primary uk-modal-close">Submit
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            <div class="uk-modal" style="z-index: 998" id="change_status{{ $invoice->id }}">
                                <div class="uk-modal-dialog">
                                    <div class="uk-modal-header">
                                        <h3 class="uk-modal-title">Change Invoice Status</h3>
                                    </div>
                                    <form id="invoice_status{{ $invoice->id }}">
                                        <div class="uk-grid">
                                            <div class="uk-width-medium-1-2 parsley-row">
                                                <label for="due_date">Due Date</label>
                                                <input type="text" name="due_date" id="due_date{{ $invoice->id }}"
                                                       value="{{ $invoice->due_date }}"
                                                       class="md-input"
                                                       data-parsley-date-message="This value should be a valid date"
                                                       data-uk-datepicker="{format:'YYYY-MM-DD'}"/>
                                            </div>
                                            <div class="uk-width-medium-1-2 parsley-row">
                                                <label for="">Percentage ( % )</label>
                                                <input type="text" class="md-input" name="percent" id="percent{{ $invoice->id }}"
                                                       value="{{ $invoice->percent }}">
                                            </div>
                                        </div>
                                        <div class="uk-grid">
                                            <div class="uk-width-medium-1-2 parsley-row">
                                                <label for="invoice_date">Invoice Date</label>
                                                <input type="text" name="invoice_date" id="invoice_date{{ $invoice->id }}"
                                                       value="{{ $invoice->due_date }}"
                                                       class="md-input"
                                                       data-parsley-date-message="This value should be a valid date"
                                                       data-uk-datepicker="{format:'YYYY-MM-DD'}"/>
                                            </div>
                                            <div class="uk-width-medium-1-2 parsley-row">
                                                <label for="payment_date">Payment Date</label>
                                                <input type="text" name="payment_date" id="payment_date{{ $invoice->id }}"
                                                       value="{{ $invoice->payment_date }}"
                                                       class="md-input"
                                                       data-parsley-date-message="This value should be a valid date"
                                                       data-uk-datepicker="{format:'YYYY-MM-DD'}"/>
                                            </div>
                                        </div>
                                        <div class="uk-grid">
                                            <div class="uk-width-medium-1-2 parsley-row">
                                                <label for="payment_date">Paid Date</label>
                                                <input type="text" name="paid_date" id="paid_date{{ $invoice->id }}"
                                                       value="{{ $invoice->paid_date }}"
                                                       class="md-input"
                                                       data-parsley-date-message="This value should be a valid date"
                                                       data-uk-datepicker="{format:'YYYY-MM-DD'}"/>
                                            </div>
                                            <div class="uk-width-medium-1-2 parsley-row">
                                                <label for="ref_invoice{{ $invoice->id }}">Ref Invoice No</label>
                                                <input type="text" name="payment_date" id="ref_invoice{{ $invoice->id }}"
                                                       value="{{ $invoice->ref_invoice }}"
                                                       class="md-input" />
                                            </div>
                                        </div>
                                        <div class="uk-grid">
                                            <div class="uk-width-medium-1-2 parsley-row">
                                                @if($invoice->status == 1)
                                                    <input checked type="checkbox" data-switchery name="is_paid"
                                                           id="user_edit_active"/>
                                                @else
                                                    <input type="checkbox" data-switchery name="is_paid"
                                                           id="user_edit_active"/>
                                                @endif
                                                <label for="user_edit_active" class="inline-label">Is Paid</label>
                                            </div>
                                        </div>
                                        <div class="uk-modal-footer uk-text-right">
                                            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close
                                            </button>
                                            <button type="button" onclick="change_status({{ $invoice->id }})"
                                                    class="md-btn md-btn-flat md-btn-flat-primary uk-modal-close">Submit
                                            </button>
                                        </div>
                                    </form>
                                </div>
                            </div>

                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>

            <div class="md-fab-wrapper">
                <a class="md-fab md-fab-primary"
                   data-uk-modal="{target : '#new_invoice'}"
                   data-uk-tooltip="{pos:'left'}"
                   title="Create New Invoice" href="#">
                    <i class="material-icons">&#xE145;</i>
                </a>
            </div>

            <div class="uk-modal" style="z-index: 998" id="new_invoice">
                <div class="uk-modal-dialog">
                    <div class="uk-modal-header">
                        <h3 class="uk-modal-title">Create Invoice</h3>
                    </div>
                    <form id="invoice_data">
                        <div class="uk-grid">
                            <div class="uk-width-medium-1-2 parsley-row">
                                <?php $client = \App\Models\Client::all() ?>
                                <select id="company_name" name="company_name" data-placeholder="Select Company"
                                        multiple>
                                    @foreach($client as $row)
                                        <option value="{{ $row->company_code }}">
                                            {{ $row->company_name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="uk-width-medium-1-2 parsley-row">
                                <label for="percent">Percentage ( % )</label>
                                <input type="text" class="md-input" name="percent" id="percent" value="">
                            </div>
                        </div>
                        <div class="uk-grid">
                            <div class="uk-width-medium-1-2 parsley-row">
                                <input type="text" name="due_date" id=""
                                       class="md-input" placeholder="Due Date"
                                       data-parsley-date-message="This value should be a valid date"
                                       data-uk-datepicker="{format:'YYYY-MM-DD'}"/>
                            </div>
                            <div class="uk-width-medium-1-2 parsley-row">
                                <input type="text" name="invoice_date" id=""
                                       class="md-input" placeholder="Invoice Date"
                                       data-parsley-date-message="This value should be a valid date"
                                       data-uk-datepicker="{format:'YYYY-MM-DD'}"/>
                            </div>
                        </div>
                        <div class="uk-grid">
                            <div class="uk-width-medium-1-2 parsley-row">
                                <input type="text" name="payment_date" id=""
                                       class="md-input" placeholder="Payment Date"
                                       data-parsley-date-message="This value should be a valid date"
                                       data-uk-datepicker="{format:'YYYY-MM-DD'}"/>
                            </div>
                            <div class="uk-width-medium-1-2 parsley-row">
                                <label for="ref_invoice">Ref Invoice No</label>
                                <input type="text" name="ref_invoice" id="ref_invoice"
                                       class="md-input"/>
                            </div>
                        </div>
                        <div class="uk-grid">
                            <div class="uk-width-medium-1-2">
                                <input type="text" name="paid_date" id=""
                                       class="md-input" placeholder="Paid Date"
                                       data-parsley-date-message="This value should be a valid date"
                                       data-uk-datepicker="{format:'YYYY-MM-DD'}"/>
                            </div>
                            <div class="uk-width-medium-1-2">
                                <input type="checkbox" data-switchery name="is_paid" id="user_edit_active"/>
                                <label for="user_edit_active" class="inline-label">Is Paid</label>
                            </div>
                        </div>

                        <div class="uk-modal-footer uk-text-right">
                            <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                            <button type="button" onclick="add_invoice()"
                                    class="md-btn md-btn-flat md-btn-flat-primary uk-modal-close">Submit
                            </button>
                        </div>
                    </form>
                </div>
            </div>

        </div>
    </div>

    <script>
        function deleteInvoice(id) {

            UIkit.modal.confirm(
                    'Are you sure?',
                    function () {

                        (function (modal) {
                            modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'>Wait <br/><img class=\'uk-margin-top\' src=\'{{ url('backend/img/spinners/spinner.gif') }}\' alt=\'\'>');
                            setTimeout(function () {
                                $.ajax({
                                    url: '{{ url('/invoice') }}' + '/' + id,
                                    type: 'DELETE',
                                    data: '',
                                    success: function (msg) {
                                        modal.hide();
                                        UIkit.notify({
                                            message: 'Successfully removed.',
                                            status: 'success',
                                            timeout: 2000,
                                            pos: 'top-right',
                                            onClose: function () {
                                                $('#tr-' + id).hide('slow').remove();
                                            }
                                        });
                                    },
                                    error: function (data) {
                                        modal.hide();
                                        UIkit.notify({
                                            message: 'Something went wrong.',
                                            status: 'danger',
                                            timeout: 3000,
                                            pos: 'top-right',
                                            onClose: function () {
                                                location.reload();
                                            }
                                        });
                                    }
                                });


                            }, 5000)
                        })();
                    });
        }

        function deleteEmpInvoice(id) {

            UIkit.modal.confirm(
                    'Are you sure?',
                    function () {

                        (function (modal) {
                            modal = UIkit.modal.blockUI('<div class=\'uk-text-center\'>Wait <br/><img class=\'uk-margin-top\' src=\'{{ url('backend/img/spinners/spinner.gif') }}\' alt=\'\'>');
                            setTimeout(function () {
                                $.ajax({
                                    url: '{{ url('/invoice/remove_added_employee') }}' + '/' + id,
                                    type: 'post',
                                    data: '',
                                    success: function (msg) {
                                        modal.hide();
                                        UIkit.notify({
                                            message: 'Successfully removed.',
                                            status: 'success',
                                            timeout: 1000,
                                            pos: 'top-right',
                                            onClose: function () {
                                                $('#li-' + id).hide('slow').remove();
                                            }
                                        });
                                    },
                                    error: function (data) {
                                        modal.hide();
                                        UIkit.notify({
                                            message: 'Something went wrong.',
                                            status: 'danger',
                                            timeout: 3000,
                                            pos: 'top-right',
                                            onClose: function () {

                                            }
                                        });
                                    }
                                });


                            }, 5000)
                        })();
                    });
        }

        function add_invoice() {
            $contact_data = $('#invoice_data');
            var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);
//            UIkit.modal.alert('<p>Wizard data:</p><pre>' + form_serialized + '</pre>');

            $.ajax({
                url: '{{ url('invoice') }}',
                type: 'post',
                data: {
                    'invoice_data': $contact_data.serializeObject(),
                },
                success: function (data, textStatus, jQxhr) {
                    UIkit.notify({
                        message: 'Sucessfully Added Invoice',
                        status: 'success',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                            location.reload();
                        }
                    });
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    UIkit.notify({
                        message: 'Something went wrong.',
                        status: 'danger',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                        }
                    });
                }
            });
        }

        function check_employee(id) {
            altair_helpers.content_preloader_show();

            $contact_data = $('#emp_status' + id);
            var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);
//            UIkit.modal.alert('<p>Wizard data:</p><pre>' + form_serialized + '</pre>');

            $.ajax({
                url: '{{ url('invoice/check_employee') }}',
                type: 'post',
                data: {
                    'invoice_data': $contact_data.serializeObject(),
                },
                success: function (data, textStatus, jQxhr) {
                    if(data == 'exist'){
                        UIkit.notify({
                            message: 'Already Exist.',
                            status: 'danger',
                            timeout: 1000,
                            pos: 'top-right',
                            onClose: function () {
                                altair_helpers.content_preloader_hide();
                            }
                        });
                    }else{
                        add_employee(id);
                    }
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    UIkit.notify({
                        message: 'Something went wrong.',
                        status: 'danger',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                        }
                    });
                }
            });
        }

        function add_employee(id) {
            altair_helpers.content_preloader_show();

            $contact_data = $('#emp_status' + id);
            var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);
//            UIkit.modal.alert('<p>Wizard data:</p><pre>' + form_serialized + '</pre>');

            $.ajax({
                url: '{{ url('invoice/add_employee') }}',
                type: 'post',
                data: {
                    'invoice_data': $contact_data.serializeObject(),
                },
                success: function (data, textStatus, jQxhr) {
                    UIkit.notify({
                        message: 'Sucessfully Added Employee',
                        status: 'success',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                            location.reload();
                        }
                    });
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    UIkit.notify({
                        message: 'Something went wrong.',
                        status: 'danger',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                        }
                    });
                }
            });

        }

        function change_status(id) {
            altair_helpers.content_preloader_show();

            $contact_data = $('#invoice_status' + id);
            var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);
//            UIkit.modal.alert('<p>Wizard data:</p><pre>' + form_serialized + '</pre>');
            $.ajax({
                url: '{{ url('/invoice') }}' + '/' + id,
                type: 'PUT',
                data: {
                    'invoice_edit_data': $contact_data.serializeObject(),
                    'due_date': $('#due_date' + id).val(),
                    'invoice_date': $('#invoice_date' + id).val(),
                    'payment_date': $('#payment_date' + id).val(),
                    'ref_invoice': $('#ref_invoice' + id).val(),
                    'paid_date': $('#paid_date' + id).val(),
                    'percent': $('#percent' + id).val(),
                },
                success: function (data, textStatus, jQxhr) {
                    UIkit.notify({
                        message: 'Sucessfully Updated.',
                        status: 'success',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                            location.reload();
                        }
                    });
                },
                error: function (jqXhr, textStatus, errorThrown) {
                    UIkit.notify({
                        message: 'Something went wrong.',
                        status: 'danger',
                        timeout: 1000,
                        pos: 'top-right',
                        onClose: function () {
                            altair_helpers.content_preloader_hide();
                        }
                    });
                }
            });
        }
    </script>
@stop