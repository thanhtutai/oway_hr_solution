@extends('admin.layout.app')

@section('content')
    <div id="page_content">
        <div id="page_content_inner">

            <!-- statistics (small charts) -->
            <div class="uk-grid uk-grid-width-large-1-4 uk-grid-width-medium-1-2 uk-grid-medium uk-sortable sortable-handler hierarchical_show"
                 data-uk-sortable data-uk-grid-margin>
                <div>
                    <div class="md-card">
                        <div class="md-card-content">
                            <div class="uk-float-right uk-margin-top uk-margin-small-right"><span
                                        class="peity_visitors peity_data">5,3,9,6,5,9,7</span></div>
                            @if(Sentinel::hasAccess('admin'))
                                <a href="{{ url('admin') }}">
                                    <span class="uk-text-muted uk-text-small">User List</span>
                                    <h2 class="uk-margin-remove"><span class="countUpMe">0<noscript>{{ $count }}</noscript></span>
                                    </h2>
                                </a>
                            @else
                                <a href="#">
                                    <span class="uk-text-muted uk-text-small">User List</span>
                                    <h2 class="uk-margin-remove"><span class="countUpMe">0<noscript>{{ $count }}</noscript></span>
                                    </h2>
                                </a>
                            @endif
                        </div>
                    </div>
                </div>
                <div>
                    <div class="md-card">
                        <div class="md-card-content">
                            <div class="uk-float-right uk-margin-top uk-margin-small-right"><span
                                        class="peity_sale peity_data">5,3,9,6,5,9,7,3,5,2</span></div>
                            <a href="{{ url('client') }}">
                                <span class="uk-text-muted uk-text-small">Client List</span>
                                <h2 class="uk-margin-remove"><span class="countUpMe">0<noscript>{{ $client_count }}</noscript></span>
                                </h2>
                            </a>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="md-card">
                        <div class="md-card-content">
                            <div class="uk-float-right uk-margin-top uk-margin-small-right"><span
                                        class="peity_orders peity_data">64/100</span></div>
                            <span class="uk-text-muted uk-text-small">Applicant List</span>
                            <h2 class="uk-margin-remove"><span class="countUpMe">0<noscript>{{ $cv_count }}</noscript></span>
                            </h2>
                        </div>
                    </div>
                </div>
                <div>
                    <div class="md-card">
                        <div class="md-card-content">
                            <div class="uk-float-right uk-margin-top uk-margin-small-right"><span
                                        class="peity_live peity_data">5,3,9,6,5,9,7,3,5,2,5,3,9,6,5,9,7,3,5,2</span>
                            </div>
                            <span class="uk-text-muted uk-text-small">Employed Person</span>
                            <h2 class="uk-margin-remove"><span class="countUpMe">0<noscript>{{ $employed_count }}</noscript></span>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>

            <div class="uk-grid uk-grid-medium" data-uk-grid-margin>
                <div class="uk-width-large-1-2">
                    <div class="md-card">
                        <div id="clndr_events" class="clndr-wrapper">

                            <script>
                                clndrEvents = {
                                    url : '{{ url('interview/jsondata') }}'
                                };
                            </script>

                            <script id="clndr_events_template" type="text/x-handlebars-template">
                                <div class="md-card-toolbar">
                                    <div class="md-card-toolbar-actions">
                                        <i class="md-icon clndr_add_event material-icons">&#xE145;</i>
                                        <i class="md-icon clndr_today material-icons">&#xE8DF;</i>
                                        <i class="md-icon clndr_previous material-icons">&#xE408;</i>
                                        <i class="md-icon clndr_next material-icons uk-margin-remove">&#xE409;</i>
                                    </div>
                                    <h3 class="md-card-toolbar-heading-text">
                                        @{{ month }} @{{ year }}
                                    </h3>
                                </div>
                                <div class="clndr_days">
                                    <div class="clndr_days_names">
                                        @{{#each daysOfTheWeek}}
                                        <div class="day-header">@{{ this }}</div>
                                        @{{/each}}
                                    </div>
                                    <div class="clndr_days_grid">
                                        @{{#each days}}
                                        <div class="@{{ this.classes }}" @{{#if this.id }} id="@{{ this.id }}" @{{/if}}>
                                            <span>@{{ this.day }}</span>
                                        </div>
                                        @{{/each}}
                                    </div>
                                </div>
                                <div class="clndr_events">
                                    <i class="material-icons clndr_events_close_button">&#xE5CD;</i>
                                    @{{#each eventsThisMonth}}
                                    <div class="clndr_event"
                                         data-clndr-event="@{{ dateFormat this.date format='YYYY-MM-DD' }}">
                                        <a href="@{{ this.url }}">
                                            <span class="clndr_event_title">@{{ this.title }}</span>
                                            <span class="clndr_event_more_info">
                                                @{{~dateFormat this.date format='MMM Do'}}
                                                @{{~#ifCond this.timeStart '||' this.timeEnd}} (@{{/ifCond}}
                                                @{{~#if this.timeStart }} @{{~this.timeStart~}} @{{/if}}
                                                @{{~#ifCond this.timeStart '&&' this.timeEnd}} - @{{/ifCond}}
                                                @{{~#if this.timeEnd }} @{{~this.timeEnd~}} @{{/if}}
                                                @{{~#ifCond this.timeStart '||' this.timeEnd}})@{{/ifCond~}}
                                            </span>
                                        </a>
                                    </div>
                                    @{{/each}}
                                </div>
                            </script>
                        </div>
                        <div class="uk-modal" id="modal_clndr_new_event" style="z-index: 998">
                            <div class="uk-modal-dialog">
                                <div class="uk-modal-header">
                                    <h3 class="uk-modal-title">New Event</h3>
                                </div>
                                <div class="uk-margin-bottom">
                                    <label for="clndr_event_title_control">Event Title</label>
                                    <input type="text" class="md-input" id="clndr_event_title_control"/>
                                </div>
                                <div class="uk-margin-medium-bottom">
                                    <label for="clndr_event_link_control">Event Link</label>
                                    <input type="text" class="md-input" id="clndr_event_link_control"/>
                                </div>
                                <div class="uk-grid uk-grid-width-medium-1-3 uk-margin-large-bottom"
                                     data-uk-grid-margin>
                                    <div>
                                        <div class="uk-input-group">
                                            <span class="uk-input-group-addon"><i
                                                        class="uk-input-group-icon uk-icon-calendar"></i></span>
                                            <label for="clndr_event_date_control">Event Date</label>
                                            <input class="md-input" type="text" id="clndr_event_date_control"
                                                   data-uk-datepicker="{format:'YYYY-MM-DD', addClass: 'dropdown-modal', minDate: '2015-11-13' }">
                                        </div>
                                    </div>
                                    <div>
                                        <div class="uk-input-group">
                                            <span class="uk-input-group-addon"><i
                                                        class="uk-input-group-icon uk-icon-clock-o"></i></span>
                                            <label for="clndr_event_start_control">Event Start</label>
                                            <input class="md-input" type="text" id="clndr_event_start_control"
                                                   data-uk-timepicker>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="uk-input-group">
                                            <span class="uk-input-group-addon"><i
                                                        class="uk-input-group-icon uk-icon-clock-o"></i></span>
                                            <label for="clndr_event_end_control">Event End</label>
                                            <input class="md-input" type="text" id="clndr_event_end_control"
                                                   data-uk-timepicker>
                                        </div>
                                    </div>
                                </div>
                                <div class="uk-modal-footer uk-text-right">
                                    <button type="button" class="md-btn md-btn-flat uk-modal-close">Close</button>
                                    <button type="button" class="md-btn md-btn-flat md-btn-flat-primary"
                                            id="clndr_new_event_submit">Add Event
                                    </button>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

                <div class="uk-width-medium-1-2">
                    <div class="md-card">
                        <div class="md-card-toolbar">
                            <div class="md-card-toolbar-actions">
                                <i class="md-icon material-icons md-card-fullscreen-activate">&#xE5D0;</i>
                            </div>
                            <h3 class="md-card-toolbar-heading-text">
                                Total Income
                            </h3>
                        </div>
                        <div class="md-card-content">
                            <div class="mGraph-wrapper">
                                <div id="mGraph_sale" class="mGraph"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop