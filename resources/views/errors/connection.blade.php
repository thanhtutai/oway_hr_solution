<link rel="stylesheet" href="{{ asset('front/css/main.css') }}">
<link rel="stylesheet" href="{{ asset('front/css/grid12.css') }}">
{{--    <link rel="stylesheet" href="{{ asset('front/css/james_typography.css') }}">--}}
<link href="{{ asset('front/css/color_scheme_light.css') }}" rel="stylesheet" media="screen">
<!-- Color scheme (dark/light)-->
<link href="{{ asset('front/css/colors/color_palette_indigo.css') }}" rel="stylesheet" media="screen">
<!-- Color palette -->
<link href="{{ asset('front/css/rapid-icons.css') }}" rel="stylesheet" media="screen">
<!-- James Framework - ICONS -->
<link href="{{ asset('front/css/responsivity.css') }}" rel="stylesheet" media="screen">
<!-- Responsive Fixes -->
<link href="{{ asset('front/css/animate.css') }}" rel="stylesheet" media="screen">
<!-- Animate - animations -->
<link href="{{ asset('front/css/owl.carousel.css') }}" rel="stylesheet" media="screen">
<!-- Owl - Carousel -->
<link href="{{ asset('front/css/owl.theme.css') }}" rel="stylesheet" media="screen">
<!-- Owl - Carousel -->
<link href="{{ asset('front/css/owl.transitions.css') }}" rel="stylesheet" media="screen">
<!-- Owl - Carousel -->
<link href="{{ asset('front/css/nivo-lightbox.css') }}" rel="stylesheet" media="screen">                          <!-- Lightbox Styles -->
<link href="{{ asset('front/css/nivo_lightbox_themes/default/default.css') }}" rel="stylesheet" media="screen">   <!-- Lightbox Styles -->
<script src="{{ asset('front/js/modernizr.custom.js') }}"></script>

<main class="content">

    <div class="content-heading" >
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-lg-push-3 col-sm-10 col-sm-push-1">
                    <h2 class="heading" style="text-align: center"></h2>
                </div>
            </div>
        </div>
    </div>

    <div class="container animated fadeIn">
        <div class="row">
            <div class="col-lg-10 col-lg-push-1 col-sm-10 col-sm-push-1 animated fadeIn">

                <section class="content-inner margin-top-no">
                    <div class="card">
                        <div class="card-main">
                            <div class="card-inner">
                                <div class="align-center">

                                    <!-- Review 1 -->
                                    <div class="single_review">
                                        <i class="ion ion-ios-pulse" style="font-size: 10em"></i>
                                        <div class="review_content">
                                            <p>- Internet Conection Require...</p>
                                        </div>
                                    </div>
                                    <!-- //Review 1 -->

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
</main>


<script src="{{ asset('front/js/jquery.cycle.all.min.js') }}"></script>
<script src="{{ asset('front/js/jquery.easing.min.js') }}"></script>
<script src="{{ asset('front/js/jquery-2.1.3.min.js') }}"></script>
<script src="{{ asset('front/js/nivo-lightbox.min.js') }}"></script>
<script src="{{ asset('front/js/waypoints.min.js') }}"></script>
<script src="{{ asset('front/js/retina.js') }}"></script>
<script src="{{ asset('front/js/preloader.js') }}"></script>
<script src="{{ asset('front/js/pathLoader.js') }}"></script>
<script src="{{ asset('front/js/classie.js') }}"></script>
<script src="{{ asset('front/js/owl.carousel.js') }}"></script>
<script src="{{ asset('front/js/main.js') }}"></script>
