var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function(mix) {
    mix.styles([
        'backend/bower_components/weather-icons/css/weather-icons.min.css',
        'backend/bower_components/metrics-graphics/dist/metricsgraphics.css',
        'backend/bower_components/c3js-chart/c3.min.css',
        'backend/bower_components/codemirror/lib/codemirror.css',
        'backend/bower_components/fullcalendar/dist/fullcalendar.min.css',
        'backend/bower_components/uikit/css/uikit.almost-flat.min.css',
        'backend/skins/dropify/css/dropify.css',
        'backend/icons/flags/flags.min.css',
        'backend/css/main.min.css',
        'backend/skins/jtable/jtable.min.css',
        'backend/skins/jquery-ui/material/jquery-ui.min.css',
        'backend/bower_components/kendo-ui/styles/kendo.common-material.min.css',
        'backend/bower_components/kendo-ui/styles/kendo.material.min.css'
        ]);
});
