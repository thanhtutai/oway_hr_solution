<?php 

define('DATE_META', json_encode([
	1 => 'Non Experience ( 0 - 1 Year)',
	2 => 'Experience (2 - 3 Year)',
	3 => 'Manager (3 - 5 Year)',
	4 => 'Executive (5 - 8 Year)',
	5 => 'Senior Executive (8 Year ++)'
	]));
