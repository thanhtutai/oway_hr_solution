<?php
return [
	'page_format' => 'A4',
	'page_orientation' => 'P',
	'page_units' => 'mm',
	'unicode' => true,
	'encoding' => 'UTF-8',
	'font_directory' => __DIR__.'/../public/pdf_data/fonts/',
	'image_directory' => __DIR__.'/../public/pdf_data/images/',
	'tcpdf_throw_exception' => false,
];