/**
 * Created by Nyi Nyi Lwin on 4/10/2016.
 */

var url = window.location.hostname;

function coverImage() {
    var preview = '';
    var file = document.getElementById('file_upload-select-cover').files[0];
    var reader = new FileReader();

    reader.addEventListener("load", function () {
        $('#cover_image').val(reader.result);
        $("#original_image").attr("src", reader.result);
    }, false);

    if (file) {
        reader.readAsDataURL(file);
    }
}

function attchImage() {
    var preview = '';
    var file = document.getElementById('file_upload-select-cover_attach').files[0];
    var reader = new FileReader();

    reader.addEventListener("load", function () {
        $('#attach_image').val(reader.result);
        $("#attach_original_image").attr("src", reader.result);
    }, false);

    if (file) {
        reader.readAsDataURL(file);
    }
}

function applicantImage() {
    var preview = '';
    var file = document.getElementById('file_upload-select-cover_applicant').files[0];
    var reader = new FileReader();

    reader.addEventListener("load", function () {
        $('#applicant_image').val(reader.result);
        $("#applicant_original_image").attr("src", reader.result);
    }, false);

    if (file) {
        reader.readAsDataURL(file);
    }
}

function edit_contact() {
    altair_helpers.content_preloader_show();

    $contact_data = $('#contact_data');
    var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);

    $.ajax({
        url: 'update_contact_data',
        type: 'post',
        data: {
            'contact_data': $contact_data.serializeObject(),
        },
        success: function (data, textStatus, jQxhr) {
            UIkit.notify({
                message: 'Sucessfully Updated.',
                status: 'success',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                    location.reload();
                }
            });
        },
        error: function (jqXhr, textStatus, errorThrown) {
            UIkit.notify({
                message: 'Something went wrong.',
                status: 'danger',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                }
            });
            window.location = url;
        }
    });
}

function edit_personal_data() {
    altair_helpers.content_preloader_show();

    $contact_data = $('#personal_data');
    var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);

    $.ajax({
        url: 'update_personal_data',
        type: 'post',
        data: {
            'personal_data': $contact_data.serializeObject(),
        },
        success: function (data, textStatus, jQxhr) {
            UIkit.notify({
                message: 'Sucessfully Updated.',
                status: 'success',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                    location.reload();
                }
            });
        },
        error: function (jqXhr, textStatus, errorThrown) {
            UIkit.notify({
                message: 'Something went wrong.',
                status: 'danger',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                }
            });
        }
    });
}

function edit_career_obj() {
    altair_helpers.content_preloader_show();

    $contact_data = $('#c_obj_data');
    var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);

    $.ajax({
        url: 'update_career_obj_data',
        type: 'post',
        data: {
            'c_obj_data': $contact_data.serializeObject(),
        },
        success: function (data, textStatus, jQxhr) {
            UIkit.notify({
                message: 'Sucessfully Updated.',
                status: 'success',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                    location.reload();
                }
            });
        },
        error: function (jqXhr, textStatus, errorThrown) {
            UIkit.notify({
                message: 'Something went wrong.',
                status: 'danger',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                }
            });
        }
    });
}

function edit_job(id) {
    altair_helpers.content_preloader_show();

    $contact_data = $('#edit_job_data' + id);
    var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);
    $.ajax({
        url: 'update_job_data',
        type: 'post',
        data: {
            'edit_job_data': $contact_data.serializeObject(),
        },
        success: function (data, textStatus, jQxhr) {
            UIkit.notify({
                message: 'Sucessfully Updated Job Position.',
                status: 'success',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                    location.reload();
                }
            });
        },
        error: function (jqXhr, textStatus, errorThrown) {
            UIkit.notify({
                message: 'Something went wrong.',
                status: 'danger',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                }
            });
        }
    });
}

function edit_work_exp(id) {
    altair_helpers.content_preloader_show();


    $contact_data = $('#edit_work_exp_data' + id);
    var as = $('.work').html(tinymce.get('work'+id).getContent());
    var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);

    $.ajax({
        url: 'update_work_exp_data',
        type: 'post',
        data: {
            'edit_work_exp_data': $contact_data.serializeObject(),
        },
        success: function (data, textStatus, jQxhr) {
            UIkit.notify({
                message: 'Sucessfully Updated Work Exp.',
                status: 'success',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                    location.reload();
                }
            });
        },
        error: function (jqXhr, textStatus, errorThrown) {
            UIkit.notify({
                message: 'Something went wrong.',
                status: 'danger',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                }
            });
        }
    });
}

function edit_edu(id) {
    altair_helpers.content_preloader_show();

    $contact_data = $('#edit_edu_data' + id);
    var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);
    $.ajax({
        url: 'update_edu_data',
        type: 'post',
        data: {
            'edit_edu_data': $contact_data.serializeObject(),
        },
        success: function (data, textStatus, jQxhr) {
            UIkit.notify({
                message: 'Sucessfully Updated Edu Back.',
                status: 'success',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                    location.reload();
                }
            });
        },
        error: function (jqXhr, textStatus, errorThrown) {
            UIkit.notify({
                message: 'Something went wrong.',
                status: 'danger',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                }
            });
        }
    });
}

function edit_other_qual(id) {
    altair_helpers.content_preloader_show();

    $contact_data = $('#edit_other_qual_data' + id);
    var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);
    $.ajax({
        url: 'update_other_data',
        type: 'post',
        data: {
            'edit_other_data': $contact_data.serializeObject(),
        },
        success: function (data, textStatus, jQxhr) {
            UIkit.notify({
                message: 'Sucessfully Updated Other Qualifications.',
                status: 'success',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                    location.reload();
                }
            });
        },
        error: function (jqXhr, textStatus, errorThrown) {
            UIkit.notify({
                message: 'Something went wrong.',
                status: 'danger',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                }
            });
        }
    });
}

function edit_it_skill(id) {
    altair_helpers.content_preloader_show();

    $contact_data = $('#edit_it_data' + id);
    var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);
    $.ajax({
        url: 'update_it_data',
        type: 'post',
        data: {
            'edit_it_data': $contact_data.serializeObject(),
        },
        success: function (data, textStatus, jQxhr) {
            UIkit.notify({
                message: 'Sucessfully Updated Other Qualifications.',
                status: 'success',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                    location.reload();
                }
            });
        },
        error: function (jqXhr, textStatus, errorThrown) {
            UIkit.notify({
                message: 'Something went wrong.',
                status: 'danger',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                }
            });
        }
    });
}

function edit_lang_skill(id) {
    altair_helpers.content_preloader_show();

    $contact_data = $('#edit_lang_data' + id);
    var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);
    $.ajax({
        url: 'update_lang_data',
        type: 'post',
        data: {
            'edit_lang_data': $contact_data.serializeObject(),
        },
        success: function (data, textStatus, jQxhr) {
            UIkit.notify({
                message: 'Sucessfully Updated Language Skill.',
                status: 'success',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                    location.reload();
                }
            });
        },
        error: function (jqXhr, textStatus, errorThrown) {
            UIkit.notify({
                message: 'Something went wrong.',
                status: 'danger',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                }
            });
        }
    });
}

function edit_cover_letter() {
    altair_helpers.content_preloader_show();

    $contact_data = $('#edit_cover_letter_data');
    var as = $('#wysiwyg_tinymce').html(tinymce.get('wysiwyg_tinymce').getContent());
    var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);

    $.ajax({
        url: 'update_cover_letter_data',
        type: 'post',
        data: {
            'edit_cover_letter': $contact_data.serializeObject(),
        },
        success: function (data, textStatus, jQxhr) {
            UIkit.notify({
                message: 'Sucessfully Updated Cover Letter',
                status: 'success',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                    location.reload();
                }
            });
        },
        error: function (jqXhr, textStatus, errorThrown) {
            UIkit.notify({
                message: 'Something went wrong.',
                status: 'danger',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                }
            });
        }
    });
}

function edit_cover_image() {
    altair_helpers.content_preloader_show();

    $contact_data = $('#edit_cover_image_data');
    var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);
    $.ajax({
        url: 'update_cover_image_data',
        type: 'post',
        data: {
            'edit_cover_image': $contact_data.serializeObject(),
        },
        success: function (data, textStatus, jQxhr) {
            UIkit.notify({
                message: 'Sucessfully Updated Cover Image',
                status: 'success',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                    location.reload();
                }
            });
        },
        error: function (jqXhr, textStatus, errorThrown) {
            UIkit.notify({
                message: 'Something went wrong.',
                status: 'danger',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                }
            });
        }
    });
}

function edit_applicant_image() {
    altair_helpers.content_preloader_show();

    $contact_data = $('#edit_applicant_image_data');
    var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);
    $.ajax({
        url: 'update_applicant_image_data',
        type: 'post',
        data: {
            'edit_applicant_image': $contact_data.serializeObject(),
        },
        success: function (data, textStatus, jQxhr) {
            UIkit.notify({
                message: 'Sucessfully Updated Cover Image',
                status: 'success',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                    location.reload();
                }
            });
        },
        error: function (jqXhr, textStatus, errorThrown) {
            UIkit.notify({
                message: 'Something went wrong.',
                status: 'danger',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                }
            });
        }
    });
}

function edit_ref_person(id) {
    altair_helpers.content_preloader_show();

    $contact_data = $('#edit_ref_data' + id);
    var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);
    $.ajax({
        url: 'update_ref_data',
        type: 'post',
        data: {
            'update_ref_data': $contact_data.serializeObject(),
        },
        success: function (data, textStatus, jQxhr) {
            UIkit.notify({
                message: 'Sucessfully Updated Ref Person.',
                status: 'success',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                    location.reload();
                }
            });
        },
        error: function (jqXhr, textStatus, errorThrown) {
            UIkit.notify({
                message: 'Something went wrong.',
                status: 'danger',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                }
            });
        }
    });
}

function add_new_job() {
    altair_helpers.content_preloader_show();

    $contact_data = $('#new_job_data');
    var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);
    $.ajax({
        url: 'add_new_job_data',
        type: 'post',
        data: {
            'new_job_data': $contact_data.serializeObject(),
        },
        success: function (data, textStatus, jQxhr) {
            UIkit.notify({
                message: 'Sucessfully Added New Job Position.',
                status: 'success',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                    location.reload();
                }
            });
        },
        error: function (jqXhr, textStatus, errorThrown) {
            UIkit.notify({
                message: 'Something went wrong.',
                status: 'danger',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                }
            });
        }
    });
}

function add_new_work_exp() {
    altair_helpers.content_preloader_show();

    $contact_data = $('#new_work_exp_data');
    var as = $('#description').html(tinymce.get('description').getContent());
    var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);
    $.ajax({
        url: 'add_new_work_exp_data',
        type: 'post',
        data: {
            'new_work_exp_data': $contact_data.serializeObject(),
        },
        success: function (data, textStatus, jQxhr) {
            UIkit.notify({
                message: 'Sucessfully Added New Work Exp.',
                status: 'success',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                    location.reload();
                }
            });
        },
        error: function (jqXhr, textStatus, errorThrown) {
            UIkit.notify({
                message: 'Something went wrong.',
                status: 'danger',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                }
            });
        }
    });
}

function add_new_edu() {
    altair_helpers.content_preloader_show();

    $contact_data = $('#new_edu_data');
    var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);
    $.ajax({
        url: 'add_new_edu_data',
        type: 'post',
        data: {
            'new_edu_data': $contact_data.serializeObject(),
        },
        success: function (data, textStatus, jQxhr) {
            UIkit.notify({
                message: 'Sucessfully Added Education.',
                status: 'success',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                    location.reload();
                }
            });
        },
        error: function (jqXhr, textStatus, errorThrown) {
            UIkit.notify({
                message: 'Something went wrong.',
                status: 'danger',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                }
            });
        }
    });
}

function add_new_other_qual() {
    altair_helpers.content_preloader_show();

    $contact_data = $('#new_other_qual_data');
    var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);
    $.ajax({
        url: 'add_new_other_qual_data',
        type: 'post',
        data: {
            'new_certi_data': $contact_data.serializeObject(),
        },
        success: function (data, textStatus, jQxhr) {
            UIkit.notify({
                message: 'Sucessfully Added Certificate.',
                status: 'success',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                    location.reload();
                }
            });
        },
        error: function (jqXhr, textStatus, errorThrown) {
            UIkit.notify({
                message: 'Something went wrong.',
                status: 'danger',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                }
            });
        }
    });
}

function add_new_it_skill() {
    altair_helpers.content_preloader_show();

    $contact_data = $('#new_other_it_data');
    var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);
    $.ajax({
        url: 'add_new_other_it_data',
        type: 'post',
        data: {
            'new_it_data': $contact_data.serializeObject(),
        },
        success: function (data, textStatus, jQxhr) {
            UIkit.notify({
                message: 'Sucessfully Added It Skill.',
                status: 'success',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                    location.reload();
                }
            });
        },
        error: function (jqXhr, textStatus, errorThrown) {
            UIkit.notify({
                message: 'Something went wrong.',
                status: 'danger',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                }
            });
        }
    });
}

function add_new_language_skill() {
    altair_helpers.content_preloader_show();

    $contact_data = $('#new_lang_data');
    var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);
    $.ajax({
        url: 'add_new_lang_data',
        type: 'post',
        data: {
            'new_lang_data': $contact_data.serializeObject(),
        },
        success: function (data, textStatus, jQxhr) {
            UIkit.notify({
                message: 'Sucessfully Added Language Skill.',
                status: 'success',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                    location.reload();
                }
            });
        },
        error: function (jqXhr, textStatus, errorThrown) {
            UIkit.notify({
                message: 'Something went wrong.',
                status: 'danger',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                }
            });
        }
    });
}

function add_attach_image() {
    altair_helpers.content_preloader_show();

    $contact_data = $('#add_new_attachment_data');
    var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);
    $.ajax({
        url: 'add_new_attach_image',
        type: 'post',
        data: {
            'attach_image': $contact_data.serializeObject(),
        },
        success: function (data, textStatus, jQxhr) {
            UIkit.notify({
                message: 'Sucessfully Add New Attachment',
                status: 'success',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                    location.reload();
                }
            });
        },
        error: function (jqXhr, textStatus, errorThrown) {
            UIkit.notify({
                message: 'Something went wrong.',
                status: 'danger',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                }
            });
        }
    });
}

function add_ref_person() {
    altair_helpers.content_preloader_show();

    $contact_data = $('#new_ref_data');
    var form_serialized = JSON.stringify($contact_data.serializeObject(), null, 2);
    $.ajax({
        url: 'add_new_ref_person',
        type: 'post',
        data: {
            'new_ref_data': $contact_data.serializeObject(),
        },
        success: function (data, textStatus, jQxhr) {
            UIkit.notify({
                message: 'Sucessfully Add Ref Person',
                status: 'success',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                    location.reload();
                }
            });
        },
        error: function (jqXhr, textStatus, errorThrown) {
            UIkit.notify({
                message: 'Something went wrong.',
                status: 'danger',
                timeout: 1000,
                pos: 'top-right',
                onClose: function () {
                    altair_helpers.content_preloader_hide();
                }
            });
        }
    });
}

function delete_job(id) {

    UIkit.modal.confirm('Are you sure?', function () {
        $.ajax({
            url: 'delete_job_data',
            type: 'post',
            data: {
                'delete_job_data': id,
            },
            success: function (data, textStatus, jQxhr) {
                UIkit.notify({
                    message: 'Sucessfully Removed Applied Job Position.',
                    status: 'danger',
                    timeout: 1000,
                    pos: 'top-right',
                    onClose: function () {
                        location.reload();
                    }
                });
            },
            error: function (jqXhr, textStatus, errorThrown) {
                UIkit.notify({
                    message: 'Something went wrong.',
                    status: 'danger',
                    timeout: 1000,
                    pos: 'top-right',
                    onClose: function () {

                    }
                });
            }
        });
    });
}

function delete_exp(id) {

    UIkit.modal.confirm('Are you sure?', function () {
        $.ajax({
            url: 'delete_work_exp_data',
            type: 'post',
            data: {
                'delete_exp_data': id,
            },
            success: function (data, textStatus, jQxhr) {
                UIkit.notify({
                    message: 'Sucessfully Removed.',
                    status: 'danger',
                    timeout: 1000,
                    pos: 'top-right',
                    onClose: function () {
                        location.reload();
                    }
                });
            },
            error: function (jqXhr, textStatus, errorThrown) {
                UIkit.notify({
                    message: 'Something went wrong.',
                    status: 'danger',
                    timeout: 1000,
                    pos: 'top-right',
                    onClose: function () {

                    }
                });
            }
        });
    });
}

function delete_edu(id) {

    UIkit.modal.confirm('Are you sure?', function () {
        $.ajax({
            url: 'delete_edu_data',
            type: 'post',
            data: {
                'delete_edu_data': id,
            },
            success: function (data, textStatus, jQxhr) {
                UIkit.notify({
                    message: 'Sucessfully Removed.',
                    status: 'danger',
                    timeout: 1000,
                    pos: 'top-right',
                    onClose: function () {
                        location.reload();
                    }
                });
            },
            error: function (jqXhr, textStatus, errorThrown) {
                UIkit.notify({
                    message: 'Something went wrong.',
                    status: 'danger',
                    timeout: 1000,
                    pos: 'top-right',
                    onClose: function () {

                    }
                });
            }
        });
    });
}

function delete_other(id) {

    UIkit.modal.confirm('Are you sure?', function () {
        $.ajax({
            url: 'delete_other_data',
            type: 'post',
            data: {
                'delete_other_data': id,
            },
            success: function (data, textStatus, jQxhr) {
                UIkit.notify({
                    message: 'Sucessfully Removed.',
                    status: 'danger',
                    timeout: 1000,
                    pos: 'top-right',
                    onClose: function () {
                        location.reload();
                    }
                });
            },
            error: function (jqXhr, textStatus, errorThrown) {
                UIkit.notify({
                    message: 'Something went wrong.',
                    status: 'danger',
                    timeout: 1000,
                    pos: 'top-right',
                    onClose: function () {

                    }
                });
            }
        });
    });
}

function delete_it(id) {

    UIkit.modal.confirm('Are you sure?', function () {
        $.ajax({
            url: 'delete_it_data',
            type: 'post',
            data: {
                'delete_it_data': id,
            },
            success: function (data, textStatus, jQxhr) {
                UIkit.notify({
                    message: 'Sucessfully Removed.',
                    status: 'danger',
                    timeout: 1000,
                    pos: 'top-right',
                    onClose: function () {
                        location.reload();
                    }
                });
            },
            error: function (jqXhr, textStatus, errorThrown) {
                UIkit.notify({
                    message: 'Something went wrong.',
                    status: 'danger',
                    timeout: 1000,
                    pos: 'top-right',
                    onClose: function () {

                    }
                });
            }
        });
    });
}

function delete_lang(id) {

    UIkit.modal.confirm('Are you sure?', function () {
        $.ajax({
            url: 'delete_lang_data',
            type: 'post',
            data: {
                'delete_lang_data': id,
            },
            success: function (data, textStatus, jQxhr) {
                UIkit.notify({
                    message: 'Sucessfully Removed.',
                    status: 'danger',
                    timeout: 1000,
                    pos: 'top-right',
                    onClose: function () {
                        location.reload();
                    }
                });
            },
            error: function (jqXhr, textStatus, errorThrown) {
                UIkit.notify({
                    message: 'Something went wrong.',
                    status: 'danger',
                    timeout: 1000,
                    pos: 'top-right',
                    onClose: function () {

                    }
                });
            }
        });
    });
}

function delete_attach(id) {

    UIkit.modal.confirm('Are you sure?', function () {
        $.ajax({
            url: 'delete_attach_data',
            type: 'post',
            data: {
                'delete_attach_data': id,
            },
            success: function (data, textStatus, jQxhr) {
                UIkit.notify({
                    message: 'Sucessfully Removed.',
                    status: 'danger',
                    timeout: 1000,
                    pos: 'top-right',
                    onClose: function () {
                        location.reload();
                    }
                });
            },
            error: function (jqXhr, textStatus, errorThrown) {
                UIkit.notify({
                    message: 'Something went wrong.',
                    status: 'danger',
                    timeout: 1000,
                    pos: 'top-right',
                    onClose: function () {

                    }
                });
            }
        });
    });
}

function delete_ref(id) {

    UIkit.modal.confirm('Are you sure?', function () {
        $.ajax({
            url: 'delete_ref',
            type: 'post',
            data: {
                'delete_ref': id,
            },
            success: function (data, textStatus, jQxhr) {
                UIkit.notify({
                    message: 'Sucessfully Removed.',
                    status: 'danger',
                    timeout: 1000,
                    pos: 'top-right',
                    onClose: function () {
                        location.reload();
                    }
                });
            },
            error: function (jqXhr, textStatus, errorThrown) {
                UIkit.notify({
                    message: 'Something went wrong.',
                    status: 'danger',
                    timeout: 1000,
                    pos: 'top-right',
                    onClose: function () {

                    }
                });
            }
        });
    });
}