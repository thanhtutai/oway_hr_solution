/**
 * Created by Nyi Nyi Lwin on 3/23/2016.
 */

var total = '{{@$total}}';
var count = 1;
var countEl = document.getElementById("count");
var book = '{{ @$value }}';


function plus(){
    count++;

    $.ajax({
        type: 'post',
        url: '{{ URL::to("search/next") }}',
        data: {
            book: book,
            page: count,
        },
        success: function (response) {

            $('#next_data').html(response);
            countEl.value = count;
            if(count == ( total - 1 )){
                $('#next').css('display','none');
            }
        }
    });

}

function minus(){

    if (count > 1) {
        count--;
        if( count < total){
            $('#next').css('display','');
        }
        $.ajax({
            type: 'post',
            url: '{{ URL::to("search/next") }}',
            data: {
                book: book,
                page: count,
            },
            success: function (response) {

                $('#next_data').html(response);
                countEl.value = count;
                if( countEl.value < total){
                    $('#next').css('display','');
                }
            }
        });
    }
}

function search() {
    var book = $('#search').val();
    $.ajax({
        type: 'post',
        url: '{{ URL::to("search") }}',
        data: {
            book: book
        },
        success: function (response) {

            $('#content').html(response);

            if(count == total){
                $('#next').css('display','none');
            }

        }
    });
}

function next_page(id) {
    var book = '{{ @$value }}';
    var page = id;

    $('.remove').removeClass('active');
    $('#paginate'+id).addClass('active');

    $.ajax({
        type: 'post',
        url: '{{ URL::to("search/next") }}',
        data: {
            book: book,
            page: page,
        },
        success: function (response) {

            $('#next_data').html(response);

        }
    });
}

function view_detail(book_id) {
    $.ajax({
        type: 'get',
        url: '{{ URL::to("book_detail") }}'+'/'+book_id,
        data: '',
        success: function (response) {
            $('#book_detail').html(response);
        }
    });
}

function goback() {
    var book = $('#search_value').val();
    $.ajax({
        type: 'post',
        url: '{{ URL::to("search") }}',
        data: {
            book : book
        },
        success: function (response) {

            $('#content').html(response);

        }
    });
}

