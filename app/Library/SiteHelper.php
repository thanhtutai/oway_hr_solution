<?php
use App\Models\Activity;
use App\Models\ApplicantAttachment;
use App\Models\ApplicantCover;
use App\Models\ApplicantEducation;
use App\Models\ApplicantInformation;
use App\Models\ApplicantItSkill;
use App\Models\ApplicantJobPosition;
use App\Models\ApplicantLanguageSkill;
use App\Models\ApplicantOtherQualifications;
use App\Models\ApplicantReferencePerson;
use App\Models\ApplicantWorkExperience;

use App\Models\Email;
use App\Models\EmployeeRecord;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;

/**
 * Created by PhpStorm.
 * User: Nyi Nyi Lwin
 * Date: 3/24/2016
 * Time: 2:45 AM
 */

class SiteHelper
{
    public static function get_user_permission($id)
    {
        $user = Sentinel::findById($id);
        return $user;
    }

    public static function moduleInfo($name)
    {
        $row =  DB::table('modules')->where('module_name', $name)->first();
        return $row;
    }

    public static function checkPermission($id)
    {
        $row = DB::table('user_accesses')->where('module_id','=', $id)
            ->where('user_id','=', Sentinel::check()->id )
            ->get();

        if (count($row) >= 1) {
            $row = $row[0];
            if ($row->access_data != '') {
                $data = json_decode($row->access_data, true);
            } else {
                $data = array();
            }
            return $data;

        } else {
            return false;
        }
    }

    public static function cv_strength($cv_no)
    {
        $percentage = 0;
        $data = ApplicantInformation::where('cv_no',$cv_no)->get();
        if (count($data) != 0){
            $percentage += 10;
        }
        $data = ApplicantJobPosition::where('cv_id',$cv_no)->get();
        if (count($data) != 0){
            $percentage += 10;
        }
        $data = ApplicantAttachment::where('cv_id',$cv_no)->get();
        if (count($data) != 0){
            $percentage += 10;
        }
        $data = ApplicantCover::where('cv_id',$cv_no)->get();
        if (count($data) != 0){
            $percentage += 10;
        }
        $data = ApplicantEducation::where('cv_id',$cv_no)->get();
        if (count($data) != 0){
            $percentage += 10;
        }
        $data = ApplicantItSkill::where('cv_id',$cv_no)->get();
        if (count($data) != 0){
            $percentage += 10;
        }
        $data = ApplicantLanguageSkill::where('cv_id',$cv_no)->get();
        if (count($data) != 0){
            $percentage += 10;
        }
        $data = ApplicantOtherQualifications::where('cv_id',$cv_no)->get();
        if (count($data) != 0){
            $percentage += 10;
        }
        $data = ApplicantReferencePerson::where('cv_id',$cv_no)->get();
        if (count($data) != 0){
            $percentage += 10;
        }
        $data = ApplicantWorkExperience::where('cv_id',$cv_no)->get();
        if (count($data) != 0){
            $percentage += 10;
        }
        
        return $percentage;
    }

    public static function get_applicant_apply_position($cv_no)
    {
        return ApplicantJobPosition::where('cv_id',$cv_no)->get();
    }

    public static function get_applicant_work_exp($cv_no)
    {
        return ApplicantWorkExperience::where('cv_id',$cv_no)->get();
    }

    public static function get_applicant_education($cv_no)
    {
        return ApplicantEducation::where('cv_id',$cv_no)->get();
    }

    public static function get_applicant_other_qualification($cv_no)
    {
        return ApplicantOtherQualifications::where('cv_id',$cv_no)->get();
    }

    public static function get_applicant_it_skill($cv_no)
    {
        return ApplicantItSkill::where('cv_id',$cv_no)->get();
    }

    public static function get_applicant_lang_skill($cv_no)
    {
        return ApplicantLanguageSkill::where('cv_id',$cv_no)->get();
    }

    public static function get_applicant_ref_person($cv_no)
    {
        return ApplicantReferencePerson::where('cv_id',$cv_no)->get();
    }

    public static function get_employeed_job_position($cv_no,$com_code)
    {
        $data = EmployeeRecord::where('cv_id',$cv_no)->where('company_code',$com_code)->first();
        return ApplicantJobPosition::where('id',$data->job_position)->where('cv_id',$cv_no)->pluck('apply_position');
    }

    public static function get_employeed_salary($cv_no,$com_code)
    {
        return EmployeeRecord::where('cv_id',$cv_no)->where('company_code',$com_code)->first();
    }

    public static function add_activity($user_id , $type , $description )
    {
        $activity = new Activity();
        $activity->user_id = $user_id;
        $activity->type = $type;
        $activity->description = $description;
        $activity->save();
    }

    public static function dateDiff($query,$request)
    {
        switch ($request) {

                    case '1':
                        $query->where('date_diff','<=','1')->get();
                        break;
                    case '2':
                        $query->whereBetween('date_diff',['2','3'])->get();
                        break;
                    case '3':
                        $query->whereBetween('date_diff',['3','5'])->get();
                        //dd($key->whereBetween('new','<=',)->get());
                        break;
                    case '4':
                        $query->whereBetween('date_diff',['5','8'])->get();
                        break;
                    case '5':
                        $query->where('date_diff','>=','8')->get();                       
                        break;
                    
                    default:break;
                }        
                
        return $query;
            
       
        
    }
}