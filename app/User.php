<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    public static function get_all_admin_list()
    {
        return \DB::table('users')
            ->join('role_users', 'users.id','=','role_users.user_id')
            ->join('roles', 'roles.id','=','role_users.role_id')
            ->select('*')
            ->get();
    }

    public static function get_user_role($id)
    {
        return \DB::table('users')
            ->join('role_users', 'users.id','=','role_users.user_id')
            ->join('roles', 'roles.id','=','role_users.role_id')
            ->select('*')
            ->where('users.id',$id)
            ->first();
    }
}
