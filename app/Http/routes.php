<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Route::group(['middleware' => 'htmlminify'], function () {
    Route::group(['namespace' => 'Admin'], function()
    {
        Route::get('/','LoginController@getLogin');
        Route::get('byebye','LoginController@getLogout');
        Route::post('letmein','LoginController@postLogin');

        Route::get('cv/preview/{id}','CvController@preview');

        Route::group(['middleware' => 'admin'], function () {

            Route::get('activity',function(){
               return view('admin.layout.activity');
            });

            Route::get('interview/second_interview','InterviewController@second_interview');
            Route::get('interview/showall/{id}','InterviewController@showall');
            Route::get('interview/get_interview_person','InterviewController@get_interview_person');
            Route::get('interview/jsondata','InterviewController@interview_jsondata');

            //Setting
            Route::get('general_setting/email_setting','EmailController@index');
            Route::get('general_setting/site_setting','SettingController@site_setting');
            Route::get('general_setting/clear_cache','SettingController@clear_cache');
            Route::get('general_setting/clear_cache_data','SettingController@clear_cache_data');
            Route::post('general_setting/email_server_data','EmailController@email_server_data');

            //User Permission
            Route::get('module/manage_permission/{id}','ModuleController@manage_permission')->where('id', '[0-9]+');

            Route::post('module/save_user_permission/{id}/{module_id}','ModuleController@save_user_permission')->where(['id' => '[0-9]+', 'module_id' => '[0-9]+']);


            Route::post('interview/change_time/{id}','InterviewController@change_time');
            Route::post('interview/change_company_and_position/{id}','InterviewController@change_company_and_position');
            Route::post('interview/employee_record/{id}','InterviewController@employee_record');
            Route::post('interview/search','InterviewController@search');
            Route::post('interview/add_interview_person','InterviewController@add_interview_person');
            Route::post('interview/remark/{id}','InterviewController@remark');
            Route::post('interview/remark_delete/{id}','InterviewController@remark_delete');
            Route::post('interview/delete_interview_section/{id}','InterviewController@delete_interview_section');

            Route::controller('pdf','PdfController');

            Route::resource('admin','AdminController');
            Route::resource('cv','CvController');
            Route::resource('dashboard','DashboardController');
            Route::resource('general_setting','GeneralSettingController');
            Route::resource('role','RoleController');
            Route::resource('country','CountryController');
            Route::resource('client','ClientController');
            Route::resource('job_category','JobCategoryController');
            Route::resource('job_industry','JobIndustryController');
            Route::resource('invoice','InvoiceController');
            Route::resource('interview','InterviewController');
            Route::resource('employee','EmployeeController');
            Route::resource('module','ModuleController');

            Route::post('invoice/add_employee','InvoiceController@add_employee');
            Route::post('invoice/check_employee','InvoiceController@check_employee');
            Route::post('invoice/remove_added_employee/{id}','InvoiceController@remove_added_employee');
            Route::post('employee/search','EmployeeController@search');
            Route::post('employee/resign','EmployeeController@resign');

            Route::post('changePassword','AdminController@changePassword');
            Route::post('password','AdminController@password');

            Route::get('send_cv','CvController@multi_send_cv');
            Route::get('cv/send_cv/{id}','CvController@send_cv');
            Route::post('cv/send_cv','CvController@send');
            Route::post('send_multi_cv','CvController@send_multi_cv');
            Route::post('cv/add_new_job_data','CvController@add_new_job_data');
            Route::post('cv/add_new_edu_data','CvController@add_new_edu_data');
            Route::post('cv/add_new_work_exp_data','CvController@add_new_work_exp_data');
            Route::post('cv/add_new_other_qual_data','CvController@add_new_other_qual_data');
            Route::post('cv/add_new_other_it_data','CvController@add_new_other_it_data');
            Route::post('cv/add_new_lang_data','CvController@add_new_lang_data');
            Route::post('cv/add_new_attach_image','CvController@add_new_attach_image');
            Route::post('cv/add_new_ref_person','CvController@add_new_ref_person');
            Route::post('cv/set_as_interview_person','CvController@set_as_interview_person');
            Route::post('cv/change_cv_status/{id}','CvController@change_cv_status');

            Route::post('cv/update_contact_data','CvController@update_contact_data');
            Route::post('cv/update_personal_data','CvController@update_personal_data');
            Route::post('cv/update_career_obj_data','CvController@update_career_obj_data');
            Route::post('cv/update_job_data','CvController@update_job_data');
            Route::post('cv/update_edu_data','CvController@update_edu_data');
            Route::post('cv/update_other_data','CvController@update_other_data');
            Route::post('cv/update_it_data','CvController@update_it_data');
            Route::post('cv/update_lang_data','CvController@update_lang_data');
            Route::post('cv/update_work_exp_data','CvController@update_work_exp_data');
            Route::post('cv/update_ref_data','CvController@update_ref_data');
            Route::post('cv/update_cover_letter_data','CvController@update_cover_letter_data');
            Route::post('cv/update_cover_image_data','CvController@update_cover_image_data');
            Route::post('cv/update_applicant_image_data','CvController@update_applicant_image_data');

            Route::post('cv/delete_job_data','CvController@delete_job_data');
            Route::post('cv/delete_work_exp_data','CvController@delete_work_exp_data');
            Route::post('cv/delete_edu_data','CvController@delete_edu_data');
            Route::post('cv/delete_other_data','CvController@delete_other_data');
            Route::post('cv/delete_it_data','CvController@delete_it_data');
            Route::post('cv/delete_lang_data','CvController@delete_lang_data');
            Route::post('cv/delete_attach_data','CvController@delete_attach_data');
            Route::post('cv/delete_ref','CvController@delete_ref');

            Route::get('total_income','DashboardController@total_income');

            Route::get('export_excel', 'CvController@exportExcel');
            Route::get('import_excel', function(){
                return view('admin.excel_upload');  
            });
            Route::post('import_excel', 'CvController@importExcel');
        });

    });
//});


