<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\EmployeeInvoice;
use App\Models\EmployeeRecord;
use App\Models\Invoice;
use App\User;
use App\Models\ApplicantInformation;
use Illuminate\Http\Request;

use App\Http\Requests;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cv = ApplicantInformation::all();
        $client = Client::all();
        $user = User::all();
        $employed = EmployeeRecord::all();
        $client_count = count($client);
        $count = count($user);
        $cv_count = count($cv);
        $employed_count = count($employed);
        return view('admin.dashboard.index',compact('count','client_count','cv_count','employed_count'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function total_income()
    {
        $total = 0 ;
        $dates = Invoice::select('paid_date')->groupBy('paid_date')->get();

        foreach($dates as $date){
            $invoices = Invoice::where('paid_date',$date->paid_date)->get();


            foreach($invoices as $invoice ){
                $total += EmployeeInvoice::get_emp_total_salary($invoice->invoice_code,$invoice->company_code);
            }

            $data[] = [
                'date' => date('Y-m-d',strtotime($date->paid_date)),
                'value' => $total
            ];

            $total = 0 ;
        }



//        $data = json_decode(file_get_contents(public_path().'/data/mg_some_currency.json'));

        return response()->json($data);
    }
}
