<?php

namespace App\Http\Controllers\Admin;

use App\Library\MYPDF;
use App\Models\ApplicantAttachment;
use App\Models\ApplicantCover;
use App\Models\ApplicantEducation;
use App\Models\ApplicantInformation;
use App\Models\ApplicantItSkill;
use App\Models\ApplicantJobPosition;
use App\Models\ApplicantLanguageSkill;
use App\Models\ApplicantOtherQualifications;
use App\Models\ApplicantReferencePerson;
use App\Models\ApplicantWorkExperience;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Country;

use App\Models\IT;
use App\Models\Job_Category;
use App\Models\Job_Industry;
use App\Models\Language;
use App\Models\University;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\View;
use PDF;
use SiteHelper;

class PdfController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
//
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
//
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
//
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
//
    }

    /**
     * @param Request $request
     * @param $id
     */
    public function postPrint(Request $request, $id)
    {

        $cv_no = ApplicantInformation::find($id);
        $data = $this->getCoverData($cv_no->cv_no);

        $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->SetAuthor('Oway HR Solutions');
        $pdf->SetTitle($cv_no->applicant_name);
        $pdf->SetSubject('CV Form');
        $pdf->SetKeywords('CV');

// remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(true);

// set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, 15 , PDF_MARGIN_RIGHT);

// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);



// $pdf->AddPage();
//
// $html = '<div style="text-align:center">
// <img src="' . $data->cover_image . '" alt="test alt attribute" width="500" height="500" border="0" />
// </div>
// <div style="text-align:center">
// <p><h1>' . $data->cover_letter . '</h1></p>
// </div>
// ';
//
// $pdf->writeHTML($html, true, false, true, false, '');


//
// $pdf->lastPage();
//
// $pdf->AddPage();
// $html = file_get_contents( url('cv/preview').'/'.$id ) ;
//
// $pdf->writeHTML($html, true, false, true, false, '');
//
// $pdf->lastPage();

// $pdf->AddPage();
//
//
// $opts = array(
// 'http' => array(
// 'header' => "User-Agent:MyAgent/1.0\r\n"
// ),
// );
// $context = stream_context_create($opts);
// $html = file_get_contents('http://oway.local/cv/preview/3');
//
//
// $pdf->writeHTML($html, true, false, true, false, '');

        $pdf->SetLineWidth(2);

// set alpha to semi-transparency
        $pdf->SetAlpha(0.2);

// draw jpeg image
        $pdf->Image(public_path('img/water.png'), 150, 160, 150, 150, '', '#', '', true, 72);

// restore full opacity
        $pdf->SetAlpha(1);

        $pdf->AddPage();
        $applicant_job_position = $this->getJobPosition($id);


        $applicant_information = $this->getContactInformation($id);

        if ($applicant_information->cv_image) {
            $image = $applicant_information->cv_image ;
        }else{
            $image = 'http://www.gravatar.com/avatar/00000000000000000000000000000000?d=mm&amp;f=y';
        }
        if ($applicant_information->gender == 1) {
            $gender = 'Male';
        } else {
            $gender = 'Female';
        }

        if ($applicant_information->gender == 1) {
            $gender = 'Male';
        } else {
            $gender = 'Female';
        }

        if ($request->input('contact_info')== '1'){
            $style = "display:block";
        }else{
            $style = "display:none";
        }

        if ($applicant_information->martial_status == 1) {
            $martial_status = 'Married';
        } else {
            $martial_status = 'Single';
        }


        if($applicant_information->currency == '1'){
            $salary = '$ ' . $applicant_information->salary_exp ;
        }else{
            $salary = $applicant_information->salary_exp .' MMK';
        }
        $html = '
<style>
body {
font-size: 8px;
}
#wrapper {
width: 900px;
margin: 0 auto;
margin-top: 60px;
margin-bottom: 100px;
}

.wrapper-top {
width: 900px;
height: 19px;
margin: 0 auto;
background-repeat: no-repeat;
}

.wrapper-mid {
width: 900px;
margin: 0 auto;
background-repeat: repeat-y;
padding-bottom: 40px;
}

.wrapper-bottom {
width: 900px;
height: 22px;
padding: 0;
margin: 0 auto;
background-repeat: no-repeat;
}

#paper {
width: 800px;
margin: 0 auto;
}

#wrapper, .wrapper-mid {
background: none;
}

.wrapper-top, .wrapper-bottom {
display: none;
}

.print-pp p {
font-size: 9px;
}

h3 {
font-size: 12px;
color: #7dc24b;
}

h5 {
font-weight: bold;
font-size: 10px;
}

td{
font-size: 9px;

 
}
td1{
font-size: 9px;
 
width:70px;
}
div.line {
border-bottom: 2px solid #7dc24b;
}

.list-symbol-bullet li {
line-height: 30px;
list-style-type: none;
}

.address {
margin-top: 20px;
}

.fileAttachmentAnnotation{
background: black;
}
</style>
<table cellpadding="3" cellspacing="2">
<tr nobr="true">
<td style="float:left;padding: 50px">
<img src="/img/oway.png" width="180px" height="85px" style="float: left;padding: 50px"/>
</td>

<td style="float:left">

</td>

<td >
<div class="col-md-8 pull-right smallpadding" style="float: left;">
<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 49/B,1st Floor, Moe Sandar Street, Ward (1),<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Kamayut, Yangon, Myanmar</p>
<span class="text-muted">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Telephone:</span> 01 525011, 01 503196 <br>
<span class="text-muted">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Email:</span> <a
href="#">cvs@owayhrsolutions
.com.mm<br></a>
</div>
</td>
</tr>
</table>
<div style="margin-bottom:5px;padding: 1px;border-bottom: 2px solid #7dc24b;"></div>
<table  cellpadding="9" cellspacing="3">
 <tr nobr="true">
<td style="float:left">
 <img src="' .  $image . '" style="height:130px;width:115px" alt="Applicant Image" class="img-responsive">
</td>
</tr>
 
 
</table>
<br/>';

        if ($request->input('contact_info')== '1') {

        $html .= '<h3>Contact</h3>
<div class="line" style="border-bottom: 2px solid #7dc24b;"></div>


        <table  cellpadding="9" cellspacing="3">
       
    
 <tr nobr="true">
  <td style="float:left"><span style="color: #7dc24b;font-size: 14px"><b>'.         $applicant_information->applicant_name . '</b></span><br/>

  <table  cellpadding="2" cellspacing="2">
 <tr nobr="true">
  <td style="float:left">Ref
  </td>
  <td style="float:left">: '. $applicant_information->cv_no . '
    </td>
 </tr>

 <tr nobr="true">
  <td style="float:left">Mobile
  </td>
  <td style="float:left">: '. $applicant_information->contact_number . ',<br>  ' . $applicant_information->sec_contact . '</td>
 </tr>

 <tr nobr="true">
  <td style="float:left;">Email  </td>
  <td style="font-size: 8.2px;">: '. $applicant_information->email . '
    </td>
 </tr>

 <tr nobr="true">
  <td style="float:left">Apply Position
    </td>
  <td style="float:left">: '. $applicant_job_position ->apply_position . '
    </td>
 </tr>

 <tr nobr="true">
  <td style="float:left">Salary Expectations
    </td>
  <td style="float:left">: ' . $salary . '
    </td>
 </tr>
 
  <tr nobr="true">
  <td style="float:left">Noticed Period
    </td>
  <td style="float:left">: ' . $applicant_information->not_period . '
    </td>
 </tr>
 
</table>

  </td>

 </tr>
</table>
        
        <br/>
';}


        if ($request->input('personal_info')== '1') {
            $html .= '<h3>Summary</h3>
<div class="line" style="border-bottom: 2px solid #7dc24b;"></div>
<table cellpadding="1" cellspacing="2">
<tr nobr="true">
<td style="float:left">
Profile
</td>
<td style="float:left">-
' . $gender . '&nbsp;&nbsp;,' . $martial_status . '
</td>
</tr>
<tr nobr="true">
<td style="float:left">
Date of birth
</td>
<td style="float:left">-
' . date('F d, Y', strtotime($applicant_information->dob)) . '
</td>
</tr>
<tr nobr="true">
<td style="float:left">
Father Name
</td>
<td style="float:left">-
' . $applicant_information->father_name . '
</td>
</tr>
<tr nobr="true">
<td style="float:left">
N.R.C No
</td>
<td style="float:left">-
' . $applicant_information->nrc . '
</td>
</tr>
<tr nobr="true">
<td style="float:left">
Nationality
</td>
<td style="float:left">-
' . $applicant_information->nationality . '
</td>
</tr>
<tr nobr="true">
<td style="float:left">
Height
</td>

<td style="float:left">-
' . $applicant_information->height . '
</td>
</tr>
<tr nobr="true">
<td style="float:left">
Weight
</td>

<td style="float:left">-
' . $applicant_information->weight . '
</td>
</tr>
<tr nobr="true">
<td style="float:left">
Current Location
</td>

<td style="float:left">-
' . Country::where('id', $applicant_information->curr_location)->pluck('country_name') . '
</td>
</tr>
<tr nobr="true" >
<td style="float:left">
Address
</td>

<td style="float:left">-
' . $applicant_information->address . '
</td>
</tr>

<tr nobr="true">
<td style="float:left">
Last Position
</td>

<td style="float:left">-
' . $applicant_information->curr_position. '

</td>
</tr>
<tr nobr="true">
<td style="float:left">
Last Company
</td>

<td style="float:left">-
' . $applicant_information->curr_company . '
</td>
</tr>
</table><br/>
';

        }
        if ($request->input('c_obj')== '1') {
            $html .= '<h3>Career Objective</h3>
<div class="line" style="border-bottom: 2px solid #7dc24b;"></div>
<table cellpadding="2" cellspacing="2">
<tr nobr="true">
<td style="float:left">
' . $applicant_information->c_objective . '
</td>
</tr>
</table><br/>
';
        }

        if ($request->input('edu_back')== '1') {
            $edu_back = SiteHelper::get_applicant_education($cv_no->cv_no);

            $html .= '<h3>Educational Background</h3>
<div class="line" style="border-bottom: 2px solid #7dc24b;"></div>
<table cellpadding="4" cellspacing="2">';
            foreach ($edu_back as $edu) {
                $degree = University::where('id', $edu->degree_lvl)->pluck('degree_name') . ' ';
                $other = ($edu->other) ? ' '.$edu->other : ' ';
                $major_name = ($edu->major_name) ? ','.$edu->major_name : '';
                $html .=
                    '<tr nobr="true">
<td style="float:left">
' . $edu->uni_start_date . ' - ' . $edu->uni_graduation_date . '
</td>
<td style="float:left">
<b>' . $edu->uni_name . '</b>
' . Country::where('id', $edu->uni_country)->pluck('country_name') . '
</td>
<td style="float:left">
' . $degree . $other . $major_name . '
</td>
</tr>
<tr>
<td style="float:left"></td>
</tr>';
            }
            $html .= '</table><br/>
';
        }

        if ($request->input('other_qual')== '1') {
            $other_qualification = SiteHelper::get_applicant_other_qualification($cv_no->cv_no);
            $html .= '<h3>Other Qualifications</h3>
<div class="line" style="border-bottom: 2px solid #7dc24b;"></div>
<table cellpadding="2" cellspacing="2">';
            foreach ($other_qualification as $other) {
                $html .=
                    '<tr nobr="true">
<td style="float:left">
' . $other->certi_date . '
</td>
<td style="float:left">
<b>' . $other->training_center . '</b><br>
' . Country::where('id', $other->certi_country)->pluck('country_name') . '
</td>
<td style="float:left">
<b>' . $other->certi_name . '</b>
</td>
</tr>
<tr>
<td style="float:left"></td>
</tr>';
            }
            $html .= '</table><br/>
';
        }

        if ($request->input('it_skill')== '1') {
            $it_skill = SiteHelper::get_applicant_it_skill($cv_no->cv_no);
            $html .= '<h3>IT Skill</h3>
<div class="line" style="border-bottom: 2px solid #7dc24b;"></div>
<table cellpadding="2" cellspacing="2">';
            foreach ($it_skill as $it) {
                $html .=
                    '<tr nobr="true">
<td style="float:left">
<b> - ' . $it->topic_app_name . '</b>
</td>
<td style="float:left">
' . IT::where('id', $it->it_type)->pluck('type_name') . '
</td>
<td style="float:left">
<b>' . DB::table('it_lvl')->where('id', $it->it_skill)->pluck('it_lvl_name') . '</b>
</td>
</tr>
<tr>
<td style="float:left"></td>
</tr>';
            }
            $html .= '</table><br/>
';
        }

        if ($request->input('lang_skill')== '1') {
            $lang_skill = SiteHelper::get_applicant_lang_skill($cv_no->cv_no);
            $html .= '<h3>Language Skill</h3>
<div class="line" style="border-bottom: 2px solid #7dc24b;"></div>
<table cellpadding="2" cellspacing="2">';
            foreach ($lang_skill as $lang) {
                $html .=
                    '<tr nobr="true">
<td style="float:left">
<b> - ' . Language::where('id', $lang->language_name)->pluck('lang_name') . '</b>
</td>
<td style="float:left">
<b>' . DB::table('language_lavel')->where('id', $lang->language_skill)->pluck('lang_lvl') . '</b>
</td>
</tr>
<tr>
<td style="float:left"></td>
</tr>
';
            }
            $html .= '</table><br/>
';
        }

        if ($request->input('work_exp')== '1') {
            $applicant_work_exp = SiteHelper::get_applicant_work_exp($cv_no->cv_no);
            $html .= '<h3>Working Experience</h3>
<div class="line" style="border-bottom: 2px solid #7dc24b;"></div>
<table cellpadding="2" cellspacing="2">';
            foreach ($applicant_work_exp as $work) {
                $html .=
                    '<tr nobr="true">
<td style="float:left">' . $work->exp_from_date . ' - ' . $work->exp_to_date . '
</td>
<td style="float:left">
<b> ' . $work->exp_job_title . ' </b>
</td>
<td style="float:left">
<b> ' . $work->exp_company_name . '</b>
</td>
</tr>
<tr nobr="true">
<td style="float:left"><b>Industry </b>
</td>
<td style="float:left">
: ' . Job_Industry::where('id', $work->exp_job_industry)->pluck('industry_description') . '
</td>
<td style="float:left">
' . $work->exp_job_city . ' , ' . Country::where('id', $work->exp_job_country)->pluck('country_name') . '
</td>
</tr>
<tr nobr="true">
<td style="float:left" ><b>Job Description </b>
</td>
<td colspan="2">' . $work->exp_description . '
</td>
</tr>
<tr>
<td style="float:left"></td>
</tr>
';
            }
            $html .= '</table><br/>
';
        }

        if ($request->input('ref_person')== '1') {
            $ref_person = SiteHelper::get_applicant_ref_person($cv_no->cv_no);
            $html .= '<h3>Reference Person</h3>
<div class="line" style="border-bottom: 2px solid #7dc24b;"></div>
<table cellpadding="2" cellspacing="2">';
            foreach ($ref_person as $ref) {
                $html .=
                    '<tr nobr="true">
<td style="float:left">'. $ref->reference_person_name . '
</td>
<td style="float:left"><b>'. $ref->reference_person_position . '</b>
</td>
<td style="float:left">
<b>" ' . $ref->reference_person_company . ' "</b>
</td>
</tr>
<tr nobr="true">
<td colspan="2"><b>Mobile No : </b>' . @$ref->reference_person_mobile . '
</td>
<td style="float:left">'. $ref->reference_person_email . '
</td>
</tr>
<tr>
<td style="float:left"></td>
</tr>';
            }
            $html .= '</table><br/>
';
        }


        $pdf->SetLineWidth(2);

// set alpha to semi-transparency
        $pdf->SetAlpha(0.2);

// draw jpeg image
        $pdf->Image(public_path('img/water.png'), 50, 90, 110, 60, '', '#', '', true, 72);

// restore full opacity
        $pdf->SetAlpha(1);



        $pdf->writeHTML($html, true, false, true, false, '');
       

 
        $pdf->SetLineWidth(2);

// set alpha to semi-transparency
        $pdf->SetAlpha(0.2);

// draw jpeg image
        $pdf->Image(public_path('img/water.png'), 50, 90, 110, 60, '', '#', '', true, 72);

// restore full opacity
        $pdf->SetAlpha(1);








        if ( count($request->input('attachment')) > 0 ) {
            $pdf->AddPage();


            $html = '';

            $pos = 10;
            for ($q = 0; $q < count($request->input('attachment')); $q++) {
                $pdf->Annotation($pos, 50, 30 , 30 , $request->input('attachment'. '.' . $q), array('Subtype' => 'FileAttachment', 'Name' => 'Comment', 'T' => 'title example', 'Subj' => 'example' , 'FS' => public_path() . '/upload/attachment/'.'/'.$request->input('attachment'. '.' . $q)));
                $pos += 10;
            }
            $pdf->writeHTML($html, true, false, true, false, '');

            $pdf->lastPage();
        }

        $pdf->Output($_SERVER['DOCUMENT_ROOT'] . '/pdf/cv/' . $cv_no->applicant_name .'('.$cv_no->cv_no.').pdf', 'FD');

        $pdf->Open();


    }

    public function getCoverData($cv)
    {
        return ApplicantCover::where('cv_id', $cv)->first();

    }

    public function getContactInformation($id)
    {
        return ApplicantInformation::find($id);
    }
    public function getJobPosition($id)
    {
        return ApplicantJobPosition::find($id);
    }
}