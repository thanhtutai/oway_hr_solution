<?php

namespace App\Http\Controllers\Admin;

use App\Models\ApplicantInformation;
use App\Models\EmployeeRecord;
use App\Models\Interview;
use App\Models\RejectPerson;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use SiteHelper;

class EmployeeController extends Controller
{
    protected $user;

    /**
     * EmployeeController constructor.
     */
    public function __construct()
    {
        $this->user = Sentinel::check();
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employee_list = EmployeeRecord::select('cv_id', 'job_receive_date')->groupBy('job_receive_date')->paginate(30);
        return view('admin.employee.index', compact('employee_list'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request)
    {
        $from = $request->input('from_date');
        $to = $request->input('to_date');
        $employee_list = EmployeeRecord::searchRecord($from, $to);
        return view('admin.employee.search', compact('employee_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $emp_record = EmployeeRecord::find($id);
        $emp_record->job_receive_date = date('Y-m-d', strtotime($request->input('receive')));
        $emp_record->joining_date = date('Y-m-d', strtotime($request->input('joining')));
        $emp_record->invoicing_date = date('Y-m-d', strtotime($request->input('invoicing')));
        $emp_record->payment_date = date('Y-m-d', strtotime($request->input('payment')));
        $emp_record->company_contact_person = $request->input('com_contact');
        $emp_record->owaycompany_contact_person = $request->input('oway_contact');
        $emp_record->company_email = $request->input('com_email');
        $emp_record->actual_amount = $request->input('actual_amount');
        $emp_record->salary = $request->input('salary');

        if ($emp_record->save()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param Request $request
     */
    public function resign(Request $request)
    {
        $resign = new RejectPerson();
        $resign->company_code = $request->input('emp_resign_data.company_code');
        $resign->cv_no = $request->input('emp_resign_data.cv_no');
        $resign->job_position = $request->input('emp_resign_data.job_position');
        $resign->resign_date = date('Y-m-d', strtotime($request->input('emp_resign_data.resign_date')));
        $resign->reason = $request->input('emp_resign_data.resign_reason');
        $resign->create_user_id = Sentinel::check()->id;

        if ($resign->save()) {
            EmployeeRecord::where('company_code', $request->input('emp_resign_data.company_code'))
                ->where('job_position', $request->input('emp_resign_data.job_position'))
                ->where('cv_id', $request->input('emp_resign_data.cv_no'))->delete();

            DB::table('applicant_information')
                ->where('cv_no', $request->input('emp_resign_data.cv_no'))
                ->update(
                    ['cv_status' => 1]
                );

            $empinfo = ApplicantInformation::where('cv_no', $request->input('emp_resign_data.cv_no'))->first();

            $type = "resign";
            $des = "make resign - <a target='_blank' href='" . url('cv') . "/" . $empinfo->id . "' >" . $empinfo->applicant_name . "</a>";

            SiteHelper::add_activity(Sentinel::check()->id, $type, $des);
        }

    }

}
