<?php

namespace App\Http\Controllers\Admin;

use App\Models\Client;
use App\Models\ClientAttach;
use App\Models\EmployeeRecord;
use App\Models\Interview;
use App\Models\Job_Industry;
use App\Models\RejectPerson;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Faker\Factory;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use SiteHelper;


class ClientController extends Controller
{
    protected $module = 'client';
    protected $permission = array();
    protected $info;
    protected $access;

    /**
     * ClientController constructor.
     */
    public function __construct()
    {
        $this->info = SiteHelper::moduleInfo($this->module);
        $this->access = SiteHelper::checkPermission($this->info->id);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if ($this->access['view'] != '1')
            return view('admin.errors.403');

        $client = Client::all();
        return view('admin.client.index',compact('client'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ($this->access['create'] != '1')
            return view('admin.errors.403');

        $industry = Job_Industry::orderBy('industry_description','asc')->get();
        return view('admin.client.create_client_form',compact('industry'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($this->access['create'] != '1')
            return view('admin.errors.403');

        $ran = mt_rand(1, 1000);
        $before = str_pad('', 3, $ran, STR_PAD_LEFT);
        $y = date('Y');
        $y = substr($y, -2);
        $m = date('m');
        $m = substr($m, -1);
        $d = date('d');
        $code = "CC".$before . '-' . $y . $m . $d ;

        $rules = [
            'company_name' => 'required',
            'industry' => 'required',
            'address' => 'required',
            'contact_person' => 'required',
            'contact_number' => 'required',
            'email' => 'required',
            'plan' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()
                ->withInput()
                ->withErrors($validator);
        }

        if($validator->passes()){
            $client = new Client();
            $client->company_name = $request->input('company_name');
            $client->industry = $request->input('industry');
            $client->address = $request->input('address');
            $client->contact_person = $request->input('contact_person');
            $client->contact_number = $request->input('contact_number');
            $client->email = $request->input('email');
            $client->company_code = $code;
            $client->plan = $request->input('plan');
            if ($request->input('link')){
                $client->client_link = $request->input('link');
            }
            if ($request->input('manual_date')) {
                $client->expire_date = $request->input('manual_date');
            }

            if ($request->input('manual_date_description')) {
                $client->plan_description = $request->input('manual_date_description');
            }
            $client->create_user_id = Sentinel::check()->id;

//            if ($request->input('is_paid')){
//                $client->is_paid = 1;
//            }

            if (Input::hasFile('company_logo')) {
//                $filename = time().'_company_logo.'.Input::file('company_logo')->getClientOriginalExtension();

                $img = file_get_contents(Input::file('company_logo'));
                $imagefile = 'data:image/png;base64,' . base64_encode($img);

//                Input::file('company_logo')->move($clientLogoPath , $filename );
                $client->client_logo = $imagefile;
            }

            $client->save();

            if (Input::hasFile('signed_contract_image')) {
                $signed_contract = new ClientAttach();
//                $filenames = time().'_signed_contract.'.Input::file('signed_contract_image')->getClientOriginalExtension();

                $img = file_get_contents(Input::file('signed_contract_image'));
                $imagefile = 'data:image/png;base64,' . base64_encode($img);

//                Input::file('signed_contract_image')->move($signedcontractPath , $filenames );
                $signed_contract->client_id = $client->id;
                $signed_contract->image_name = $imagefile;
                $signed_contract->save();
            }

            $type = "create";
            $des = "create Company Name - ".$request->input('company_name') ;

            SiteHelper::add_activity(Sentinel::check()->id, $type, $des);

            \Session::flash('status','Client added sucessful!');
            return Redirect::to('client');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $client = Client::find($id);
        $interview_person = Interview::where('company_code',$client->company_code)->where('interview_status','!=','4')->paginate(3);
        $employee_list = EmployeeRecord::where('company_code',$client->company_code)->get();
        $resign_list = RejectPerson::where('company_code',$client->company_code)->get();
        return view('admin.client.detail',compact('client','interview_person','employee_list','resign_list'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if ($this->access['update'] != '1')
            return view('admin.errors.403');

        $client = Client::find($id);
        $industry = Job_Industry::orderBy('industry_description','asc')->get();
        return view('admin.client.edit_client_form',compact('industry','client'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($this->access['update'] != '1')
            return view('admin.errors.403');

        $signedcontractPath = "upload/signed_contract";
        $clientLogoPath = "upload/company_logo";

        $rules = [
            'company_name' => 'required',
            'address' => 'required',
            'contact_person' => 'required',
            'contact_number' => 'required',
            'email' => 'required',
            'plan' => 'required',
        ];

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return Redirect::back()
                ->withInput()
                ->withErrors($validator);
        }

        if ($validator->passes()) {
            $client = Client::find($id);
            $client->company_name = $request->input('company_name');
            $client->industry = $request->input('industry');
            $client->address = $request->input('address');
            $client->contact_person = $request->input('contact_person');
            $client->contact_number = $request->input('contact_number');
            $client->email = $request->input('email');
            $client->plan = $request->input('plan');
            if ($request->input('link')) {
                $client->client_link = $request->input('link');
            }
            if ($request->input('manual_date')) {
                $client->expire_date = $request->input('manual_date');
            }

            if ($request->input('manual_date_description')) {
                $client->plan_description = $request->input('manual_date_description');
            }

//            if ($request->input('status')) {
//                $client->status = 1;
//            } else {
//                $client->status = 0;
//            }

//            if ($request->input('is_paid')) {
//                $client->is_paid = 1;
//            }else {
//                $client->status = 0;
//            }

            if ($request->hasFile('company_logo')) {
//                $logo = $client->client_logo;
//                if($logo != null){
//                    if(file_exists('upload/company_logo/'.$logo )){
//                        unlink('upload/company_logo/'.$logo);
//                    }
//                }

                $img = file_get_contents($request->file('company_logo'));
                $imagefile = 'data:image/png;base64,' . base64_encode($img);

//                Input::file('company_logo')->move($clientLogoPath , $filename );
                $client->client_logo = $imagefile;
            }

            $client->save();

            if ($request->hasFile('signed_contract_image')) {
                $signed_contract = new ClientAttach();

                $img = file_get_contents(Input::file('signed_contract_image'));
                $imagefile = 'data:image/png;base64,' . base64_encode($img);

//                Input::file('signed_contract_image')->move($signedcontractPath , $filenames );
                $signed_contract->client_id = $client->id;
                $signed_contract->image_name = $imagefile;
                $signed_contract->save();
            }


            $type = "update";
            $des =   "updated Company Name - <a href='".url('client')."/".$id."' target='_blank' >".$client->company_name ."</a>"  ;

            SiteHelper::add_activity(Sentinel::check()->id, $type, $des);

            \Session::flash('status', 'Client updated sucessful!');
            return Redirect::to('client');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if($this->access['delete'] != '1')
            return view('admin.errors.403');

        $client = Client::find($id);

        Client::destroy($id);

        $type = "delete";

        $des = "delete Company Name - ".$client->company_name ;

        SiteHelper::add_activity(Sentinel::check()->id, $type, $des);

        ClientAttach::where('client_id',$id)->delete();

        \Session::flash('status', 'Client sucessfully removed!');

        return Redirect::back();
    }
}
