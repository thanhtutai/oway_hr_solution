<?php

namespace App\Http\Controllers\Admin;

use App\Library\MYPDF;
use App\Models\ApplicantAttachment;
use App\Models\ApplicantCover;
use App\Models\ApplicantEducation;
use App\Models\ApplicantInformation;
use App\Models\ApplicantItSkill;
use App\Models\ApplicantJobPosition;
use App\Models\ApplicantLanguageSkill;
use App\Models\ApplicantOtherQualifications;
use App\Models\ApplicantReferencePerson;
use App\Models\ApplicantWorkExperience;
use App\Models\Client;
use App\Models\Country;
use App\Models\Email;
use App\Models\Interview;
use App\Models\InterviewCompany;
use App\Models\IT;
use App\Models\Job_Category;
use App\Models\Job_Industry;
use App\Models\Language;
use App\Models\University;
use Carbon\Carbon;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use PDF;
use Session;
use SiteHelper;
use TeamTNT\TNTSearch\TNTSearch;
use Excel;
/**
 * Class CvController
 * @package App\Http\Controllers\Admin
 */
class CvController extends Controller
{   
    protected $module = 'cv';
    protected $permission = array();
    protected $info;
    protected $access;

    /**
     * CountryController constructor.
     */
    public function __construct()
    {
        $this->info = SiteHelper::moduleInfo($this->module);
        $this->access = SiteHelper::checkPermission($this->info->id);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index(Request $request)
    {                
        if ($this->access['view'] != '1'){
            return view('admin.errors.403');
        }
        if(count($request->all())!=0){  
            $applicant_information=ApplicantInformation::search($request)->with('apply_job_post')->paginate(10);   

        }else{
            $applicant_information = ApplicantInformation::paginate(10);
        }      
        
        return view('admin.cv.cv_list', compact('applicant_information'));    
    }

    public function search_cv_data(Request $request)
    {
        $tnt = new TNTSearch;
        $tnt->loadConfig([
            'driver' => 'mysql',
            'host' => 'localhost',
            'database' => 'owayss',
            'username' => 'owayhrso_nyinyi',
            'password' => 'C9p5au8naa',
            'storage' => storage_path('app')
        ]);

        $indexer = $tnt->createIndex('index');
        $indexer->disableOutput = true;
        $indexer->query('SELECT id, nrc, FROM applicant_information');
        $indexer->run();

        $tnt->selectIndex("index");

        $res = $tnt->search($request->input('search'));

        dd($res);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if ($this->access['create'] != '1')
            return view('admin.errors.403');

        return view('admin.cv.create_cv_form');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ran = mt_rand(1, 1000);
        $ran2 = mt_rand(1, 100);
        $job_code = Job_Category::where('id', $request->input('cv_data.job_category'))->pluck('job_code');
        $detail = ApplicantJobPosition::where('job_category', $request->input('cv_data.job_category'))->get();
        $count = count($detail) + 1;
        $before = str_pad('', 3, $ran, STR_PAD_LEFT);
        $after = str_pad('', 2, $ran2, STR_PAD_LEFT);
        $y = date('Y');
        $y = substr($y, -2);
        $m = date('m');
        $m = substr($m, -1);

        $cv_no = $job_code . '-' . $before . $y . $m . $after . '-' . $count;

        #applicant inforcation
        $applicant_information = new ApplicantInformation();
        $applicant_information->cv_no = $cv_no;
        $applicant_information->cv_image = $request->input('cv_data.applicant_image');
        $applicant_information->applicant_name = $request->input('cv_data.applicant');
        $applicant_information->nrc = $request->input('cv_data.nrc');
        $applicant_information->father_name = $request->input('cv_data.father_name');
        $applicant_information->address = $request->input('cv_data.address');
        $applicant_information->curr_location = $request->input('cv_data.cur_location');
        $applicant_information->dob = $request->input('cv_data.dob');
        $applicant_information->martial_status = $request->input('cv_data.martial_status');
        $applicant_information->gender = $request->input('cv_data.gender');
        $applicant_information->weight = $request->input('cv_data.weight');
        $applicant_information->height = $request->input('cv_data.height');
        $applicant_information->nationality = $request->input('cv_data.nationality');
        $applicant_information->religion = $request->input('cv_data.religion');
        $applicant_information->curr_position = $request->input('cv_data.current_position');
        $applicant_information->curr_company = $request->input('cv_data.current_company_name');
        $applicant_information->salary_exp = $request->input('cv_data.exp_salary') ;
        $applicant_information->currency = $request->input('cv_data.currency');
        $applicant_information->negotiable = $request->input('cv_data.negotiable');
        $applicant_information->not_period = $request->input('cv_data.noticed_period');
        $applicant_information->contact_number = $request->input('cv_data.contact_number');
        $applicant_information->sec_contact = $request->input('cv_data.sec_contact_number');
        $applicant_information->email = $request->input('cv_data.email');
        $applicant_information->c_objective = $request->input('cv_data.c_objective');
        $applicant_information->create_user_id = \Session::get('admin_id');
        //$applicant_information->cv_status = 1;

        if ($applicant_information->save()) {

            if ($request->input('cv_data.job_category')) {
                $job_position = new ApplicantJobPosition();
                $job_position->cv_id = $applicant_information->cv_no;
                $job_position->job_category = $request->input('cv_data.job_category');
                $job_position->job_industry = $request->input('cv_data.job_industry');
                $job_position->job_type = $request->input('cv_data.job_type');
                $job_position->location = $request->input('cv_data.target_location');
                $job_position->apply_position = $request->input('cv_data.apply_position');
                $job_position->save();
            }

            if ($request->input('cv_data.exp_job_idustry')) {
                $date_from = new Carbon($request->input('cv_data.from_date'));
                $date_to = new Carbon($request->input('cv_data.to_date'));
                $f = date('Y',strtotime($date_to)) - date('Y',strtotime($date_from));
                $exprience_job = new ApplicantWorkExperience();
                $exprience_job->cv_id = $applicant_information->cv_no;
                $exprience_job->exp_job_industry = $request->input('cv_data.exp_job_idustry');
                $exprience_job->exp_job_category = $request->input('cv_data.exp_job_category');
                $exprience_job->exp_job_country = $request->input('cv_data.exp_country');
                $exprience_job->exp_job_city = $request->input('cv_data.exp_city');
                $exprience_job->exp_company_name = $request->input('cv_data.exp_company_name');
                $exprience_job->exp_job_title = $request->input('cv_data.exp_job_title');
                $exprience_job->exp_from_date = date('Y-m',strtotime($date_from));
                $exprience_job->exp_to_date = date('Y-m',strototime($date_to));
                $experience_job->date_diff = $f;
                $exprience_job->exp_description = $request->input('cv_data.description');
                $exprience_job->save();
            }

            if ($request->input('cv_data.university')) {
                $education = new ApplicantEducation();
                $education->cv_id = $applicant_information->cv_no;
                $education->uni_name = $request->input('cv_data.university');
                $education->uni_start_date = $request->input('cv_data.uni_from_date');
                $education->uni_graduation_date = $request->input('cv_data.uni_to_date');
                $education->major_name = $request->input('cv_data.major');
                $education->uni_country = $request->input('cv_data.edu_country');
                $education->degree_lvl = $request->input('cv_data.degree_level');
                $education->save();
            }

            if ($request->input('cv_data.certificate')) {
                $other_qualifications = new ApplicantOtherQualifications();
                $other_qualifications->cv_id = $applicant_information->cv_no;
                $other_qualifications->certi_name = $request->input('cv_data.certificate');
                $other_qualifications->training_center = $request->input('cv_data.training_center');
                $other_qualifications->certi_country = $request->input('cv_data.certi_country');
                $other_qualifications->certi_date = $request->input('cv_data.certi_date');
                $other_qualifications->save();
            }

            if ($request->input('cv_data.topic_app')) {
                $it_skill = new ApplicantItSkill();
                $it_skill->cv_id = $applicant_information->cv_no;
                $it_skill->topic_app_name = $request->input('cv_data.topic_app');
                $it_skill->it_type = $request->input('cv_data.it_type');
                $it_skill->it_skill = $request->input('cv_data.it_skill_level');
                $it_skill->save();
            }

            if ($request->input('cv_data.language')) {
                $lang_skill = new ApplicantLanguageSkill();
                $lang_skill->cv_id = $applicant_information->cv_no;
                $lang_skill->language_name = $request->input('cv_data.language');
                $lang_skill->language_skill = $request->input('cv_data.language_skill');
                $lang_skill->save();
            }

            if ($request->input('cv_data.reference_person_name')) {
                $refrence_person = new ApplicantReferencePerson();
                $refrence_person->cv_id = $applicant_information->cv_no;
                $refrence_person->reference_person_name = $request->input('cv_data.reference_person_name');
                $refrence_person->reference_person_position = $request->input('cv_data.reference_person_position');
                $refrence_person->reference_person_company = $request->input('cv_data.reference_person_company');
                $refrence_person->reference_person_mobile = $request->input('cv_data.reference_person_mobile');
                $refrence_person->reference_person_email = $request->input('cv_data.reference_person_email');
                $refrence_person->save();
            }

            if ($request->input('cv_data.attachment')) {
                $attachment = new ApplicantAttachment();
                $attachment->cv_id = $applicant_information->cv_no;
                $attachment->attachment = $request->input('cv_data.attachment');
                $attachment->save();
            }

            $cover = new ApplicantCover();
            $cover->cv_id = $applicant_information->cv_no;
            if ($request->input('cv_data.cover_image') != null) {
                $cover->cover_image = $request->input('cv_data.cover_image');
            }
            if ($request->input('cv_data.cover_letter') != null) {
                $cover->cover_letter = $request->input('cv_data.cover_letter');
            }
            if ($request->input('cv_data.cover_image') != null || $request->input('cv_data.cover_letter') != null) {
                $cover->save();
            }

        }

        $type = "create";
        $des = "created CV";

        SiteHelper::add_activity(Sentinel::check()->id, $type, $des);

        return $applicant_information->id;

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $applicant_information = ApplicantInformation::find($id);
        return view('admin.cv.cv_detail', compact('applicant_information'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if ($this->access['view'] != '1')
            return false;

        $applicant = ApplicantInformation::find($id);
        $applicant->delete();
    }

    public function preview(Request $request, $id)
    {
        $applicant_information = ApplicantInformation::find($id);
        if ($request->input('apply_job')) {
            $contact_info = $request->input('contact_info');
            $apply_job = $request->input('apply_job');
            return view('admin.cv.cv_preview', compact('applicant_information', 'apply_job', 'contact_info'));
        }
        return view('admin.cv.cv_preview', compact('applicant_information'));
    }


    public function send_cv($id)
    {
        $information = ApplicantInformation::find($id);
        return view('admin.cv.send_cv', compact('information'));
    }

    public function change_cv_status(Request $request , $id)
    {
        ApplicantInformation::where('id',$id)
            ->update(
                [
                    'cv_status' => $request->input('status')
                ]
            );

        $emp_info = ApplicantInformation::find($id);

        if($request->input('status') == 3 ) {
            $type = "hold";
            $des =   "make Hold/Cancel - <a target='_blank' href='".url('cv')."/".$emp_info->id."' >".$emp_info->applicant_name ."</a>"  ;

        }else{
            $type = "available";
            $des =   "make available - <a target='_blank' href='".url('cv')."/".$emp_info->id."' >".$emp_info->applicant_name ."</a>"  ;
        }
        SiteHelper::add_activity(Sentinel::check()->id, $type, $des);
    }

    public function send(Request $request)
    {
        $cv_no = ApplicantInformation::where('cv_no', $request->input('cv_data.cv_no'))->first();
        $name = $cv_no->applicant_name;
        $data = $this->getData($cv_no->cv_no);
        $pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        $pdf->SetAuthor('Oway HR Solutions');
        $pdf->SetTitle($cv_no->applicant_name);
        $pdf->SetSubject('CV Form');
        $pdf->SetKeywords('CV');

        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(true);

// set default monospaced font
        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
        $pdf->SetMargins(PDF_MARGIN_LEFT, 15 , PDF_MARGIN_RIGHT);

// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);



//        $pdf->AddPage();
        
        $applicant_information = $this->getContactInformation($cv_no->id);

        if ($applicant_information->gender == 1) {
            $gender = 'Male';
        } else {
            $gender = 'Female';
        }

        if ($applicant_information->martial_status == 1) {
            $martial_status = 'Married';
        } else {
            $martial_status = 'Single';
        }

        if($applicant_information->currency == '1'){
            $salary = '$ ' . $applicant_information->salary_exp ;
        }else{
            $salary = $applicant_information->salary_exp .' MMK';
        }

//        $html = '<div style="text-align:center">
//                    <img src="' . $data->cover_image . '"  alt="test alt attribute" width="500" height="500" border="0" />
//                 </div>
//                 <div style="text-align:center">
//                    <p><h1>' . $data->cover_letter . '</h1></p>
//                 </div>
//                 ';
//
//        $pdf->writeHTML($html, true, false, true, false, '');


//
//        $pdf->lastPage();
//
//        $pdf->AddPage();
//        $html = file_get_contents( url('cv/preview').'/'.$id ) ;
//
//        $pdf->writeHTML($html, true, false, true, false, '');
//
//        $pdf->lastPage();

//        $pdf->AddPage();
//
//
//        $opts = array(
//            'http' => array(
//                'header' => "User-Agent:MyAgent/1.0\r\n"
//            ),
//        );
//        $context = stream_context_create($opts);
//        $html = file_get_contents('http://oway.local/cv/preview/3');
//
//
//        $pdf->writeHTML($html, true, false, true, false, '');

        $pdf->AddPage();


        $html = '
<style>
         body {
         font-size: 8px;
         }
        #wrapper {
            width: 900px;
            margin: 0 auto;
            margin-top: 60px;
            margin-bottom: 100px;
        }

        .wrapper-top {
            width: 900px;
            height: 19px;
            margin: 0 auto;
            background-repeat: no-repeat;
        }

        .wrapper-mid {
            width: 900px;
            margin: 0 auto;
            background-repeat: repeat-y;
            padding-bottom: 40px;
        }

        .wrapper-bottom {
            width: 900px;
            height: 22px;
            padding: 0;
            margin: 0 auto;
            background-repeat: no-repeat;
        }

        #paper {
            width: 800px;
            margin: 0 auto;
        }

        #wrapper, .wrapper-mid {
            background: none;
        }

        .wrapper-top, .wrapper-bottom {
            display: none;
        }

        .print-pp p {
            font-size: 8px;
        }

        h3 {
            font-size: 12px;
            color: #7dc24b;
        }

        h5 {
            font-weight: bold;
            font-size: 10px;
        }

        td{
            font-size: 8px;
        }
        div.line {
            border-bottom: 2px solid #7dc24b;
        }

        .list-symbol-bullet li {
            line-height: 30px;
            list-style-type: none;
        }

        .address {
            margin-top: 20px;
        }

        .fileAttachmentAnnotation{
            background: black;
        }
    </style>
    <table  cellpadding="2" cellspacing="2">
 <tr nobr="true">
  <td style="float:left;padding: 50px">
  <img src="/img/oway.png" width="180px" style="float: left;padding: 50px"/>
  </td>
  <td style="float:left ">
 <div class="col-md-8 pull-right smallpadding" style="float: right;">
                        <p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;49/B, 1st Floor, Moe Sandar Street, Ward (1),<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Kamayut, Yangon, Myanmar</p>
                        <span class="text-muted">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Telephone:</span> 01 525011, 01 503196 <br>
                        <span class="text-muted">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Email:</span> <a
                                href="#">cvs@owayhrsolutions
                            .com.mm<br></a>
                    </div>
  </td>
 </tr>
</table>
 <div style="margin-bottom:10px;padding: 1px;border-bottom: 2px solid #7dc24b;"></div>
  <table  cellpadding="9" cellspacing="3">
 <tr nobr="true">
<td style="float:left">
<img src="' . $applicant_information->cv_image . '" style="width:150px;height:150px" class="img-responsive">
</td>
</tr>
 
 
</table>

<br/>';

        if ($request->input('cv_data.contact_info') == '1') {
            $html .= '<h3>Summary</h3>
<div class="line" style="border-bottom: 2px solid #7dc24b;"></div>
<table  cellpadding="2" cellspacing="2">
 <tr nobr="true">
    <td style="float:left">
    Profile
    </td>
    <td style="float:left">-
    ' . $gender . '&nbsp;&nbsp;' . $martial_status . '
    </td>
 </tr>
 <tr nobr="true">
    <td style="float:left">
    Date of birth
    </td>
    <td style="float:left">-
    ' . date('F d, Y', strtotime($applicant_information->dob)) . '
    </td>
 </tr>
 <tr nobr="true">
    <td style="float:left">
    Father Name
    </td>
    <td style="float:left">-
    ' . $applicant_information->father_name . '
    </td>
 </tr>
 <tr nobr="true">
    <td style="float:left">
    N.R.C No
    </td>
    <td style="float:left">-
    ' . $applicant_information->nrc . '
    </td>
 </tr>
 <tr nobr="true">
    <td style="float:left">
    Nationality
    </td>
    <td style="float:left">-
    ' . $applicant_information->nationality . '
    </td>
 </tr>
 <tr nobr="true">
    <td style="float:left">
    Height
    </td>

    <td style="float:left">-
   ' . $applicant_information->height . '
    </td>
 </tr>
 <tr nobr="true">
    <td style="float:left">
    Weight
    </td>

    <td style="float:left">-
    ' . $applicant_information->weight . '
    </td>
 </tr>
 <tr nobr="true">
    <td style="float:left">
    Current Location
    </td>

    <td style="float:left">-
     ' . Country::where('id', $applicant_information->curr_location)->pluck('country_name') . '
    </td>
 </tr>
 <tr nobr="true">
    <td style="float:left">
     Address
    </td>

    <td style="float:left">-
    ' . $applicant_information->address . '
    </td>
 </tr>
 <tr nobr="true">
    <td style="float:left">
     Last Position
    </td>

    <td style="float:left">-
    ' . $applicant_information->curr_position . '
    </td>
 </tr>
 <tr nobr="true">
    <td style="float:left">
     Last Company
    </td>

    <td style="float:left">-
    ' . $applicant_information->curr_company . '
    </td>
 </tr>
</table><br/>
';
        }

        if ($request->input('cv_data.c_objective')== '1') {
            $html .= '<h3>Career Objective</h3>
<div class="line" style="border-bottom: 2px solid #7dc24b;"></div>
<table  cellpadding="2" cellspacing="2">
 <tr nobr="true">
    <td style="float:left">
   ' . $applicant_information->c_objective . '
    </td>
 </tr>
</table><br/>
';
        }

        if ($request->input('cv_data.edu_back')== '1') {
            $edu_back = SiteHelper::get_applicant_education($cv_no->cv_no);
            $html .= '<h3>Educational Background</h3>
<div class="line" style="border-bottom: 2px solid #7dc24b;"></div>
<table  cellpadding="2" cellspacing="2">';
            foreach ($edu_back as $edu) {
                $html .=
                    '<tr nobr="true">
    <td style="float:left">
   ' . $edu->uni_start_date . ' - ' . $edu->uni_graduation_date . '
    </td>
    <td style="float:left">
   <b>' . $edu->uni_name . '</b><br>
   ' . Country::where('id', $edu->uni_country)->pluck('country_name') . '
    </td>
    <td style="float:left">
   <b>' . University::where('id', $edu->degree_lvl)->pluck('degree_name') . ',' . $edu->other . ',' . $edu->major_name . '</b>
    </td>
 </tr>
 <tr>
 <td style="float:left"></td>
 </tr>';
            }
            $html .= '</table><br/>
';
        }

        if ($request->input('cv_data.other_qual')== '1') {
            $other_qualification = SiteHelper::get_applicant_other_qualification($cv_no->cv_no);
            $html .= '<h3>Other Qualifications</h3>
<div class="line" style="border-bottom: 2px solid #7dc24b;"></div>
<table  cellpadding="2" cellspacing="2">';
            foreach ($other_qualification as $other) {
                $html .=
                    '<tr nobr="true">
    <td style="float:left">
   ' . $other->certi_date . '
    </td>
    <td style="float:left">
   <b>' . $other->training_center . '</b><br>
   ' . Country::where('id', $other->certi_country)->pluck('country_name') . '
    </td>
    <td style="float:left">
   <b>' . $other->certi_name . '</b>
    </td>
 </tr>
 <tr>
 <td style="float:left"></td>
 </tr>';
            }
            $html .= '</table><br/>
';
        }

        if ($request->input('cv_data.work_exp')== '1') {
            $applicant_work_exp = SiteHelper::get_applicant_work_exp($cv_no->cv_no);
            $html .= '<h3>Working Experience</h3>
<div class="line" style="border-bottom: 2px solid #7dc24b;"></div>
<table  cellpadding="2" cellspacing="2">';
            foreach ($applicant_work_exp as $work) {
                $html .=
                    '<tr nobr="true">
    <td style="float:left">' . date('M Y',strtotime($work->exp_from_date)) . ' - ' . date('M Y',strtotime($work->exp_to_date)) . '
    </td>
    <td style="float:left">
  <b> ' . $work->exp_job_title . ' </b>
    </td>
    <td style="float:left">
  <b> ' . $work->exp_company_name . '</b>
    </td>
 </tr>
 <tr nobr="true">
    <td style="float:left"><b>Industry  </b>
    </td>
    <td style="float:left">
    : ' . Job_Industry::where('id', $work->exp_job_industry)->pluck('industry_description') . '
    </td>
    <td style="float:left">
   ' . $work->exp_job_city . ' , ' . Country::where('id', $work->exp_job_country)->pluck('country_name') . '
    </td>
 </tr>
 <tr nobr="true">
    <td  style="float:left" ><b>Job Description  </b>
    </td>
    <td colspan="2">' . $work->exp_description . '
    </td>
 </tr>
 <tr>
 <td style="float:left"></td>
 </tr>
 ';
            }
            $html .= '</table><br/>
';
        }


        if ($request->input('cv_data.it_skill')== '1') {
            $it_skill = SiteHelper::get_applicant_it_skill($cv_no->cv_no);
            $html .= '<h3>IT Skill</h3>
<div class="line" style="border-bottom: 2px solid #7dc24b;"></div>
<table  cellpadding="2" cellspacing="2">';
            foreach ($it_skill as $it) {
                $html .=
                    '<tr nobr="true">
    <td style="float:left">
   <b> - ' . $it->topic_app_name . '</b>
    </td>
    <td style="float:left">
   ' . IT::where('id', $it->it_type)->pluck('type_name') . '
    </td>
    <td style="float:left">
   <b>' . DB::table('it_lvl')->where('id', $it->it_skill)->pluck('it_lvl_name') . '</b>
    </td>
 </tr>
 <tr>
 <td style="float:left"></td>
 </tr>';
            }
            $html .= '</table><br/>
';
        }

        if ($request->input('cv_data.lang_skill')== '1') {
            $lang_skill = SiteHelper::get_applicant_lang_skill($cv_no->cv_no);
            $html .= '<h3>Language Skill</h3>
<div class="line" style="border-bottom: 2px solid #7dc24b;"></div>
<table  cellpadding="2" cellspacing="2">';
            foreach ($lang_skill as $lang) {
                $html .=
                    '<tr nobr="true">
    <td style="float:left">
   <b> - ' . Language::where('id', $lang->language_name)->pluck('lang_name') . '</b>
    </td>
    <td style="float:left">
   <b>' . DB::table('language_lavel')->where('id', $lang->language_skill)->pluck('lang_lvl') . '</b>
    </td>
 </tr>
 <tr>
 <td style="float:left"></td>
 </tr>
 ';
            }
            $html .= '</table><br/>
';
        }



        if ($request->input('cv_data.ref_person')== '1') {
            $ref_person = SiteHelper::get_applicant_ref_person($cv_no->cv_no);
            $html .= '<h3>Reference Person</h3>
<div class="line" style="border-bottom: 2px solid #7dc24b;"></div>
<table  cellpadding="2" cellspacing="2">';
            foreach ($ref_person as $ref) {
                $html .=
                    '<tr nobr="true">
    <td style="float:left">'. $ref->reference_person_name . '
    </td>
    <td style="float:left"><b>'. $ref->reference_person_position . '</b>
   </td>
    <td style="float:left">
  <b>" ' . $ref->reference_person_company . ' "</b>
    </td>
 </tr>
 <tr nobr="true">
    <td colspan="2"><b>Mobile No : </b>' . @$ref->reference_person_mobile . '
    </td>
    <td style="float:left">'. $ref->reference_person_email . '
    </td>
 </tr>
 <tr>
 <td style="float:left"></td>
 </tr>';
            }
            $html .= '</table><br/>
';
        }

        $pdf->SetLineWidth(2);

// set alpha to semi-transparency
        $pdf->SetAlpha(0.2);

// draw jpeg image
        $pdf->Image(public_path('img/water.png'), 50, 90, 110, 60, '', '#', '', true, 72);

// restore full opacity
        $pdf->SetAlpha(1);

        $pdf->writeHTML($html, true, false, true, false, '');

        $pdf->SetLineWidth(2);

// set alpha to semi-transparency
        $pdf->SetAlpha(0.2);

// draw jpeg image
        $pdf->Image(public_path('img/water.png'), 50, 90, 110, 60, '', '#', '', true, 72);

// restore full opacity
        $pdf->SetAlpha(1);




        if ( count($request->input('cv_data.attachment')) > 0 ) {
            $html = '';

            $pos = 10;
            for ($q = 0; $q < count($request->input('attachment')); $q++) {
                $pdf->Annotation($pos, 50, 30 , 30 , $request->input('cv_data.attachment'. '.' . $q), array('Subtype' => 'FileAttachment',  'Name' => 'Comment', 'T' => 'title example', 'Subj' => 'example' , 'FS' => public_path() . '/upload/attachment/'.'/'.$request->input('cv_data.attachment'. '.' . $q)));
                $pos += 10;
            }
            $pdf->writeHTML($html, true, false, true, false, '');
        }

        $pdf->Output($_SERVER['DOCUMENT_ROOT'] . '/pdf/cv/' . $cv_no->applicant_name .'('.$cv_no->cv_no.').pdf', 'FD');

        $data = array(
            'body' => $request->input('cv_data.cover_letter'),
        );

        Mail::send('email.welcome', array('data' => $data) , function ($message) use ( $request ,$cv_no ) {

            $message->to($request->input('cv_data.to'))->subject($request->input('cv_data.subject'));

            $message->attach(public_path() . '/pdf/cv/' . $cv_no->applicant_name .'('.$cv_no->cv_no.').pdf');

        });

        if (count(Mail::failures()) > 0) {
            return false;
        }
    }

    public function getContactInformation($id)
    {
        return ApplicantInformation::find($id);
    }

    public function getData($cv)
    {
        return ApplicantCover::where('cv_id', $cv)->first();

    }

    public function multi_send_cv()
    {
        return view('admin.cv.multi_send_cv');
    }

    public function send_multi_cv(Request $request)
    {
        $data = array(
            'body' => $request->input('cover_letter'),
        );

        $files = $request->file('attachment');

        foreach ($files as $file) {
            $destinationPath = public_path() . '/upload/';
            $filename = $file->getClientOriginalName();
            $file->move($destinationPath, $filename);
            $name[] = $filename;
        }

        Mail::send('email.welcome', array('data' => $data), function ($message) use ($name, $request) {

            $message->to($request->input('to'))->subject($request->input('subject'));

            $size = count($name);

            for ($i = 0; $i < $size; $i++) {
                $message->attach(public_path() . '/upload/' . $name[$i]);
            }

        });

        $size = count($name);

        for ($i = 0; $i < $size; $i++) {
            if (file_exists('upload/' . $name[$i])) {
                unlink('upload/' . $name[$i]);
            }
        }

        if (count(Mail::failures()) > 0) {

            Session::flash('error', 'Something went worng!Plz send again!');
            return Redirect::back();

        } else {

            $email = new Email();
            $email->to = $request->input('to');
            $email->subject = $request->input('subject');
            $email->message = $request->input('cover_letter');
            $email->send_date = date('Y-m-d', strtotime(Carbon::now()));
            $email->create_user_id = Sentinel::check()->id;
            $email->save();

            $type = "email";
            $des = "sent CV to " . $request->input('to');
            SiteHelper::add_activity(Sentinel::check()->id, $type, $des);

            Session::flash('status', 'Your email has been sent successfully');
            return Redirect::back();

        }
    }

    public function set_as_interview_person(Request $request)
    {
        $status = ApplicantInformation::where('cv_no',$request->input('interview_data.interview_person'))->first();
        if($status->cv_status == 3){
            return false;
        }

        $ran = mt_rand(1, 1000);
        $ran2 = mt_rand(1, 100);
        $before = str_pad('', 3, $ran, STR_PAD_LEFT);
        $after = str_pad('', 2, $ran2, STR_PAD_LEFT);
        $detail = Interview::where('interview_date', date('Y-m-d', strtotime($request->input('interview_data.interview_date'))))->count();
        $count = $detail + 1;
        $y = date('Y');
        $y = substr($y, -2);
        $m = date('m');
        $m = substr($m, -1);
        $d = date('d');

        $data = InterviewCompany::where('company_code', $request->input('interview_data.company_name'))->where('interview_date', date('Y-m-d', strtotime($request->input('interview_date.interview_date'))))->first();

        if (count($data) > 0) {

            $interview = new Interview();
            $interview->cv_id = $request->input('interview_data.interview_person');
            $interview->interview_code = $data->interview_code;
            $interview->company_code = $request->input('interview_data.company_name');
            $interview->interview_date = date('Y-m-d', strtotime($request->input('interview_date.interview_date')));
            $interview->interview_time = date('H:i:s', strtotime($request->input('interview_date.interview_date')));
            $interview->interview_position = $request->input('interview_data.apply_job');
            $interview->interview_status = 1;
            $interview->save();

        } else {
            $interview_code = "INTV" . '-' . $before . '-' . $y . $m . $d . '-' . $count;

            $interview_company = new InterviewCompany();
            $interview_company->interview_code = $interview_code;
            $interview_company->company_code = $request->input('interview_data.company_name');
            $interview_company->interview_date = date('Y-m-d', strtotime($request->input('interview_date.interview_date')));
            $interview_company->create_user_id = Sentinel::check()->id;
            $interview_company->save();

            $interview = new Interview();
            $interview->cv_id = $request->input('interview_data.interview_person');
            $interview->interview_code = $interview_code;
            $interview->company_code = $request->input('interview_data.company_name');
            $interview->interview_date = date('Y-m-d', strtotime($request->input('interview_date.interview_date')));
            $interview->interview_time = date('H:i:s', strtotime($request->input('interview_date.interview_date')));
            $interview->interview_position = $request->input('interview_data.apply_job');
            $interview->interview_status = 1;
            $interview->save();
        }

        $company_name = Client::where('company_code',$request->input('interview_data.company_name'))->pluck('company_name');
        $type = "create";
        $des = "added interview person to ".$company_name.", interview date : ". date('Y-m-d',strtotime($request->input('interview_date.interview_date'))) ;

        SiteHelper::add_activity(Sentinel::check()->id, $type, $des);
        ApplicantInformation::where('cv_no',$interview->cv_id)->update(['cv_status'=>1]);
    }

    public function add_new_job_data(Request $request)
    {
        $job_position = new ApplicantJobPosition();
        $job_position->cv_id = $request->input('new_job_data.cv_no');
        $job_position->job_category = $request->input('new_job_data.job_category');
        $job_position->job_industry = $request->input('new_job_data.job_industry');
        $job_position->job_type = $request->input('new_job_data.job_type');
        $job_position->location = $request->input('new_job_data.target_location');
        $job_position->apply_position = $request->input('new_job_data.apply_position');
        $job_position->save();
    }

    public function add_new_work_exp_data(Request $request)
    {   
        $date_from = new Carbon($request->input('new_work_exp_data.from_date'));
        $date_to = new Carbon($request->input('new_work_exp_data.to_date'));
        $f = date('Y',strtotime($date_to)) - date('Y',strtotime($date_from));
        $exprience_job = new ApplicantWorkExperience();
        $exprience_job->cv_id = $request->input('new_work_exp_data.cv_no');
        $exprience_job->exp_job_industry = $request->input('new_work_exp_data.exp_job_idustry');
        $exprience_job->exp_job_category = $request->input('new_work_exp_data.exp_job_category');
        $exprience_job->exp_job_country = $request->input('new_work_exp_data.exp_country');
        $exprience_job->exp_job_city = $request->input('new_work_exp_data.exp_city');
        $exprience_job->exp_company_name = $request->input('new_work_exp_data.exp_company_name');
        $exprience_job->exp_job_title = $request->input('new_work_exp_data.exp_job_title');
        $exprience_job->exp_from_date = date('Y-m',strtotime($date_from));
        $exprience_job->exp_to_date = date('Y-m',strtotime($date_to));
        $exprience_job->date_diff = $f;
        $exprience_job->exp_description = $request->input('new_work_exp_data.description');
        $exprience_job->save();
    }

    public function add_new_edu_data(Request $request)
    {
        $education = new ApplicantEducation();
        $education->cv_id = $request->input('new_edu_data.cv_no');
        $education->uni_name = $request->input('new_edu_data.university');
        $education->uni_start_date = $request->input('new_edu_data.uni_from_date');
        $education->uni_graduation_date = $request->input('new_edu_data.uni_to_date');
        $education->major_name = $request->input('new_edu_data.major');
        $education->uni_country = $request->input('new_edu_data.edu_country');
        $education->degree_lvl = $request->input('new_edu_data.degree_level');
        $education->other = $request->input('new_edu_data.other');
        $education->save();
    }

    public function add_new_other_qual_data(Request $request)
    {
        $other_qualifications = new ApplicantOtherQualifications();
        $other_qualifications->cv_id = $request->input('new_certi_data.cv_no');
        $other_qualifications->certi_name = $request->input('new_certi_data.certificate');
        $other_qualifications->training_center = $request->input('new_certi_data.training_center');
        $other_qualifications->certi_country = $request->input('new_certi_data.certi_country');
        $other_qualifications->certi_date = $request->input('new_certi_data.certi_date');
        $other_qualifications->save();
    }

    public function add_new_other_it_data(Request $request)
    {
        $it_skill = new ApplicantItSkill();
        $it_skill->cv_id = $request->input('new_it_data.cv_no');
        $it_skill->topic_app_name = $request->input('new_it_data.topic_app');
        $it_skill->it_type = $request->input('new_it_data.it_type');
        $it_skill->it_skill = $request->input('new_it_data.it_skill_level');
        $it_skill->save();
    }

    public function add_new_lang_data(Request $request)
    {
        $lang_skill = new ApplicantLanguageSkill();
        $lang_skill->cv_id = $request->input('new_lang_data.cv_no');
        $lang_skill->language_name = $request->input('new_lang_data.language');
        $lang_skill->language_skill = $request->input('new_lang_data.language_skill');
        $lang_skill->language_other = $request->input('new_lang_data.language_other');
        $lang_skill->save();
    }

    public function add_new_attach_image(Request $request)
    {
        $file = $request->file('attachment');

        $destinationPath = public_path() . '/upload/attachment';
        $filename = $file->getClientOriginalName();
        $filext = $file->getClientOriginalExtension();
        $file->move($destinationPath, $filename);

        $attachment = new ApplicantAttachment();
        $attachment->cv_id = $request->input('cv_no');
        $attachment->filename = $request->input('filename');
        $attachment->attachment = $filename;
        $attachment->ext = $filext;
        $attachment->save();

        return Redirect::back();
    }

    public function add_new_ref_person(Request $request)
    {
        $refrence_person = new ApplicantReferencePerson();
        $refrence_person->cv_id = $request->input('new_ref_data.cv_no');
        $refrence_person->reference_person_name = $request->input('new_ref_data.reference_person_name');
        $refrence_person->reference_person_position = $request->input('new_ref_data.reference_person_position');
        $refrence_person->reference_person_company = $request->input('new_ref_data.reference_person_company');
        $refrence_person->reference_person_mobile = $request->input('new_ref_data.reference_person_mobile');
        $refrence_person->reference_person_email = $request->input('new_ref_data.reference_person_email');
        $refrence_person->save();
    }

    public function update_contact_data(Request $request)
    {
        ApplicantInformation::where('cv_no', $request->input('contact_data.cv_no'))
            ->update([
                'contact_number' => $request->input('contact_data.contact_number'),
                'sec_contact' => $request->input('contact_data.sec_contact_number'),
                'email' => $request->input('contact_data.email'),
                'address' => $request->input('contact_data.address')
            ]);
    }

    public function update_personal_data(Request $request)
    {
        ApplicantInformation::where('cv_no', $request->input('personal_data.cv_no'))
            ->update([
                'applicant_name' => $request->input('personal_data.applicant'),
                'father_name' => $request->input('personal_data.father_name'),
                'nrc' => $request->input('personal_data.nrc'),
                'dob' => $request->input('personal_data.dob'),
                'gender' => $request->input('personal_data.gender'),
                'martial_status' => $request->input('personal_data.martial_status'),
                'weight' => $request->input('personal_data.weight'),
                'height' => $request->input('personal_data.height'),
                'nationality' => $request->input('personal_data.nationality'),
                'curr_position' => $request->input('personal_data.current_position'),
                'salary_exp' => $request->input('personal_data.exp_salary'),
                'currency' => $request->input('personal_data.currency'),
                'religion' => $request->input('personal_data.religion'),
                'curr_company' => $request->input('personal_data.current_company_name'),
                'curr_location' => $request->input('personal_data.cur_location'),
                'not_period' => $request->input('personal_data.noticed_period'),
            ]);
    }

    public function update_career_obj_data(Request $request)
    {
        ApplicantInformation::where('cv_no', $request->input('c_obj_data.cv_no'))
            ->update([
                'c_objective' => $request->input('c_obj_data.c_objective')
            ]);
    }

    public function update_job_data(Request $request)
    {
        ApplicantJobPosition::where('cv_id', $request->input('edit_job_data.cv_no'))
            ->where('id', $request->input('edit_job_data.job_id'))
            ->update([
                'job_category' => $request->input('edit_job_data.job_category'),
                'job_industry' => $request->input('edit_job_data.job_industry'),
                'job_type' => $request->input('edit_job_data.job_type'),
                'location' => $request->input('edit_job_data.target_location'),
                'apply_position' => $request->input('edit_job_data.apply_position')
            ]);
    }

    public function update_work_exp_data(Request $request)
    {   
        $date_from = new Carbon($request->input('edit_work_exp_data.from_date'));
        $date_to = new Carbon($request->input('edit_work_exp_data.to_date'));
        $f = date('Y',strtotime($date_to)) - date('Y',strtotime($date_from));
        ApplicantWorkExperience::where('cv_id', $request->input('edit_work_exp_data.cv_no'))
            ->where('id', $request->input('edit_work_exp_data.exp_id'))
            ->update([
                'exp_job_industry' => $request->input('edit_work_exp_data.exp_job_idustry'),
                'exp_job_category' => $request->input('edit_work_exp_data.exp_job_category'),
                'exp_job_country' => $request->input('edit_work_exp_data.exp_country'),
                'exp_job_city' => $request->input('edit_work_exp_data.exp_city'),
                'exp_company_name' => $request->input('edit_work_exp_data.exp_company_name'),
                'exp_job_title' => $request->input('edit_work_exp_data.exp_job_title'),
                'exp_from_date' => date('Y-m',strtotime($date_from)),
                'exp_to_date' => date('Y-m',strtotime($date_to)),
                'date_diff'=>$f,
                'exp_description' => $request->input('edit_work_exp_data.description')
            ]);
    }

    public function update_edu_data(Request $request)
    {
        ApplicantEducation::where('cv_id', $request->input('edit_edu_data.cv_no'))
            ->where('id', $request->input('edit_edu_data.edu_id'))
            ->update([
                'uni_name' => $request->input('edit_edu_data.university'),
                'uni_start_date' => $request->input('edit_edu_data.uni_from_date'),
                'uni_graduation_date' => $request->input('edit_edu_data.uni_to_date'),
                'major_name' => $request->input('edit_edu_data.major'),
                'uni_country' => $request->input('edit_edu_data.edu_country'),
                'degree_lvl' => $request->input('edit_edu_data.degree_level'),
                'other' => $request->input('edit_edu_data.other')
            ]);
    }

    public function update_other_data(Request $request)
    {
        ApplicantOtherQualifications::where('cv_id', $request->input('edit_other_data.cv_no'))
            ->where('id', $request->input('edit_other_data.other_id'))
            ->update([
                'certi_name' => $request->input('edit_other_data.certificate'),
                'training_center' => $request->input('edit_other_data.training_center'),
                'certi_country' => $request->input('edit_other_data.certi_country'),
                'certi_date' => $request->input('edit_other_data.certi_date')
            ]);
    }

    public function update_it_data(Request $request)
    {
        ApplicantItSkill::where('cv_id', $request->input('edit_it_data.cv_no'))
            ->where('id', $request->input('edit_it_data.it_id'))
            ->update([
                'topic_app_name' => $request->input('edit_it_data.topic_app'),
                'it_type' => $request->input('edit_it_data.it_type'),
                'it_skill' => $request->input('edit_it_data.it_skill_level')
            ]);
    }

    public function update_lang_data(Request $request)
    {
        ApplicantLanguageSkill::where('cv_id', $request->input('edit_lang_data.cv_no'))
            ->where('id', $request->input('edit_lang_data.lang_id'))
            ->update([
                'language_name' => $request->input('edit_lang_data.language'),
                'language_skill' => $request->input('edit_lang_data.language_skill'),
                'language_other' => $request->input('edit_lang_data.language_other')

            ]);
    }

    public function update_cover_letter_data(Request $request)
    {
        $update = ApplicantCover::where('cv_id', $request->input('edit_cover_letter.cv_no'))
            ->update([
                'cover_letter' => $request->input('edit_cover_letter.cover_letter')
            ]);

        if ($update == false) {
            $cover = new ApplicantCover();
            $cover->cv_id = $request->input('edit_cover_image.cv_no');
            $cover->cover_letter = $request->input('edit_cover_letter.cover_letter');
            $cover->save();
        }
    }

    public function update_applicant_image_data(Request $request)
    {
        $update = ApplicantInformation::where('cv_no', $request->input('edit_applicant_image.cv_no'))
            ->update([
                'cv_image' => $request->input('edit_applicant_image.applicant_image')
            ]);

        if ($update == false) {
            $cover = new ApplicantInformation();
            $cover->cv_no = $request->input('edit_cover_image.cv_no');
            $cover->cv_image = $request->input('edit_cover_letter.applicant_image');
            $cover->save();
        }
    }

    public function update_cover_image_data(Request $request)
    {
        $update = ApplicantCover::where('cv_id', $request->input('edit_cover_image.cv_no'))
            ->update([
                'cover_image' => $request->input('edit_cover_image.cover_image')
            ]);

        if ($update == false) {
            $cover = new ApplicantCover();
            $cover->cv_id = $request->input('edit_cover_image.cv_no');
            $cover->cover_image = $request->input('edit_cover_image.cover_image');
            $cover->save();
        }
    }

    public function update_ref_data(Request $request)
    {
        ApplicantReferencePerson::where('cv_id', $request->input('update_ref_data.cv_no'))
            ->where('id', $request->input('update_ref_data.ref_id'))
            ->update([
                'reference_person_name' => $request->input('update_ref_data.reference_person_name'),
                'reference_person_position' => $request->input('update_ref_data.reference_person_position'),
                'reference_person_company' => $request->input('update_ref_data.reference_person_company'),
                'reference_person_mobile' => $request->input('update_ref_data.reference_person_mobile'),
                'reference_person_email' => $request->input('update_ref_data.reference_person_email')
            ]);
    }

    public function delete_job_data(Request $request)
    {
        ApplicantJobPosition::where('id', $request->input('delete_job_data'))->delete();
    }

    public function delete_work_exp_data(Request $request)
    {
        ApplicantWorkExperience::where('id', $request->input('delete_exp_data'))->delete();
    }

    public function delete_edu_data(Request $request)
    {
        ApplicantEducation::where('id', $request->input('delete_edu_data'))->delete();
    }

    public function delete_other_data(Request $request)
    {
        ApplicantOtherQualifications::where('id', $request->input('delete_other_data'))->delete();
    }

    public function delete_it_data(Request $request)
    {
        ApplicantItSkill::where('id', $request->input('delete_it_data'))->delete();
    }

    public function delete_lang_data(Request $request)
    {
        ApplicantLanguageSkill::where('id', $request->input('delete_lang_data'))->delete();
    }

    public function delete_attach_data(Request $request)
    {
        $file = ApplicantAttachment::find($request->input('delete_attach_data'))->attachment ;
        if(file_exists(public_path().'/upload/attachment/'.$file )){
            unlink(public_path().'/upload/attachment/'.$file);
        }
        ApplicantAttachment::where('id', $request->input('delete_attach_data'))->delete();
    }

    public function delete_ref(Request $request)
    {
        ApplicantReferencePerson::where('id', $request->input('delete_ref'))->delete();
    }

    public function exportExcel()
    { 
        $data = ['No','Name','Father Name','NRC No','Address','* Location', 'Birthday (2016-12-24)','Gender(Male/ Female)','Martial Status(Married/Single)','Weight','Height','Nationality','Religion','Last Postion','Last Company Name','*Expected Salary','Salary Negotiable(Yes/No)','Currency(Dollar/MMK)','Noticed Period','Phone Number','Second Phone Number','Email','Career Objective'];

        $apply_pos = ['NRC No','*Job Category','*Job Industry','Apply Postion','*Job Type','*Job Location(Local/Oversea)'];

        $edu = ['NRC No','University','Subject/Major','Start Date','Graduation Date','*Country','*Degree Level','Other Degree(Optional)'];

        $other_qly = ['NRC No','Certificate','Training Center','*Country','Date Issue'];

        $it_sk = ['NRC No','Application,topic or language','*Type','*Skill Level'];

        $lan_sk = ['NRC No','*Language','*Skill Level','Other(Optional)'];
        $title= ['id','Name'];
        $work_exp = ['NRC No','*Job_Category','*Job Industry','*Country','City','Company Name','Job Title','From Date','To Date','Description'];

        $ref_p = ['NRC No','Name','Position','Company Name','Mobile No','Email']; 

        $country = Country::select('id','country_name')->get();  
        $exp_salary = \App\Models\Salary::select('id','salary')->get();  
        $type = IT::select('id','type_name')->get();
        $it_level = \App\Models\IT::it_lvl();
        $lang = Language::select('id','lang_name')->get();
        $l_level = \App\Models\Language::language_lvl(); 
        $job_cat = Job_Category::select('id','description')->get();
        $job_ind = Job_Industry::select('id','industry_description')->get();
        $edu_lev= University::select('id','degree_name')->get();
        $job_t = \App\Models\JobType::select('id','type')->get();

        Excel::create('Create CV',function($excel) use ($data,$title,$apply_pos,$edu,$other_qly,$it_sk,$lan_sk,$work_exp,$ref_p,$country,$exp_salary,$type,$it_level,$lang,$l_level,$job_cat,$job_ind,$edu_lev,$job_t){       
            $excel->sheet('Applicant Information',function($sheet) use ($data){
                $sheet->row(1,$data);
                $sheet->row(1, function ($row) {
                    $row->setAlignment('center');
                    $row->setFontColor('#ffffff');
                    $row->setBackground('#00a65a');
                });
                $sheet->setHeight(1,25);
            });
            $excel->sheet('Apply Job Position',function($sheet2) use ($apply_pos){
                $sheet2->row(1,$apply_pos);
                $sheet2->row(1, function ($row) {
                    $row->setAlignment('center');
                    $row->setFontColor('#ffffff');
                    $row->setBackground('#00a65a');
                });
                $sheet2->setHeight(1,25);
            });
            $excel->sheet('Educationl Background',function($sheet3) use ($edu){
                $sheet3->row(1,$edu);
                $sheet3->row(1, function ($row) {
                    $row->setAlignment('center');
                    $row->setFontColor('#ffffff');
                    $row->setBackground('#00a65a');
                });
                $sheet3->setHeight(1,25);
            });
            $excel->sheet('Other Qualifications',function($sheet4) use ($other_qly){
                $sheet4->row(1,$other_qly);
                $sheet4->row(1, function ($row) {
                    $row->setAlignment('center');
                    $row->setFontColor('#ffffff');
                    $row->setBackground('#00a65a');
                });
                $sheet4->setHeight(1,25);
            });
            $excel->sheet('IT Skill',function($sheet5) use ($it_sk){
                $sheet5->row(1,$it_sk);
                $sheet5->row(1, function ($row) {
                    $row->setAlignment('center');
                    $row->setFontColor('#ffffff');
                    $row->setBackground('#00a65a');
                });
                $sheet5->setHeight(1,25);
            });
            $excel->sheet('Language Skill',function($sheet6) use ($lan_sk){
                $sheet6->row(1,$lan_sk);
                $sheet6->row(1, function ($row) {
                    $row->setAlignment('center');
                    $row->setFontColor('#ffffff');
                    $row->setBackground('#00a65a');
                });
                $sheet6->setHeight(1,25);

            });
            $excel->sheet('Working Experience',function($sheet7) use ($work_exp){
                $sheet7->row(1,$work_exp);
                $sheet7->row(1, function ($row) {
                    $row->setAlignment('center');
                    $row->setFontColor('#ffffff');
                    $row->setBackground('#00a65a');
                });
                $sheet7->setHeight(1,25);
            });
            $excel->sheet('Reference Person',function($sheet8) use ($ref_p){
                $sheet8->row(1,$ref_p);
                $sheet8->row(1, function ($row) {
                    $row->setAlignment('center');
                    $row->setFontColor('#ffffff');
                    $row->setBackground('#00a65a');
                });
                $sheet8->setHeight(1,25);
            });
            $excel->sheet('Country Format',function($sheet9) use ($title,$country){
                $sheet9->row(1,$title);
                $sheet9->row(1, function ($row) {
                    $row->setAlignment('center');
                    $row->setFontColor('#ffffff');
                    $row->setBackground('#00a65a');
                });
                $sheet9->setHeight(1,25);
                for($i=0;$i<count($country);$i++){
                    
                    $sheet9->setHeight($i+2,20);
                    $sheet9->setCellValueByColumnAndRow(0,$i+2, str_replace(",", "\n", $country[$i]['id']));
                    $sheet9->setCellValueByColumnAndRow(1,$i+2, str_replace(",", "\n", $country[$i]['country_name']));
                }
            });
            $excel->sheet('Expected Salary Format',function($sheet10) use ($title,$exp_salary){
                $sheet10->row(1,$title);
                $sheet10->row(1, function ($row) {
                    $row->setAlignment('center');
                    $row->setFontColor('#ffffff');
                    $row->setBackground('#00a65a');
                });
                $sheet10->setHeight(1,25);
                for ($i=0; $i < count($exp_salary); $i++) { 
                    $sheet10->setHeight($i+2,35);
                    $sheet10->setCellValueByColumnAndRow(0,$i+2, str_replace(",", "\n", $exp_salary[$i]['id']));
                    $sheet10->setCellValueByColumnAndRow(1,$i+2, str_replace(",", "\n", $exp_salary[$i]['salary']));
                }
            });
            $excel->sheet('Type Format',function($sheet11) use ($title,$type,$it_level){
                array_push($title,'level_id','level_name');
                $sheet11->row(1,$title);
                $sheet11->row(1, function ($row) {
                    $row->setAlignment('center');
                    $row->setFontColor('#ffffff');
                    $row->setBackground('#00a65a');
                });
                $sheet11->setHeight(1,25);
                for ($i=0; $i < count($type); $i++) {
                    $sheet11->setHeight($i+2,35);
                    $sheet11->setCellValueByColumnAndRow(0,$i+2, str_replace(",", "\n", $type[$i]['id']));
                    $sheet11->setCellValueByColumnAndRow(1,$i+2, str_replace(",", "\n", $type[$i]['type_name']));
                    $sheet11->setCellValueByColumnAndRow(2,$i+2, str_replace(",", "\n", $it_level[$i]->id));
                    $sheet11->setCellValueByColumnAndRow(3,$i+2, str_replace(",", "\n", $it_level[$i]->it_lvl_name));
                }

            });           
            $excel->sheet('Language Format',function($sheet12) use ($title,$lang,$l_level){
                array_push($title,'level_id','level_name');
                $sheet12->row(1,$title);
                $sheet12->row(1, function ($row) {
                    $row->setAlignment('center');
                    $row->setFontColor('#ffffff');
                    $row->setBackground('#00a65a');
                });
                $sheet12->setHeight(1,25);
                for ($i=0; $i < count($lang); $i++) { 
                    $sheet12->setHeight($i+2,35); 
                    $sheet12->setCellValueByColumnAndRow(0,$i+2, str_replace(",", "\n", $lang[$i]['id']));
                    $sheet12->setCellValueByColumnAndRow(1,$i+2, str_replace(",", "\n", $lang[$i]['lang_name']));
                    for($j=0;$j<count($l_level);$j++){
                       $sheet12->setCellValueByColumnAndRow(2,$j+2, str_replace(",", "\n", $l_level[$j]->id));
                       $sheet12->setCellValueByColumnAndRow(3,$j+2, str_replace(",", "\n", $l_level[$j]->lang_lvl));
                    }
                }
            });

            $excel->sheet('Job Category Format',function($sheet13) use ($title,$job_cat){
                $sheet13->row(1,$title);
                $sheet13->row(1, function ($row) {
                    $row->setAlignment('center');
                    $row->setFontColor('#ffffff');
                    $row->setBackground('#00a65a');
                });
                $sheet13->setHeight(1,25);
                for ($i=0; $i < count($job_cat); $i++) { 
                    $sheet13->setHeight($i+2,35);
                    $sheet13->setCellValueByColumnAndRow(0,$i+2, str_replace(",", "\n", $job_cat[$i]['id']));
                    $sheet13->setCellValueByColumnAndRow(1,$i+2, str_replace(",", "\n", $job_cat[$i]['description']));
                }
            });           

            $excel->sheet('Degree Level Format',function($sheet14) use ($title,$edu_lev){
                $sheet14->row(1,$title);
                $sheet14->row(1, function ($row) {
                    $row->setAlignment('center');
                    $row->setFontColor('#ffffff');
                    $row->setBackground('#00a65a');
                });
                $sheet14->setHeight(1,25);
                for ($i=0; $i < count($edu_lev); $i++) { 
                    $sheet14->setHeight($i+2,35);
                    $sheet14->setCellValueByColumnAndRow(0,$i+2, str_replace(",", "\n", $edu_lev[$i]['id']));
                    $sheet14->setCellValueByColumnAndRow(1,$i+2, str_replace(",", "\n", $edu_lev[$i]['degree_name']));
                }
            });

            $excel->sheet('Job Industry Format',function($sheet15) use ($title,$job_ind){
                $sheet15->row(1,$title);
                $sheet15->row(1, function ($row) {
                    $row->setAlignment('center');
                    $row->setFontColor('#ffffff');
                    $row->setBackground('#00a65a');
                });
                $sheet15->setHeight(1,25);
                for ($i=0; $i < count($job_ind); $i++) { 
                    $sheet15->setHeight($i+2,35);
                    $sheet15->setCellValueByColumnAndRow(0,$i+2, str_replace(",", "\n", $job_ind[$i]['id']));
                    $sheet15->setCellValueByColumnAndRow(1,$i+2, str_replace(",", "\n", $job_ind[$i]['industry_description']));
                }
            });

            $excel->sheet('Job Type Format',function($sheet16) use ($title,$job_t){
                $sheet16->row(1,$title);
                $sheet16->row(1, function ($row) {
                    $row->setAlignment('center');
                    $row->setFontColor('#ffffff');
                    $row->setBackground('#00a65a');
                });
                $sheet16->setHeight(1,25);
                for ($i=0; $i < count($job_t); $i++) { 
                    $sheet16->setHeight($i+2,35);
                    $sheet16->setCellValueByColumnAndRow(0,$i+2, str_replace(",", "\n", $job_t[$i]['id']));
                    $sheet16->setCellValueByColumnAndRow(1,$i+2, str_replace(",", "\n", $job_t[$i]['type']));
                }
            })->export('xls');
        });

       
    }

    public function importExcel(Request $request)
    {   
        if ($request->hasFile('excel_file')) {            
            $destinationPath = str_replace('\\', '/' ,public_path().'/upload/csv/'.date('Y').'/'.date('m').'/'); // upload path
            $extension = Input::file('excel_file')->getClientOriginalExtension();
            if($extension == 'xls' || $extension == 'xlsx'){
              $fileName = time().'.'.$extension; // renameing image
              $where  = Input::file('excel_file')->move($destinationPath, $fileName); // uploading file to given path
            }          
         
        } 
        
        $des = $where;    
        
        //$default = ini_get('max_execution_time');
        //set_time_limit(1000);    
        
        Excel::load($des,function($file){
            $data = $file->toArray();              
            
            for ($n=0; $n<count($data[0]);$n++) {
                foreach ($data[1] as $value) {
                    $job_code = Job_Category::where('id', $value['job_category'])->pluck('job_code');
                    $detail = ApplicantJobPosition::where('job_category', $value['job_category'])->get();
                    $count = count($detail) + 1; 
                    $ran = mt_rand(1, 1000);
                    $ran2 = mt_rand(1, 100); 
                    $before = str_pad('', 3, $ran, STR_PAD_LEFT);
                    $after = str_pad('', 2, $ran2, STR_PAD_LEFT);
                    $y = date('Y');
                    $y = substr($y, -2);
                    $m = date('m');
                    $m = substr($m, -1);                
                     
                }
                $cv_no[] = $job_code . '-' . $before . $y . $m. $after . '-' . $count;                 
            }   
                
                for($i=0;$i<count($data[0]);$i++) {
                    $ap_inf = new ApplicantInformation;
                    $ap_inf->cv_no = $cv_no[$i];
                    $ap_inf->applicant_name = ($data[0][$i]['name']==null)?'':$data[0][$i]['name'];
                    $ap_inf->father_name = ($data[0][$i]['father_name']==null)?'':$data[0][$i]['father_name'];
                    $ap_inf->nrc = ($data[0][$i]['nrc_no']==null)?'':$data[0][$i]['nrc_no'];
                    $ap_inf->address = ($data[0][$i]['address']==null)?'':$data[0][$i]['address'];
                    $ap_inf->curr_location = ($data[0][$i]['location']==null)?'':$data[0][$i]['location'];
                    $ap_inf->dob = ($data[0][$i]['birthday_2016_12_24']==null)?'':date('Y-m-d',strtotime($data[0][$i]['birthday_2016_12_24']));
                    $ap_inf->gender = ($data[0][$i]['gendermale_female']==null)?'':$data[0][$i]['gendermale_female'];
                    $ap_inf->martial_status = ($data[0][$i]['martial_statusmarriedsingle']==null)?'':$data[0][$i]['martial_statusmarriedsingle']; 
                    $ap_inf->height = ($data[0][$i]['weight']==null)?'':$data[0][$i]['weight'];
                    $ap_inf->height = ($data[0][$i]['height']==null)?'':$data[0][$i]['height'];
                    $ap_inf->nationality = ($data[0][$i]['nationality']==null)?'':$data[0][$i]['nationality'];
                    $ap_inf->religion = ($data[0][$i]['religion']==null)?'':$data[0][$i]['religion'];
                    $ap_inf->curr_position = ($data[0][$i]['last_postion']==null)?'':$data[0][$i]['last_postion'];
                    $ap_inf->curr_company = ($data[0][$i]['last_company_name']==null)?'':$data[0][$i]['last_company_name'];
                    $ap_inf->salary_exp = ($data[0][$i]['expected_salary']==null)?'':$data[0][$i]['expected_salary'];
                    $ap_inf->negotiable = ($data[0][$i]['salary_negotiableyesno']==null)?'':$data[0][$i]['salary_negotiableyesno'];
                    $ap_inf->currency = ($data[0][$i]['currencydollarmmk']==null)?'':$data[0][$i]['currencydollarmmk'];
                    $ap_inf->not_period = ($data[0][$i]['noticed_period']==null)?'':$data[0][$i]['noticed_period'];
                    $ap_inf->contact_number = ($data[0][$i]['phone_number']==null)?'':$data[0][$i]['phone_number'];
                    $ap_inf->sec_contact = ($data[0][$i]['second_phone_number']==null)?'':$data[0][$i]['second_phone_number']; 
                    $ap_inf->email = ($data[0][$i]['email']==null)?'':$data[0][$i]['email'];
                    $ap_inf->c_objective = ($data[0][$i]['career_objective']==null)?'':$data[0][$i]['career_objective'];
                    $ap_inf->create_user_id = 1;
                    $ap_inf->save();  
                }
                
                for($x=0;$x<count($data[1]);$x++) {
                    
                    //foreach($ap as $list){
                        
                        $job_pos = new ApplicantJobPosition;
                        $job_pos->cv_id = (ApplicantInformation::where('nrc',$data[1][$x]['nrc_no'])->value('cv_no')==null)?'':ApplicantInformation::where('nrc',$data[1][$x]['nrc_no'])->value('cv_no');
                        $job_pos->job_category = ($data[1][$x]['job_category']==null)?'':$data[1][$x]['job_category'];
                        $job_pos->job_industry = ($data[1][$x]['job_industry']==null)?'':$data[1][$x]['job_industry'];
                        $job_pos->job_type = ($data[1][$x]['job_type']==null)?'':$data[1][$x]['job_type'];
                        $job_pos->location = ($data[1][$x]['job_locationlocaloversea']==null)?'':$data[1][$x]['job_locationlocaloversea'];
                        $job_pos->apply_position = ($data[1][$x]['apply_postion']==null)?'':$data[1][$x]['apply_postion'];
                        $job_pos->save();
                    //}

                }
                
                for($y=0;$y<count($data[2]);$y++){
                   $job_edu = new ApplicantEducation;
                    $job_edu->cv_id = (ApplicantInformation::where('nrc',$data[2][$y])->value('cv_no')==null)?'':ApplicantInformation::where('nrc',$data[2][$y])->value('cv_no');
                    $job_edu->uni_name = ($data[2][$y]['university']==null)?'':$data[2][$y]['university'];
                    $job_edu->major_name = ($data[2][$y]['subjectmajor']==null)?'':$data[2][$y]['subjectmajor'];
                    $job_edu->uni_start_date = ($data[2][$y]['start_date']==null)?'':date('Y-m-d',strtotime($data[2][$y]['start_date']));
                    $job_edu->uni_graduation_date = ($data[2][$y]['graduation_date']==null)?'':date('Y-m-d',strtotime($data[2][$y]['graduation_date']));
                    $job_edu->uni_country = ($data[2][$y]['country']==null)?'':$data[2][$y]['country'];
                    $job_edu->degree_lvl = ($data[2][$y]['degree_level']==null)?'':$data[2][$y]['degree_level'];
                    $job_edu->other = ($data[2][$y]['other_degreeoptional']==null)?'':$data[2][$y]['other_degreeoptional']; 
                    $job_edu->save();                  
                }
                              
                for($b=0;$b<count($data[3]);$b++){
                        //$ap = ApplicantInformation::where('nrc',$value_3['nrc_no'])->select('id')->first();
                        
                        $app_other = new ApplicantOtherQualifications;
                        $app_other->cv_id = (ApplicantInformation::where('nrc',$data[3][$b]['nrc_no'])->value('cv_no')==null)?'':ApplicantInformation::where('nrc',$data[3][$b]['nrc_no'])->value('cv_no');
                        $app_other->certi_name = ($data[3][$b]['certificate']==null)?'':$data[3][$b]['certificate'];
                        $app_other->training_center = ($data[3][$b]['training_center']==null)?'':$data[3][$b]['training_center'];
                        $app_other->certi_country = ($data[3][$b]['country']==null)?'':$data[3][$b]['country'];
                        $app_other->certi_date = ($data[3][$b]['date_issue']==null)?'':$data[3][$b]['date_issue'];
                        $app_other->save();                  
                }
                
                
                for($c=0;$c<count($data[4]);$c++){
                    //$ap = ApplicantInformation::where('nrc',$value_4['nrc_no'])->select('id')->first();
                        
                    $app_it = new ApplicantItSkill;
                    $app_it->cv_id = (ApplicantInformation::where('nrc',$data[4][$c]['nrc_no'])->value('cv_no')==null)?'':ApplicantInformation::where('nrc',$data[4][$c]['nrc_no'])->value('cv_no');
                    $app_it->topic_app_name = ($data[4][$c]['applicationtopic_or_language']==null)?'':$data[4][$c]['applicationtopic_or_language'];
                    $app_it->it_type = ($data[4][$c]['type']==null)?'':$data[4][$c]['type'];
                    $app_it->it_skill = ($data[4][$c]['skill_level']==null)?'':$data[4][$c]['skill_level'];
                    $app_it->save();                  
                }
                
                
                for($e=0;$e<count($data[5]);$e++){
                    //$ap = ApplicantInformation::where('nrc',$value_5['nrc_no'])->select('id')->first();
                    
                    $lang = new ApplicantLanguageSkill;
                    $lang->cv_id = (ApplicantInformation::where('nrc',$data[5][$e]['nrc_no'])->value('cv_no')==null)?'':ApplicantInformation::where('nrc',$data[5][$e]['nrc_no'])->value('cv_no');
                    $lang->language_name = ($data[5][$e]['language']==null)?'':$data[5][$e]['language'];
                    $lang->language_skill = ($data[5][$e]['skill_level']==null)?'':$data[5][$e]['skill_level'];
                    $lang->language_other = ($data[5][$e]['otheroptional']==null)?'':$data[5][$e]['otheroptional'];
                    $lang->save();                  
                }
                
                for($f=0;$f<count($data[6]);$f++){
                    //$ap = ApplicantInformation::where('nrc',$value_6['nrc_no'])->select('id')->first();
                    
                    $app_exp = new ApplicantWorkExperience;
                    $app_exp->cv_id = (ApplicantInformation::where('nrc',$data[6][$f]['nrc_no'])->value('cv_no')==null)?'':ApplicantInformation::where('nrc',$data[6][$f]['nrc_no'])->value('cv_no');
                    $app_exp->exp_job_category = ($data[6][$f]['job_category']==null)?'':$data[6][$f]['job_category'];
                    $app_exp->exp_job_industry = ($data[6][$f]['job_industry']==null)?'':$data[6][$f]['job_industry'];
                    $app_exp->exp_job_country = ($data[6][$f]['country']==null)?'':$data[6][$f]['country'];
                    $app_exp->exp_job_city = ($data[6][$f]['city']==null)?'':$data[6][$f]['city'];
                    $app_exp->exp_company_name = ($data[6][$f]['company_name']==null)?'':$data[6][$f]['company_name'];
                    $app_exp->exp_job_title = ($data[6][$f]['job_title']==null)?'':$data[6][$f]['job_title'];
                    $app_exp->exp_from_date = ($data[6][$f]['from_date']==null)?'':$data[6][$f]['from_date'];
                    $app_exp->exp_to_date = ($data[6][$f]['to_date']==null)?'':$data[6][$f]['to_date'];
                    $app_exp->date_diff = ($data[6][$f]['to_date']==null)?'':date('Y',strtotime($data[6][$f]['to_date'])) - date('Y',strtotime($data[6][$f]['from_date']));
                    $app_exp->exp_description = ($data[6][$f]['description']==null)?'':$data[6][$f]['description'];
                    $app_exp->save();                  
                }
                
                for($k=0;$k<count($data[7]);$k++){
                    //$ap = ApplicantInformation::where('nrc',$value_7['nrc_no'])->select('id')->first();
                    
                    $app_ref = new ApplicantReferencePerson;
                    $app_ref->cv_id = (ApplicantInformation::where('nrc',$data[7][$k]['nrc_no'])->value('cv_no')==null)?'':ApplicantInformation::where('nrc',$data[7][$k]['nrc_no'])->value('cv_no');
                    $app_ref->reference_person_name = ($data[7][$k]['name']==null)?'':$data[7][$k]['name'];
                    $app_ref->reference_person_position = ($data[7][$k]['position']==null)?'':$data[7][$k]['position'];
                    $app_ref->reference_person_company = ($data[7][$k]['company_name']==null)?'':$data[7][$k]['company_name'];
                    $app_ref->reference_person_mobile = ($data[7][$k]['mobile_no']==null)?'':$data[7][$k]['mobile_no'];
                    $app_ref->reference_person_email = ($data[7][$k]['email']==null)?'':$data[7][$k]['email'];
                    $app_ref->save();                  
                }
             
        })->get();
        
        //set_time_limit($default);
//return 'success excel load';

    }

}
