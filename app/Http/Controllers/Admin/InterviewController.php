<?php

namespace App\Http\Controllers\Admin;

use App\Models\ApplicantInformation;
use App\Models\Client;
use App\Models\EmployeeRecord;
use App\Models\Interview;
use App\Models\InterviewCompany;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use SiteHelper;

class InterviewController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $interview_code = InterviewCompany::select('interview_code', 'interview_date', 'company_code', 'create_user_id')->groupBy('interview_date')->paginate(15); 
        return view('admin.interview.index', compact('interview_code'));
    }

    public function showall($id)
    {
        $interview_code = Interview::select('interview_code', 'interview_date', 'company_code')->groupBy('interview_date')->where('company_code', $id)->paginate(15);
        return view('admin.interview.client', compact('interview_code', 'id'));
    }

    public function second_interview()
    {
        $interview_code = Interview::select('interview_code', 'interview_date', 'create_user_id')->groupBy('interview_date')->where('interview_status', '<=', '6')->where('sec_employed','>',0)->where('interview_status', '!=', '4')->paginate(15);
        return view('admin.interview.second', compact('interview_code'));
    }

    public function change_time(Request $request, $id)
    {
        $interview = Interview::find($id);
        if ($request->input('second')) {
            $interview->interview_date = date('Y-m-d', strtotime($request->input('interview_edit_data')));
        }
        $interview->interview_time = date('H:i:s', strtotime($request->input('interview_edit_data')));
        $interview->save();

        if ($request->input('second')) {

            $user_name = ApplicantInformation::where('cv_no', $interview->cv_id)->pluck('applicant_name');

            $type = "create";
            $des = "change second interview  " . $user_name . ", interview date : " . date('F j, Y, g:i a', strtotime($request->input('interview_edit_data')));

            SiteHelper::add_activity(Sentinel::check()->id, $type, $des);
        }
    }

    public function change_company_and_position(Request $request, $id)
    {
        $interview = Interview::find($id);
//        $interview->company_code = $request->input('interview_company_data.company_name');
        $interview->interview_position = $request->input('interview_company_data.apply_job');
        $interview->save();
    }

    public function employee_record(Request $request, $id)
    {
        $emp_record = new EmployeeRecord();
        $emp_record->cv_id = $request->input('employee_record_data.interview_person');
        $emp_record->job_position = $request->input('employee_record_data.job_position');
        $emp_record->company_code = $request->input('employee_record_data.company_code');
        $emp_record->job_receive_date = date('Y-m-d', strtotime($request->input('receive')));
        $emp_record->joining_date = date('Y-m-d', strtotime($request->input('joining')));
        $emp_record->invoicing_date = date('Y-m-d', strtotime($request->input('invoicing')));
        $emp_record->payment_date = date('Y-m-d', strtotime($request->input('payment')));
        $emp_record->company_contact_person = $request->input('com_contact');
        $emp_record->owaycompany_contact_person = $request->input('oway_contact');
        $emp_record->company_email = $request->input('com_email');
        $emp_record->actual_amount = $request->input('actual_amount');
        $emp_record->salary = $request->input('salary');

        if ($emp_record->save()) {
            $interview = Interview::where('id',$id)->update(['interview_status'=>4]);
            $interview = Interview::where('id',$id)->first();
                if ($interview->sec_employed==1) {
                    $interview->interview_status = 6;
                    $interview->sec_employed = 2;
                    $interview->save();
                }
            if (isset($interview)) {                
                DB::table('applicant_information')
                    ->where('cv_no', $interview->cv_id)
                    ->update(
                        ['cv_status' => $request->input('employee_record_data.interview_status')]
                    );
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }

    public function add_interview_person(Request $request)
    {
        if (count($request->input('interview_data.interview_person')) > 1) {

            for ($i = 0; $i < count($request->input('interview_data.interview_person')); $i++) {

                $interview = new Interview();
                $interview->cv_id = $request->input('interview_data.interview_person' . '.' . $i);
                $interview->company_code = $request->input('interview_data.company_code');
                $interview->interview_code = $request->input('interview_data.interview_code');
                $interview->interview_date = date('Y-m-d', strtotime($request->input('interview_data.interview_date')));
                $interview->interview_status = 1;
                $interview->save();
            }

        } else {

            $interview = new Interview();
            $interview->cv_id = $request->input('interview_data.interview_person');
            $interview->company_code = $request->input('interview_data.company_code');
            $interview->interview_code = $request->input('interview_data.interview_code');
            $interview->interview_date = date('Y-m-d', strtotime($request->input('interview_data.interview_date')));
            $interview->interview_status = 1;
            $interview->save();
        }

        $company_name = Client::where('company_code', $request->input('interview_data.company_code'))->pluck('company_name');
        $type = "create";
        $des = "added interview person to " . $company_name . ", interview date : " . date('Y-m-d', strtotime($request->input('interview_data.interview_date')));

        SiteHelper::add_activity(Sentinel::check()->id, $type, $des);
    }

    public function search(Request $request)
    {
//        dd($request);
        $from = $request->input('from_date');
        $to = $request->input('to_date');
        $status = $request->input('interview_status');
        $interview_code = Interview::searchInterview($from, $to, $status);
//dd($interview_code);
        return view('admin.interview.search', compact('interview_code'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $ran = mt_rand(1, 1000);
        $ran2 = mt_rand(1, 100);
        $before = str_pad('', 3, $ran, STR_PAD_LEFT);
        $after = str_pad('', 2, $ran2, STR_PAD_LEFT);
        $detail = Interview::where('interview_date', date('Y-m-d', strtotime($request->input('interview_data.interview_date'))))->count();
        $count = $detail + 1;
        $y = date('Y');
        $y = substr($y, -2);
        $m = date('m');
        $m = substr($m, -1);
        $d = date('d');

//        if (count($request->input('interview_data.company_name')) > 1) {
//            for ($q = 0; $q < count($request->input('interview_data.company_name')); $q++) {
//
//                $interview_code = "INTV". '-' . $before . '-' . $y . $m . $d . '-' . $count++;
//
//                $interview_company = new InterviewCompany();
//                $interview_company->interview_code = $interview_code;
//                $interview_company->company_code = $request->input('interview_data.company_name' . '.' . $q);
//                $interview_company->create_user_id = Sentinel::check()->id ;
//                $interview_company->interview_date = date('Y-m-d',strtotime($request->input('interview_data.interview_date')));
//                $interview_company->save();
//
//            }
//        } else {

        $interview_code = "INTV" . '-' . $before . '-' . $y . $m . $d . '-' . $count;

        $interview_company = new InterviewCompany();
        $interview_company->interview_code = $interview_code;
        $interview_company->company_code = $request->input('interview_data.company_name');
        $interview_company->interview_date = date('Y-m-d', strtotime($request->input('interview_data.interview_date')));
        $interview_company->create_user_id = Sentinel::check()->id;
        $interview_company->save();

//        }

//        $count = $detail + 1 ;
//
        if (count($request->input('interview_data.interview_person')) > 1) {

            for ($i = 0; $i < count($request->input('interview_data.interview_person')); $i++) {

//                if (count($request->input('interview_data.company_name')) > 1) {
//                    for ($q = 0; $q < count($request->input('interview_data.company_name')); $q++) {
//
//                        $interview_code = "INTV". '-' . $before . '-' . $y . $m . $d . '-' . $count;
//
//                        $interview = new Interview();
//                        $interview->cv_id = $request->input('interview_data.interview_person' . '.' . $i);
//                        $interview->interview_code = $interview_code;
//                        $interview->company_code = $request->input('interview_data.company_name' . '.' . $q);
//                        $interview->interview_date = date('Y-m-d',strtotime($request->input('interview_data.interview_date')));
//                        $interview->interview_status = 1;
//                        $interview->save();
//                    }
//                } else {

                $interview_code = "INTV" . '-' . $before . '-' . $y . $m . $d . '-' . $count;

                $interview = new Interview();
                $interview->cv_id = $request->input('interview_data.interview_person' . '.' . $i);
                $interview->company_code = $request->input('interview_data.company_name');
                $interview->interview_code = $interview_code;
                $interview->interview_date = date('Y-m-d', strtotime($request->input('interview_data.interview_date')));
                $interview->interview_status = 1;
                $interview->save();
//                }
            }
        } else {

//            if (count($request->input('interview_data.company_name')) > 1) {
//
//                for ($i = 0; $i < count($request->input('interview_data.company_name')); $i++) {
//
//                    $interview_code = "INTV". '-' . $before . '-' . $y . $m . $d . '-' . $count++;
//
//                    $interview = new Interview();
//                    $interview->cv_id = $request->input('interview_data.interview_person');
//                    $interview->company_code = $request->input('interview_data.company_name' . '.' . $i);
//                    $interview->interview_date = date('Y-m-d',strtotime($request->input('interview_data.interview_date')));
//                    $interview->interview_code = $interview_code;
//                    $interview->interview_status = 1;
//                    $interview->save();
//                }
//
//            } else {
            $interview_code = "INTV" . '-' . $before . '-' . $y . $m . $d . '-' . $count;

            $interview = new Interview();
            $interview->cv_id = $request->input('interview_data.interview_person');
            $interview->company_code = $request->input('interview_data.company_name');
            $interview->interview_code = $interview_code;
            $interview->interview_date = date('Y-m-d', strtotime($request->input('interview_data.interview_date')));
            $interview->interview_status = 1;
            $interview->save();
//            }
        }

        $company_name = Client::where('company_code', $request->input('interview_data.company_name'))->pluck('company_name');
        $type = "create";
        $des = "create new interview session : " . $company_name . ", interview date : " . date('Y-m-d', strtotime($request->input('interview_data.interview_date')));

        SiteHelper::add_activity(Sentinel::check()->id, $type, $des);
        ApplicantInformation::where('cv_no',$interview->cv_id)->update(['cv_status'=>1]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $interview = Interview::find($id);
        $status = $request->input('interview_edit_data.interview_status');
        if ($request->input('interview_edit_data.interview_status') == 3 || $request->input('interview_edit_data.interview_status') == 4) {
            DB::table('applicant_information')
                ->where('cv_no', $interview->cv_id)
                ->update(
                    ['cv_status' => $request->input('interview_edit_data.interview_status')]
                );
        }

        if ($request->input('interview_date')) {
            $interview->interview_date = date('Y-m-d', strtotime($request->input('interview_date')));
            $interview->create_user_id = Sentinel::check()->id;
        }
        $interview->sec_employed = ($status==6)?1:0;
        
        $interview->interview_status = $request->input('interview_edit_data.interview_status');
        $interview->save();
        
        if ($status==5) {$status=2;}        
        $cv = ApplicantInformation::where('cv_no',$interview->cv_id)->update(['cv_status'=>$status]);
    }

    public function remark(Request $request, $id)
    {
        $interview = InterviewCompany::find($id);
        $interview->remark = $request->input('interview_remark.remark');
        $interview->save();

        $company_name = Client::where('company_code', $interview->company_code)->pluck('company_name');
        $type = "remark";
        $des = "add remark interview session : " . $company_name . ", interview date : " . date('Y-m-d', strtotime($interview->interview_date));

        SiteHelper::add_activity(Sentinel::check()->id, $type, $des);
    }

    public function remark_delete(Request $request, $id)
    {
        $interview = InterviewCompany::find($id);
        $interview->remark = '';
        $interview->save();

        $company_name = Client::where('company_code', $interview->company_code)->pluck('company_name');
        $type = "delete";
        $des = "add remark interview session : " . $company_name . ", interview date : " . date('Y-m-d', strtotime($interview->interview_date));

        SiteHelper::add_activity(Sentinel::check()->id, $type, $des);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //$company = InterviewCompany::find($id);
        //Interview::where('company_code', $company->company_code)->where('interview_date', $company->interview_date)->delete();
        $int = Interview::find($id);
        InterviewCompany::where('company_code',$int->company_code)->where('interview_date',$int->interview_date)->delete();
        $int->delete();
    }

    public function delete_interview_section($id)
    {
        $company = InterviewCompany::find($id);
        $company->delete();

        Interview::where('interview_code', $company->interview_code)
            ->where('company_code', $company->company_code)->delete();

        $company_name = Client::where('company_code', $company->company_code)->pluck('company_name');
        $type = "delete";
        $des = "deleted interview session : " . $company_name . ", interview date : " . date('Y-m-d', strtotime($company->interview_date));

        SiteHelper::add_activity(Sentinel::check()->id, $type, $des);
    }

    public function interview_jsondata()
    {
        $data[] = [
            'date'      => '2016-07-30',
            'title'     => 'Doctor appointment',
            'url'       => 'javascript:void(0)',
            'timeStart' => '10:00',
            'timeEnd'   => '11:00'
        ];
        return response()->json($data);
    }
}
