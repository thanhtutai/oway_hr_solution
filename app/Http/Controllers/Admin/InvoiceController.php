<?php

namespace App\Http\Controllers\Admin;

use App\Models\Client;
use App\Models\EmployeeInvoice;
use App\Models\Invoice;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use SiteHelper;

class InvoiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $invoice_list = Invoice::all();
        return view('admin.invoice.index',compact('invoice_list'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $ran = mt_rand(1, 1000);
        $before = str_pad('', 3, $ran, STR_PAD_LEFT);
        $y = date('Y');
        $y = substr($y, -2);
        $m = date('m');
        $m = substr($m, -1);
        $d = date('d');

        $detail = Invoice::where('invoice_code','like','%'. $y . $m . $d .'%')->count();
        $count = $detail + 1 ;

        if (count($request->input('invoice_data.company_name')) > 1) {
            for ($q = 0; $q < count($request->input('invoice_data.company_name')); $q++) {

                $invoice_no = "INVC". '-' . $before . '-' . $y . $m . $d . '-' . $count++;

                $invoice = new Invoice();
                $invoice->invoice_code = $invoice_no;
                $invoice->company_code = $request->input('invoice_data.company_name' . '.' . $q);
                $invoice->due_date = $request->input('invoice_data.due_date');
                $invoice->payment_date = $request->input('invoice_data.payment_date');
                $invoice->invoice_date = $request->input('invoice_data.invoice_date');
                $invoice->ref_invoice = $request->input('invoice_data.ref_invoice');
                $invoice->paid_date = $request->input('invoice_data.paid_date');
                $invoice->percent = $request->input('invoice_data.percent');
//                $invoice->amount = $request->input('invoice_data.amount');
                if ($request->input('invoice_data.is_paid')) {
                    $invoice->status = 1;
                }
                $invoice->create_user_id = Sentinel::check()->id;
                $invoice->save();
            }
        } else {

            $invoice_no = "INVC". '-' . $before . '-' . $y . $m . $d . '-' . $count;

            $invoice = new Invoice();
            $invoice->invoice_code = $invoice_no;
            $invoice->company_code = $request->input('invoice_data.company_name');
            $invoice->due_date = $request->input('invoice_data.due_date');
            $invoice->payment_date = $request->input('invoice_data.payment_date');
            $invoice->invoice_date = $request->input('invoice_data.invoice_date');
            $invoice->ref_invoice = $request->input('invoice_data.ref_invoice');
            $invoice->paid_date = $request->input('invoice_data.paid_date');
//            $invoice->amount = $request->input('invoice_data.amount');
            $invoice->percent = $request->input('invoice_data.percent');
            if ($request->input('invoice_data.is_paid')) {
                $invoice->status = 1;
            }
            $invoice->create_user_id = Sentinel::check()->id;
            $invoice->save();
        }

        $type = "create";
        $des = "create new Invoice." ;

        SiteHelper::add_activity(Sentinel::check()->id, $type, $des);
    }

    public function check_employee(Request $request)
    {
        $emp = EmployeeInvoice::where('invoice_code',$request->input('invoice_data.invoice_code'))
            ->where('company_code',$request->input('invoice_data.company_code'))
            ->where('cv_id',$request->input('invoice_data.employee_name'))
            ->count();

        if($emp){
            return 'exist';
        }
    }

    public function add_employee(Request $request)
    {
        if (count($request->input('invoice_data.employee_name')) > 1) {
            for ($q = 0; $q < count($request->input('invoice_data.employee_name')); $q++) {
                $invoice = new EmployeeInvoice();
                $invoice->invoice_code = $request->input('invoice_data.invoice_code');
                $invoice->cv_id = $request->input('invoice_data.employee_name' . '.' . $q);
                $invoice->company_code = $request->input('invoice_data.company_code');
                $invoice->save();
            }
        } else {
            $invoice = new EmployeeInvoice();
            $invoice->invoice_code = $request->input('invoice_data.invoice_code');
            $invoice->company_code = $request->input('invoice_data.company_code');
            $invoice->cv_id = $request->input('invoice_data.employee_name');
            $invoice->save();
        }

        $type = "update";
        $des = "added employed person." ;

        SiteHelper::add_activity(Sentinel::check()->id, $type, $des);
    }

    public function remove_added_employee($id)
    {
        EmployeeInvoice::destroy($id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $invoice = Invoice::find($id);
        $invoice->due_date = $request->input('due_date');
        $invoice->invoice_date = $request->input('invoice_date');
        $invoice->payment_date = $request->input('payment_date');
        $invoice->amount = $request->input('amount');
        $invoice->percent = $request->input('percent');
        $invoice->ref_invoice = $request->input('ref_invoice');
        $invoice->paid_date = $request->input('paid_date');
        if ($request->input('invoice_edit_data.is_paid')) {
            $invoice->status = 1;
        }else{
            $invoice->status = 0;
        }
        $invoice->save();

        $type = "update";
        $des = "updated Invoice - " . Client::where('company_code',$invoice->company_code)->pluck('company_name');

        SiteHelper::add_activity(Sentinel::check()->id, $type, $des);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $type = "delete";
        $des = "delete Invoice." ;

        SiteHelper::add_activity(Sentinel::check()->id, $type, $des);

        Invoice::destroy($id);
    }
}
