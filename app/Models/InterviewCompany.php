<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InterviewCompany extends Model
{
    protected $table = "interview_companies";
}
