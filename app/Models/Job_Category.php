<?php
/**
 * Created by PhpStorm.
 * User: Nyi Nyi Lwin
 * Date: 3/25/2016
 * Time: 10:59 PM
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Job_Category extends Model
{
    protected $table = 'job_category';

    public static function applicant_job_category($cv_id)
    {
        return DB::table('job_category')
            ->join('applicant_job_position','job_category.id','=','applicant_job_position.job_category')
            ->select('job_category.*')
            ->where('applicant_job_position.cv_id',$cv_id)
            ->first();

    }

    public static function jsonData()
    {

    }
}