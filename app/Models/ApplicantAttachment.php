<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApplicantAttachment extends Model
{
    protected $table = "applicant_attachment";
}
