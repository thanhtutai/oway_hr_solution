<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Interview extends Model
{
    protected $table = 'interview';

    public static function searchInterview($from, $to, $status)
    {
        $from_date = date('Y-m-d', strtotime($from));
        $to_date = date('Y-m-d', strtotime($to));

        return DB::table('interview')
//            ->where(function ($query) use ($from_date, $to_date, $status) {
//                $query->whereBetween('interview_date', [$from_date, $to_date])
//                    ->where('interview_status', $status);
//            })
//            ->orWhere(function ($query) use ($from_date, $status) {
//                $query->where('interview_date', $from_date)
//                    ->where('interview_status', $status);
//            })
//            ->orWhere(function ($query) use ($to_date, $status) {
//                $query->where('interview_date', $to_date)
//                    ->where('interview_status', $status);
//            })
//            ->orWhere(function ($query) use ($from_date, $to_date) {
//                $query->whereBetween('interview_date', [$from_date, $to_date]);
//            })
//            ->orWhere(function ($query) use ($from_date, $to_date, $status) {
//                if (isset($from_date))
//                    $query->where('interview_date', $from_date);
//
//                if (isset($to_date))
//                    $query->where('interview_date', $to_date);
//
//                if (isset($status))
//                    $query->where('interview_status', $status);
//
//            })
            ->whereBetween('interview_date', [$from_date, $to_date])
                ->where('interview_status', $status)
            ->groupBy('interview_date')
            ->paginate(15);

    }
}
