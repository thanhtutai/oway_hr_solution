<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class IT extends Model
{
    protected $table="it_type";

    public static function it_lvl()
    {
        return DB::table('it_lvl')->get();
    }
}
