<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Email extends Model
{
    protected $table = "emails";

    public static function old_email()
    {
        return Email::whereNotBetween('send_date', [date('Y-m-d', strtotime(\Carbon\Carbon::now()->startOfMonth())), date('Y-m-d', strtotime(\Carbon\Carbon::yesterday()))])->where('send_date','!=' ,date('Y-m-d', strtotime(\Carbon\Carbon::today())))->get();
    }
}
