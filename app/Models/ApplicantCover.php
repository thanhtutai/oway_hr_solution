<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApplicantCover extends Model
{
    protected $table = "applicant_cover";

    protected $fillable = ['cv_id','cover_image','cover_letter'];
}
