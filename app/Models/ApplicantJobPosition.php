<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApplicantJobPosition extends Model
{
    protected $table = "applicant_job_position";

    public function appInformation()
    {
    	return $this->belongsTo(ApplicantInformation::class);
    }
    
}
