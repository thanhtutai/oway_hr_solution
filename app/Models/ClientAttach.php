<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClientAttach extends Model
{
    protected $table = 'clients_attachment';
}
