<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EmployeeRecord extends Model
{
    protected $table = "employee_record";

    public static function searchRecord($from, $to)
    {
        $from_date = date('Y-m-d', strtotime($from));
        $to_date = date('Y-m-d', strtotime($to));

        return DB::table('employee_record')
            ->whereBetween('job_receive_date', [$from_date, $to_date])
            ->groupBy('job_receive_date')
            ->paginate(30);
    }
}
