<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApplicantLanguageSkill extends Model
{
    protected $table = "applicant_language_skill";
}
