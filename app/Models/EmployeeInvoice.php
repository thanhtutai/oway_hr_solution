<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class EmployeeInvoice extends Model
{
    protected $table = "employee_invoice";

    public static function get_emp_total_salary($invoice_code, $company_code)
    {
        $cv_no = EmployeeInvoice::where('invoice_code',$invoice_code)->where('company_code',$company_code)->get();

        $total = 0 ;
        foreach ($cv_no as $cv) {
            $salary = EmployeeRecord::where('cv_id',$cv->cv_id)->pluck('salary');
            $total += $salary ;
        }

        return $total;

    }
}
