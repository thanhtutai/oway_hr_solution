<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApplicantReferencePerson extends Model
{
    protected $table="applicant_reference_person";
}
