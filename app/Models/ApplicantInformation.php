<?php

namespace App\Models;

use DB;
use SiteHelper;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ApplicantInformation extends Model
{
    use SoftDeletes;

    protected $table = "applicant_information";

    protected $dates = ['deleted_at'];

    public function apply_job_post()
    {
    	return $this->hasMany(ApplicantJobPosition::class,'cv_id','cv_no');
    }

    // public function work_exp()
    // {
    //     return $this->hasOne(ApplicantJobPosition::class,'cv_id','cv_no');
    // }

    public function scopeSearch($query, $request)
    {	        
    	if ($request->job_cat!='' || $request->job_type!='' || $request->apply_job_industry!='' || $request->app_job_post!=''){
            $query->join('applicant_job_position','cv_no','=','applicant_job_position.cv_id');
        }     
        if ($request->exp_job_title!='' || $request->job_industry!='' || $request->exp) {
            $query->join('applicant_work_experience','cv_no','=','applicant_work_experience.cv_id');
        }
        if ($request->exp_salary!='') {
            $query->join('salary_exp_tbl','salary_exp','=','salary_exp_tbl.id');
        }
    	if ($request->nrc!=0) {    		
    		$query->where('nrc',$request->nrc);
    	}
    	if ($request->exp_salary!='') {
    		$query->where('salary_exp','LIKE','%'.$request->exp_salary.'%');
    	}
    	if ($request->job_cat!='') {
    		$query->where('applicant_job_position.job_category',$request->job_cat);    			
    	}
    	if ($request->job_type!='') {
    		$query->where('applicant_job_position.job_type',$request->job_type);
    	}
    	if ($request->apply_job_industry!='') {
    		$query->where('applicant_job_position.job_industry',$request->apply_job_industry);
    	}
    	if ($request->app_job_post!='') {
    		$query->where('applicant_job_position.apply_position',$request->app_job_post);
    	}
    	if ($request->exp_job_title!='') { 
    		$query->where('applicant_work_experience.exp_job_title',$request->exp_job_title);
    	}
    	if ($request->job_industry!='') {
    		$query->where('applicant_work_experience.exp_job_industry',$request->job_industry);
    	}
        if ($request->exp!='') {

            SiteHelper::dateDiff($query,$request->exp);
           
            //dd($time);
            // foreach ($time as $value) {                
              //  $query
            // }           

        }
    	
    	return $query->select('applicant_information.*');    	
    }

    
}
