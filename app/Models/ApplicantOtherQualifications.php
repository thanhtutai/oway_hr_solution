<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApplicantOtherQualifications extends Model
{
    protected $table = "applicant_other_qualifications";
}
