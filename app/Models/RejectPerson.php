<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RejectPerson extends Model
{
    protected $table = "reject_people";
}
