<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Language extends Model
{
    protected $table = "all_lang_tbl";

    public static function language_lvl()
    {
        return DB::table('language_lavel')->get();
    }
}
